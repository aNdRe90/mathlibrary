package algorithm.big;

import algorithm.ExtendedEuclideanAlgorithm;

import java.math.BigInteger;

/**
 * Version of {@link ExtendedEuclideanAlgorithm} that operates on {@link BigInteger} values.
 *
 * @author Andrzej Walkowiak
 */
public class BigExtendedEuclideanAlgorithm {
    /**
     * Version of {@link ExtendedEuclideanAlgorithm#getBezoutsCoefficientsWithGCD(long, long)} that
     * operates on {@link BigInteger} values.
     *
     * @throws IllegalArgumentException when \(a\) or \(b\) is null
     */
    public BigInteger[] getBezoutsCoefficientsWithGCD( BigInteger a, BigInteger b ) throws IllegalArgumentException {
        if( a == null || b == null ) {
            throw new IllegalArgumentException( "Given values cannot be equal to null." );
        }
        if( a.equals( BigInteger.ZERO ) || b.equals( BigInteger.ZERO ) ) {
            throw new IllegalArgumentException( "Cannot calculate Bezouts coefficients when a or b is equal zero." );
        }

        //We need to prepare the input so that the Extended Euclidean algorithm is executed for (a, b) such that
        //a > 0, b > 0, a >= b
        boolean changeSignOfX = a.signum() < 0; //If a < 0 then later on value of x needs to be changed to -x
        boolean changeSignOfY = b.signum() < 0; //If b < 0 then later on value of y needs to be changed to -y
        a = a.abs(); //a = |a|
        b = b.abs(); //b = |b|
        boolean swapXY = b.compareTo( a ) >= 0; //If b >= a then later on change the returned (x, y) to (y, x)

        //Proceed with the Extended Euclidean algorithm for a > 0, b > 0, a >= b
        BigInteger[] bezoutsCoefficientsWithGCD = calculateBezoutsCoefficientsWithGCD( max( a, b ), min( a, b ) );
        //x = bezoutsCoefficientsWithGCD[0]
        //y = bezoutsCoefficientsWithGCD[1]
        //gcd(a, b) = bezoutsCoefficientsWithGCD[2]

        if( swapXY ) {
            //Change the returned coefficients to (y, x) because we swaped a with b before executing the algorithm
            bezoutsCoefficientsWithGCD = new BigInteger[]{ bezoutsCoefficientsWithGCD[1], bezoutsCoefficientsWithGCD[0],
                    bezoutsCoefficientsWithGCD[2] };
        }

        if( changeSignOfX ) {
            //Change x to -x because a was originally a negative number
            bezoutsCoefficientsWithGCD[0] = bezoutsCoefficientsWithGCD[0].negate();
        }
        if( changeSignOfY ) {
            //Change y to -y because b was originally a negative number
            bezoutsCoefficientsWithGCD[1] = bezoutsCoefficientsWithGCD[1].negate();
        }

        return bezoutsCoefficientsWithGCD;
    }

    private BigInteger[] calculateBezoutsCoefficientsWithGCD( BigInteger a, BigInteger b ) {
        //a > 0, b > 0, a >= b

        //Following the rules that:
        //1) gcd(a, 0) = gcd(0, a) = a
        //2) gcd(a, b) = gcd(b, r) where a = q*b + r

        BigInteger x_i_2 = BigInteger.ONE; //x_{i - 2}
        BigInteger x_i_1 = BigInteger.ZERO; //x_{i - 1}
        BigInteger y_i_2 = BigInteger.ZERO; //y_{i - 2}
        BigInteger y_i_1 = BigInteger.ONE; //y_{i - 1}

        while( true ) {
            //a = q_i*b + r_i
            //0 <= r_i < b
            BigInteger q_i = a.divide( b );
            BigInteger r_i = a.mod( b );

            if( r_i.equals( BigInteger.ZERO ) ) {
                //gcd(a, b) = gcd(b, 0) = b
                //Return [x_{i - 1}, y_{i - 1}, gcd(a, b)]
                return new BigInteger[]{ x_i_1, y_i_1, b };
            }

            BigInteger x_i = x_i_2.subtract( q_i.multiply( x_i_1 ) ); //x_i = x_{i - 2} - q_i*x_{i - 1}
            BigInteger y_i = y_i_2.subtract( q_i.multiply( y_i_1 ) ); //y_i = y_{i - 2} - q_i*y_{i - 1}

            //Updating the values for next iteration
            x_i_2 = x_i_1;
            x_i_1 = x_i;
            y_i_2 = y_i_1;
            y_i_1 = y_i;

            //Updating variables to calculate gcd(b, r_i) in the next iteration
            a = b;
            b = r_i;
        }
    }

    private BigInteger max( BigInteger a, BigInteger b ) {
        return a.compareTo( b ) > 0 ? a : b;
    }

    private BigInteger min( BigInteger a, BigInteger b ) {
        return a.compareTo( b ) < 0 ? a : b;
    }
}
