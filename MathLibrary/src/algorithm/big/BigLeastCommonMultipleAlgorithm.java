package algorithm.big;

import algorithm.LeastCommonMultipleAlgorithm;

import java.math.BigInteger;
import java.util.List;

/**
 * Version of {@link LeastCommonMultipleAlgorithm} that operates on {@link BigInteger} values.
 *
 * @author Andrzej Walkowiak
 */
public class BigLeastCommonMultipleAlgorithm {
    /**
     * Variant of {@link #getLeastCommonMultiple(BigInteger...)} for {@link List} input.
     */
    public BigInteger getLeastCommonMultiple( List<BigInteger> values ) {
        return getLeastCommonMultiple( toBigIntegerArray( values ) );
    }

    /**
     * Version of {@link LeastCommonMultipleAlgorithm#getLeastCommonMultiple(long...)} (long...)} that
     * operates on {@link BigInteger} values.
     */
    public BigInteger getLeastCommonMultiple( BigInteger... values ) {
        if( values == null ) {
            throw new IllegalArgumentException( "Values cannot be null." );
        }
        if( values.length < 2 ) {
            throw new IllegalArgumentException( "Provide at least 2 values to look for least common multiple." );
        }

        //Using the fact that lcm(a,b,c) = lcm(lcm(a,b),c) and analogically for more values
        BigInteger lcm = values[0];
        for( int i = 1; i < values.length; i++ ) {
            lcm = getLeastCommonMultiple( lcm, values[i] );
        }

        return lcm;
    }

    private BigInteger[] toBigIntegerArray( List<BigInteger> values ) {
        if( values == null ) {
            return null;
        }

        BigInteger[] valuesArray = new BigInteger[values.size()];
        for( int i = 0; i < valuesArray.length; i++ ) {
            valuesArray[i] = values.get( i );
        }
        return valuesArray;
    }

    private BigInteger getLeastCommonMultiple( BigInteger a, BigInteger b ) {
        if( a == null || b == null ) {
            throw new IllegalArgumentException( "No value given can be equal to null." );
        }

        if( a.equals( BigInteger.ZERO ) && b.equals( BigInteger.ZERO ) ) {
            //lcm(0,0) = 0
            return BigInteger.ZERO;
        }

        BigInteger gcd = a.gcd( b );
        a = a.abs();
        b = b.abs();

        //lcm(a, b) = |ab| / gcd(a, b) = |a|/gcd(a,b) * b, since gcd(a,b) divides a
        return a.divide( gcd ).multiply( b );
    }
}
