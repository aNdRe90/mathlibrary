package algorithm;

import algorithm.big.BigExtendedEuclideanAlgorithm;
import algorithm.big.BigLeastCommonMultipleAlgorithm;
import number.representation.ModularCongruence;
import number.representation.big.BigModularCongruence;

import java.math.BigInteger;
import java.util.List;

/**
 * Chinese Remainder Theorem is a theorem that let us find a unique solution to simultaneous modular congruences.
 * <br>It means that for given system of simultaneous congruences
 * \begin{align}
 * x &amp; \equiv a_0 \text{ mod } m_0 \\
 * x &amp; \equiv a_1 \text{ mod } m_1 \\
 * x &amp; \equiv a_2 \text{ mod } m_2 \\
 * &amp; ... \\
 * x &amp; \equiv a_{N - 1} \text{ mod } m_{N - 1}
 * \end{align}
 * we can find the value of \(x\) that is a solution to every congruence.
 *
 * <br><br>Usually, the theorem is presented with an assumption that modulo values are pairwise coprime which means
 * that for \(A \neq B, 0 \leq A &lt; N, 0 \leq B &lt; N\) we have \(\text{gcd}(m_A, m_B) = 1\).
 * <br>It does not need to be satisfied in order to find the solution \(x\), so we will show how to solve the general
 * case for any positive modulo values.
 *
 * <br><br>Let`s first consider a system of two congruences:
 * \begin{align}
 * x &amp; \equiv a_0 \text{ mod } m_0 \\
 * x &amp; \equiv a_1 \text{ mod } m_1
 * \end{align}
 * It means, by modulo definition, that \(x = a_0 + im_0 = a_1 + jm_1\) for arbitrary integers \(i, j\).
 * \begin{align}
 * a_0 + im_0 &amp; = a_1 + jm_1 \\
 * im_0 - jm_1 &amp; = a_1 - a_0 \\
 * m_0i + m_1y &amp; = a_1 - a_0 \text{ for } y = -j
 * \end{align}
 *
 * <br>From the Bézout's lemma we know that such equation has integer solutions \((i, y)\) only if
 * \(a_1 - a_0 \equiv 0 \text{ mod } \text{gcd}(m_0, m_1)\) so this is our main condition here.
 *
 * <br><br>Let \(a_1 - a_0 = s \cdot \text{gcd}(m_0, m_1)\), where \(s \neq 0\). Then we have:
 * \begin{align}
 * m_0i + m_1y &amp; = a_1 - a_0 \\
 * m_0i + m_1y &amp; = s \cdot \text{gcd}(m_0, m_1) \\
 * m_0\frac{i}{s} + m_1\frac{y}{s} &amp; = \text{gcd}(m_0, m_1) \\
 * m_0i' + m_1y' &amp; = \text{gcd}(m_0, m_1) \text{ for } i' = \frac{i}{s}, y' = \frac{y}{s}
 * \end{align}
 *
 * <br><br>\(m_0i' + m_1y' = \text{gcd}(m_0, m_1)\) is the kind of equation that can be solved for integers
 * \((i', y')\) by using the Extended Euclidean algorithm (please see {@link ExtendedEuclideanAlgorithm} for more
 * details on how it is done).
 *
 * <br><br>Having the value of \(i'\) and knowing that \(i' = \frac{i}{s}, a_1 - a_0 = s \cdot \text{gcd}(m_0, m_1),
 * x = a_0 + im_0\) we can get the result \(x = a_0 + i'\frac{a_1 - a_0}{\text{gcd}(m_0, m_1)}m_0\).
 * <br>If \(x_0, x_1\) are two solutions of the system:
 * \begin{align}
 * x &amp; \equiv a_0 \text{ mod } m_0 \\
 * x &amp; \equiv a_1 \text{ mod } m_1
 * \end{align}
 * then we have:
 * \begin{align}
 * x_1 \equiv x_0 \text{ mod } m_0 \Leftrightarrow x_1 - x_0 \equiv 0 \text{ mod } m_0\\
 * x_1 \equiv x_0 \text{ mod } m_1 \Leftrightarrow x_1 - x_0 \equiv 0 \text{ mod } m_1
 * \end{align}
 *
 * <br>It means that \(x_1 - x_0\) is divisible by both \(m_0\) and \(m_1\) so it is divisible by
 * \(\text{lcm}(m_0, m_1)\). The final result to presented system of two congruences is
 * \(x \equiv a_0 + i'\frac{a_1 - a_0}{\text{gcd}(m_0, m_1)}m_0 \text{ mod } \text{lcm}(m_0, m_1)\).
 *
 * <br><br>In order to solve the system of more than two congruences, we need to use the recurrence procedure to
 * reduce the number of congruences using the fact that the system:
 * \begin{align}
 * x &amp; \equiv a_0 \text{ mod } m_0 \\
 * x &amp; \equiv a_1 \text{ mod } m_1 \\
 * &amp; ... \\
 * x &amp; \equiv a_{K - 2} \text{ mod } m_{K - 2} \\
 * x &amp; \equiv a_{K - 1} \text{ mod } m_{K - 1} \\
 * x &amp; \equiv a_K \text{ mod } m_K \\
 * x &amp; \equiv a_{K + 1} \text{ mod } m_{K + 1} \\
 * &amp; ... \\
 * x &amp; \equiv a_{N - 1} \text{ mod } m_{N - 1}
 * \end{align}
 *
 * is equal to:
 *
 * \begin{align}
 * x &amp; \equiv a_0 \text{ mod } m_0 \\
 * x &amp; \equiv a_1 \text{ mod } m_1 \\
 * &amp; ... \\
 * x &amp; \equiv a_{K - 2} \text{ mod } m_{K - 2} \\
 * x &amp; \equiv a_{K - 1} + i'\frac{a_K - a_{K - 1}}{\text{gcd}(m_{K - 1}, m_K)}m_{K - 1} \text{ mod }
 * \text{lcm}(m_{K - 1}, m_K) \\
 * x &amp; \equiv a_{K + 1} \text{ mod } m_{K + 1} \\
 * &amp; ... \\
 * x &amp; \equiv a_{N - 1} \text{ mod } m_{N - 1}
 * \end{align}
 * for \(i'\) being a solution to \(m_{K - 1}i' + m_Ky' = \text{gcd}(m_{K - 1}, m_K)\) and \(a_K - a_{K - 1}\)
 * divisible by \(\text{gcd}(m_{K - 1}, m_K)\).
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Chinese_remainder_theorem">Wikipedia - Chinese Remainder Theorem</a>
 * @see <a href="http://nptel.ac.in/courses/111103020/module2_lec4.pdf">NPTEL Number Theory web course - System of
 * congruences with non-coprime moduli</a>
 */
public class ChineseRemainderTheoremAlgorithm {
    private BigExtendedEuclideanAlgorithm extendedEuclideanAlgorithm = new BigExtendedEuclideanAlgorithm();
    private BigLeastCommonMultipleAlgorithm leastCommonMultipleAlgorithm = new BigLeastCommonMultipleAlgorithm();

    /**
     * Solves the system of modular congruences for value \(x\) by using the Chinese Remainder Theorem:
     * \begin{align}
     * x &amp; \equiv a_0 \text{ mod } m_0 \\
     * x &amp; \equiv a_1 \text{ mod } m_1 \\
     * x &amp; \equiv a_2 \text{ mod } m_2 \\
     * &amp; ... \\
     * x &amp; \equiv a_{N - 1} \text{ mod } m_{N - 1}
     * \end{align}
     * <br>For the detailed description of the implementation, see {@link ChineseRemainderTheoremAlgorithm}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param modularCongruences system of congruences represented by the {@link List} of {@link ModularCongruence}
     *                           values
     *
     * @return The result value of \(x\) solving all the given congruences. The returned {@link ModularCongruence}
     * has a modulo equal to the least common multiplier of all the moduli from the congruence system
     * (\(\text{lcm}(m_0, m_1, m_2, ... , m_{N - 1}\)).
     * <br>If no such \(x\) exists, null value is returned.
     *
     * @throws IllegalArgumentException when given {@code modularCongruences} is null or empty
     * @see <a href="https://en.wikipedia.org/wiki/Chinese_remainder_theorem">Wikipedia - Chinese Remainder Theorem</a>
     * @see <a href="http://nptel.ac.in/courses/111103020/module2_lec4.pdf">NPTEL Number Theory web course - System of
     * congruences with non-coprime moduli</a>
     */
    public BigModularCongruence getSolution( List<ModularCongruence> modularCongruences ) {
        if( modularCongruences == null ) {
            throw new IllegalArgumentException( "Modular congruences cannot be equal to null." );
        }
        if( modularCongruences.isEmpty() ) {
            throw new IllegalArgumentException( "At least one modular congruence must be given." );
        }

        return calculateSolution( modularCongruences );
    }

    private BigModularCongruence calculateSolution( List<ModularCongruence> modularCongruences ) {
        //Solving the system of congruences by using the recurrence method
        //reducing the number of congruences one at a time

        BigInteger a_K_1 = BigInteger.valueOf( modularCongruences.get( 0 ).getResidue() ); //a_{K - 1}
        BigInteger m_K_1 = BigInteger.valueOf( modularCongruences.get( 0 ).getModulo() ); //m_{K - 1}

        for( int K = 1; K < modularCongruences.size(); K++ ) {
            ModularCongruence modularCongruence = modularCongruences.get( K );
            BigInteger a_K = BigInteger.valueOf( modularCongruence.getResidue() ); //a_K
            BigInteger m_K = BigInteger.valueOf( modularCongruence.getModulo() ); //m_K

            //Solving m_{K - 1}i' + m_Ky' = gcd(m_{K - 1}, m_K)
            //Where i' = i/s, a_K - a_{K - 1} = s*gcd(m_{K - 1}, m_K)
            BigInteger[] bezoutsCoefficientsWithGCD = extendedEuclideanAlgorithm
                    .getBezoutsCoefficientsWithGCD( m_K_1, m_K );

            BigInteger i = bezoutsCoefficientsWithGCD[0]; //i' solving m_{K - 1}i' + m_Ky' = gcd(m_{K - 1}, m_K)
            //No need to use the value of y' = bezoutsCoefficientsWithGCD[1]
            BigInteger gcd = bezoutsCoefficientsWithGCD[2]; //gcd(m_{K - 1}, m_K)

            //In order for the system of congruences:
            //x = a_{K - 1} mod m_{K - 1}
            //x = a_K mod m_K
            //to have a solution, a_K - a_{K - 1} needs to be divisible by gcd(m_{K - 1}, m_K)

            //s = (a_K - a_{K - 1}) / gcd(m_{K - 1}, m_K)
            BigInteger[] sWithRemainder = a_K.subtract( a_K_1 ).divideAndRemainder( gcd );

            if( !sWithRemainder[1].equals( BigInteger.ZERO ) ) {
                //sWithRemainder[1] is the remainder of (a_K - a_{K - 1}) / gcd(m_{K - 1}, m_K)

                //If a_K - a_{K - 1} is not divisible by gcd(m_{K - 1}, m_K), then no solution to the system exists
                //because one of the pairs of congruences contradicts each other
                //e.g.
                //x = 1 mod 4, x = 2 mod 8 means (2 - 1) is not divisible by 4 and no solution exists
                //x = 1 mod 4, x = 5 mod 8 means (5 - 1) is divisible by 4 and x = 5 mod 8 is a solution
                return null;
            }

            BigInteger s = sWithRemainder[0]; //value of s = (a_K - a_{K - 1}) / gcd(m_{K - 1}, m_K)
            i = i.multiply( s ); //i = i's

            BigInteger lcm = leastCommonMultipleAlgorithm.getLeastCommonMultiple( m_K_1, m_K ); //lcm(m_{K - 1}, m_K)

            //x = a_{K - 1} + im_{K - 1} mod lcm(m_{K - 1}, m_K)
            BigInteger x = ( a_K_1.add( i.multiply( m_K_1 ) ) ).mod( lcm );

            //For the system of congruences:
            //x = a_{K - 1} mod m_{K - 1}
            //x = a_K mod m_K

            //we get a reduced one congruence:
            //x = a_{K - 1} + im_{K - 1} mod lcm(m_{K - 1}, m_K)

            //Preparing the values of a_{K - 1}, m_{K - 1} for the next iteration
            //x is a reduced value of a_{K - 1} for the next iteration
            //and modulo m_{K - 1} is equal to lcm(m_{K - 1}, m_K)
            a_K_1 = x;
            m_K_1 = lcm;
        }

        //Having solved all the congruences by reducing the system pairwise
        //the final result is the solution of the last system congruence pair
        return new BigModularCongruence( a_K_1, m_K_1 );
    }
}
