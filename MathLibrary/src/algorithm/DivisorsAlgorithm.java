package algorithm;

import generator.IterationIndexesGenerator;
import number.representation.FactorizationFactor;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.incrementExact;
import static java.lang.Math.multiplyExact;
import static java.util.Collections.singletonList;
import static java.util.Collections.sort;
import static util.BasicOperations.abs;
import static util.BasicOperations.power;

/**
 * Divisor (or a factor) of number \(n\) is an integer \(d\) such that \(n = k \cdot d\) for integer \(k\).
 * This class allows for retrieving all the divisors of given number \(n\).
 *
 * <br><br>In order to calculate all the divisors of given number, we first need to factorize it.
 * <br>To factorize a number \(n\) is to find the representation of its value in a form of:
 * \(n = p_0^{e_0} \cdot p_1^{e_1} \cdot p_2^{e_2} \cdot ... \cdot p_{m - 1}^{e_{m - 1}}\), where \(p_i\) is a prime
 * number and \(e_i\) is its exponent.
 * <br>Additionally \(p_i &gt; p_{i - 1}, e_i &gt; 0\). Please see {@link FactorizationAlgorithm} for more information
 * about how to factorize a number.
 *
 * <br><br>Every divisor of \(n = p_0^{e_0} \cdot p_1^{e_1} \cdot p_2^{e_2} \cdot ... \cdot p_{m - 1}^{e_{m - 1}}\)
 * is equal to \(d = p_0^{e'_0} \cdot p_1^{e'_1} \cdot p_2^{e'_2} \cdot ... \cdot p_{m - 1}^{e'_{m - 1}}\), where
 * \(0 \leq e'_i \leq e_i\). This tells us that we can generate all of them by checking every possible combination of
 * \(e'_i\) values.
 *
 * <br><br>Let`s consider \(n = 180 = 2^2 \cdot 3^2 \cdot 5^1\).
 * <br>Calculating every possible divisor is the following:
 * \begin{align}
 * d_0 &amp; = 2^0 \cdot 3^0 \cdot 5^0 = 1 \\
 * d_1 &amp; = 2^1 \cdot 3^0 \cdot 5^0 = 2 \\
 * d_2 &amp; = 2^2 \cdot 3^0 \cdot 5^0 = 4 \\
 * d_3 &amp; = 2^0 \cdot 3^1 \cdot 5^0 = 3 \\
 * d_4 &amp; = 2^1 \cdot 3^1 \cdot 5^0 = 6 \\
 * d_5 &amp; = 2^2 \cdot 3^1 \cdot 5^0 = 12 \\
 * d_6 &amp; = 2^0 \cdot 3^2 \cdot 5^0 = 9 \\
 * d_7 &amp; = 2^1 \cdot 3^2 \cdot 5^0 = 18 \\
 * d_8 &amp; = 2^2 \cdot 3^2 \cdot 5^0 = 36 \\
 * d_9 &amp; = 2^0 \cdot 3^0 \cdot 5^1 = 5 \\
 * d_{10} &amp; = 2^1 \cdot 3^0 \cdot 5^1 = 10 \\
 * d_{11} &amp; = 2^2 \cdot 3^0 \cdot 5^1 = 20 \\
 * d_{12} &amp; = 2^0 \cdot 3^1 \cdot 5^1 = 15 \\
 * d_{13} &amp; = 2^1 \cdot 3^1 \cdot 5^1 = 30 \\
 * d_{14} &amp; = 2^2 \cdot 3^1 \cdot 5^1 = 60 \\
 * d_{15} &amp; = 2^0 \cdot 3^2 \cdot 5^1 = 45 \\
 * d_{16} &amp; = 2^1 \cdot 3^2 \cdot 5^1 = 90 \\
 * d_{17} &amp; = 2^2 \cdot 3^2 \cdot 5^1 = 180
 * \end{align}
 *
 * It indeed gives us all the divisors of \(n = 180\) but they are not sorted. We are sorting them after that to get the
 * divisors \(1, 2, 3, 4, 5, 6, 9, 10, 12, 15, 18, 20, 30, 36, 45, 60, 90, 180\).
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Divisor">Wikipedia - Divisor</a>
 */
public class DivisorsAlgorithm {
    private FactorizationAlgorithm factorizator = new FactorizationAlgorithm();

    /**
     * Calculates the divisors of the given number.
     * <br>For the detailed description of the implementation, see {@link DivisorsAlgorithm}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param n the number \(n\) whose divisors will be calculated
     *
     * @return Unique positive divisors of \(n\), including \(1\) and \(|n|\). The divisors returned are
     * sorted in ascending order e.g. divisors of \(n = 6\) are [\(1, 2, 3, 6\)], same goes for \(n = -6\).
     *
     * @throws IllegalArgumentException when \(n = 0\)
     * @see <a href="https://en.wikipedia.org/wiki/Divisor">Wikipedia - Divisor</a>
     */
    public List<Long> getDivisors( long n ) {
        if( n == 0 ) {
            throw new IllegalArgumentException( "Cannot return divisors of zero." );
        }
        if( n == 1 ) {
            return singletonList( 1L );
        }

        //First factorize the number |n|
        List<FactorizationFactor> factorization = factorizator.getFactorization( abs( n ) );

        //Then use the factorization to calculate divisors
        return calculateDivisors( factorization );
    }

    private List<Long> calculateDivisors( List<FactorizationFactor> factorization ) {
        //If n = p_0^e_0 * p_1^{e_1} * p_2^{e_2} * ... * p_{k - 1}^e_{k - 1}
        //Then every divisor d is equal to p_0^e'_0 * p_1^{e'_1} * p_2^{e'_2} * ... * p_{k - 1}^e'_{k - 1}
        //where 0 <= e'_i <= e_i

        List<Long> divisors = new ArrayList<>();

        int[] indexExclusiveLimitsForIteration = getIndexExclusiveLimitsForIteration( factorization );
        //Values [e_0 + 1, e_1 + 1, e_2 + 1, ..., e_{k - 1} + 1] so that we iterate over
        //[0...e_0, 0...e_1, 0...e_2, ..., 0...e_{k - 1}] in the loop below

        for( List<Integer> factorExponents : new IterationIndexesGenerator( indexExclusiveLimitsForIteration ) ) {
            //factorExponents contains [e'_0, e'_1, e'_2, ..., e'_0], where 0 <= e'_i <= e_i

            divisors.add( getDivisor( factorExponents, factorization ) );
        }

        sort( divisors );
        return divisors;
    }

    private int[] getIndexExclusiveLimitsForIteration( List<FactorizationFactor> factorization ) {
        //We need to create [e_0 + 1, e_1 + 1, e_2 + 1, ..., e_{k - 1} + 1] array here, so that
        //later on we iterate over [0...e_0, 0...e_1, 0...e_2, ..., 0...e_{k - 1}] values

        int[] iterationLimits = new int[factorization.size()];

        for( int i = 0; i < factorization.size(); i++ ) {
            int e_i = factorization.get( i ).getExponent();
            iterationLimits[i] = incrementExact( e_i );
        }

        return iterationLimits;
    }

    private long getDivisor( List<Integer> factorExponents, List<FactorizationFactor> factorization ) {
        //Calculate the value of divisor d = p_0^e'_0 * p_1^{e'_1} * p_2^{e'_2} * ... * p_{k - 1}^e'_{k - 1}
        //where primes p_0, p_1, p_2, ..., p_{k - 1} are taken from the factorization
        //and e'_0, e'_1, e'_2, ..., e'_{k - 1} are in factorExponents

        long divisor = 1;

        for( int i = 0; i < factorExponents.size(); i++ ) {
            long p_i = factorization.get( i ).getPrime();
            int e_i = factorExponents.get( i ); //e'_i
            divisor = multiplyExact( power( p_i, e_i ), divisor );
        }

        return divisor;
    }

}
