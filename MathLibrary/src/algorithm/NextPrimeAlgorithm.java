package algorithm;

import number.properties.checker.PrimeNumberChecker;

import static java.lang.Math.addExact;

/**
 * Class which calculates the next prime value from the given number \(n\).
 * <br>A positive integer \(x\) is prime if it has only two divisors: \(1, x\). For more information on how to check
 * if a number is prime, see {@link PrimeNumberChecker}.
 *
 * <br><br>Checking the value of next prime uses the fact that every prime number \(p \geq 5\) satisfies
 * \(p \equiv \pm 1 \text{ mod } 6\). It is due to the fact that:
 * \begin{align}
 * 6 | &amp; 6k \\
 * 2 | &amp; 6k + 2 = 2(3k + 1) \\
 * 3 | &amp; 6k + 3 = 3(2k + 1) \\
 * 2 | &amp; 6k + 4 = 2(3k + 2)
 * \end{align}
 *
 * <br>To use this fact in algorithm iteration we need to e.g. iterate over values of \(m &gt; n\) such that
 * \(m \equiv 5 \text{ mod } 6\) and check if \(m\) or \(m + 2\) is prime.
 * <br>First, we need to find the value of \(m\) though. Manually iterating over values \(n + 1, n + 2, ...\) until
 * we find the one of form \(6k + 5\) for arbitrary integer \(k\) is enough.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Prime_number">Wikipedia - Prime number</a>
 * @see <a href="https://en.wikipedia.org/wiki/Prime_gap">Wikipedia - Prime gap</a>
 */
public class NextPrimeAlgorithm {
    private PrimeNumberChecker primeChecker = new PrimeNumberChecker();

    /**
     * Finds the next prime that is bigger than the given value \(n\).
     * <br>For the detailed description of the implementation, see {@link NextPrimeAlgorithm}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param n value of \(n\)
     *
     * @return Lowest prime number bigger than given number \(n\). \(2\) is returned for \(n &lt; 2\).
     *
     * @see <a href="https://en.wikipedia.org/wiki/Prime_number">Wikipedia - Prime number</a>
     * @see <a href="https://en.wikipedia.org/wiki/Prime_gap">Wikipedia - Prime gap</a>
     */
    public long getNextPrime( long n ) {
        if( n < 2 ) {
            return 2;
        }
        if( n == 2 ) {
            //Checking this case manually because 2 and 3 are the only prime numbers which are not of a form
            //6k + 1 or 6k + 5
            return 3;
        }

        long m; //Will be initialized with the first value of the form 6k + 5 that is greater than n
        //It will happen only if there is no prime value of form 6k + 1 between n and m

        for( int i = 1; ; i++ ) {
            long potentialNextPrime = addExact( n, i );

            if( potentialNextPrime % 6 == 1 && primeChecker.isPrime( potentialNextPrime ) ) {
                //If it is a prime greater than n and of form 6k + 1, it is the next prime after n
                return potentialNextPrime;
            }

            if( potentialNextPrime % 6 == 5 ) {
                //If it is a value greater than n and of form 6k + 5, then start the loop with it later on
                m = potentialNextPrime;
                break;
            }
        }

        for( long i = m; ; i = addExact( i, 6 ) ) {
            if( primeChecker.isPrime( i ) ) { //Checking value of form 6k + 5
                return i;
            }

            if( primeChecker.isPrime( addExact( i, 2 ) ) ) { //Checking value of form 6k + 1
                return addExact( i, 2 );
            }
        }
    }
}
