package algorithm;

import generator.EratosthenesSievePrimeGenerator;
import number.properties.checker.PrimeNumberChecker;
import number.representation.FactorizationFactor;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.*;
import static java.util.Arrays.asList;
import static util.BasicOperations.power;

/**
 * According to the Fundamental theorem of arithmetic, every integer greater than \(1\) either is a prime number or can
 * be represented as the product of prime numbers:
 * <br>\(n = p_0^{e_0} \cdot p_1^{e_1} \cdot p_2^{e_2} \cdot ... \cdot p_{k - 1}^{e_{k - 1}}\), where \(p_i &gt; p_{i -
 * 1}, e_i &gt; 0\).
 * <br>We then say that \(n\) is expressed in standard form. Apart from rearrangement of factors, \(n\) can be
 * expressed as a product of primes in one way only.
 * <br>To factorize \(n\) is to find the standard form for \(n\) and the process of it is called factorization
 * (or prime factorization).
 *
 * <br><br>There are various different factorization methods but none of it is an efficient one that can be applied
 * to any number (small and very large ones). In this class, we will limit ourselves to the factorization of numbers
 * that are at most a value of {@link Long#MAX_VALUE}.
 *
 * <br><br>Two different approaches will be used to make the process as efficient as possible for {@link Long} values:
 *
 * <br><br>1) <u>Trial division</u>
 * <br>It is the easiest way of factorizing numbers that have relatively small prime factors. The method is based on
 * systematically dividing the number \(n\) by consecutive primes \(p = 2, 3, 5, 7, ...\) up to \(\sqrt{n}\).
 * <br>e.g. factorizing \(n = 84\):
 * \begin{align}
 * 2 | 84 &amp; \text{ so } n = \frac{84}{2} = 42 &amp; \rightarrow \text{ factor } = 2 \\
 * 2 | 42 &amp; \text{ so } n = \frac{42}{2} = 21 &amp; \rightarrow \text{ factor } = 2 \\
 * 3 | 21 &amp; \text{ so } n = \frac{21}{3} = 7 &amp; \rightarrow \text{ factor } = 3 \\
 * 5 \not| 7 &amp; \\
 * 7 | 7 &amp; \text{ so } n = \frac{7}{7} = 1 &amp; \rightarrow \text{ factor } = 7
 * \end{align}
 * <br>Final factorization is \(n = 84 = 2 \cdot 2 \cdot 3 \cdot 7 = 2^2 \cdot 3^1 \cdot 7^1\).
 *
 * <br><br>There are cases where \(n\) is not a prime power and the last prime factor \(p_{k - 1} &gt; \sqrt{n}\).
 * Obviously \(p_{k - 1}^{e_{k - 1}} \leq n\), then \(p_{k - 1} \leq \sqrt[e_{k - 1}]{n}\), so \(e_{k - 1} = 1\),
 * because otherwise \(p_{k - 1} \leq \sqrt{n}\) which is a contradiction.
 * <br>e.g. factorizing \(n = 138\) (\(\sqrt{138} \approx 11,747\)):
 * \begin{align}
 * 2 | 138 &amp; \text{ so } n = \frac{138}{2} = 69 &amp; \rightarrow \text{ factor } = 2 \\
 * 3 | 69 &amp; \text{ so } n = \frac{69}{3} = 23 &amp; \rightarrow \text{ factor } = 3 \\
 * 5 \not| 23 &amp; \\
 * 7 \not| 23 &amp; \\
 * 11 \not| 23 &amp;
 * \end{align}
 * At this point we have checked the maximum prime factor less than or equal to \(\sqrt{n}\) and we are still left with
 * the value of 23.
 * <br>During the trial division, we will be left here with either 1 (see the example of factorizing \(84\) above)
 * or a prime number which is the last factor \(p_{k - 1}^1\) (as \(23\) is in factorizing \(138\)).
 * <br>Final factorization is \(n = 138 = 2 \cdot 3 \cdot 23 = 2^1 \cdot 3^1 \cdot 23^1\).
 *
 * <br><br>In the implementation of trial division used by this class, we are generating primes up to
 * {@value GENERATED_PRIME_LIMIT} when the instance of this class is created using the Sieve of Eratosthenes
 * (please see {@link EratosthenesSievePrimeGenerator} for more details on how it is done).
 * <br>These primes are used to perform the trial division on the given number \(n\). It means that trial division can
 * effectively factorize any number \(n\) that is divisible by no more than one prime greater than
 * {@value GENERATED_PRIME_LIMIT}.
 *
 * <br><br>2) <u>Pollard's Rho algorithm:</u>
 * <br>It is an algorithm based on generating a random sequences of numbers and relying on the birthday paradox to
 * find the factors of given number \(n\). For more information about how it is done, please see
 * {@link Long}.
 *
 * <br><br><br>The factorization performed by this class is first using the trial division algorithm. If it does not
 * manage to factorize the given number, then Pollard`s Rho algorithm is used.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Fundamental_theorem_of_arithmetic">Wikipedia - Fundamental theorem of
 * arithmetic</a>
 * @see <a href="https://en.wikipedia.org/wiki/Integer_factorization">Wikipedia - Integer factorization</a>
 * @see <a href="https://en.wikipedia.org/wiki/Trial_division">Wikipedia - Trial division</a>
 * @see <a href="http://mathworld.wolfram.com/PrimeFactorizationAlgorithms.html">Mathworld Wolfram - Prime Factorization
 * Algorithms</a>
 */
public class FactorizationAlgorithm {
    /**
     * Limit for the prime values generated during the class instance creation and used for trial division
     * factorization part. It is equal to {@value GENERATED_PRIME_LIMIT}.
     */
    public static final int GENERATED_PRIME_LIMIT = 100000;

    private PollardsRhoFactorizationAlgorithm pollardsRhoFactorizationAlgorithm
            = new PollardsRhoFactorizationAlgorithm();
    private PrimeNumberChecker primeChecker = new PrimeNumberChecker();

    //Generate primes using Sieve of Eratosthenes when the class is instantiated
    //They will be used for trial division factorization method
    private List<Long> generatedPrimes = generatePrimes( GENERATED_PRIME_LIMIT );

    /**
     * Factorizes the given number \(n\).
     * <br>For the detailed description of the implementation, see {@link FactorizationAlgorithm}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param n value of \(n\)
     *
     * @return {@link List} containing the {@link FactorizationFactor}s of \(n\).
     * <br>\(n = p_0^{e_0} \cdot p_1^{e_1} \cdot p_2^{e_2} \cdot ... \cdot p_{k - 1}^{e_{k - 1}}\), where
     * \(p_i &gt; p_{i - 1}, e_i &gt; 0\) so the returned list is sorted by factors` prime values e.g. factorization of
     * \(2200 = 2^3 \cdot 5^2 \cdot 11^1\) returns \([2, 3], [5, 2], [11, 1]\).
     *
     * @throws IllegalArgumentException when \(n &lt; 2\)
     * @see <a href="https://en.wikipedia.org/wiki/Fundamental_theorem_of_arithmetic">Wikipedia - Fundamental theorem of
     * arithmetic</a>
     * @see <a href="https://en.wikipedia.org/wiki/Integer_factorization">Wikipedia - Integer factorization</a>
     * @see <a href="https://en.wikipedia.org/wiki/Trial_division">Wikipedia - Trial division</a>
     * @see <a href="http://mathworld.wolfram.com/PrimeFactorizationAlgorithms.html">Mathworld Wolfram - Prime
     * Factorization Algorithms</a>
     */
    public List<FactorizationFactor> getFactorization( long n ) {
        if( n < 2 ) {
            throw new IllegalArgumentException( "Cannot factorize number less than 2." );
        }

        if( primeChecker.isPrime( n ) ) {
            //For n = p^1 return just n^1 because n = p
            return asList( new FactorizationFactor( n, 1 ) );
        }

        //Trial division factorization
        List<FactorizationFactor> trialDivisionFactorization = factorizeUsingTrialDivision( n );

        //Check if trial division was enough
        if( isCorrectFactorization( n, trialDivisionFactorization ) ) {
            //If so, it is the result
            return trialDivisionFactorization;
        }

        //If trial division did not give the correct factorization, it means that number n has large prime factors
        //Apply Pollard`s Rho factorization algorithm to deal with such values of n
        return pollardsRhoFactorizationAlgorithm.getFactorization( n );
    }

    private List<FactorizationFactor> factorizeUsingTrialDivision( long n ) {
        long primeValueLimit = (long) sqrt( n ); //Limit for checking primes is equal to sqrt(n)

        //Storing the value of n to the separate variable as we will be decreasing it when dividing by primes
        long currentN = n;

        List<FactorizationFactor> factorization = new ArrayList<>();
        for( long p_i : generatedPrimes ) {
            if( p_i > primeValueLimit ) { //Check only primes up to sqrt(n)
                break;
            }

            //Divide the currentN by the prime p_i until it is not divisible by it
            //Increment the exponent along the way so that we have the factor p_i^e_i
            int e_i = 0;
            while( currentN % p_i == 0 ) {
                currentN /= p_i;
                e_i = incrementExact( e_i );
            }

            //If currentN was divisible by the prime p_i then add the factor p_i^e_i because e_i > 0
            if( e_i > 0 ) {
                factorization.add( new FactorizationFactor( p_i, e_i ) );
            }
        }

        //Now currentN is the value of p_0^e_0 * p_1^e_1 * p_2^e_2 * ... p_{L - 1}^e_{L - 1},
        //where there are L prime divisors of n less than or equal to sqrt(n)
        long currentFactorizationValue = currentN;

        //There is a potential last factor prime factor if n has a prime factor bigger than sqrt(n)
        //We calculate it by dividing n by already found p_0^e_0 * p_1^e_1 * p_2^e_2 * ... p_{L - 1}^e_{L - 1}
        //e.g. n = 138 = 2^1*3^1*23^1, sqrt(n) = 11.7473401, 23 > sqrt(n)
        //So up to this point we have the factorization equal to [2,1], [3,1] and
        //p_0^e_0 * p_1^e_1 * p_2^e_2 * ... p_{L - 1}^e_{L - 1} = 2^1*3^1 = 6
        //Last prime factor will be 138 / 6 = 23

        long potentialLastPrimeFactor = n / currentFactorizationValue;

        //potentialLastPrimeFactor is:
        //- equal to 1: trial division already has the final factorization
        //- greater than 1 and being a prime: we need to add it to the factorization with the exponent equal to 1 to
        //  have a final factorization
        //- greater than 1 and not being a prime: trial division will not be enough to factorize n
        if( primeChecker.isPrime( potentialLastPrimeFactor ) ) {
            factorization.add( new FactorizationFactor( potentialLastPrimeFactor, 1 ) );
        }

        return factorization;
    }

    private boolean isCorrectFactorization( long n, List<FactorizationFactor> factorization ) {
        //To check if given factorization is correct, calculate the value:
        //p_0^e_0 * p_1^e_1 * p_2^e_2 * ... p_{k - 1}^e_{k - 1}
        //Then check if it is equal to n

        long factorizationValue = 1;
        for( FactorizationFactor factor : factorization ) {
            long p_i = factor.getPrime();
            long e_i = factor.getExponent();
            factorizationValue = multiplyExact( factorizationValue, power( p_i, e_i ) );
        }

        return factorizationValue == n;
    }

    private List<Long> generatePrimes( int primesLimitInclusive ) {
        //Generates primes using Sieve of Eratosthenes

        List<Long> primes = new ArrayList<>();

        for( long prime : new EratosthenesSievePrimeGenerator( incrementExact( primesLimitInclusive ) ) ) {
            primes.add( prime );
        }

        return primes;
    }
}
