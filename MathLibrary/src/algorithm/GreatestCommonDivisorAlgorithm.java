package algorithm;

import java.util.List;

/**
 * Algorithm for calculating the greatest common divisor (\(\text{gcd}\)) of the given numbers. It is the greatest
 * number that divides all of them without leaving a remainder.
 * <br>Greatest common divisor of two values is calculated using the Euclidean algorithm (see {@link EuclideanAlgorithm}
 * for more details on how it is done).
 * <br>To get the \(\text{gcd}\) of more than two values, we use the fact that:
 * <br>\(\text{gcd}(a_1, a_2, a_3, a_4, ..., a_n) =
 * \text{gcd}(... \text{gcd}(\text{gcd}(\text{gcd}(a_1, a_2), a_3), a_4), ..., a_n)\)
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Greatest_common_divisor">Wikipedia - Greatest common divisor</a>
 */
public class GreatestCommonDivisorAlgorithm {
    private EuclideanAlgorithm euclideanAlgorithm = new EuclideanAlgorithm();

    /**
     * Variant of {@link #getGreatestCommonDivisor(long...)} for {@link List} input.
     */
    public long getGreatestCommonDivisor( List<Long> values ) {
        return getGreatestCommonDivisor( toLongArray( values ) );
    }

    /**
     * Calculates the greatest common divisor of the given values.
     * <br>For the detailed description of the implementation, see {@link GreatestCommonDivisorAlgorithm}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param values numbers for which the greatest common divisor will be found
     *
     * @return Greatest common divisor of the given values. It is always a positive number.
     *
     * @throws IllegalArgumentException when {@code values} is null, less than two values are given or all of given
     *                                  values are equal to zero
     * @throws ArithmeticException      when at least one of given value is equal to {@link Long#MIN_VALUE}
     * @see <a href="https://en.wikipedia.org/wiki/Greatest_common_divisor">Wikipedia - Greatest common divisor</a>
     */
    public long getGreatestCommonDivisor( long... values ) {
        if( values == null ) {
            throw new IllegalArgumentException( "Values cannot be null." );
        }
        if( values.length < 2 ) {
            throw new IllegalArgumentException( "Provide at least 2 values to look for greatest common divisor." );
        }

        //Using the fact that gcd(a,b,c) = gcd(gcd(a,b),c) and analogically for more values
        long gcd = values[0];
        for( int i = 1; i < values.length; i++ ) {
            if( values[i] != 0 ) {
                //gcd(a,0) = a so there is no need to change gcd for value equal to 0
                //Please note that the final gcd equal to zero means the entire array is zeroes in which case we
                //cannot give the correct gcd value and IllegalArgumentException is thrown
                gcd = euclideanAlgorithm.getGreatestCommonDivisor( gcd, values[i] );
            }
        }

        if( gcd == 0 ) {
            //All the values are equal to zero - cannot return the correct gcd
            throw new IllegalArgumentException( "All given values are equal to zero. Cannot calculate gcd." );
        }

        return gcd;
    }

    private long[] toLongArray( List<Long> values ) {
        if( values == null ) {
            return null;
        }

        long[] valuesArray = new long[values.size()];
        for( int i = 0; i < valuesArray.length; i++ ) {
            valuesArray[i] = values.get( i );
        }
        return valuesArray;
    }
}
