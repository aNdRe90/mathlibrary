package algorithm;

import static java.lang.Math.*;
import static util.BasicOperations.abs;

/**
 * Extended Euclidean algorithm is an extension to the Euclidean algorithm (please see the
 * {@link EuclideanAlgorithm} for more details), which allows to compute the values of \(x\) and \(y\) such
 * that \(ax + by = \text{gcd}(a, b)\), where \(a,b,x,y\) are non-zero integers.
 * <br>The fact that such values exists is asserted by Bézout's lemma (or Bézout's identity) and \(x, y\) are also
 * called Bézout's coefficients.
 *
 * <br><br>Like in Euclidean algorithm (see {@link EuclideanAlgorithm}), we will be relying on the facts:
 * <br>\(\text{gcd}(a, b) = \text{gcd}(b, a \text{ mod } b)\)
 * <br>\(\text{gcd}(a, 0) = \text{gcd}(0, a) = a\)
 *
 * <br><br>The algorithm procedure is best shown by the example. Let`s find the
 * \(\text{gcd}(\color{purple}{240}, \color{purple}{46})\). Remainders are \(\color{red}{\text{red}}\), quotients are
 * \(\color{blue}{\text{blue}}\).
 * \begin{align}
 * \color{purple}{240} &amp; = \color{blue}{5} \cdot \color{purple}{46} + \color{red}{10} \\
 * \color{purple}{46} &amp; = \color{blue}{4} \cdot 10 + \color{red}{6} \\
 * 10 &amp; = \color{blue}{1} \cdot 6 + \color{red}{4} \\
 * 6 &amp; = \color{blue}{1} \cdot 4 + \color{red}{2} \\
 * 4 &amp; = \color{blue}{2} \cdot 2 + \color{red}{0}
 * \end{align}
 *
 * <br>The result is of Euclidean algorithm is \(\text{gcd}(\color{purple}{240}, \color{purple}{46}) = 2\).
 *
 * <br><br>Let`s find the Bézout's coefficients now.
 * \begin{align}
 * \color{red}{10} &amp; = \color{green}{1} \cdot \color{purple}{240} \color{green}{-5} \cdot \color{purple}{46} \\
 *
 * \color{red}{6} &amp; = \color{purple}{46} - \color{blue}{4} \cdot 10 = \color{purple}{46} - \color{blue}{4}
 * \cdot (\color{purple}{240} - \color{blue}{5} \cdot \color{purple}{46}) = \color{green}{-4} \cdot \color{purple}{240}
 * + \color{green}{21} \cdot \color{purple}{46} \\
 *
 * \color{red}{4} &amp; = 10 - \color{blue}{1} \cdot 6 = (\color{purple}{240} - \color{blue}{5} \cdot
 * \color{purple}{46}) - \color{blue}{1} \cdot (-4 \cdot \color{purple}{240} +21 \cdot \color{purple}{46}) =
 * \color{green}{5} \cdot \color{purple}{240} \color{green}{-26} \cdot \color{purple}{46} \\
 *
 * \color{red}{2} &amp; = 6 - \color{blue}{1} \cdot 4 = (-4 \cdot \color{purple}{240} + 21 \cdot \color{purple}{46}) -
 * \color{blue}{1} \cdot (5 \cdot \color{purple}{240} - 26 \cdot \color{purple}{46}) = \color{green}{-9} \cdot
 * \color{purple}{240} + \color{green}{47} \cdot \color{purple}{46}
 * \end{align}
 *
 * <br>Looking at the values of \(\color{green}{x}, \color{green}{y}\) during each step we can see that:
 * \begin{align}
 * \color{green}{x_i} &amp; = x_{i - 2} - \color{blue}{q_i} \cdot x_{i - 1} \\
 * \color{green}{y_i} &amp; = y_{i - 2} - \color{blue}{q_i} \cdot y_{i - 1}
 * \end{align}
 *
 * <br>The consecutive values being: \((x_0, y_0) = (1, -5), (x_1, y_1) = (-4, 21), (x_2, y_2) = (5, -26),
 * (x_3, y_3) = (-9, 47)\)
 *
 * <br><br>Let`s now establish the values of \(x_{-2}, x_{-1}, y_{-2}, y_{-1}\).
 *
 * \begin{align}
 * \begin{cases}
 * x_1 &amp; = x_{-1} - \color{blue}{q_1} \cdot x_0 \\
 * \color{green}{-4} &amp; = x_{-1} - \color{blue}{4} \cdot \color{green}{1} \\
 * x_{-1} &amp; = 0
 * \end{cases}
 * \begin{cases}
 * x_0 &amp; = x_{-2} - \color{blue}{q_0} \cdot x_{-1} \\
 * \color{green}{1} &amp; = x_{-2} - \color{blue}{5} \cdot 0 \\
 * x_{-2} &amp; = 1
 * \end{cases}
 * \begin{cases}
 * y_1 &amp; = y_{-1} - \color{blue}{q_1} \cdot y_0 \\
 * \color{green}{21} &amp; = y_{-1} - \color{blue}{4} \cdot (\color{green}{-5}) \\
 * y_{-1} &amp; = 1
 * \end{cases}
 * \begin{cases}
 * y_0 &amp; = y_{-2} - \color{blue}{q_0} \cdot y_{-1} \\
 * \color{green}{-5} &amp; = y_{-2} - \color{blue}{5} \cdot 1 \\
 * y_{-2} &amp; = 0
 * \end{cases}
 * \end{align}
 *
 * <br>If we get the \(\color{red}{\text{remainder}}\) equal to zero in \(i\)-th iteration, then Bézout's coefficients
 * are \((x, y) = (x_{i - 1}, y_{i - 1})\).
 *
 *
 *
 * <br><br><br><br>When one pair of (\(x, y\)) has been computed, all pairs can be represented in the
 * form \(\left(x + k\frac{b}{\text{gcd}(a, b)}, y - k\frac{a}{\text{gcd}(a, b)}\right)\), where \(k\) is an arbitrary
 * integer.
 * <br>Quick proof for that is the following:
 * <br>Let \((x_1, y_1), (x_2, y_2)\) be the solutions to \(ax + by = \text{gcd}(a,b)\).
 * <br>
 * \begin{align}
 * ax_1 + by_1 &amp; = \text{gcd}(a,b) \\
 * ax_2 + by_2 &amp; = \text{gcd}(a,b) \\
 * \\
 * ax_2 + by_2 - (ax_1 + by_1) &amp; = 0 \\
 * a(x_2 - x_1) + b(y_2 - y_1) &amp; = 0 \\
 * a(x_2 - x_1) &amp; = b(y_1 - y_2) \\
 * \frac{a}{\text{gcd}(a, b)}(x_2 - x_1) &amp; = \frac{b}{\text{gcd}(a, b)}(y_1 - y_2) \\
 * a'(x_2 - x_1) &amp; = b'(y_1 - y_2)
 * \end{align}
 * Since \(\text{gcd}(a', b') = 1\), where \(a' = \frac{a}{\text{gcd}(a, b)}, b' = \frac{b}{\text{gcd}(a, b)}\), then
 * \(\frac{x_2 - x_1}{b'} = \frac{y_1 - y_2}{a'} = k\) and from that:
 * <br>\(x_2 - x_1 = b'k \Leftrightarrow x_2 = x_1 + k\frac{b}{\text{gcd}(a, b)}\)
 * <br>\(y_1 - y_2 = a'k \Leftrightarrow y_2 = y_1 - k\frac{a}{\text{gcd}(a, b)}\)
 *
 * <br><br>Among the pairs of Bézout coefficients, exactly two of them satisfy
 * \(|x| \leq \left|\frac{b}{\text{gcd}(a, b)}\right|\) and \(|y| \leq \left|\frac{a}{\text{gcd}(a, b)}\right|\).
 * The Extended Euclidean algorithm always produces one of these two minimal pairs.
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href= "https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm">Wikipedia - Extended Euclidean
 * algorithm</a>
 * @see <a href= "https://en.wikipedia.org/wiki/B%C3%A9zout%27s_identity">Wikipedia - Bézout's identity</a>
 */
public class ExtendedEuclideanAlgorithm {
    /**
     * Performs the Extended Euclidean algorithm to find the Bézout's coefficients and greatest common divisor of
     * given integers \(a, b\).
     * <br>For the detailed description of the implementation, see {@link ExtendedEuclideanAlgorithm}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param b value of \(b\)
     *
     * @return An integer array with Bézout's coefficients \((x,y)\) and greatest common divisor of \(a, b\) in the
     * form: [\(x, y, \text{gcd}(a, b)\)].
     * <br>The returned values of \((x, y)\) are one of the two minimal pair satisfying:
     * \(|x| \leq \left|\frac{b}{\text{gcd}(a, b)}\right|\) and \(|y| \leq \left|\frac{a}{\text{gcd}(a, b)}\right|\).
     *
     * @throws IllegalArgumentException when \(a = 0\) or \(b = 0\)
     * @throws ArithmeticException      when overflow occurs (when either \(a\) or \(b\) is equal to
     *                                  {@link Long#MIN_VALUE})
     * @see <a href= "https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm">Wikipedia - Extended Euclidean
     * algorithm</a>
     * @see <a href= "https://en.wikipedia.org/wiki/B%C3%A9zout%27s_identity">Wikipedia - Bézout's identity</a>
     */
    public long[] getBezoutsCoefficientsWithGCD( long a, long b ) throws IllegalArgumentException {
        if( a == 0 || b == 0 ) {
            throw new IllegalArgumentException( "Cannot calculate Bezouts coefficients when a or b is equal zero." );
        }

        //We need to prepare the input so that the Extended Euclidean algorithm is executed for (a, b) such that
        //a > 0, b > 0, a >= b
        boolean changeSignOfX = a < 0; //If a < 0 then later on value of x needs to be changed to -x
        boolean changeSignOfY = b < 0; //If b < 0 then later on value of y needs to be changed to -y
        a = abs( a ); //a = |a|
        b = abs( b ); //b = |b|
        boolean swapXY = b >= a; //If b >= a then later on change the returned (x, y) to (y, x)

        //Proceed with the Extended Euclidean algorithm for a > 0, b > 0, a >= b
        long[] bezoutsCoefficientsWithGCD = calculateBezoutsCoefficientsWithGCD( max( a, b ), min( a, b ) );
        //x = bezoutsCoefficientsWithGCD[0]
        //y = bezoutsCoefficientsWithGCD[1]
        //gcd(a, b) = bezoutsCoefficientsWithGCD[2]

        if( swapXY ) {
            //Change the returned coefficients to (y, x) because we swaped a with b before executing the algorithm
            bezoutsCoefficientsWithGCD = new long[]{ bezoutsCoefficientsWithGCD[1], bezoutsCoefficientsWithGCD[0],
                    bezoutsCoefficientsWithGCD[2] };
        }

        if( changeSignOfX ) {
            //Change x to -x because a was originally a negative number
            //Using subtractExact to assure that ArithmeticException is thrown for Long.MIN_VALUE
            bezoutsCoefficientsWithGCD[0] = subtractExact( 0, bezoutsCoefficientsWithGCD[0] );
        }
        if( changeSignOfY ) {
            //Change y to -y because b was originally a negative number
            //Using subtractExact to assure that ArithmeticException is thrown for Long.MIN_VALUE
            bezoutsCoefficientsWithGCD[1] = subtractExact( 0, bezoutsCoefficientsWithGCD[1] );
        }

        return bezoutsCoefficientsWithGCD;
    }

    private long[] calculateBezoutsCoefficientsWithGCD( long a, long b ) {
        //a > 0, b > 0, a >= b

        //Following the rules that:
        //1) gcd(a, 0) = gcd(0, a) = a
        //2) gcd(a, b) = gcd(b, r) where a = q*b + r

        long x_i_2 = 1; //x_{i - 2}
        long x_i_1 = 0; //x_{i - 1}
        long y_i_2 = 0; //y_{i - 2}
        long y_i_1 = 1; //y_{i - 1}

        while( true ) {
            //a = q_i*b + r_i
            //0 <= r_i < b
            long q_i = a / b;
            long r_i = a % b;

            if( r_i == 0 ) {
                //gcd(a, b) = gcd(b, 0) = b
                //Return [x_{i - 1}, y_{i - 1}, gcd(a, b)]
                return new long[]{ x_i_1, y_i_1, b };
            }

            long x_i = subtractExact( x_i_2, multiplyExact( q_i, x_i_1 ) ); //x_i = x_{i - 2} - q_i*x_{i - 1}
            long y_i = subtractExact( y_i_2, multiplyExact( q_i, y_i_1 ) ); //y_i = y_{i - 2} - q_i*y_{i - 1}

            //Updating the values for next iteration
            x_i_2 = x_i_1;
            x_i_1 = x_i;
            y_i_2 = y_i_1;
            y_i_1 = y_i;

            //Updating variables to calculate gcd(b, r_i) in the next iteration
            a = b;
            b = r_i;
        }
    }
}
