package algorithm;

import java.util.List;

import static java.lang.Math.multiplyExact;
import static util.BasicOperations.abs;

/**
 * A common multiple is a value that is a multiple of two or more numbers. The common multiples of 3 and 4 are 12, 24,
 * ...
 * <br>The least common multiple (\(lcm\)) of the given numbers is the smallest number that is divisble by all of the
 * given values e.g. \(lcm(3,5,10) = 30\).
 * <br>If any of the given value is equal to \(0\) then \(lcm = 0\) because only \(0\) is a multiple of \(0\).
 *
 * <br><br>The formula for calculating the least commom multiple of the given numbers is the following:
 * <br>\(lcm(a,b) = \frac{|a \cdot b|}{\text{gcd}(a,b)}\)
 * <br>\(lcm(a_1, a_2, a_3, a_4, ..., a_n) = lcm(... lcm(lcm(lcm(a_1, a_2), a_3), a_4), ..., a_n)\)
 *
 * <br><br>Calculating \(\text{gcd}(a,b)\) is done by using the {@link GreatestCommonDivisorAlgorithm}.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href= "https://en.wikipedia.org/wiki/Least_common_multiple">Wikipedia - Least common multiple</a>
 */
public class LeastCommonMultipleAlgorithm {
    private GreatestCommonDivisorAlgorithm gcdAlgorithm = new GreatestCommonDivisorAlgorithm();

    /**
     * Variant of {@link #getLeastCommonMultiple(long...)} for {@link List} input.
     */
    public long getLeastCommonMultiple( List<Long> values ) {
        return getLeastCommonMultiple( toLongArray( values ) );
    }

    /**
     * Calculates the least common multiple of the given values.
     * <br>For the detailed description of the implementation, see {@link LeastCommonMultipleAlgorithm}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param values numbers for which the least common multiple will be found
     *
     * @return Least common multiple of the given values. It is always a positive number.
     *
     * @throws IllegalArgumentException when {@code values} is null or less than two values are given
     * @throws ArithmeticException      when at least one of given value is equal to {@link Long#MIN_VALUE} or when
     *                                  overflow occurs
     * @see <a href= "https://en.wikipedia.org/wiki/Least_common_multiple">Wikipedia - Least common multiple</a>
     */
    public long getLeastCommonMultiple( long... values ) {
        if( values == null ) {
            throw new IllegalArgumentException( "Values cannot be null." );
        }
        if( values.length < 2 ) {
            throw new IllegalArgumentException( "Provide at least 2 values to look for least common multiple." );
        }

        //Using the fact that lcm(a,b,c) = lcm(lcm(a,b),c) and analogically for more values
        long lcm = values[0];
        for( int i = 1; i < values.length; i++ ) {
            lcm = getLeastCommonMultiple( lcm, values[i] );
        }

        return lcm;
    }

    private long[] toLongArray( List<Long> values ) {
        if( values == null ) {
            return null;
        }

        long[] valuesArray = new long[values.size()];
        for( int i = 0; i < valuesArray.length; i++ ) {
            valuesArray[i] = values.get( i );
        }
        return valuesArray;
    }

    private long getLeastCommonMultiple( long a, long b ) {
        if( a == 0 && b == 0 ) {
            //lcm(0,0) = 0
            return 0;
        }

        long gcd = gcdAlgorithm.getGreatestCommonDivisor( a, b );
        a = abs( a );
        b = abs( b );

        //lcm(a, b) = |ab| / gcd(a, b) = |a|/gcd(a,b) * b, since gcd(a,b) divides a
        return multiplyExact( ( a / gcd ), b );
    }
}
