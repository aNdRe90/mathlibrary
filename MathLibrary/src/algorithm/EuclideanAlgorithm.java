package algorithm;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static util.BasicOperations.abs;

/**
 * Euclidean algorithm (or Euclid's algorithm) is an efficient method for computing the greatest common divisor
 * (\(\text{gcd}\)) of two integers which is the greatest number that divides both of them without leaving a remainder.
 * <br>For computing \(\text{gcd}\) of more than two numbers, use the {@link GreatestCommonDivisorAlgorithm}.
 *
 * <br><br>The Euclidean Algorithm for finding \(\text{gcd}(a,b)\), where \(a \geq b \geq 0\) and \(a, b\) are not
 * both equal to \(0\) is based on the facts:
 * <ul><li>
 * If \(b = 0\) then \(\text{gcd}(a, b) = a\), since \(\text{gcd}(x, 0) = \text{gcd}(0, x) = x\).
 * </li></ul>
 * <ul><li>
 * \(\text{gcd}(a, b) = \text{gcd}(b, r)\), where \(a = qb + r\) is the quotient remainder form of \(a\) which means
 * that \(q, r\) are integers and \(0 \leq r &lt; b\).
 *
 * <br><br>The proof of \(\text{gcd}(a, b) = \text{gcd}(b, r)\) is the following:
 * <br>1) Let \(c = a - b\) and \(a = x \cdot \text{gcd}(a, b), b = y \cdot \text{gcd}(a, b)\).
 * Then \(c = (x - y) \cdot \text{gcd}(a, b)\) so \(\text{gcd}(a, b)\) divides \(c\).
 * <br>2) \(c = a - b \Leftrightarrow a = b + c\). If \(b = m \cdot \text{gcd}(b, c)\) and
 * \(c = n \cdot \text{gcd}(b, c)\), then \(a = (m + n) \cdot \text{gcd}(b, c)\) so \(\text{gcd}(b, c)\) divides \(a\).
 * <br>3) If \(\text{gcd}(a, b)\) divides both \(b\) (by definition) and \(c\) (shown in 1)), then it is a common
 * divisor of \(b\) and \(c\). It means that \(\text{gcd}(a, b) \leq \text{gcd}(b, c)\).
 * <br>4) If \(\text{gcd}(b, c)\) divides both \(b\) (by definition) and \(a\) (shown in 2)), then it is a common
 * divisor of \(a\) and \(b\). It means that \(\text{gcd}(b, c) \leq \text{gcd}(a, b)\).
 * <br>5) If \(\text{gcd}(a, b) \leq \text{gcd}(b, c)\) and \(\text{gcd}(b, c) \leq \text{gcd}(a, b)\), then
 * \(\text{gcd}(a, b) = \text{gcd}(b, c) = \text{gcd}(b, a - b)\).
 * <br>6) If \(\text{gcd}(a, b) = \text{gcd}(b, a) = \text{gcd}(b, a - b)\), then we can write:
 * \(\text{gcd}(b, a) = \text{gcd}(b, a - b) = \text{gcd}(b, a - 2b) = \text{gcd}(b, a - 3b) = ... =
 * \text{gcd}(b, a - qb)\).
 * <br>Since \(a = qb + r\), then \(r = a - qb\) and finally
 * \(\text{gcd}(a, b) = \text{gcd}(b, a - qb) = \text{gcd}(b, r)\).
 * </li></ul>
 *
 * <br>The Euclidean Algorithm is a recursive procedure. To find \(\text{gcd}(a, b)\) write \(b\) in quotient
 * remainder form \(a = b \cdot q + r\) (\(q, r\) are integers and \(0 \leq r &lt; b\)).
 * <br>Then find \(\text{gcd}(b, r)\) using the Euclidean Algorithm since \(\text{gcd}(a, b) = \text{gcd}(b, r)\).
 * If \(r = 0\), then use the fact that \(\text{gcd}(x, 0) = x\) to get the result.
 *
 * <br><br>So the implementation is just a simple recursion or iteration including modulo operations until we need to
 * calculate \(\text{gcd}(x, 0)\) since we know it is equal to \(x\).
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Euclidean_algorithm">Wikipedia - Euclidean algorithm</a>
 * @see <a href="https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/the-euclidean-algorithm">
 * Khan Academy - The Euclidean algorithm with proof</a>
 */
public class EuclideanAlgorithm {
    /**
     * Calculates the greatest common divisor for the given two numbers.
     * <br>For the detailed description of the implementation, see {@link EuclideanAlgorithm}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param b value of \(b\)
     *
     * @return Greatest common divisor of \(a\) and \(b\). It is always a positive number.
     *
     * @throws IllegalArgumentException when \(a = 0\) and \(b = 0\)
     * @throws ArithmeticException      when overflow occurs (when either \(a\) or \(b\) is equal to
     *                                  {@link Long#MIN_VALUE})
     * @see <a href="https://en.wikipedia.org/wiki/Euclidean_algorithm">Wikipedia - Euclidean algorithm</a>
     * @see <a href="https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/the-euclidean-algorithm">
     * Khan Academy - The Euclidean algorithm with proof</a>
     */
    public long getGreatestCommonDivisor( long a, long b ) {
        if( a == 0 && b == 0 ) {
            throw new IllegalArgumentException( "Both numbers are equal to zero - cannot calculate gcd." );
        }

        if( a == 0 ) {
            //gcd(0, b) = b
            return abs( b );
        }
        if( b == 0 ) {
            //gcd(a, 0) = a
            return abs( a );
        }

        a = abs( a ); //a = |a|
        b = abs( b ); //b = |b|
        return calculateGreatestCommonDivisor( max( a, b ), min( a, b ) );
    }

    private long calculateGreatestCommonDivisor( long a, long b ) {
        //a >= b >= 0  here

        //Following the rules that:
        //1) gcd(a, 0) = gcd(0, a) = a
        //2) gcd(a, b) = gcd(b, r) where a = q*b + r

        while( true ) {
            //a = q*b + r
            //0 <= r < b
            long r = a % b;
            //We do not need to use the value of q

            if( r == 0 ) {
                return b; //Because next gcd(b, 0) will be calculated and it is equal to b
            }

            //Updating variables to calculate gcd(b, r) in the next iteration
            a = b;
            b = r;
        }
    }
}
