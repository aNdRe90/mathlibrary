package algorithm;

import generator.IterationIndexesGenerator;
import number.properties.checker.QuadraticResidueChecker;
import number.representation.FactorizationFactor;
import number.representation.ModularCongruence;
import number.representation.big.BigModularCongruence;
import solver.LinearCongruenceSolver;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static java.lang.Math.*;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static util.BasicOperations.power;
import static util.ModuloOperations.*;

/**
 * A modular square root \(x\) of an integer \(a\) modulo an integer \(m &gt; 0\) is an integer \(x\) such that
 * \(x^2 \equiv a \text{ mod } m\). Value \(a\) is then called a quadratic residue modulo \(m\). If no such \(x\)
 * exists, then \(a\) is called a quadratic non-residue modulo \(m\).
 * <br>In order to solve this, we first need to split it to the set of cases:
 * \begin{align}
 * x^2 &amp; \equiv a \text{ mod } p_0^{e_0} \\
 * x^2 &amp; \equiv a \text{ mod } p_1^{e_1} \\
 * &amp; \text{ ... } \\
 * x^2 &amp; \equiv a \text{ mod } p_{k - 1}^{e_{k - 1}}
 * \end{align}
 * <br>Where \(m = p_0^{e_0} \cdot p_1^{e_1} \cdot  \text{ ... } \cdot p_{k - 1}^{e_{k - 1}}\), \(e^i &gt; 0\) is a
 * prime factorization of \(m\).
 * <br>If any of the cases \(x^2 \equiv a \text{ mod } p_i^{e_i}\) has no solutions then the whole
 * \(x^2  \equiv a \text{ mod } m\) has no solutions.
 *
 * <br><br>Solving the \(x^2 \equiv a \text{ mod } p_i^{e_i}\) means:
 * <ul><li>
 * Solving \(x^2 \equiv a \text{ mod } p_i\).
 * </li></ul>
 * <ul><li>
 * Lifting the solutions of \(x^2 \equiv a \text{ mod } p_i\) so that we obtain the solutions for
 * \(x^2 \equiv a \text{ mod } p_i^{e_i}\).
 * </li></ul>
 *
 * <br>Solving \(x^2 \equiv a \text{ mod } p_i\) must be considered separately for:
 * <ul><li>
 * \(p_i = 2:\)
 * <br>Then \(x \equiv 0 \text{ mod } 2\) for even \(a\) and \(x \equiv 1 \text{ mod } 2\) for odd \(a\).
 * </li></ul>
 * <ul><li>
 * \(p_i \neq 2:\)
 * <br>Then we use the Tonelli–Shanks algorithm. It requires however that \(\text{gcd}(a, p_i) = 1\). The algorithm is
 * the following:
 * <br>1. Factor out powers of \(2\) from \(p_i − 1\), defining \(Q, S\) as: \(p_i − 1 = Q2^S\) with \(Q\) odd.
 * If \(S = 1\) i.e. \(p_i \equiv 3 \text{ mod } 4\), then solutions are given directly by
 * \(R \equiv \pm a^{\frac{p_i + 1}{4}} \text{ mod } p_i\).
 * <br>2. Select a \(z\) that is a quadratic non-residue modulo \(p_i\). Every odd prime \(p_i\) has exactly
 * \(\frac{p_i - 1}{2}\) quadratic residues and \(\frac{p_i - 1}{2}\) quadratic non-residues. Pick the values at random
 * or iterate over them starting from \(1\) (\(0\) is a quadratic residue) to spot the first quadratic non-residue.
 * <br>How to check if a given value is a quadratic residue modulo \(p_i\) is shown in the description of
 * {@link QuadraticResidueChecker}.
 * <br>3. Set \(M = S, c \equiv z^Q \text{ mod } p_i, t \equiv a^Q \text{ mod } p_i,
 * R \equiv a^{\frac{Q + 1}{2}} \text{ mod } p_i\).
 * <br>Loop:
 * <ul><li>
 * If \(t \equiv 1 \text{ mod } p_i\), return \(R\).
 * </li></ul>
 * <ul><li>
 * Otherwise, find the lowest \(i\), \(0 &lt; i &lt; M\), such that \(t^{2^i} \equiv 1 \text{ mod } p_i\) (e.g. via
 * repeated squaring).
 * </li></ul>
 * <ul><li>
 * Set \(b \equiv c^{2^{M - i - 1}} \text{ mod } p_i, R \equiv Rb \text{ mod } p_i, t \equiv tb^2 \text{ mod } p_i,
 * c \equiv b^2 \text{ mod } p_i, M = i\).
 * </li></ul>
 * Once it is finished, the solutions are \(R\) and \(p_i - R\).
 *
 * <br><br>In order to apply this algorithm let`s notice that \(x^2 \equiv a \text{ mod } p_i\) is equivalent to
 * \(x^2 \equiv a' \text{ mod } p_i\) where \(a' \equiv a \text{ mod } p_i\). Value \(a'\) is now either coprime with
 * \(p_i\) or \(a' = 0\).
 * <br>In the first case Tonelli-Shanks algorithm can be used but we first check if \(a'\) is a quadratic residue
 * using Euler`s criterion which states that \(a'\) is a quadratic residue modulo \(p_i\) if
 * \(\text{gcd}(a', p_i) = 1\) and \(a'^{\frac{p_i - 1}{2}} \equiv 1 \text{ mod } p\). If it is not, no solution for
 * \(x^2 \equiv a' \text{ mod } p_i\) exists.
 * <br>In the latter case we will be using the fact that \(x^2 \equiv AC \text{ mod } BC\) is equal to
 * \(\frac{x^2}{C} \equiv A \text{ mod } B\) for integers \(A, B, C\). So \(C\) needs to be a square number in order
 * for this to have a solution.
 *
 * <br>We are dealing with \(x^2 \equiv 0 \text{ mod } p_i^{e_i}\). In this case we consider the following:
 * <ul><li>
 * If \(e_i \equiv 0 \text{ mod } 2\), then \(\left(\frac{x}{p_i^{e_i/2}}\right)^2 \equiv 0 \text{ mod } 1\) and the
 * solution is \(x \equiv 0 \text{ mod } p_i^{e_i/2}\).
 * </li></ul>
 * <ul><li>
 * If \(e_i \equiv 1 \text{ mod } 2\), then \(\left(\frac{x}{p_i^{\lfloor e_i/2 \rfloor}}\right)^2 \equiv 0
 * \text{ mod } p_i\) and the solution is \(x \equiv 0 \text{ mod } p_i^{\lfloor e_i/2 \rfloor + 1}\).
 * </li></ul>
 * <br>In general the solution to \(x^2 \equiv 0 \text{ mod } M^K\) is
 * \(x \equiv 0 \text{ mod } M^{\lfloor K/2 \rfloor + K \text{ mod } 2}\).
 *
 *
 * <br><br><br>If \(a = p_i^{j}\), where \(0 &lt; j &lt; e_i\), then it is faster to solve
 * \(x^2 \equiv p_i^{j} \text{ mod } p_i^{e_i}\) once again using the fact that \(x^2 \equiv AC \text{ mod } BC\) is
 * equal to \(\frac{x^2}{C} \equiv A \text{ mod } B\) for integers \(A, B, C\) (instead of using the Tonelli-Shanks
 * algorithm and lifting the solutions).
 * <ul><li>
 * If \(j \equiv 0 \text{ mod } 2\) then solve the
 * \(\left(\frac{x}{p_i^{j/2}}\right)^2 \equiv 1 \text{ mod } p_i^{e_i - j}\) by solving
 * \(y^2 \equiv 1 \text{ mod } p_i^{e_i - j}\) (\(\text{gcd}(1, p_i) = 1\) so apply the Tonelli-Shanks algorithm and
 * lift the solutions), where \(y = \frac{x}{p_i^{j/2}}\) and then multiplying the results to that by \(p_i^{j/2}\)
 * (both residue and modulo).
 * </li></ul>
 * <ul><li>
 * If \(j \equiv 1 \text{ mod } 2\) then no solution exists for \(x^2 \equiv p_i^{j} \text{ mod } p_i^{e_i}\).
 * </li></ul>
 * </li></ul>
 *
 * <br><br>Lifting the solution of \(x^2 \equiv a' \text{ mod } p_i\) based on Hensel`s Lemma (\(f(x) = x^2 - a'\)):
 * <br>Let \(p_i\) be a prime and let \(k\) be an arbitrary positive integer, and suppose that \(x_k\) is a solution of
 * \(f(x) \equiv 0 \text{ mod } p_i^k, k \geq 1\).
 * <ul><li>
 * If \(p_i \not| f'(x_k)\), then there is precisely one solution \(x_{k+1}\) of
 * \(f(x) \equiv 0 \text{ mod } p_i^{k + 1}\) such that \(x_{k+1} \equiv x_k \text{ mod } p_i^k\).
 * The solution is given by \(x_{k+1} = x_k + p_i^kt\), where \(t\) is the unique solution of
 * \(f'(x_k)t \equiv \frac{-f(x_k)}{p_i^k} \text{ mod } p_i\).
 * </li></ul>
 * <ul><li>
 * If \(p_i | f'(x_k)\) and \(p_i^{k + 1} | f(x_k)\), then there are \(p_i\) solutions of the congruence
 * \(f(x) \equiv 0 \text{ mod } p_i^{k + 1}\) that are congruent to a modulo \(p_i^k\).
 * These solutions are \(x_{k+1} = x_k + p_i^kj\) for \(j = 0, 1, 2, ..., p_i - 1\).
 * </li></ul>
 * <ul><li>
 * If \(p_i | f'(x_k)\) and \(p_i^{k + 1} \not| f(x_k)\), then there are no solutions of the congruence
 * \(f(x) \equiv 0 \text{ mod } p_i^{k + 1}\) that are congruent to a modulo \(p_i^k\).
 * </li></ul>
 *
 * <br>Note that, if we have a solution \(x_1\) to \(f(x) \equiv 0 \text{ mod } p_i^1\), then if \(x_2\) solves
 * \(f(x) \equiv 0 \text{ mod } p_i^2\), we have \(x_2 \equiv x_1 \text{ mod } p_i^1\). It also means that
 * \(f'(x_2) \equiv f'(x_1) \text{ mod } p_i^1\). By the same reason, \(f'(x_k) \equiv f'(x_1) \text{ mod } p_i^{k - 1}
 * \text{ for }k \geq 2\).
 * <br>This fact makes it easier for us as we only need to calculate \(f'(x_1)\) and if it is equal to zero, then
 * \(f'(x_k) \equiv 0 \text{ mod } p_i^{k - 1} \text{ for }k \geq 2\).
 *
 * <br><br>Having all the cases \(x^2 \equiv a \text{ mod } p_i^{e_i}\) solved, Chinese Remainder Theorem
 * needs to be applied in order to form a final solution to \(x^2 \equiv a \text{ mod } m\), where
 * \(m = p_0^{e_0} \cdot p_1^{e_1} \cdot  \text{ ... } \cdot p_{k - 1}^{e_{k - 1}}\), \(e^i &gt; 0\).
 * <br>e.g. If for m = 10 the solutions are [1 mod 2] and [3 mod 5, 4 mod 5], then we need to use Chinese Remainder
 * Theorem for two inputs: [1 mod 2, 3 mod 5] and [1 mod 2, 4 mod 5].
 * <br>For more details on how the Chinese Remainder Theorem works, see {@link ChineseRemainderTheoremAlgorithm}.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Quadratic_residue">Wikipedia - Quadratic residue</a>
 * @see <a href="https://en.wikipedia.org/wiki/Euler%27s_criterion">Wikipedia - Euler's criterion</a>
 * @see <a href="https://en.wikipedia.org/wiki/Tonelli%E2%80%93Shanks_algorithm">Wikipedia - Tonelli–Shanks
 * algorithm</a>
 * @see <a href="https://ipfs.io/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco/wiki/Tonelli%E2%80%93Shanks_algorithm.html">Tonelli-Shanks
 * algorithm pseudocode</a>
 * @see <a href="https://en.wikipedia.org/wiki/Hensel%27s_lemma#Hensel_lifting">Wikipedia - Hensel lifting</a>
 * @see <a href="https://www.johndcook.com/blog/quadratic_congruences">John D. Cook - Quadratic congruences modulo
 * power of 2</a>
 */
public class ModularSquareRootAlgorithm {
    private ChineseRemainderTheoremAlgorithm chineseRemainderTheoremAlgorithm = new ChineseRemainderTheoremAlgorithm();
    private LinearCongruenceSolver linearCongruenceSolver = new LinearCongruenceSolver();
    private QuadraticResidueChecker quadraticResidueChecker = new QuadraticResidueChecker();
    private FactorizationAlgorithm factorizationAlgorithm = new FactorizationAlgorithm();
    private ModularCongruenceComparator modularCongruenceComparator = new ModularCongruenceComparator();

    /**
     * Finds the modular square roots of \(a \text{ mod } m\), where \(a, m\) are integers and \(m &gt; 0\).
     * <br>For the detailed description of the implementation, see {@link ModularSquareRootAlgorithm}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param m modulo \(m &gt; 0\)
     *
     * @return {@link List} of {@link ModularCongruence} representing the modular square roots of
     * \(a \text{ mod } m\) or empty {@link List} if no solution exists.
     * <br>The list has elements with the same modulo value, ordered from the smallest to greatest
     * residue value. The solution modulo is not necessarily equal to input \(m\) (but is always the same
     * for every output congruence) as the result is returned in its simplest form
     * e.g. \(x^2 \equiv 1 \text{ mod } 4\) gives \([1 \text{ mod } 2]\) as a result instead of
     * \([1 \text{ mod } 4, 3 \text{ mod } 4]\).
     *
     * @throws IllegalArgumentException when \(m &lt; 1\)
     * @see <a href="https://en.wikipedia.org/wiki/Quadratic_residue">Wikipedia - Quadratic residue</a>
     * @see <a href="https://en.wikipedia.org/wiki/Euler%27s_criterion">Wikipedia - Euler's criterion</a>
     * @see <a href="https://en.wikipedia.org/wiki/Tonelli%E2%80%93Shanks_algorithm">Wikipedia - Tonelli–Shanks
     * algorithm</a>
     * @see <a href="https://ipfs.io/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco/wiki/Tonelli%E2%80%93Shanks_algorithm.html">Tonelli-Shanks
     * algorithm pseudocode</a>
     * @see <a href="https://en.wikipedia.org/wiki/Hensel%27s_lemma#Hensel_lifting">Wikipedia - Hensel lifting</a>
     * @see <a href="https://www.johndcook.com/blog/quadratic_congruences">John D. Cook - Quadratic congruences modulo
     * power of 2</a>
     */
    public List<ModularCongruence> getSquareRoots( long a, long m ) {
        if( m < 1 ) {
            throw new IllegalArgumentException( "Given modulo must be greater than zero." );
        }
        if( m == 1 ) {
            //Every number is a solution when modulo = 1
            return singletonList( new ModularCongruence( 0, 1 ) );
        }

        return calculateSquareRoots( a, m );
    }

    private List<ModularCongruence> calculateSquareRoots( long a, long m ) {
        //x^2 ≡ a mod m is split into:

        //x^2 ≡ a mod p_0^e_0
        //x^2 ≡ a mod p_1^e_1
        //...
        //x^2 ≡ a mod p_{k - 1}^e_{k - 1}
        //where m = p_0^e_0*p_1^e_1*...*p_{k - 1}^e_{k - 1} is a prime factorization of m

        //Each of these has 0, 1 or 2 solutions, so the whole x^2 ≡ a mod m has up to 2^k solutions

        //Holds the solutions for each case x^2 = a mod p_i^e_i
        List<List<ModularCongruence>> allCasesSolutions = new ArrayList<>();

        for( FactorizationFactor factor : factorizationAlgorithm.getFactorization( m ) ) {
            //Handling the case x^2 = a mod p_i^e_i
            long p_i = factor.getPrime();
            int e_i = factor.getExponent();

            List<ModularCongruence> currentCaseSolutions = calculateSquareRoots( a, p_i, e_i );

            if( currentCaseSolutions.isEmpty() ) {
                //If any of the cases x^2 = a mod p_i^e_i has no solutions then the whole x^2 = a mod m has no solutions
                return emptyList();
            }

            allCasesSolutions.add( currentCaseSolutions );
        }

        //Chinese Remainder Theorem needs to be applied now to obtain the final result for ax^2 + bx + c = 0 mod m
        return calculateFinalSolutionsUsingChineseRemainderTheorem( allCasesSolutions );
    }

    private List<ModularCongruence> calculateSquareRoots( long a, long p_i, int e_i ) {
        if( p_i == 2 ) {
            //x^2 = a mod 2 has solution equal to x = 1 mod 2 for odd a
            //x^2 = a mod 2 has solution equal to x = 0 mod 2 for even a
            List<ModularCongruence> solutionsModuloTwo = singletonList( new ModularCongruence( a % 2, 2 ) );
            return liftSolutions( a, solutionsModuloTwo, 2, e_i );
        }

        return calculateSquareRootsModuloOddPrimePower( a, p_i, e_i );
    }

    private List<ModularCongruence> calculateSquareRootsModuloOddPrimePower( long a, long p_i, int e_i ) {
        //p_i != 2

        a = mod( a, power( p_i, e_i ) ); //makes sure 0 <= a < p_i^{e_i}

        if( a == 0 ) {
            //x^2 ≡ 0 mod p_i^k has solution equal to 0 mod p_i^(k/2) if k % 2 == 0
            //and 0 mod p_i^(k/2 + 1) if k % 2 == 1
            return singletonList( new ModularCongruence( 0, power( p_i, e_i / 2 + e_i % 2 ) ) );
        }

        long multiplier = 1; //used in case of a = p_i^(2j) mod p_i^k, 2j < k, see below
        if( a % p_i == 0 ) {
            //a and modulo are not coprime, not what we want to have later on for the Tonelli-Shank algorithm
            //In this case it means that x^2 ≡ p_i^r mod p_i^k, r < k

            long r = 0;
            do {
                a /= p_i;
                r = incrementExact( r );
            } while( a % p_i == 0 );

            //If r is even (a is a square number) then we can use the following:
            //x^2 ≡ p_i^r mod p_i^k, r < k
            //x^2 ≡ p_i^(2j) mod p_i^k, 2j < k
            //(x/p_i^j)^2 ≡ 1 mod p_i^(k - 2j)

            //Solving y^2 ≡ 1 mod p_i^(k - 2j)
            //and then x = (p_i^j)y so p_i^j is the multiplier

            if( r % 2 == 0 ) { //r == 2j
                long j = r / 2;
                multiplier = power( p_i, j );
                e_i -= r;
            }
            else {
                //r is odd and that means that j is not an integer
                //No solutions exists
                return emptyList();
            }
        }

        //a here is coprime with p_i
        List<ModularCongruence> result = calculateSolutionsForOddPrimeModuloBase( a, p_i, e_i );
        return multiplyResidueAndModulo( result, multiplier );
    }

    private List<ModularCongruence> multiplyResidueAndModulo( List<ModularCongruence> result, long multiplier ) {
        List<ModularCongruence> resultMultiplied = new ArrayList<>( result.size() );

        for( ModularCongruence resultCongruence : result ) {
            long multipliedResidue = multiplyExact( multiplier, resultCongruence.getResidue() );
            long multipliedModulo = multiplyExact( multiplier, resultCongruence.getModulo() );
            resultMultiplied.add( new ModularCongruence( multipliedResidue, multipliedModulo ) );
        }

        return resultMultiplied;
    }

    private List<ModularCongruence> calculateSolutionsForOddPrimeModuloBase( long a, long p_i, int e_i ) {
        //a here is coprime with p_i
        List<ModularCongruence> solutionsModuloPrime
                = calculateSolutionsForPrimeModuloPowerOneUsingTonelliShanksAlgorithm( a,
                                                                                       p_i ); //solutions for modulo prime to power 1

        if( solutionsModuloPrime.isEmpty() ) {
            return emptyList();
        }

        return e_i > 1 ? liftSolutions( a, solutionsModuloPrime, p_i, e_i ) : solutionsModuloPrime;
    }

    private List<ModularCongruence> calculateSolutionsForPrimeModuloPowerOneUsingTonelliShanksAlgorithm( long a,
            long p_i ) {
        a = mod( a, p_i );
        if( !isQuadraticResidueByEulersCriterion( a, p_i ) ) {
            //If by Euler`s criterion a is not a quadratic residue mod p_i, then no solutions is returned
            return emptyList();
        }

        //Inputs:
        //p_i, an odd prime
        //a, an integer which is a quadratic residue mod p_i

        //Outputs: x, an integer satisfying x^{2} ≡ a mod p_i

        //Factor out powers of 2 from p_i − 1, defining Q and S as: p_i - 1 = Q*2^S with Q odd.
        long Q = decrementExact( p_i );
        int S = 0;
        while( Q % 2 == 0 ) {
            Q /= 2;
            S = incrementExact( S );
        }

        //Note that if S = 1, i.e. p_i ≡ 3 mod 4, then solutions are given directly by x = +- a^((p_i+1) / 4) mod p_i
        if( S == 1 ) {
            long x = powerModulo( a, ( incrementExact( p_i ) ) / 4, p_i );
            return getSolutionsFromOneResultValue( x, p_i );
        }

        //Select a z such that z is a quadratic non-residue modulo p_i, and set c = z^Q mod p_i
        long z = 1;
        while( quadraticResidueChecker.isQuadraticResidue( z, p_i ) ) {
            z = incrementExact( z );
        }
        long c = powerModulo( z, Q, p_i );

        //Let:
        //x = a^( (Q+1)/2 ) mod p_i
        //t = a^Q mod p_i
        //M = S
        long x = powerModulo( a, incrementExact( Q ) / 2, p_i );
        long t = powerModulo( a, Q, p_i );
        int M = S;

        while( true ) {
            //If t = 1 mod p_i, return x.
            if( mod( t, p_i ) == 1 ) {
                return getSolutionsFromOneResultValue( x, p_i );
            }

            //Otherwise, find the lowest i, 0 < i < M, such that t^(2^i) = 1 mod p_i
            int i = 1;
            while( mod( powerModulo( t, power( 2, i ), p_i ), p_i ) != 1 ) {
                i = incrementExact( i );
            }

            //Let b = c^( 2^(M - i - 1) ) mod p_i
            long b = powerModulo( c, power( 2, decrementExact( subtractExact( M, i ) ) ), p_i );

            //And set x ≡ xb mod p_i
            //t ≡ tb^2 mod p_i
            //c ≡ b^2 mod p_i
            //M = i
            x = multiplyModulo( x, b, p_i );
            t = multiplyModulo( t, powerModulo( b, 2, p_i ), p_i );
            c = powerModulo( b, 2, p_i );
            M = i;
        }
    }

    private List<ModularCongruence> getSolutionsFromOneResultValue( long x, long modulo ) {
        //Uses the fact that x^2 = a mod modulo has two solutions
        //If one of them is x mod modulo, then the second one is -x mod modulo as (-x)^2 = x^2
        //works for any modulo value

        long secondSolution = mod( subtractExact( 0, x ), modulo ); //-x mod modulo
        return asList( new ModularCongruence( min( x, secondSolution ), modulo ),
                       new ModularCongruence( max( x, secondSolution ), modulo ) );
    }

    private List<ModularCongruence> liftSolutions( long a, List<ModularCongruence> solutionsForPrimeModulo, long p_i,
            int e_i ) {
        //solutionsForPrimeModulo holds solutions to x^2 = a mod p_i

        List<ModularCongruence> allLiftedSolutions = new ArrayList<>();

        for( ModularCongruence solutionForPrimeModulo : solutionsForPrimeModulo ) {
            //f(x) = 0 mod p_i
            long x = solutionForPrimeModulo.getResidue();

            //f'(x) mod p_i = 2x mod p_i
            long fDerivativeModPrime = multiplyModulo( 2, x, p_i );

            //If x_k solves f(x) = 0 mod p_i^k then f'(x_k) = f'(x_1) mod p_i^{k - 1} for k >= 2
            //So we need to calculate the derivative only once
            //We also did it for "mod p_i" straight away because it was easier, would not cause overflow or need to use
            //BigInteger and it will be used in "mod p_i" equation for the first time anyway

            if( fDerivativeModPrime == 0 ) {
                //This means that p_i | f'(x)
                List<ModularCongruence> liftedSolutions = liftedSolutionForDerivativeZero( a, solutionForPrimeModulo,
                                                                                           p_i, e_i );
                allLiftedSolutions.addAll( liftedSolutions );
            }
            else {
                //This means that p_i does not divide f'(x)
                ModularCongruence liftedSolution = liftSolutionForNonZeroDerivative( BigInteger.valueOf( a ),
                                                                                     solutionForPrimeModulo,
                                                                                     fDerivativeModPrime, p_i, e_i );
                if( liftedSolution != null ) {
                    allLiftedSolutions.add( liftedSolution );
                }
            }
        }

        return allLiftedSolutions;
    }

    private List<ModularCongruence> liftedSolutionForDerivativeZero( long a, ModularCongruence solutionForPrimeModulo,
            long p_i, int e_i ) {
        //Case "If p_i | f'(X)" where X is a solution to f(X) = 0 mod p_i^{k - 1}
        //solutionForPrimeModulo holds solution to x^2 = a mod p_i

        //Holds solution to x^2 = a mod p_i^{k - 1}
        List<ModularCongruence> liftedSolutions = new ArrayList<>();
        liftedSolutions.add( solutionForPrimeModulo );

        //This implementation naming differs from the javadoc description because instead of thinking of
        //knowing solutions to x^2 = a mod p_i^k and getting solutions to x^2 = a mod p_i^{k + 1} we
        //assume we know solutions to x^2 = a mod p_i^{k - 1} and are solving x^2 = a mod p_i^k
        for( int k = 2; k <= e_i; k++ ) {
            long currentModulo = power( p_i, k ); //p_i^k

            //Holds solution to x^2 = a mod p_i^k
            List<ModularCongruence> newLiftedSolutions = new ArrayList<>();

            for( ModularCongruence liftedSolution : liftedSolutions ) {
                for( long X = liftedSolution.getResidue(); X < currentModulo; X += liftedSolution.getModulo() ) {
                    //For every solution to x^2 = a mod p_i^{k - 1}

                    //f(X) = X^2 - a mod p_i^k
                    long f = subtractModulo( multiplyModulo( X, X, currentModulo ), a, currentModulo );
                    if( f == 0 ) {
                        //If p_i | f'(X) and p_i^k | f(X) then there are p solutions of the congruence
                        //f(x) = 0 mod p_i^k that are congruent to mod p_i^{k - 1}
                        //These are X + jp_i^{k - 1} for j = 0, 1, 2, ..., p_i - 1

                        //We add only X mod p_i^k because these all solutions can be written as that
                        //and we care about minimal form of modular solutions
                        newLiftedSolutions.add( new ModularCongruence( X, currentModulo ) );
                    }
                    //Else there are no solutions congruent to p^{k - 1}
                }
            }

            if( !newLiftedSolutions.isEmpty() ) {
                //Newly lifted solutions are often not in its minimal modulo e.g. [x = 0 mod 49, x = 7 mod 49,
                //x = 14 mod 49, x = 21 mod 49, x = 28 mod 49, x = 35 mod 49, x = 42 mod 49]
                //The reduction below makes it so that we have just x = 0 mod 7 instead
                //It reduces the number of iterations for the next values of k (modulo powers)
                newLiftedSolutions = reduceSolutions( newLiftedSolutions, p_i );
            }

            liftedSolutions = newLiftedSolutions;
        }

        return liftedSolutions;
    }

    private List<ModularCongruence> reduceSolutions( List<ModularCongruence> solutions, long p_i ) {
        //Needs sorting from lowest to greatest residue before it starts the reduction
        solutions.sort( modularCongruenceComparator );

        List<ModularCongruence> reducedSolutions = new ArrayList<>( solutions );
        long originalModulo = solutions.iterator().next().getModulo(); //equal to p_i^currentK

        //Reduced solutions can have modulo p_i^y where 1 < y < currentK
        for( long newModulo = originalModulo / p_i; newModulo > 1; newModulo /= p_i ) {
            if( reducedSolutions.size() % p_i != 0 ) {
                //It means that the reduction is not possible
                //Only size = 0 mod p_i means there is equivalent representation for given solutions
                //containing modulo of the same base p_i but with lower power
                break;
            }

            //Establishing new solution list size considering we reduce the power of modulo by 1
            int newReducedSolutionsSize = (int) ( reducedSolutions.size() / p_i );
            List<ModularCongruence> newReducedSolutions = new ArrayList<>( newReducedSolutionsSize );

            int i = 0;
            //Adding the number of solutions until they exceed the new modulo
            for( ModularCongruence reducedSolution : reducedSolutions ) {
                if( i++ == newReducedSolutionsSize ) {
                    break;
                }

                newReducedSolutions.add( new ModularCongruence( reducedSolution.getResidue(), newModulo ) );
            }

            if( areEquivalentSolutions( newReducedSolutions, reducedSolutions ) ) {
                //If solutions represented by mod p_i^{k - 1} are equal to the ones represented by mod p_i^k
                //then replace them by the simpler ones
                reducedSolutions = newReducedSolutions;
            }
            else {
                //Stop if solutions represented by mod p_i^{k - 1} are not the same as the ones required and
                //represented by mod p_i^k
                break;
            }
        }

        return reducedSolutions;
    }

    private boolean areEquivalentSolutions( List<ModularCongruence> newSolutions,
            List<ModularCongruence> originalSolutions ) {
        //Checks if given two lists contain the same modular values e.g.
        //[1 mod 8, 3 mod 8, 5 mod 8, 7 mod 8] is equal to [1 mod 4, 3 mod 4] and to [1 mod 2]

        //newSolutions is never empty here
        long newModulo = newSolutions.get( 0 ).getModulo();

        for( int originalSolutionsIndex = 0;
             originalSolutionsIndex < originalSolutions.size(); originalSolutionsIndex++ ) {
            int newSolutionsIndex = originalSolutionsIndex % newSolutions.size();
            long originalSolutionResidue = originalSolutions.get( originalSolutionsIndex ).getResidue();
            long newSolutionResidue = newSolutions.get( newSolutionsIndex ).getResidue();

            if( originalSolutionResidue % newModulo != newSolutionResidue ) {
                return false;
            }
        }

        return true;
    }

    private ModularCongruence liftSolutionForNonZeroDerivative( BigInteger a, ModularCongruence solutionForPrimeModulo,
            long fDerivativeModPrime, long p_i, int e_i ) {
        //Case "If p_i does not divide f'(X)" where X is a solution to f(x) = 0 mod p_i^{k - 1}
        //Then there is precisely one solution to f(x) = 0 mod p_i^k equal to X + tp_i^{k - 1} where X is a solution to
        //f(x) = 0 mod p_i^{k - 1} and t is the unique solution of f'(X)t = -f(X)/p_i^{k - 1} mod p_i

        //This implementation naming differs from the javadoc description because instead of thinking of
        //knowing solutions to x^2 = a mod p_i^k and getting solutions to x^2 = a mod p_i^{k + 1} we
        //assume we know solutions to x^2 = a mod p_i^{k - 1} and are solving x^2 = a mod p_i^k

        //Initialized with solution to x^2 = a mod p_i
        ModularCongruence liftedSolution = solutionForPrimeModulo;

        long currentModulo = p_i;
        for( int k = 2; k <= e_i; k++ ) {
            BigInteger X = BigInteger.valueOf( liftedSolution.getResidue() );
            BigInteger f = X.pow( 2 ).subtract( a ); //f(X) = X^2 - a

            BigInteger bigPrimeModulo = BigInteger.valueOf( p_i );

            //Solving for t: f`(X)t = -f(X)/p^{k - 1} mod p_i
            ModularCongruence t = linearCongruenceSolver.getSolution( fDerivativeModPrime, BigInteger.ZERO
                    .subtract( f.divide( bigPrimeModulo.pow( decrementExact( k ) ) ) ).mod( bigPrimeModulo )
                    .longValueExact(), p_i );

            currentModulo = multiplyExact( currentModulo, p_i );
            long liftedSolutionResidue = addModulo( X.longValueExact(), multiplyModulo(
                    powerModulo( p_i, decrementExact( k ), currentModulo ), t.getResidue(), currentModulo ),
                                                    currentModulo ); //(X + tp_i^{k - 1}) mod p^k

            liftedSolution = new ModularCongruence( liftedSolutionResidue, currentModulo );
        }

        return liftedSolution;
    }

    private boolean isQuadraticResidueByEulersCriterion( long a, long p_i ) {
        //Euler`s criterion:
        //Let p_i be an odd prime and a an integer coprime to p_i.
        //Then if a^{(p - 1) / 2} = 1 mod p, then  is a quadratic residue modulo p_i
        return powerModulo( a, ( ( decrementExact( p_i ) ) / 2 ), p_i ) == 1;
    }

    private List<ModularCongruence> calculateFinalSolutionsUsingChineseRemainderTheorem(
            List<List<ModularCongruence>> allCasesSolutions ) {
        //Having all solutions to every case:
        //x^2 = a mod p_0^e_0 -> [x0_0, x0_1, ...
        //x^2 = a mod p_1^e_1 -> [x1_0, x1_1, ...
        //...
        //x^2 = a mod p_{k - 1}^e_{k - 1} -> [x{k-1}_0, x{k-1}_1, ...

        //We need to form inputs for chinese remainder theorem by creating every possible combination of solutions
        //e.g. [x0_0, x1_0, ..., x{k-1}_0] is the first one, [x0_0, x1_0, ..., x{k-1}_1] is the second one etc.

        //For case mod 10 (mod 2, mod 5) if the solutions are [1 mod 2], [3 mod 5, 4 mod 5], then we need to apply
        //Chineses Remainder Theorem for two inputs: [1 mod 2, 3 mod 5] and [1 mod 2, 4 mod 5].
        //Two results are obtained that way

        List<ModularCongruence> finalSolutions = new ArrayList<>();

        int[] caseSolutionsSizes = getCaseSolutionsSizes( allCasesSolutions );
        IterationIndexesGenerator iterationIndexesGenerator = new IterationIndexesGenerator( caseSolutionsSizes );

        //Iterates through all combinations of chinese remainder theorem inputs
        for( List<Integer> primeFactorSolutionsIndexes : iterationIndexesGenerator ) {
            List<ModularCongruence> chineseRemainderTheoremInput = getChineseRemainderTheoremInput(
                    primeFactorSolutionsIndexes, allCasesSolutions );

            BigModularCongruence solution = chineseRemainderTheoremAlgorithm
                    .getSolution( chineseRemainderTheoremInput );
            finalSolutions.add( convertToModularCongruence( solution ) );
        }

        finalSolutions.sort( modularCongruenceComparator );
        return finalSolutions;
    }

    private int[] getCaseSolutionsSizes( List<List<ModularCongruence>> allCasesSolutions ) {
        int[] caseSolutionsSizes = new int[allCasesSolutions.size()];

        for( int i = 0; i < allCasesSolutions.size(); i++ ) {
            caseSolutionsSizes[i] = allCasesSolutions.get( i ).size();
        }

        return caseSolutionsSizes;
    }

    private List<ModularCongruence> getChineseRemainderTheoremInput( List<Integer> factorSolutionsIndexes,
            List<List<ModularCongruence>> allCasesSolutions ) {
        List<ModularCongruence> chineseRemainderTheoremInput = new ArrayList<>( factorSolutionsIndexes.size() );

        for( int i = 0; i < factorSolutionsIndexes.size(); i++ ) {
            chineseRemainderTheoremInput.add( allCasesSolutions.get( i ).get( factorSolutionsIndexes.get( i ) ) );
        }

        return chineseRemainderTheoremInput;
    }

    private ModularCongruence convertToModularCongruence( BigModularCongruence modularCongruence ) {
        return new ModularCongruence( modularCongruence.getResidue().longValueExact(),
                                      modularCongruence.getModulo().longValueExact() );
    }

    //Comparator for modular congruences which can be used only for values with the same modulo
    //It compares the residue values and can be used in sorting by ascending residue values
    private class ModularCongruenceComparator implements Comparator<ModularCongruence> {
        @Override
        public int compare( ModularCongruence modularCongruence1, ModularCongruence modularCongruence2 ) {
            return Long.compare( modularCongruence1.getResidue(), modularCongruence2.getResidue() );
        }
    }
}
