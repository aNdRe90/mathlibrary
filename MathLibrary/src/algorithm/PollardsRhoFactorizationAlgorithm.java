package algorithm;

import number.properties.checker.PrimeNumberChecker;
import number.representation.FactorizationFactor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Math.incrementExact;
import static java.lang.Math.subtractExact;
import static java.util.Collections.sort;
import static util.BasicOperations.abs;
import static util.ModuloOperations.addModulo;
import static util.ModuloOperations.multiplyModulo;

/**
 * Pollard's rho algorithm (also known as Pollard Monte Carlo factorization method) is an algorithm for integer
 * factorization.
 * <br>It uses only a small amount of space and is very fast for numbers with small factors, but slower in cases where
 * all factors are large (still significantly faster than the trial division in such case).
 *
 * <br>The idea is to first find a divisor of number \(n\). Let`s say that for \(n = d_1 \cdot d_2\), where
 * \(d_1 &gt; d_2\), we found the value of \(d_1\), then \(d_2 = \frac{n}{d_1}\).
 * <br>In order to factorize \(n\) we need to recursively continue this process for both \(d_1, d_2\) until \(d_1\)
 * is a prime number and \(d_2 = 1\). Then we add \(d_1^1\) to the result factorization.
 * <br>The pseudocode for that would be:
 * <br>factorize(\(n\)) {
 * <br>&emsp; find \(d_1\) and \(d_2 = \frac{n}{d_1}\)
 * <br>&emsp; if \(d_1\) is prime and \(d_2 = 1\)
 * <br>&emsp; then add [\(d_1, 1\)] to the factorization and stop
 * <br>
 * <br>&emsp; factorize(\(d_1\))
 * <br>&emsp; factorize(\(d_2\))
 * <br>}
 *
 * <br><br>Suppose we want to find the value of 42 from \([1; 1000]\). The probability of picking it at random is
 * \(\frac{1}{1000}\) = 0.001.
 * <br>However, if instead of one, we pick two values randomly wanting their
 * difference to be equal to 42, the probability is \(2!\frac{958}{1000}\frac{1}{1000} = \frac{1916}{1000000} =
 * 0.001916\). We can see that the probability almost doubled by using this trick.
 *
 * <br><br>If we pick \(k\) numbers from \([1; 1000]\) and ask whether any two numbers \(x_i, x_j\) satisfy
 * \(|x_j - x_i| = 42\), then the probability \(P\) of that is around:
 * \begin{array}{|c|c|c|c|}
 * \hline
 * k &amp; 2 &amp; 3 &amp; 4 &amp; 5 &amp; 6 &amp; 10 &amp; 15 &amp; 20 &amp; 25 &amp; 30 &amp; 100\\ \hline
 * P &amp; 0.0018 &amp; 0.0054 &amp; 0.01123 &amp; 0.018 &amp; 0.0284 &amp; 0.08196 &amp; 0.18204 &amp; 0.30648
 * &amp; 0.4376 &amp; 0.566 &amp; 0.9999\\
 * \hline
 * \end{array}
 * Around \(k = 30 \approx \sqrt{1000}\), there is more than \(50\)% probability of success, so we can say that even
 * odds appear for \(k \approx \sqrt{n}\).
 * <br>It is due to the nature of Birthday Paradox which is a similiar well-known
 * problem. It states that the probability that among \(n\) people, there exist two who have birthday on the same day,
 * is equal to \(P_n = 1 - \prod_{i = 0}^{n - 1}\frac{365 - i}{365}\).
 * <br>Turns out that \(P_n &gt; 0.5\) for \(n = 23\).
 *
 * <br><br>This kind of using the probability to ones advantage is the essence of the Pollard`s Rho algorithm which
 * goes even one step further.
 * <br>Finding the divisor of \(n\) is done by picking \(k\) values \(x_1, x_2, ..., x_k\)
 * from \([1; n]\) and instead of checking if \(|x_i - x_j|\) divides \(n\), we check whether
 * \(\text{gcd}(|x_i - x_j|, n) &gt; 1\). If so, it is a factor of \(n\).
 * <br>Turns out we need \(k \approx \sqrt[4]{n}\) for this to be efficient.
 *
 * <br><br>It would still be bad to try to store \(\sqrt[4]{n}\) values for big \(n\), so the Pollard`s Rho algorithm is
 * using the random number generating function \(f(x) = x^2 + a \text { mod } n\) in order to have a way of
 * "choosing" random numbers from \([1; n]\) one by one. Value \(a\) is picked randomly from \([0; n - 1]\).
 * <br>Having a random starting value \(x_0\) picked from \([0; n - 1]\), we can obtain next \(x_{m + 1} = f(x_m)\).
 * Then \(x_m, x_{m + 1}\) is our \(x_i, x_j\) and we can check if \(\text{gcd}(|x_i - x_j|, n) &gt; 1\).
 *
 * <br><br>There is still a problem of cycles that can appear before actually finding the divisor. It means that the
 * generating function can start repeating values quickly and thus making us stuck in infinite loop producing e.g.
 * \(x = 4, 8, 287, 13, 67, 287, 13, 67, ...\) values for which we cannot pick two consecutive ones allowing to get a
 * divisor of \(n\).
 * <br>We can avoid this by using the Floyd`s cycle-finding approach:
 * <br>If we start two runners at the same spot and one of them is twice as fast as the other, they will eventually meet
 * at the end of second lap for the faster runner and the first lap for the slower runner. Thus, at that moment, we can
 * say that the slower one made the whole lap (cycle).
 *
 * <br><br>To apply this idea to Pollard's Rho algorithm is to have the second sequence \(x'\) such that \(x'_{m + 1} =
 * f(f(x'_m))\). We even can now treat \(x_m, x'_m\) as the pair of random values \(x_i, x_j\) and use them for
 * checking if
 * \(\text{gcd}(|x_i - x_j|, n) &gt; 1\). To start \(x\) and \(x'\) from the same spot, we make sure that \(x_0 =
 * x'_0\).
 * <br>If \(x_m = x'_m\) for \(m &gt; 0\) then we have reached the cycle and need to make a new generating function
 * (choose a different \(a\) in \(f(x) = x^2 + a \text{ mod } n\)) if no divisor has been found yet.
 *
 * <br><br><br>In order to factorize any number efficiently, use the {@link FactorizationAlgorithm} which combines both
 * the trial division for the smaller numbers and Pollard`s Rho algorithm for the ones with larger prime factors.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Integer_factorization">Wikipedia - Integer factorization</a>
 * @see <a href="https://en.wikipedia.org/wiki/Pollard%27s_rho_algorithm">Wikipedia - Pollard's rho algorithm</a>
 * @see <a href="https://www.cs.colorado.edu/~srirams/courses/csci2824-spr14/pollardsRho.html">University of Colorado
 * - A Quick Tutorial on Pollard's Rho Algorithmm</a>
 * @see <a href="https://en.wikipedia.org/wiki/Birthday_problem">Wikipedia - Birthday problem</a>
 * @see <a href="https://en.wikipedia.org/wiki/Cycle_detection#Floyd's_Tortoise_and_Hare">Wikipedia - Floyd's
 * cycle-finding algorithm</a>
 */
public class PollardsRhoFactorizationAlgorithm {
    private PrimeNumberChecker primeChecker = new PrimeNumberChecker();
    private GreatestCommonDivisorAlgorithm gcdAlgorithm = new GreatestCommonDivisorAlgorithm();

    /**
     * Factorizes the given number \(n\) using the Pollard's Rho algorithm.
     * <br>For the detailed description of the implementation, see {@link PollardsRhoFactorizationAlgorithm}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param n value of \(n\)
     *
     * @return {@link List} containing the {@link FactorizationFactor}s of \(n\).
     * <br>\(n = p_0^{e_0} \cdot p_1^{e_1} \cdot p_2^{e_2} \cdot ... \cdot p_{k - 1}^{e_{k - 1}}\), where
     * \(p_i &gt; p_{i - 1}, e_i &gt; 0\) so the returned list is sorted by factors` prime values e.g. factorization of
     * \(2200 = 2^3 \cdot 5^2 \cdot 11^1\) returns \([2, 3], [5, 2], [11, 1]\).
     *
     * @throws IllegalArgumentException when \(n &lt; 2\)
     * @see <a href="https://en.wikipedia.org/wiki/Integer_factorization">Wikipedia - Integer factorization</a>
     * @see <a href="https://en.wikipedia.org/wiki/Pollard%27s_rho_algorithm">Wikipedia - Pollard's rho algorithm</a>
     * @see <a href="https://www.cs.colorado.edu/~srirams/courses/csci2824-spr14/pollardsRho.html">University of
     * Colorado
     * - A Quick Tutorial on Pollard's Rho Algorithmm</a>
     * @see <a href="https://en.wikipedia.org/wiki/Birthday_problem">Wikipedia - Birthday problem</a>
     * @see <a href="https://en.wikipedia.org/wiki/Cycle_detection#Floyd's_Tortoise_and_Hare">Wikipedia - Floyd's
     * cycle-finding algorithm</a>
     */
    public List<FactorizationFactor> getFactorization( long n ) {
        if( n < 2 ) {
            throw new IllegalArgumentException( "Cannot factorize a number less than 2." );
        }

        Map<Long, Integer> factorization = new HashMap<>(); //Holds the primes as keys and their exponents as values
        factorize( n, factorization ); //Perform the factorization filling the map with prime factors

        return transformToResult( factorization );
    }

    private void factorize( long n, Map<Long, Integer> factorization ) {
        if( n == 1 ) {
            //No need to factorize value of 1
            return;
        }

        if( primeChecker.isPrime( n ) ) {
            //If the number is prime, add it to the factorization (increase the exponent for it in the map)
            factorization.put( n, factorization.containsKey( n ) ? incrementExact( factorization.get( n ) ) : 1 );
            return;
        }

        //Factorize n = d_1 * d_2 by finding the divisor d_1 and factorizing d_1, d_2
        long d_1 = findDivisor( n );
        factorize( d_1, factorization );
        factorize( n / d_1, factorization ); //factorizing d_2 = n / d_1
    }

    private long findDivisor( long n ) {
        long divisor = 1;

        long a = ThreadLocalRandom.current().nextLong( n ); //value a for generating function f(x) = x^2 + a mod n
        long x_i = ThreadLocalRandom.current().nextLong( n ); //represents the value of sequence x
        long x_j = x_i; //represents the value of sequence x' (x'_0 = x_0)

        //Somehow even values of n cause the StackOverflowError because the factorization cannot end,
        //so check this case manually
        if( n % 2 == 0 ) {
            return 2;
        }

        do {
            x_i = generatingFunction( x_i, a, n ); //x_{m + 1} = f(x_m)

            x_j = generatingFunction( x_j, a, n );
            x_j = generatingFunction( x_j, a, n ); //x'_{m + 1} = f(f(x'_m))

            if( x_i == x_j ) {
                //If cycle occurred in the sequence of x (it met the sequence x'), choose a new generating function
                //(value of a in f(x) = x^2 + a mod n) and start again with new x_0 = x'_0 as well
                a = ThreadLocalRandom.current().nextLong( n );
                x_i = ThreadLocalRandom.current().nextLong( n );
                x_j = x_i;
                continue;
            }

            //Check if divisor = gcd(|x_i - x_j|, n) > 1
            //If so, stop the loop and return the divisor of n that has been found
            divisor = gcdAlgorithm.getGreatestCommonDivisor( abs( subtractExact( x_i, x_j ) ), n );
        } while( divisor == 1 );

        return divisor;
    }

    private long generatingFunction( long x, long a, long n ) {
        //f(x) = (x^2 + a) mod n
        return addModulo( multiplyModulo( x, x, n ), a, n );
    }

    private List<FactorizationFactor> transformToResult( Map<Long, Integer> factorization ) {
        List<Long> primes = new ArrayList<>( factorization.keySet() );
        sort( primes ); //Result needs to be sorted by ascending primes

        List<FactorizationFactor> result = new ArrayList<>( primes.size() );

        for( long prime : primes ) {
            int exponent = factorization.get( prime );
            result.add( new FactorizationFactor( prime, exponent ) );
        }

        return result;
    }
}
