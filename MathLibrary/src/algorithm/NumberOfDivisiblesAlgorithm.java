package algorithm;

import algorithm.big.BigLeastCommonMultipleAlgorithm;
import generator.CombinationGenerator;
import util.BasicOperations;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Math.*;
import static util.ModuloOperations.mod;

/**
 * Instead of iterating through all the values in the range \([a, b]\) to calculate how many values are divisible by
 * either of the \(n\) given divisors \(d_1, d_2, d_3, ..., d_n\), we can use the Inclusion–exclusion principle.
 *
 * <br>The principle describes the way to count the sum of elements in the given sets. For example, for two sets
 * \(A, B\) we have \(|A \cup B| = |A| + |B| - |A \cap B|\). For three sets it is \(|A \cup B \cup C| = |A| + |B| +
 * |C| - |A \cap B| - |A \cap C| - |B \cap C| + |A \cap B \cap C|\).
 *
 * <br><br>In general, we have
 * $$ \left| \bigcup_{i = 1}^{n}A_i \right| = \sum_{k = 1}^{n}(-1)^{k + 1}
 * \left( \sum_{1 \leq i_1 &lt; ... &lt; i_k \leq n}|A_{i_1} \cap ... \cap A_{i_k}| \right) $$
 *
 * <br>When it comes to calculating the number of values divisible by at least one divisor from
 * \(d_1, d_2, d_3, ..., d_n\), we can say that we are looking for \(\left| \bigcup_{i = 1}^{n}A_i \right|\), where
 * \(|A_i|\) is how many numbers are divisible by \(d_i\), \(|A_i \cap A_j|\) is the number of values divisible by both
 * \(d_i\) and \(d_j\) (which means divisible by \(\text{lcm}(d_i,d_j)\)) etc.
 *
 * <br><br>Now, we should know how to count the values divisible by \(d_i\) in the range \([a, b]\), where \(a \leq b\).
 * <br>For example, there are \(3\) values divisible by \(5\) inside \([3, 19]\). In order to calculate that, we need
 * to find the first and last value divisible by \(3\) in this range. It is \(5\) and \(15\). The total count is
 * obtained by doing \(\frac{15 - 5}{5} + 1 = \frac{10}{5} + 1 = 2 + 1 = 3\).
 *
 * <br><br>The general formula to count values divisible by \(d_i\) in the range \([a, b]\) is:
 * \(\frac{\text{max}_{d_i} - \text{min}_{d_i}}{d_i} + 1\), where \(\text{min}_{d_i}, \text{max}_{d_i}\) are the minimal
 * and maximal value divisible by \(d_i\) in \([a, b]\).
 *
 * <br><br>Rules for finding \(\text{min}_{d_i}, \text{max}_{d_i}\):
 * <ul><li>
 * If \(0 \equiv a \text{ mod } d_i\), then \(\text{min}_{d_i} = a\).
 * Otherwise \(\text{min}_{d_i} = a + (d_i - a \text{ mod } d_i)\).
 * </li></ul>
 * <ul><li>
 * \(\text{max}_{d_i} = b - b \text{ mod } d_i\).
 * </li></ul>
 *
 * <br>Examples:
 * <br>\([4, 16]:\)
 * <br>\(\text{min}_{4} = 4\) (because \(0 \equiv 4 \text{ mod } 4\)),
 * <br>\(\text{max}_{4} = 16 - 16 \text{ mod } 4 = 16 - 0 = 16\)
 *
 * <br><br>\([3, 32]:\)
 * <br>\(\text{min}_{7} = 3 + (7 - 3 \text{ mod } 7) = 3 + (7 - 3) = 3 + 4 = 7\),
 * <br>\(\text{max}_{7} = 32 - 32 \text{ mod } 7 = 32 - 4 = 28\)
 *
 * <br><br>\([0, 13]:\)
 * <br>\(\text{min}_{22} = 0\) (because \(0 \equiv 0 \text{ mod } 22\)),
 * <br>\(\text{max}_{22} = 13 - 13 \text{ mod } 22 = 13 - 13 = 0\)
 *
 * <br><br>\([-10, 41]:\)
 * <br>\(\text{min}_{8} = -10 + (8 - (-10) \text{ mod } 8) = -10 + (8 - 6) = -10 + 2 = -8\),
 * <br>\(\text{max}_{8} = 41 - 41 \text{ mod } 8 = 41 - 1 = 40\)
 *
 * <br><br>\([-73, -5]:\)
 * <br>\(\text{min}_{14} = -73 + (14 - (-73) \text{ mod } 14) = -73 + (14 - 11) = -73 + 3 = -70\),
 * <br>\(\text{max}_{14} = -5 - -5 \text{ mod } 14 = -5 - 9 = -14\)
 *
 * <br><br>Calculating the number of values divisible by at least one divisor from \(\{d_1 = 3, d_2 = 7, d_3 = 9\}\)
 * in \([-400, 1000]\):
 *
 * \begin{align}
 * |A_1 \cup A_2 \cup A_3| = &amp; |A_1| + |A_2| + |A_3| - |A_1 \cap A_2| - |A_1 \cap A_3| - |A_2 \cap A_3| + |A_1 \cap
 * A_2 \cap A_3| \\
 * |A_1| = &amp; \frac{999 - (-399)}{3} + 1 = \frac{1398}{3} + 1 = 466 + 1 = 467 \\
 * |A_2| = &amp; \frac{994 - (-399)}{7} + 1 = \frac{1393}{7} + 1 = 199 + 1 = 200 \\
 * |A_3| = &amp; \frac{999 - (-396)}{9} + 1 = \frac{1395}{9} + 1 = 155 + 1 = 156 \\
 * |A_1 \cap A_2| = &amp; \frac{987 - (-399)}{\text{lcm}(3, 7)} + 1 = \frac{1386}{21} + 1 = 66 + 1 = 67 \\
 * |A_1 \cap A_3| = &amp; \frac{999 - (-396)}{\text{lcm}(3, 9)} + 1 = \frac{1395}{9} + 1 = 155 + 1 = 156 \\
 * |A_2 \cap A_3| = &amp; \frac{945 - (-378)}{\text{lcm}(7, 9)} + 1 = \frac{1323}{63} + 1 = 21 + 1 = 22 \\
 * |A_1 \cap A_2 \cap A_3| = &amp; \frac{945 - (-378)}{\text{lcm}(3, 7, 9)} + 1 = \frac{1323}{63} + 1 = 21 + 1 = 22 \\
 * \end{align}
 *
 * <br>Finally:
 * $$ |A_1 \cup A_2 \cup A_3| = 467 + 200 + 156 - 67 - 156 - 22 + 22 = 467 + 200 - 67 = 600 $$
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Inclusion%E2%80%93exclusion_principle">
 * Wikipedia - Inclusion–exclusion principle</a>
 */
public class NumberOfDivisiblesAlgorithm {
    private static final BigInteger LONG_MAX_BIGINTEGER = BigInteger.valueOf( Long.MAX_VALUE );
    private BigLeastCommonMultipleAlgorithm lcmAlgorithm = new BigLeastCommonMultipleAlgorithm();

    /**
     * Calculates how many numbers in the given range are divisible by at least one of the given divisors.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param minValue minimum value in range (inclusive)
     * @param maxValue maximum value in range (inclusive)
     * @param divisors {@link List} of divisors. Divisors can be negative (it then changes their value to the
     *                 absolute value before the calculations are done) and can contain duplicated values.
     *
     * @return Number of values divisible by at least one given divisor in the given range.
     *
     * @throws IllegalArgumentException when the given range is incorrect (<code>maxValue</code> &lt;
     *                                  <code>minValue</code>) or when <code>divisors</code> are null, empty or contain
     *                                  value zero
     * @see <a href="https://en.wikipedia.org/wiki/Inclusion%E2%80%93exclusion_principle">
     * Wikipedia - Inclusion–exclusion principle</a>
     */
    public long getNumberOfDivisibles( long minValue, long maxValue, List<Long> divisors ) {
        if( maxValue < minValue ) {
            throw new IllegalArgumentException( "Incorrect range of values. Must be minValue ≤ " + "maxValue." );
        }

        if( divisors == null || divisors.isEmpty() || divisors.contains( 0 ) ) {
            throw new IllegalArgumentException( "Divisors cannot be null, empty or contain zeroes." );
        }

        List<Long> positiveUniqueDivisors = getPositiveUniqueValues( divisors );
        return calculateNumberOfDivisibles( minValue, maxValue, positiveUniqueDivisors );
    }

    private List<Long> getPositiveUniqueValues( List<Long> values ) {
        Set<Long> positiveValues = new HashSet<>( values.size() );

        for( long value : values ) {
            positiveValues.add( value < 0 ? BasicOperations.abs( value ) : value );
        }

        return new ArrayList<>( positiveValues );
    }

    private long calculateNumberOfDivisibles( long minValue, long maxValue, List<Long> positiveUniqueDivisors ) {
        long numberOfDivisibles = 0;

        for( int divisorsTaken = 1; divisorsTaken <= positiveUniqueDivisors.size(); divisorsTaken++ ) {
            long multiplier = divisorsTaken % 2 == 0 ? -1 : 1;

            for( List<Long> divisorsAtPositions : new CombinationGenerator<>( positiveUniqueDivisors,
                                                                              divisorsTaken ) ) {
                BigInteger lcmOfDivisors = getLCM( divisorsAtPositions );

                //There is for sure no value in [long a, long b] divisible by x > Long.MAX_VALUE
                if( lcmOfDivisors.compareTo( LONG_MAX_BIGINTEGER ) <= 0 ) {
                    long valuesDivisibleByLCM = calculateNumberOfDivisibles( minValue, maxValue,
                                                                             lcmOfDivisors.longValueExact() );
                    numberOfDivisibles = addExact( numberOfDivisibles,
                                                   multiplyExact( multiplier, valuesDivisibleByLCM ) );
                }
            }
        }

        return numberOfDivisibles;
    }

    private BigInteger getLCM( List<Long> values ) {
        return values.size() == 1 ? BigInteger.valueOf( values.get( 0 ) ) :
                lcmAlgorithm.getLeastCommonMultiple( convertToBigIntegerList( values ) );
    }

    private long calculateNumberOfDivisibles( long minValue, long maxValue, long divisor ) {
        long minDivisible = getMinDivisible( minValue, divisor );
        long maxDivisible = getMaxDivisible( maxValue, divisor );

        if( minDivisible > maxValue || maxDivisible < minValue ) {
            return 0;
        }

        return incrementExact( subtractExact( maxDivisible, minDivisible ) / divisor );
    }

    private List<BigInteger> convertToBigIntegerList( List<Long> values ) {
        List<BigInteger> bigIntegerList = new ArrayList<>( values.size() );

        for( long value : values ) {
            bigIntegerList.add( BigInteger.valueOf( value ) );
        }

        return bigIntegerList;
    }

    private long getMinDivisible( long minValue, long divisor ) {
        long modulo = mod( minValue, divisor );

        if( modulo == 0 ) {
            return minValue;
        }

        return addExact( minValue, subtractExact( divisor, modulo ) );
    }

    private long getMaxDivisible( long maxValue, long divisor ) {
        return subtractExact( maxValue, mod( maxValue, divisor ) );
    }
}
