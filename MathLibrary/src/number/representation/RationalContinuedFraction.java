package number.representation;

import number.representation.big.BigFraction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.Collections.unmodifiableList;

/**
 * A continued fraction representation of rational value \(x = \frac{p}{q}\), where \(p, q\) are integers and
 * \(q \neq 0\). For more information about continued fractions, see {@link ContinuedFraction}.
 * <br>This kind of continued fraction has a finite number of coefficients:
 * \(\frac{p}{q} = [a_0; a_1, a_2, a_3, ..., a_{n - 1}]\).
 *
 * <br><br>Example of calculating the coefficients \(\color{red}{a_0, a_1, a_2, a_3, ...}\) for a continued fraction
 * representing the value \(x = x_0 = \frac{5}{33}\):
 * \begin{align}
 * a_0 &amp; = \lfloor x_0 \rfloor = \lfloor \frac{5}{33} \rfloor = \color{red}{0} \\
 * t &amp; = x_0 - a_0 = \frac{5}{33} - \color{red}{0} = \frac{5}{33} \neq 0 \\
 * x_1 &amp; = \frac{1}{t} = \frac{33}{5} \\
 * \\
 * a_1 &amp; = \lfloor x_1 \rfloor = \lfloor \frac{33}{5} \rfloor = \color{red}{6} \\
 * t &amp; = x_1 - a_1 = \frac{33}{5} - \color{red}{6} = \frac{3}{5} \neq 0 \\
 * x_2 &amp; = \frac{1}{t} = \frac{5}{3} \\
 * \\
 * a_2 &amp; = \lfloor x_2 \rfloor = \lfloor \frac{5}{3} \rfloor = \color{red}{1} \\
 * t &amp; = x_2 - a_2 = \frac{5}{3} - \color{red}{1} = \frac{2}{3} \neq 0 \\
 * x_3 &amp; = \frac{1}{t} = \frac{3}{2} \\
 * \\
 * a_3 &amp; = \lfloor x_3 \rfloor = \lfloor \frac{3}{2} \rfloor = \color{red}{1} \\
 * t &amp; = x_3 - a_3 = \frac{3}{2} - \color{red}{1} = \frac{1}{2} \neq 0 \\
 * x_4 &amp; = \frac{1}{t} = 2 \\
 * \\
 * a_4 &amp; = \lfloor x_4 \rfloor = \lfloor 2 \rfloor = \color{red}{2} \\
 * t &amp; = x_4 - a_4 = 2 - \color{red}{2} = 0
 * \end{align}
 *
 * <br>Indeed:
 * $$ \frac{5}{33} = \color{red}{0} + \frac{1}{\color{red}{6} + \frac{1}{
 * \color{red}{1} + \frac{1}{\color{red}{1} + \frac{1}{\color{red}{2}}}}} $$
 *
 * <br><br>Implementation note: The value \(x\) is represented by the {@link BigFraction}.
 *
 * <br><br>Example of calculating the convergents of \(\frac{5}{33} = [\color{red}{0; 6, 1, 1, 2}]
 * \approx 0.15151515151515151515\)
 * (\(h_i\) in \(\color{green}{\text{green}}\), \(k_i\) in \(\color{blue}{\text{blue}}\)):
 * \begin{align}
 * \frac{h_{-2}}{k_{-2}} &amp; = \frac{\color{green}{0}}{\color{blue}{1}} \\
 *
 * \frac{h_{-1}}{k_{-1}} &amp; = \frac{\color{green}{1}}{\color{blue}{0}} \\
 *
 * \frac{h_0}{k_0} &amp; = \frac{a_0h_{-1} + h_{-2}}{a_0k_{-1} + k_{-2}} =
 * \frac{\color{red}{0} \cdot \color{green}{1} + \color{green}{0}}
 * {\color{red}{0} \cdot \color{blue}{0} + \color{blue}{1}} =
 * \frac{\color{green}{0}}{\color{blue}{1}} = 0 \\
 *
 * \frac{h_1}{k_1} &amp; = \frac{a_1h_0 + h_{-1}}{a_1k_0 + k_{-1}} =
 * \frac{\color{red}{6} \cdot \color{green}{0} + \color{green}{1}}
 * {\color{red}{6} \cdot \color{blue}{1} + \color{blue}{0}} =
 * \frac{\color{green}{1}}{\color{blue}{6}} \approx 0.16666666666666666667 \\
 *
 * \frac{h_2}{k_2} &amp; = \frac{a_2h_1 + h_0}{a_2k_1 + k_0} =
 * \frac{\color{red}{1} \cdot \color{green}{1} + \color{green}{0}}
 * {\color{red}{1} \cdot \color{blue}{6} + \color{blue}{1}} =
 * \frac{\color{green}{1}}{\color{blue}{7}} \approx 0.14285714285714285714 \\
 *
 * \frac{h_3}{k_3} &amp; = \frac{a_3h_2 + h_1}{a_3k_2 + k_1} =
 * \frac{\color{red}{1} \cdot \color{green}{1} + \color{green}{1}}
 * {\color{red}{1} \cdot \color{blue}{7} + \color{blue}{6}} =
 * \frac{\color{green}{2}}{\color{blue}{13}} \approx 0.15384615384615384615 \\
 *
 * \frac{h_4}{k_4} &amp; = \frac{a_4h_3 + h_2}{a_4k_3 + k_2} =
 * \frac{\color{red}{2} \cdot \color{green}{2} + \color{green}{1}}
 * {\color{red}{2} \cdot \color{blue}{13} + \color{blue}{7}} =
 * \frac{\color{green}{5}}{\color{blue}{33}} \approx 0.15151515151515151515
 * \end{align}
 *
 * <p>
 * <br>NOTE: Overflow safe.
 * <p>
 * <br>NOTE 2: Immutable.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Continued_fraction">Wikipedia - Continued fraction</a>
 */
public class RationalContinuedFraction extends ContinuedFraction {
    private final BigFraction x; //value represented by this continued fraction
    private List<BigInteger> coefficients;

    /**
     * Creates a continued fraction representing the given {@link BigFraction} value.
     * <br>For more information about this kind of continued fractions, see {@link RationalContinuedFraction}.
     *
     * <br><br>This constructor calculates all the coefficients and convergents straight away and stores them.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param x {@link BigFraction} value represented by the continued fraction
     *
     * @see <a href="https://en.wikipedia.org/wiki/Continued_fraction">Wikipedia - Continued fraction</a>
     */
    public RationalContinuedFraction( BigFraction x ) {
        if( x == null ) {
            throw new IllegalArgumentException( "Given fraction cannot be null." );
        }

        this.x = new BigFraction( x );
        coefficients = unmodifiableList( calculateCoefficients( x ) );
    }

    private List<BigInteger> calculateCoefficients( BigFraction x_i ) {
        //Procedure for calculating coefficients a_0, a_1, a_2, a_3, ... for continued fraction representing the
        //value x = x_0 is the following:
        //for i = 0, 1, 2, 3, ...
        //  a_i = floor(x_i)
        //  t = x_i − a_i
        //  if t = 0
        //      stop
        //  else
        //      x_{i + 1} = 1 / t

        List<BigInteger> coefficients = new ArrayList<>();

        while( true ) {
            BigInteger a_i = x_i.floor(); //a_i = floor(x_i)
            coefficients.add( a_i );

            BigFraction t = x_i.subtract( new BigFraction( a_i ) ); //t = x_i − a_i

            if( t.equals( BigFraction.ZERO ) ) { //if t = 0, then stop
                break;
            }

            BigFraction x_i_1 = BigFraction.ONE.divide( t ); //else, x_{i + 1} = 1 / t
            x_i = x_i_1; //adjusting the value for the next iteration
        }

        return coefficients;
    }

    /**
     * Creates a continued fraction representing the given rational value \(\frac{p}{q}\), where \(p, q\) are integers
     * and \(q \neq 0\).
     * <br>For more information about this kind of continued fractions, see {@link RationalContinuedFraction}.
     *
     * <br><br>This constructor calculates all the coefficients and convergents straight away and stores them.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param p {@link BigInteger} numerator of rational value represented by the continued fraction
     * @param q {@link BigInteger} denominator of rational value represented by the continued fraction
     *
     * @see <a href="https://en.wikipedia.org/wiki/Continued_fraction">Wikipedia - Continued fraction</a>
     */
    public RationalContinuedFraction( BigInteger p, BigInteger q ) {
        if( q.equals( BigInteger.ZERO ) ) {
            throw new IllegalArgumentException( "Denominator cannot be equal to 0." );
        }

        x = new BigFraction( p, q );
        coefficients = calculateCoefficients( new BigFraction( p, q ) );
    }

    @Override
    public Iterable<BigInteger> getCoefficients() {
        return coefficients;
    }

    @Override
    public BigDecimal getValue( int decimalDigits ) {
        return x.toBigDecimal( decimalDigits );
    }

    /**
     * Returns the {@link BigFraction} value represented by this continued fraction.
     *
     * @return {@link BigFraction} value of this continued fraction.
     */
    public BigFraction getRationalValue() {
        return x;
    }

    @Override
    public int hashCode() {
        return Objects.hash( x );
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) {
            return true;
        }
        if( o == null || getClass() != o.getClass() ) {
            return false;
        }

        //Continued fractions are equal when they represent the same value
        return this.x.equals( ( (RationalContinuedFraction) o ).x );
    }

    /**
     * Creates a string representation of this continued fraction.
     * It has a form of "\(p/q = [a_0;a_1,a_2,a_3,..., a_{n - 1}]\)", where \(n\) is the number of coefficients of
     * this continued fraction and \(\frac{p}{q}\) is the rational value represented by this continued fraction.
     *
     * @return "\(p/q = [a_0;a_1,a_2,a_3,..., a_{n - 1}]\)" e.g. "5/33 = [0;6,1,1,2]".
     */
    @Override
    public String toString() {
        if( coefficients.size() == 1 ) {
            return x + " = [" + coefficients.get( 0 ).toString() + "]";
        }

        StringBuilder builder = new StringBuilder( x + " = [" );
        builder.append( coefficients.get( 0 ) );
        builder.append( ';' );

        for( int i = 1; i < coefficients.size(); i++ ) {
            builder.append( coefficients.get( i ) );
            builder.append( ',' );
        }

        builder.deleteCharAt( builder.length() - 1 );
        builder.append( ']' );

        return builder.toString();
    }
}
