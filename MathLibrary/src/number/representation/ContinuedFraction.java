package number.representation;

import number.representation.big.BigFraction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A continued fraction is a number represented by the formula:
 * $$a_0 + \frac{1}{a_1 + \frac{1}{a_2 + \frac{1}{a_3 + ... }}}$$
 * where \(a_i\) is an integer called the \(i\)-th coefficient of the continued fraction.
 * All coefficients except for \(a_0\), must be positive integers. If \(a_0 &lt; 0\) then the value of the continued
 * fraction is negative.
 * <br>Continued fraction with coefficients equal to \(a_0, a_1, a_2, a_3, ...\) is written as
 * \([a_0; a_1, a_2, a_3, ...]\).
 *
 * <br><br>
 * The coefficients \(\color{blue}{a_0}, \color{red}{a_1}, \color{red}{a_2}, \color{red}{a_3}, ... \) of the
 * continued fraction \(\color{green}{3.87654} = [\color{blue}{a_0}; \color{red}{a_1, a_2, a_3, ...}]\) are calculated
 * in the following way:
 * \begin{align}
 * \color{green}{3.87654} &amp; =
 * \lfloor 3.87654 \rfloor + (3.87654 - \lfloor 3.87654 \rfloor) =
 * \lfloor 3.87654 \rfloor + \frac{1}{\frac{1}{3.87654 - \lfloor 3.87654 \rfloor}} =
 * \color{blue}{3} + \frac{1}{\frac{1}{0.87654}} \approx
 * \color{blue}{3} + \frac{1}{1.140849} = \\
 *
 * &amp; = \color{blue}{3} + \frac{1}{\lfloor 1.140849 \rfloor + (1.140849 - \lfloor 1.140849 \rfloor)} =
 * \color{blue}{3} + \frac{1}{\color{red}{1} + \frac{1}{\frac{1}{1.140849 - \lfloor 1.140849 \rfloor}}} =
 * \color{blue}{3} + \frac{1}{\color{red}{1} + \frac{1}{\frac{1}{0.140849}}} \approx
 * \color{blue}{3} + \frac{1}{\color{red}{1} + \frac{1}{7.099802}} = \\
 *
 * &amp; = \color{blue}{3} + \frac{1}{\color{red}{1} + \frac{1}{\lfloor 7.099802 \rfloor + (7.099802 - \lfloor 7.099802
 * \rfloor)}} =
 * \color{blue}{3} + \frac{1}{\color{red}{1} + \frac{1}{\color{red}{7} + \frac{1}{\frac{1}{7.099802 - \lfloor 7.099802
 * \rfloor}}}} =
 * \color{blue}{3} + \frac{1}{\color{red}{1} + \frac{1}{\color{red}{7} + \frac{1}{\frac{1}{0.099802}}}} \approx
 * \color{blue}{3} + \frac{1}{\color{red}{1} + \frac{1}{\color{red}{7} + \frac{1}{10.019839}}}
 * \end{align}
 *
 * <br><br>Generally, obtaining the coefficients is a recursive job. We can say that \(x = x_0 = a_0 +
 * \frac{1}{x_1}\), where \(x_0\) has coefficients \(a_0, a_1, a_2, a_3, ...\) and \(x_1\) is the 'next value' that has
 * coefficients \(a_1, a_2, a_3, ...\). We can see that for \(i \geq 0\):
 *
 * \begin{align}
 * x_i &amp; = a_i + \frac{1}{x_{i+1}} \\
 * x_i &amp; = \frac{a_ix_{i+1} + 1}{x_{i+1}} \\
 * x_ix_{i+1} &amp; = a_ix_{i+1} + 1 \\
 * x_{i+1}(x_i - a_i) &amp; = 1 \\
 * x_{i+1} &amp; = \frac{1}{x_i - a_i}
 * \end{align}
 *
 * <br>We have \(a_i = \lfloor x_i \rfloor\) and the procedure stops when \(x_i - a_i = 0\) because then the part
 * \(\frac{1}{x_{i+1}} = \frac{1}{\frac{1}{x_i - a_i}} = x_i - a_i = 0\) and \(x_i = a_i + \frac{1}{x_{i+1}} = a_i + 0\)
 * (there are no more coefficients).
 *
 * <br><br>So the formal procedure for calculating coefficients \(a_0, a_1,
 * a_2, a_3, ...\) for the continued fraction equal to value \(x = x_0\) can be written as:
 * <br>For \(i = 0, 1, 2, 3, ...\):
 * <ul><li>
 * Set \(a_i = \lfloor x_i \rfloor\).
 * </li></ul>
 * <ul><li>
 * Set \(t = x_i - a_i\).
 * </li></ul>
 * <ul><li>
 * If \(t = 0\), then stop. Otherwise, set \(x_{i + 1} = \frac{1}{t}\).
 * </li></ul>
 *
 * <br>Some examples of continued fraction coefficients:
 * \begin{align}
 * \color{green}{3.87654} &amp; = [\color{blue}{3}; \color{red}{1, 7, 10, 47, 2, ...}] \\
 * 0.97723 &amp; = [0; 1, 42, 1, 11, 8,...] \\
 * -18.21 &amp; = [-19; 1, 3, 1, 3, 5]
 * \end{align}
 *
 * <br><br>
 * A convergent \(\frac{h_i}{k_i}\) of a continued fraction is a rational approximation of its value by using the
 * sublists of its coefficients.
 * \begin{align}
 * \frac{h_0}{k_0} &amp; = \frac{a_0}{1} \\
 *
 * \frac{h_1}{k_1} &amp; = a_0 + \frac{1}{a_1} = \frac{a_1a_0 + 1}{a_1} \\
 *
 * \frac{h_2}{k_2} &amp; = a_0 + \frac{1}{a_1 + \frac{1}{a_2}} = a_0 + \frac{1}{\frac{a_2a_1 + 1}{a_2}} =
 * a_0 + \frac{a_2}{a_2a_1 + 1} = \frac{a_2a_1a_0 + a_0 + a_2}{a_2a_1 + 1} = \frac{a_2(a_1a_0 + 1) + a_0}{a_2a_1 + 1} \\
 *
 * \frac{h_3}{k_3} &amp; = a_0 + \frac{1}{a_1 + \frac{1}{a_2 + \frac{1}{a_3}}} =
 * a_0 + \frac{1}{a_1 + \frac{1}{\frac{a_3a_2 + 1}{a_3}}} = a_0 + \frac{1}{a_1 + \frac{a_3}{a_3a_2 + 1}} =
 * a_0 + \frac{1}{\frac{a_3a_2a_1 + a_1 + a_3}{a_3a_2 + 1}} = a_0 + \frac{a_3a_2 + 1}{a_3a_2a_1 + a_1 + a_3} =
 * \frac{a_3a_2a_1a_0 + a_1a_0 + a_3a_0 + a_3a_2 + 1}{a_3a_2a_1 + a_1 + a_3} =
 * \frac{a_3(a_2(a_1a_0 + 1) + a_0) + (a_1a_0 + 1)}{a_3(a_2a_1 + 1) + a_1} \\
 *
 * ... \\
 *
 * \frac{h_i}{k_i} &amp; = \frac{a_ih_{i - 1} + h_{i - 2}}{a_ik_{i - 1} + k_{i - 2}}
 * \text{ where } h_{-1} = 1, h_{-2} = 0, k_{-1} = 0, k_{-2} = 1
 * \end{align}
 *
 * <br>Some examples of continued fraction convergents:
 * \begin{align}
 * \color{green}{3.87654} &amp; \rightarrow \frac{3}{1}, \frac{4}{1}, \frac{31}{8}, \frac{314}{81}, \frac{14789}{3815},
 * \frac{29892}{7711}, \frac{44681}{11526}, \frac{74573}{19237}, \frac{119254}{30763}, \frac{193827}{50000} = 3.87654 \\
 * 0.97723 &amp; \rightarrow \frac{0}{1}, \frac{1}{1}, \frac{42}{43}, \frac{43}{44}, \frac{515}{527}, \frac{4163}{4260},
 * \frac{4678}{4787}, \frac{93045}{95213}, \frac{97723}{100000} = 0.97723 \\
 * -18.21 &amp; \rightarrow \frac{-19}{1}, \frac{-18}{1}, \frac{-73}{4}, \frac{-91}{5}, \frac{-346}{19},
 * \frac{-1821}{100} = -18.21
 * \end{align}
 *
 * <br>It is worth noticing that \(\text{gcd}(h_i, k_i) = 1\) and if \(\frac{h_i}{k_i}\) is not the last convergent,
 * then \(\frac{h_i}{k_i}\) for \(i \equiv 0 \text{ mod } 2\) is smaller than the original value represented by the
 * continued fraction whereas for \(i \equiv 1 \text{ mod } 2\) it is bigger.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Continued_fraction">Wikipedia - Continued fraction</a>
 */
public abstract class ContinuedFraction {
    /**
     * Returns the coefficients \(a_0, a_1, a_2, a_3, ...\) for this continued fraction.
     * <br>For more information on how the coefficients are calculated, see {@link ContinuedFraction}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @return {@link Iterable} coefficients \(a_0, a_1, a_2, a_3, ...\) for this continued fraction.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Continued_fraction">Wikipedia - Continued fraction</a>
     */
    public abstract Iterable<BigInteger> getCoefficients();

    /**
     * Returns the decimal value represented by this continued fraction. {@link BigDecimal#ROUND_HALF_UP} rounding
     * mode is used.
     *
     * @param decimalDigits how many decimal digits will be return value have
     *
     * @return Decimal value of this continued fraction.
     */
    public abstract BigDecimal getValue( int decimalDigits );

    /**
     * Returns the convergents \(\frac{h_0}{k_0}, \frac{h_1}{k_1}, \frac{h_2}{k_2}, \frac{h_3}{k_3}, ...\) for this
     * continued fraction.
     * <br>For more information on how the convergents are calculated, see {@link ContinuedFraction}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @return {@link Iterable} convergents \(\frac{h_0}{k_0}, \frac{h_1}{k_1}, \frac{h_2}{k_2}, \frac{h_3}{k_3}, ...\)
     * for this continued fraction.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Continued_fraction">Wikipedia - Continued fraction</a>
     */
    public Iterable<BigFraction> getConvergents() {
        return () -> new ConvergentsIterator();
    }

    private class ConvergentsIterator implements Iterator<BigFraction> {
        //Procedure for calculating convergents h_0/k_0, h_1/k_1, h_2/k_2, h_3/k_3, ... for continued fraction
        //with coefficients a_0, a_1, a_2, a_3, ... is the following:
        //h_i = a_i*h_{i - 1} + h_{i - 2} where h_{-1} = 1, h_{-2} = 0
        //k_i = a_i*k_{i - 1} + k_{i - 2} where k_{-1} = 0, k_{-2} = 1

        private BigInteger h_i_2 = BigInteger.ZERO; //h_{i - 2}, initialized with h_{-2} = 0
        private BigInteger h_i_1 = BigInteger.ONE;  //h_{i - 1}, initialized with h_{-1} = 1
        private BigInteger k_i_2 = BigInteger.ONE;  //k_{i - 2}, initialized with k_{-2} = 1
        private BigInteger k_i_1 = BigInteger.ZERO; //k_{i - 1}, initialized with k_{-1} = 0

        //Iterator for coefficients a_0, a_1, a_2, a_3, ...
        private Iterator<BigInteger> coefficientsIterator = getCoefficients().iterator();

        @Override
        public boolean hasNext() {
            return coefficientsIterator.hasNext(); //if there is more coefficients, then there is more convergents
        }

        @Override
        public BigFraction next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            BigInteger a_i = coefficientsIterator.next(); //current coefficient a_i

            BigInteger h_i = a_i.multiply( h_i_1 ).add( h_i_2 ); //h_i = a_i*h_{i - 1} + h_{i - 2}
            BigInteger k_i = a_i.multiply( k_i_1 ).add( k_i_2 ); //k_i = a_i*k_{i - 1} + k_{i - 2}

            //Adjusting variables for the next iteration
            h_i_2 = h_i_1;
            h_i_1 = h_i;
            k_i_2 = k_i_1;
            k_i_1 = k_i;

            return new BigFraction( h_i, k_i ); //current convergent is h_i/k_i
        }
    }
}
