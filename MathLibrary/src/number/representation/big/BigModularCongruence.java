package number.representation.big;

import number.representation.ModularCongruence;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Version of {@link ModularCongruence} that operates on {@link BigInteger} values.
 *
 * @author Andrzej Walkowiak
 */
public class BigModularCongruence implements Iterable<BigInteger> {
    private BigInteger b;
    private BigInteger m;

    /**
     * Creates the modular congruence  - representation of values \(a \geq 0\) such that \(a \equiv b \text{ mod } m\).
     * <br>Given value \(b\), which will be called the "b" because it represents the b of the division
     * \(a \div m\), is stored as adjusted value \(0 \leq b &lt; m\).
     *
     * @param b value of \(b\)
     * @param m value of \(m\)
     *
     * @throws IllegalArgumentException when \(m &lt; 1\)
     * @see <a href="https://en.wikipedia.org/wiki/Modular_arithmetic">Modular arithmetic</a>
     */
    public BigModularCongruence( BigInteger b, BigInteger m ) {
        if( m.compareTo( BigInteger.ONE ) < 0 ) {
            throw new IllegalArgumentException( "Modulo must be a positive number." );
        }

        this.b = b.mod( m ); //adjusting the residue b, so that 0 <= b < m
        this.m = m;
    }

    /**
     * Version of {@link ModularCongruence#getResidue()} that operates on {@link BigInteger} values.
     */
    public BigInteger getResidue() {
        return b;
    }

    /**
     * Version of {@link ModularCongruence#getModulo()} ()} that operates on {@link BigInteger} values.
     */
    public BigInteger getModulo() {
        return m;
    }

    @Override
    public int hashCode() {
        return Objects.hash( b, m );
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) {
            return true;
        }
        if( o == null || getClass() != o.getClass() ) {
            return false;
        }

        BigModularCongruence that = (BigModularCongruence) o;

        if( b != null ? !b.equals( that.b ) : that.b != null ) {
            return false;
        }
        return m != null ? m.equals( that.m ) : that.m == null;
    }

    @Override
    public String toString() {
        return "a = " + b + " mod " + m;
    }

    @Override
    public Iterator<BigInteger> iterator() {
        //Iterator through values a >= 0 such that a = b mod m

        return new Iterator<BigInteger>() {
            private BigInteger nextA = b; //starts with b because b was adjusted to be 0 <= b < m

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public BigInteger next() {
                if( !hasNext() ) {
                    throw new NoSuchElementException();
                }

                BigInteger toReturn = nextA;
                nextA = nextA.add( m );
                return toReturn;
            }
        };
    }
}
