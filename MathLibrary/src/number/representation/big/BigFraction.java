package number.representation.big;

import number.representation.Fraction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

/**
 * Version of {@link Fraction} that operates on {@link BigInteger} values.
 *
 * @author Andrzej Walkowiak
 */
public class BigFraction implements Comparable<BigFraction> {
    /**
     * Constant value for fraction of value 0.
     */
    public static final BigFraction ZERO = new BigFraction( BigInteger.ZERO );

    /**
     * Constant value for fraction of value 1.
     */
    public static final BigFraction ONE = new BigFraction( BigInteger.ONE );

    private BigInteger numerator;
    private BigInteger denominator;

    /**
     * Creates the fraction \(\frac{a}{1}\) with the given numerator \(a\).
     *
     * @param a value of numerator \(a\)
     *
     * @throws IllegalArgumentException when {@link Long#MIN_VALUE} is used as a numerator value
     * @see <a href="https://en.wikipedia.org/wiki/Fraction_(mathematics)">Wikipedia - Fraction</a>
     */
    public BigFraction( BigInteger a ) {
        this( a, BigInteger.ONE );
    }

    /**
     * Creates the fraction \(\frac{a}{b}\) with the given numerator \(a\) and denominator \(b\). If \(b &lt; 0\) it
     * sets \(-a\) as the numerator and \(-b\) as the denominator, so that the denominator is never a negative integer.
     *
     * @param a value of numerator \(a\)
     * @param b value of denominator \(b\)
     *
     * @throws ArithmeticException when \(b = 0\) (zero in denominator)
     * @see <a href="https://en.wikipedia.org/wiki/Fraction_(mathematics)">Wikipedia - Fraction</a>
     */
    public BigFraction( BigInteger a, BigInteger b ) {
        if( b.equals( BigInteger.ZERO ) ) {
            throw new ArithmeticException( "Cannot create fraction with zero in denominator." );
        }

        numerator = a;
        denominator = b;

        if( numerator.signum() < 0 && denominator.signum() < 0 ) {
            numerator = numerator.negate();
            denominator = denominator.negate();
        }
    }

    /**
     * Creates the fraction with the same numerator and denominator as the given fraction.
     *
     * @param fraction {@link BigFraction} whose numerator and denominator are used
     *
     * @see <a href="https://en.wikipedia.org/wiki/Fraction_(mathematics)">Wikipedia - Fraction</a>
     */
    public BigFraction( BigFraction fraction ) {
        this( fraction.numerator, fraction.denominator );
    }

    /**
     * Creates the fraction with the same numerator and denominator as the given fraction.
     *
     * @param fraction {@link Fraction} whose numerator and denominator are used
     *
     * @see <a href="https://en.wikipedia.org/wiki/Fraction_(mathematics)">Wikipedia - Fraction</a>
     */
    public BigFraction( Fraction fraction ) {
        numerator = BigInteger.valueOf( fraction.getNumerator() );
        denominator = BigInteger.valueOf( fraction.getDenominator() );
    }

    /**
     * Version of {@link Fraction#reverse()} that operates on {@link BigInteger} values.
     */
    public BigFraction reverse() {
        return new BigFraction( denominator, numerator );
    }

    /**
     * Version of {@link Fraction#add(Fraction)} that operates on {@link BigInteger} values.
     */
    public BigFraction add( BigFraction fraction ) {
        BigInteger a = numerator;
        BigInteger b = denominator;
        BigInteger c = fraction.numerator;
        BigInteger d = fraction.denominator;

        // a/b + c/d = (ad + cb) / bd
        return new BigFraction( ( a.multiply( d ) ).add( c.multiply( b ) ), b.multiply( d ) );
    }

    /**
     * Version of {@link Fraction#subtract(Fraction)} that operates on {@link BigInteger} values.
     */
    public BigFraction subtract( BigFraction fraction ) {
        BigInteger a = numerator;
        BigInteger b = denominator;
        BigInteger c = fraction.numerator;
        BigInteger d = fraction.denominator;

        // a/b - c/d = (ad - cb) / bd
        return new BigFraction( ( a.multiply( d ) ).subtract( c.multiply( b ) ), b.multiply( d ) );
    }

    /**
     * Version of {@link Fraction#multiply(Fraction)} that operates on {@link BigInteger} values.
     */
    public BigFraction multiply( BigFraction fraction ) {
        BigInteger a = numerator;
        BigInteger b = denominator;
        BigInteger c = fraction.numerator;
        BigInteger d = fraction.denominator;

        // a/b * c/d = ac / bd
        return new BigFraction( a.multiply( c ), b.multiply( d ) );
    }

    /**
     * Version of {@link Fraction#divide(Fraction)} that operates on {@link BigInteger} values.
     */
    public BigFraction divide( BigFraction fraction ) {
        if( fraction.equals( ZERO ) ) {
            throw new ArithmeticException( "Dividing fraction by zero." );
        }

        BigInteger a = numerator;
        BigInteger b = denominator;
        BigInteger c = fraction.numerator;
        BigInteger d = fraction.denominator;

        // a/b : c/d = a/b * d/c = ad / bc
        return new BigFraction( a.multiply( d ), b.multiply( c ) );
    }

    @Override
    public int compareTo( BigFraction other ) {
        BigInteger a = numerator;
        BigInteger b = denominator;
        BigInteger c = other.numerator;
        BigInteger d = other.denominator;

        //b > 0, d > 0 because we assure denominators in fraction objects are positive
        // a/b < c/d
        // a/b - c/d < 0
        // (ad - cb) / bd < 0
        // (ad - cb) < 0
        // ad < cb

        return ( a.multiply( d ) ).compareTo( c.multiply( b ) );
    }

    /**
     * Version of {@link Fraction#pow(int)} that operates on {@link BigInteger} values.
     */
    public BigFraction pow( int e ) {
        if( e == Integer.MIN_VALUE ) {
            //Forbidding Integer.MIN_VALUE because it can`t be negated in the Integer data type range
            throw new IllegalArgumentException( "Power cannot be equal to " + Integer.MIN_VALUE );
        }

        if( e == 0 ) {
            //0^0 is undefined
            if( this.equals( ZERO ) ) {
                throw new ArithmeticException( "Cannot calculate 0^0" );
            }

            //Every number raised to the power of zero is one, so the fraction raised to the power of zero is one too
            return ONE;
        }

        BigInteger a = numerator;
        BigInteger b = denominator;

        if( e < 0 ) {
            // (a/b)^e = (b/a)^(-e)
            //Throws ArithmeticException if this fraction is zero (numerator is zero)
            return new BigFraction( b, a ).pow( -e );
        }

        // (a/b)^e = a^e/b^e
        return new BigFraction( a.pow( e ), b.pow( e ) );
    }

    /**
     * Version of {@link Fraction#reduce()} that operates on {@link BigInteger} values.
     */
    public BigFraction reduce() {
        if( this.equals( ZERO ) ) {
            //Not reducing fractions equal to zero
            return new BigFraction( this );
        }

        BigInteger gcd = numerator.gcd( denominator );
        return new BigFraction( numerator.divide( gcd ), denominator.divide( gcd ) );
    }

    /**
     * Version of {@link Fraction#floor()} that operates on {@link BigInteger} values.
     */
    public BigInteger floor() {
        return new BigDecimal( numerator ).divide( new BigDecimal( denominator ), BigDecimal.ROUND_FLOOR )
                                          .toBigIntegerExact();
    }

    /**
     * Version of {@link Fraction#ceil()} that operates on {@link BigInteger} values.
     */
    public BigInteger ceil() {
        return new BigDecimal( numerator ).divide( new BigDecimal( denominator ), BigDecimal.ROUND_CEILING )
                                          .toBigIntegerExact();
    }

    /**
     * Version of {@link Fraction#isProper()} that operates on {@link BigInteger} values.
     */
    public boolean isProper() {
        return signum() > 0 && numerator.compareTo( denominator ) < 0;
    }

    /**
     * Version of {@link Fraction#signum()} that operates on {@link BigInteger} values.
     */
    public int signum() {
        if( this.equals( ZERO ) ) { //zero in numerator
            return 0;
        }

        if( numerator.compareTo( BigInteger.ZERO ) > 0 ) {
            return denominator.compareTo( BigInteger.ZERO ) > 0 ? 1 : -1;
        }
        else {
            return denominator.compareTo( BigInteger.ZERO ) > 0 ? -1 : 1;
        }
    }

    /**
     * Version of {@link Fraction#isReduced()} that operates on {@link BigInteger} values.
     */
    public boolean isReduced() {
        return this.equals( ZERO ) || numerator.gcd( denominator ).equals( BigInteger.ONE );
    }

    /**
     * Converts this fraction value to {@link BigDecimal} value. {@link BigDecimal#ROUND_HALF_UP} rounding mode is used.
     *
     * @param decimalDigits how many decimal digits will be return value have
     *
     * @return {@link BigDecimal} value represented by this fraction.
     */
    public BigDecimal toBigDecimal( int decimalDigits ) {
        return new BigDecimal( numerator )
                .divide( new BigDecimal( denominator ), decimalDigits, BigDecimal.ROUND_HALF_UP );
    }

    /**
     * Returns the value of this fraction`s numerator.
     *
     * @return numerator value
     */
    public BigInteger getNumerator() {
        return numerator;
    }

    /**
     * Returns the value of this fraction`s denominator. This value is always a positive integer as zero is not
     * allowed in denominator and for negative denominators, the constructor applies \(\frac{a}{b} = \frac{-a}{-b}\)
     * to make sure denominator \(&gt; 0\).
     *
     * @return denominator value
     */
    public BigInteger getDenominator() {
        return denominator;
    }

    @Override
    public int hashCode() {
        return Objects.hash( numerator, denominator );
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) {
            return true;
        }
        if( o == null || getClass() != o.getClass() ) {
            return false;
        }

        //Two fraction are equal if they represent the same value, not when their numerator and denominator are the same
        return compareTo( (BigFraction) o ) == 0;
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }
}
