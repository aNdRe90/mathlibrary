package number.representation;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

import static util.ModuloOperations.mod;

/**
 * If two numbers \(a\) and \(b\) have the property that their difference \(a - b\) is integrally divisible by a
 * number \(m\), then \(a\) and \(b\) are said to be "congruent m \(m\)". In other words, \(b\) is the b
 * from the division \(a \div m\).
 * <br>The statement "\(a\) is congruent to  \(b\) (m \(m\))" is written as \(a \equiv b \text{ mod } m\).
 *
 * <br><br>This class represents all the values of \(a \geq 0\) which are congruent to \(b\) m \(m\), e.g. the
 * example values for the following congruences:
 *
 * \begin{align}
 * 10 \text{ mod } 13 &amp; = 10, 23, 36, 49, ... \\
 * 19 \text{ mod } 15 &amp; = 4, 19, 34, 49, ... \\
 * 0 \text{ mod } 7 &amp; = 0, 7, 14, 21, ... \\
 * -22 \text{ mod } 30 &amp; = 8, 38, 68, 98, ... \\
 * -17 \text{ mod } 5 &amp; = 3, 8, 13, 18, ...
 * \end{align}
 *
 * <p>
 * <br>NOTE: Overflow safe.
 * <p>
 * <br>NOTE 2: Immutable.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Modular_arithmetic">Modular arithmetic</a>
 */
public class ModularCongruence implements Iterable<Long> {
    private long b;
    private long m;

    /**
     * Creates the modular congruence  - representation of values \(a \geq 0\) such that \(a \equiv b \text{ mod } m\).
     * <br>Given value \(b\), which will be called the "b" because it represents the b of the division
     * \(a \div m\), is stored as adjusted value \(0 \leq b &lt; m\).
     *
     * @param b value of \(b\)
     * @param m value of \(m\)
     *
     * @throws IllegalArgumentException when \(m &lt; 1\)
     * @see <a href="https://en.wikipedia.org/wiki/Modular_arithmetic">Modular arithmetic</a>
     */
    public ModularCongruence( long b, long m ) {
        if( m < 1 ) {
            throw new IllegalArgumentException( "Modulo must be a positive number." );
        }

        this.b = mod( b, m ); //adjusting the residue b, so that 0 <= b < m
        this.m = m;
    }

    /**
     * Returns the residue \(b\) from the represented congruence values \(a \equiv b \text{ mod } m\).
     *
     * @return Residue \(b\), such that \(0 \leq b &lt; m\).
     */
    public long getResidue() {
        return b;
    }

    /**
     * Returns the modulo \(m\) from the represented congruence values \(a \equiv b \text{ mod } m\).
     *
     * @return Modulo \(m\).
     */
    public long getModulo() {
        return m;
    }

    @Override
    public int hashCode() {
        return Objects.hash( b, m );
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) {
            return true;
        }
        if( o == null || getClass() != o.getClass() ) {
            return false;
        }

        ModularCongruence that = (ModularCongruence) o;

        if( b != that.b ) {
            return false;
        }
        return m == that.m;
    }

    @Override
    public String toString() {
        return "a = " + b + " mod " + m;
    }

    @Override
    public Iterator<Long> iterator() {
        //Iterator through values a >= 0 such that a = b mod m

        return new Iterator<Long>() {
            private Long nextA = b; //starts with b because b was adjusted to be 0 <= b < m

            @Override
            public boolean hasNext() {
                return nextA != null;
            }

            @Override
            public Long next() {
                if( !hasNext() ) {
                    throw new NoSuchElementException();
                }

                long toReturn = nextA;
                nextA = nextA > Long.MAX_VALUE - m ? null : nextA + m; //if overflow, then nextA = null
                return toReturn;
            }
        };
    }
}
