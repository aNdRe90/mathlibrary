package number.representation;

import algorithm.GreatestCommonDivisorAlgorithm;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

import static java.lang.Math.*;
import static util.BasicOperations.abs;
import static util.BasicOperations.power;

/**
 * Fraction representation. It is a rational value hold in the form of division of two integers written as
 * \(\frac{a}{b}\) or \(a / b\), where \(a\) is an integer called the numerator and \(b\) is a non-zero integer called
 * the denominator.
 * <br>A proper fraction is the one with \(a &gt; 0, b &gt; 0, \frac{a}{b} &lt; 1\), whereas when
 * \(a &gt; 0, b &gt; 0, \frac{a}{b} &gt; 1\), it is called an improper fraction.
 * If \(\text{gcd}(a, b) = 1\), then the fraction is in its reduced form (reduced fraction).
 *
 * <br><br>In this class the denominator is never negative. If the whole fraction represents a negative rational
 * value, then \(a &lt; 0, b &gt; 0\).
 *
 * <p>
 * <br>NOTE: Overflow safe.
 * <p>
 * <br>NOTE 2: Immutable.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Fraction_(mathematics)">Wikipedia - Fraction</a>
 */
public class Fraction implements Comparable<Fraction> {
    /**
     * Constant value for fraction of value 0.
     */
    public static final Fraction ZERO = new Fraction( 0 );

    /**
     * Constant value for fraction of value 1.
     */
    public static final Fraction ONE = new Fraction( 1 );

    private GreatestCommonDivisorAlgorithm gcdAlgorithm = new GreatestCommonDivisorAlgorithm();
    private long numerator;
    private long denominator;

    /**
     * Creates the fraction \(\frac{a}{1}\) with the given numerator \(a\).
     *
     * @param a value of numerator \(a\)
     *
     * @throws IllegalArgumentException when {@link Long#MIN_VALUE} is used as a numerator value
     * @see <a href="https://en.wikipedia.org/wiki/Fraction_(mathematics)">Wikipedia - Fraction</a>
     */
    public Fraction( long a ) {
        this( a, 1 );
    }

    /**
     * Creates the fraction \(\frac{a}{b}\) with the given numerator \(a\) and denominator \(b\). If \(b &lt; 0\) it
     * sets \(-a\) as the numerator and \(-b\) as the denominator, so that the denominator is never a negative integer.
     *
     * @param a value of numerator \(a\)
     * @param b value of denominator \(b\)
     *
     * @throws IllegalArgumentException when {@link Long#MIN_VALUE} is used as a numerator or denominator value
     * @throws ArithmeticException      when \(b = 0\) (zero in denominator)
     * @see <a href="https://en.wikipedia.org/wiki/Fraction_(mathematics)">Wikipedia - Fraction</a>
     */
    public Fraction( long a, long b ) {
        if( b == 0 ) {
            throw new ArithmeticException( "Cannot create fraction with zero in denominator." );
        }
        if( a == Long.MIN_VALUE || b == Long.MIN_VALUE ) {
            throw new IllegalArgumentException( Long.MIN_VALUE + " cannot be a value of numerator or denominator." );
        }

        numerator = a;
        denominator = b;

        if( numerator < 0 && denominator < 0 ) {
            //Overflow safe
            numerator = -numerator;
            denominator = -denominator;
        }
    }

    /**
     * Creates the fraction with the same numerator and denominator as the given fraction.
     *
     * @param fraction fraction whose numerator and denominator are used
     *
     * @see <a href="https://en.wikipedia.org/wiki/Fraction_(mathematics)">Wikipedia - Fraction</a>
     */
    public Fraction( Fraction fraction ) {
        this( fraction.numerator, fraction.denominator );
    }

    /**
     * Returns a new fraction instance that is produced by reversing this fraction. Reversing \(\frac{a}{b}\)
     * results in \(\frac{b}{a}\).
     *
     * <p>
     * <br>NOTE: This fraction instance is not changed.
     *
     * @return Reversed fraction instance.
     *
     * @throws ArithmeticException when \(a = 0\)
     */
    public Fraction reverse() {
        return new Fraction( denominator, numerator );
    }

    /**
     * Returns a new fraction instance that is produced by adding the given fraction to this fraction.
     * \(\frac{a}{b} + \frac{c}{d} = \frac{ad + cb}{bd}\).
     *
     * <p>
     * <br>NOTE: The returned fraction is not in a reduced form. Call {@link #reduce()} to reduce it.
     * This fraction instance is not changed.
     *
     * @param fraction fraction \(\frac{c}{d}\) that is added to this fraction to create the result
     *
     * @return Fraction instance produced by adding the given {@code fraction} to this fraction.
     *
     * @throws ArithmeticException when overflow occurs
     */
    public Fraction add( Fraction fraction ) {
        long a = numerator;
        long b = denominator;
        long c = fraction.numerator;
        long d = fraction.denominator;

        // a/b + c/d = (ad + cb) / bd
        return new Fraction( addExact( multiplyExact( a, d ), multiplyExact( c, b ) ), multiplyExact( b, d ) );
    }

    /**
     * Returns a new fraction instance that is produced by subtracting the given fraction from this fraction.
     * \(\frac{a}{b} - \frac{c}{d} = \frac{ad - cb}{bd}\).
     *
     * <p>
     * <br>NOTE: The returned fraction is not in a reduced form. Call {@link #reduce()} to reduce it.
     * This fraction instance is not changed.
     *
     * @param fraction fraction \(\frac{c}{d}\) that is subtracted from this fraction to create the result
     *
     * @return Fraction instance produced by subtracting the given {@code fraction} from this fraction.
     *
     * @throws ArithmeticException when overflow occurs
     */
    public Fraction subtract( Fraction fraction ) {
        long a = numerator;
        long b = denominator;
        long c = fraction.numerator;
        long d = fraction.denominator;

        // a/b - c/d = (ad - cb) / bd
        return new Fraction( subtractExact( multiplyExact( a, d ), multiplyExact( c, b ) ), multiplyExact( b, d ) );
    }

    /**
     * Returns a new fraction instance that is produced by multiplying the given fraction by this fraction.
     * \(\frac{a}{b} \cdot \frac{c}{d} = \frac{ac}{bd}\).
     *
     * <p>
     * <br>NOTE: The returned fraction is not in a reduced form. Call {@link #reduce()} to reduce it.
     * This fraction instance is not changed.
     *
     * @param fraction fraction \(\frac{c}{d}\) that is multiplied by this fraction to create the result
     *
     * @return Fraction instance produced by multiplying the given {@code fraction} by this fraction.
     *
     * @throws ArithmeticException when overflow occurs
     */
    public Fraction multiply( Fraction fraction ) {
        long a = numerator;
        long b = denominator;
        long c = fraction.numerator;
        long d = fraction.denominator;

        // a/b * c/d = ac / bd
        return new Fraction( multiplyExact( a, c ), multiplyExact( b, d ) );
    }

    /**
     * Returns a new fraction instance that is produced by dividing this fraction by the given fraction.
     * \(\frac{a}{b} \div \frac{c}{d} = \frac{a}{b} \cdot \frac{d}{c} = \frac{ad}{bc}\).
     *
     * <p>
     * <br>NOTE: The returned fraction is not in a reduced form. Call {@link #reduce()} to reduce it.
     * This fraction instance is not changed.
     *
     * @param fraction fraction \(\frac{c}{d}\) that this fraction is divided by to create the result
     *
     * @return Fraction instance produced by dividing this fraction by the given {@code fraction}.
     *
     * @throws ArithmeticException when the given fraction is equal to zero (zero in its numerator) or when
     *                             overflow occurs
     */
    public Fraction divide( Fraction fraction ) {
        if( fraction.equals( ZERO ) ) {
            throw new ArithmeticException( "Dividing fraction by zero." );
        }

        long a = numerator;
        long b = denominator;
        long c = fraction.numerator;
        long d = fraction.denominator;

        // a/b : c/d = a/b * d/c = ad / bc
        return new Fraction( multiplyExact( a, d ), multiplyExact( b, c ) );
    }

    @Override
    public int compareTo( Fraction other ) {
        long a = numerator;
        long b = denominator;
        long c = other.numerator;
        long d = other.denominator;

        //b > 0, d > 0 because we assure denominators in fraction objects are positive
        // a/b < c/d
        // a/b - c/d < 0
        // (ad - cb) / bd < 0
        // (ad - cb) < 0
        // ad < cb

        //Using BigIntegers to avoid overflow exception in fraction comparison
        BigInteger ad = BigInteger.valueOf( a ).multiply( BigInteger.valueOf( d ) );
        BigInteger cb = BigInteger.valueOf( c ).multiply( BigInteger.valueOf( b ) );
        return ad.compareTo( cb );
    }

    /**
     * Returns a new fraction instance that is produced by raising this fraction to the power of the given
     * exponent. \(\left(\frac{a}{b}\right)^e = \frac{a^e}{b^e}\).
     * If \(e &lt; 0\), then \(\left(\frac{a}{b}\right)^e = \left(\frac{b}{a}\right)^{-e}\).
     *
     * <p>
     * <br>NOTE: The returned fraction is not in a reduced form. Call {@link #reduce()} to reduce it.
     * This fraction instance is not changed.
     *
     * @param e exponent \(e\) of the fraction power that is returned
     *
     * @return Fraction instance produced by raising this fraction to the power of the given exponent.
     *
     * @throws IllegalArgumentException when \(e = \) {@link Long#MIN_VALUE}
     * @throws ArithmeticException      when overflow occurs or when this fraction is equal to zero (zero in numerator)
     *                                  and \(e \leq 0\) (\(0^0\) cannot be calculated and zero cannot be in
     *                                  denominator after applying the \(\left(\frac{a}{b}\right)^e =
     *                                  \left(\frac{b}{a}\right)^{-e}\) when\(e &lt; 0\))
     */
    public Fraction pow( int e ) {
        if( e == Integer.MIN_VALUE ) {
            //Forbidding Integer.MIN_VALUE because it can`t be negated in the Integer data type range
            throw new IllegalArgumentException( "Power cannot be equal to " + Integer.MIN_VALUE );
        }

        if( e == 0 ) {
            //0^0 is undefined
            if( this.equals( ZERO ) ) {
                throw new ArithmeticException( "Cannot calculate 0^0" );
            }

            //Every number raised to the power of zero is one, so the fraction raised to the power of zero is one too
            return ONE;
        }

        long a = numerator;
        long b = denominator;

        if( e < 0 ) {
            // (a/b)^e = (b/a)^(-e)
            //Throws ArithmeticException if this fraction is zero (numerator is zero)
            return new Fraction( b, a ).pow( -e );
        }

        // (a/b)^e = a^e/b^e
        return new Fraction( power( a, e ), power( b, e ) );
    }

    /**
     * Returns a new fraction instance that is produced by reducing this fraction. It represents the same rational value
     * and its numerator and denominator are coprime (gcd(numerator, denominator) \(= 1\)).
     * <br>To reduce a non-zero fraction \(\frac{a}{b}\) is to divide both its numerator and denominator by
     * \(\text{gcd}(a, b)\) (see {@link GreatestCommonDivisorAlgorithm} for more details on how to calculate it).
     * <br>Fractions equal to zero (zero in numerator) are not reduced (denominator in returned fraction is the same
     * as in this fraction).
     *
     * <p>
     * <br>NOTE: This fraction instance is not changed.
     *
     * @return Fraction instance produced by reducing this fraction.
     */
    public Fraction reduce() {
        if( this.equals( ZERO ) ) {
            //Not reducing fractions equal to zero
            return new Fraction( this );
        }

        long gcd = gcdAlgorithm.getGreatestCommonDivisor( numerator, denominator );
        return new Fraction( numerator / gcd, denominator / gcd );
    }

    /**
     * Calculates the floor of this fraction - value \(\lfloor \frac{a}{b} \rfloor\).
     * <br>Floor value of \(x\) is defined as the greatest integer less than or equal to \(x\)
     * e.g. \(\lfloor 7 \rfloor = 7, \lfloor 3.6 \rfloor = 3\).
     *
     * @return Floor value of this fraction.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Floor_and_ceiling_functions">Wikipedia - Floor and ceiling
     * functions</a>
     */
    public long floor() {
        //For big values of numerator or denominator Math.floor() is not giving the exact result
        //Thus use BigIntegers in such case
        if( abs( numerator ) > Long.MAX_VALUE / 4 || abs( denominator ) > Long.MAX_VALUE / 4 ) {
            BigDecimal bigNumerator = BigDecimal.valueOf( numerator );
            BigDecimal bigDenominator = BigDecimal.valueOf( denominator );
            return bigNumerator.divide( bigDenominator, BigDecimal.ROUND_FLOOR ).longValueExact();
        }

        return (long) Math.floor( toDouble() );
    }

    /**
     * Converts this fraction value to decimal value.
     *
     * @return Decimal value represented by this fraction.
     */
    public double toDouble() {
        return (double) numerator / (double) denominator;
    }

    /**
     * Calculates the ceiling of this fraction - value \(\lceil \frac{a}{b} \rceil\).
     * <br>Floor value of \(x\) is defined as the least integer greater than or equal to \(x\)
     * e.g. \(\lceil 7 \rceil = 7, \lceil 3.6 \rceil = 4\).
     *
     * @return Ceiling value of this fraction.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Floor_and_ceiling_functions">Wikipedia - Floor and ceiling
     * functions</a>
     */
    public long ceil() {
        //For big values of numerator or denominator Math.ceil() is not giving the exact result
        //Thus use BigIntegers in such case
        if( abs( numerator ) > Long.MAX_VALUE / 4 || abs( denominator ) > Long.MAX_VALUE / 4 ) {
            BigDecimal bigNumerator = BigDecimal.valueOf( numerator );
            BigDecimal bigDenominator = BigDecimal.valueOf( denominator );
            return bigNumerator.divide( bigDenominator, BigDecimal.ROUND_CEILING ).longValueExact();
        }

        return (long) Math.ceil( toDouble() );
    }

    /**
     * Returns true if this fraction is a proper fraction, false otherwise.
     * <br>A proper fraction \(\frac{a}{b}\) is the one with \(a &gt; 0, b &gt; 0, \frac{a}{b} &lt; 1\).
     *
     * @return True if this fraction is a proper fraction, false otherwise.
     */
    public boolean isProper() {
        return signum() > 0 && numerator < denominator;
    }

    /**
     * Returns the sign of this fraction.
     *
     * @return If \(\frac{a}{b} &lt; 0\), then \(-1\) is returned. If \(\frac{a}{b} &gt; 0\), then \(1\) is returned.
     * If fraction is equal to zero, \(0\) is returned.
     */
    public int signum() {
        if( this.equals( ZERO ) ) { //zero in numerator
            return 0;
        }

        if( numerator > 0 ) {
            return denominator > 0 ? 1 : -1;
        }
        else {
            return denominator > 0 ? -1 : 1;
        }
    }

    /**
     * Returns true if this fraction is in its reduced form, false otherwise.
     * <br>A reduced fraction \(\frac{a}{b}\) is the one with \(\text{gcd}(a, b) = 1\) (numerator and denominator are
     * coprime). Additionaly, we assume that fractions with \(a = 0\) are reduced as well.
     *
     * @return True if this fraction is in its reduced form, false otherwise.
     */
    public boolean isReduced() {
        return this.equals( ZERO ) || gcdAlgorithm.getGreatestCommonDivisor( numerator, denominator ) == 1;
    }

    /**
     * Returns the value of this fraction`s numerator.
     *
     * @return numerator value
     */
    public long getNumerator() {
        return numerator;
    }

    /**
     * Returns the value of this fraction`s denominator. This value is always a positive integer as zero is not
     * allowed in denominator and for negative denominators, the constructor applies \(\frac{a}{b} = \frac{-a}{-b}\)
     * to make sure denominator \(&gt; 0\).
     *
     * @return denominator value
     */
    public long getDenominator() {
        return denominator;
    }

    @Override
    public int hashCode() {
        return Objects.hash( numerator, denominator );
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) {
            return true;
        }
        if( o == null ) {
            return false;
        }
        if( getClass() != o.getClass() ) {
            return false;
        }

        //Two fraction are equal if they represent the same value, not when their numerator and denominator are the same
        return compareTo( (Fraction) o ) == 0;
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }
}