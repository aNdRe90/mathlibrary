package number.representation;

import number.properties.checker.PolygonalNumberChecker;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.*;

import static java.lang.Math.sqrt;
import static java.util.Arrays.asList;

/**
 * Periodic continued fraction is p continued fraction that has an infinite number of coefficients that eventually
 * repeat from some point onwards. For more information about continued fractions, see {@link ContinuedFraction}.
 * <br>The minimal number of repeating terms is called the period of the continued fraction.
 * In other words, periodic continued fractions with period of length \(m\) have the coefficients \(a_0, a_1, a_2,
 * a_3, ..., a_k, a_{k + 1}, ..., a_{k + m - 1}, a_k, a_{k + 1}, ..., a_{k + m - 1}, a_k, a_{k + 1}, ...\).
 *
 * <br><br>If the periodic coefficients are \(a_k, a_{k + 1}, ..., a_{k + m - 1}\), then we write the continued
 * fraction as \([a_0; a_1, a_2, a_3, ..., \overline{a_k, a_{k + 1}, ..., a_{k + m - 1}}]\). If \(k = 0\), then the
 * continued fraction is purely periodic e.g. for golden ratio \(\phi = \frac{1 + \sqrt{5}}{2} = [1; 1, 1, 1, 1, ...]\).
 * Otherwise, it is periodic e.g. \(\sqrt{2} = [1; 2, 2, 2, 2, ...]\).
 * <br>In order for the continued fraction to be periodic, it needs to represent p quadratic irrational value.
 *
 * <br><br>Quadratic irrationals, or quadratic surds, are numbers of the form \(\frac{P + \sqrt{D}}{Q}\), where \(P, D,
 * Q\) are integers, \(Q \neq 0\) and \(D &gt; 0\) is not p square number.
 * <br>They are the solutions to the quadratic equation \(ax^2 + bx + q = 0\), for rational \(p, d, q\), where
 * \(P = -d, D = d^2 - 4ac, Q = 2a\).
 *
 * <br><br>When \(P = 0, Q = 1\), the continued fraction expansion of \(\frac{0 + \sqrt{D}}{1} = \sqrt{D}\) contains p
 * repeating period of length \(m\), in which the first \(m - 1\) coefficients form p palindromic string:
 * \(\sqrt{D} = [a_0; \overline{a_1, a_2, a_3, ..., a_3, a_2, a_1, 2a_0}]\).
 *
 * <br><br>We know that \(x = x_0 = a_0 + \frac{1}{x_1}\), where \(x_0\) has coefficients \(a_0, a_1, a_2, a_3, ...\)
 * and \(x_1\) is the 'next value' that has coefficients \(a_1, a_2, a_3, ...\).
 * In order for the fraction to be periodic all the value \(x_i\) must be quadratic irrationals, so
 * \(x_i = \frac{P_i + \sqrt{D_i}}{Q_i}\), and then we have:
 * \begin{align}
 * x_i = \frac{\color{green}{P_i} + \color{magenta}{\sqrt{D_i}}}{\color{blue}{Q_i}}
 * &amp; = a_i + \frac{1}{x_{i + 1}} \\
 * \\
 * \frac{\color{green}{P_i} + \color{magenta}{\sqrt{D_i}}}{\color{blue}{Q_i}}
 * &amp; = a_i + \frac{1}{\frac{P_{i + 1} + \sqrt{D_{i + 1}}}{Q_{i + 1}}}
 * = a_i + \frac{Q_{i + 1}}{P_{i + 1} + \sqrt{D_{i + 1}}}
 * = \frac{a_i(P_{i + 1} + \sqrt{D_{i + 1}}) + Q_{i + 1}}{P_{i + 1} + \sqrt{D_{i + 1}}}
 * = \frac{a_iP_{i + 1} + a_i\sqrt{D_{i + 1}} + Q_{i + 1}}{P_{i + 1} + \sqrt{D_{i + 1}}} \\
 * &amp; = \frac{(P_{i + 1} - \sqrt{D_{i + 1}})(a_iP_{i + 1} + a_i\sqrt{D_{i + 1}} + Q_{i + 1})}
 * {(P_{i + 1} - \sqrt{D_{i + 1}})(P_{i + 1} + \sqrt{D_{i + 1}})}
 * = \frac{a_iP^2_{i + 1} + a_iP_{i + 1}\sqrt{D_{i + 1}} + P_{i + 1}Q_{i + 1} - a_iP_{i + 1}\sqrt{D_{i + 1}}
 * - a_iD_{i + 1} - Q_{i + 1}\sqrt{D_{i + 1}}}{P^2_{i + 1} - D_{i + 1}} \\
 * &amp; = \frac{a_iP^2_{i + 1} + P_{i + 1}Q_{i + 1} - a_iD_{i + 1} - Q_{i + 1}\sqrt{D_{i + 1}}}
 * {P^2_{i + 1} - D_{i + 1}}
 * = \frac{\frac{a_iP^2_{i + 1} + P_{i + 1}Q_{i + 1} - a_iD_{i + 1}}{Q_{i + 1}} - \sqrt{D_{i + 1}}}
 * {\frac{P^2_{i + 1} - D_{i + 1}}{Q_{i + 1}}}
 * = \frac{\color{green}{\frac{a_iD_{i + 1} - a_iP^2_{i + 1} - P_{i + 1}Q_{i + 1}}{Q_{i + 1}}} +
 * \color{magenta}{\sqrt{D_{i + 1}}}}{\color{blue}{\frac{D_{i + 1} - P^2_{i + 1}}{Q_{i + 1}}}}
 * \end{align}
 *
 * <br>We can see that \(D_{i + 1} = D_i = D\). Furthermore, we have:
 * \begin{align}
 * P_i &amp; = \frac{a_iD_{i + 1} - a_iP^2_{i + 1} - P_{i + 1}Q_{i + 1}}{Q_{i + 1}} \\
 * Q_i &amp; = \frac{D - P^2_{i + 1}}{Q_{i + 1}} \Leftrightarrow Q_{i + 1} = \frac{D - P^2_{i + 1}}{Q_i}
 * \\ \\ \\
 * P_i &amp; = \frac{a_iD_{i + 1} - a_iP^2_{i + 1} - P_{i + 1}Q_{i + 1}}{Q_{i + 1}}
 * = \frac{a_i(D_{i + 1} - P^2_{i + 1})}{Q_{i + 1}} - P_{i + 1}
 * = a_i(D_{i + 1} - P^2_{i + 1}) \div Q_{i + 1} - P_{i + 1} \\
 * &amp; = a_i(D_{i + 1} - P^2_{i + 1}) \div \frac{D - P^2_{i + 1}}{Q_i} - P_{i + 1}
 * = a_i(D_{i + 1} - P^2_{i + 1}) \cdot \frac{Q_i}{D - P^2_{i + 1}} - P_{i + 1}
 * = a_iQ_i - P_{i + 1} \\
 * P_i &amp; = a_iQ_i - P_{i + 1} \Leftrightarrow P_{i + 1} = a_iQ_i - P_i
 * \end{align}
 *
 * <br>The only problem is that, in order to calculate \(Q_{i + 1}\), we need to have
 * \(D - P^2_{i + 1} \equiv 0 \text{ mod } Q_i\). We can guarantee that \(D - P^2_0 \equiv 0 \text{ mod } Q_0\), by
 * multiplying \(x\) by the absolute value of its denominator, because \(x = \frac{p + \sqrt{d}}{q}
 * = \frac{|q|p + |q|\sqrt{d}}{|q|q} = \frac{|q|p + \sqrt{dq^2}}{|q|q}\).
 * <br>If \(P_0 = |q|p, Q_0 = |q|q, D = dq^2\), then \(D - P^2_0 = dq^2 - q^2p^2 = q^2(d - p^2)
 * = \pm Q_0(d - p^2) \equiv 0 \text{ mod } Q_0\).
 *
 * <br><br>We will show that, if \(D - P^2_i \equiv 0 \text{ mod } Q_i\), then
 * \(D - P^2_{i + 1} \equiv 0 \text{ mod } Q_{i + 1}\):
 * \begin{align}
 * Q_{i + 1} &amp; = \frac{D - P^2_{i + 1}}{Q_i} = \frac{D - (a_iQ_i - P_i)^2}{Q_i}
 * = \frac{D - p^2_iQ^2_i - P^2_i + 2a_iP_iQ_i}{Q_i} = \frac{D - P^2_i}{Q_i} + 2a_iP_i - p^2_iQ_i
 * \end{align}
 *
 * <br>Since, \(D - P^2_i \equiv 0 \text{ mod } Q_i\), then \(Q_{i + 1} = \frac{D - P^2_i}{Q_i} + 2a_iP_i -
 * p^2_iQ_i\) is an integer. Because \(Q_{i + 1} = \frac{D - P^2_{i + 1}}{Q_i}\) and \(D\) is not p perfect square by
 * definition, \(Q_{i + 1} \neq 0\). We conclude that \(Q_i = \frac{D - P^2_{i + 1}}{Q_{i + 1}}\) and
 * \(D - P^2_{i + 1} \equiv 0 \text{ mod } Q_{i + 1}\).
 *
 * <br><br>In order for the period to be found in the coefficients, we need to keep track of the \(P, Q\) values.
 * <br>If \(P_i = P_j, Q_i = Q_j\), for \(i &gt; 0, 0 \leq j &lt; i\), then \(a_0, a_1, ..., a_{j - 1}\) are the
 * non-periodic coefficients and \(a_j, a_{j + 1}, ..., a_{i - 1}\) are the periodic coefficients. It means that
 * the continued fraction is \([a_0; a_1, ..., a_{j - 1}, \overline{a_j, a_{j + 1}, ..., a_{i - 1}}]\).
 * <br>Obviously, if \(j = 0\), then there are no non-periodic coefficients and the continued fraction is purely
 * periodic.
 *
 * <br><br><br>The final procedure for calculating coefficients \(a_0, a_1, a_2, a_3, ...\) for the continued fraction
 * equal to the quadratic irrational \(x = \frac{p + \sqrt{d}}{q}\), where \(p, d, q\) are integers, \(d\) is p
 * non-square positive integer and \(q \neq 0\) is the following:
 * <br>1) Set \(P_0 = |q|p, Q_0 = |q|q, D = dq^2\).
 * <br>2) Set memory = empty.
 * <br>3) For \(i = 0, 1, 2, 3, ...\):
 * <ul><li>
 * If memory contains \(P_i, Q_i\) as the values for \(j\)-th iteration, then stop with \(a_0, a_1, ..., a_{j - 1}\) as
 * the non-periodic coefficients and \(a_j, a_{j + 1}, ..., a_{i - 1}\) as the periodic coefficients.
 * </li></ul>
 * <ul><li>
 * Put \(P_i, Q_i\) in the memory as the values for the \(i\)-th iteration.
 * </li></ul>
 * <ul><li>
 * Set \(a_i = \lfloor \frac{P_i + \sqrt{D}}{Q_i} \rfloor\).
 * </li></ul>
 * <ul><li>
 * Set \(P_{i + 1} = a_iQ_i - P_i\).
 * </li></ul>
 * <ul><li>
 * Set \(Q_{i + 1}  = \frac{D - P^2_{i + 1}}{Q_i}\).
 * </li></ul>
 *
 * <br><br>Example of calculating the coefficients \(\color{red}{a_0, a_1, a_2, a_3, ...}\) for p continued fraction
 * representing the value \(x = \frac{1 + \sqrt{3}}{4}\) (\(P_i\) in \(\color{green}{\text{green}}\),
 * \(Q_i\) in \(\color{blue}{\text{blue}}\)):
 * \begin{align}
 * p &amp; = 1, q = 4, d = 3 \\
 * P_0 &amp; = |q|p = \color{green}{4}, Q_0 = q|q| = \color{blue}{16}, D = dq^2 = 3 \cdot 16 = 48 \\
 * \\ \\
 * \text{ memory } &amp; = [\{0 \rightarrow 4, 16\}] \\
 * a_0 &amp; = \lfloor \frac{P_0 + \sqrt{D}}{Q_0} \rfloor =
 * \lfloor \frac{\color{green}{4} + \sqrt{48}}{\color{blue}{16}} \rfloor
 * \approx \lfloor 0.683013 \rfloor = \color{red}{0} \\
 * P_1 &amp; = a_0Q_0 - P_0 = \color{red}{0} \cdot \color{blue}{16} - \color{green}{4} = \color{green}{-4} \\
 * Q_1 &amp; = \frac{D - P^2_1}{Q_0} = \frac{48 - \color{green}{(-4)}^2}{\color{blue}{16}}
 * = \frac{48 - 16}{\color{blue}{16}} = \color{blue}{2} \\
 * P_1, Q_1 &amp; = \color{green}{-4}, \color{blue}{2} \text{ is not included in memory } [\{0 \rightarrow 4, 16\}] \\
 * \\
 * \text{ memory } &amp; = [\{0 \rightarrow 4, 16\}, \{1 \rightarrow -4, 2\}] \\
 * a_1 &amp; = \lfloor \frac{P_1 + \sqrt{D}}{Q_1} \rfloor =
 * \lfloor \frac{\color{green}{-4} + \sqrt{48}}{\color{blue}{2}} \rfloor
 * \approx \lfloor 1.464102 \rfloor = \color{red}{1} \\
 * P_2 &amp; = a_1Q_1 - P_1 = \color{red}{1} \cdot \color{blue}{2} - \color{green}{-4} = \color{green}{6} \\
 * Q_2 &amp; = \frac{D - P^2_2}{Q_1} = \frac{48 - \color{green}{6}^2}{\color{blue}{2}}
 * = \frac{48 - 36}{\color{blue}{2}} = \color{blue}{6} \\
 * P_2, Q_2 &amp; = \color{green}{6}, \color{blue}{6} \text{ is not included in memory }
 * [\{0 \rightarrow 4, 16\}, \{1 \rightarrow -4, 2\}] \\
 * \\
 * \text{ memory } &amp; = [\{0 \rightarrow 4, 16\}, \{1 \rightarrow -4, 2\}, \{2 \rightarrow 6, 6\}] \\
 * a_2 &amp; = \lfloor \frac{P_2 + \sqrt{D}}{Q_2} \rfloor =
 * \lfloor \frac{\color{green}{6} + \sqrt{48}}{\color{blue}{6}} \rfloor
 * \approx \lfloor 2.154701 \rfloor = \color{red}{2} \\
 * P_3 &amp; = a_2Q_2 - P_2 = \color{red}{2} \cdot \color{blue}{6} - \color{green}{6} = \color{green}{6} \\
 * Q_3 &amp; = \frac{D - P^2_3}{Q_2} = \frac{48 - \color{green}{6}^2}{\color{blue}{6}}
 * = \frac{48 - 36}{\color{blue}{6}} = \color{blue}{2} \\
 * P_3, Q_3 &amp; = \color{green}{6}, \color{blue}{2} \text{ is not included in memory }
 * [\{0 \rightarrow 4, 16\}, \{1 \rightarrow -4, 2\}, \{2 \rightarrow 6, 6\}] \\
 * \\
 * \text{ memory } &amp; = [\{0 \rightarrow 4, 16\}, \{1 \rightarrow -4, 2\}, \{2 \rightarrow 6, 6\},
 * \{3 \rightarrow 6, 2\}] \\
 * a_3 &amp; = \lfloor \frac{P_3 + \sqrt{D}}{Q_3} \rfloor =
 * \lfloor \frac{\color{green}{6} + \sqrt{48}}{\color{blue}{2}} \rfloor
 * \approx \lfloor 6.464102 \rfloor = \color{red}{6} \\
 * P_4 &amp; = a_3Q_3 - P_3 = \color{red}{6} \cdot \color{blue}{2} - \color{green}{6} = \color{green}{6} \\
 * Q_4 &amp; = \frac{D - P^2_4}{Q_3} = \frac{48 - \color{green}{6}^2}{\color{blue}{2}}
 * = \frac{48 - 36}{\color{blue}{2}} = \color{blue}{6} \\
 * P_4, Q_4 &amp; = \color{green}{6}, \color{blue}{6} \text{ is included in memory }
 * [\{0 \rightarrow 4, 16\}, \{1 \rightarrow -4, 2\}, \color{magenta}{\{2 \rightarrow 6, 6\}},
 * \{3 \rightarrow 6, 2\}] \\
 * \end{align}
 *
 * <br>Since \(P_4 = P_2, Q_4 = Q_2\), then \(\color{red}{a_0, a_1 = 0, 1}\) are the non-periodic coefficients and
 * \(\color{red}{a_2, a_3 = 2, 6}\) are the periodic coefficients. This continued fraction is written as
 * \(x = \frac{1 + \sqrt{3}}{4} = [0; 1, \overline{2, 6}]\).
 *
 * <br><br>Example of calculating the convergents of \(\frac{1 + \sqrt{3}}{4} = [\color{red}{0; 1, \overline{2, 6}}]
 * \approx 0.68301270189221932338\)
 * (\(h_i\) in \(\color{green}{\text{green}}\), \(k_i\) in \(\color{blue}{\text{blue}}\)):
 * \begin{align}
 * \frac{h_{-2}}{k_{-2}} &amp; = \frac{\color{green}{0}}{\color{blue}{1}} \\
 * <p>
 * \frac{h_{-1}}{k_{-1}} &amp; = \frac{\color{green}{1}}{\color{blue}{0}} \\
 * <p>
 * \frac{h_0}{k_0} &amp; = \frac{a_0h_{-1} + h_{-2}}{a_0k_{-1} + k_{-2}} =
 * \frac{\color{red}{0} \cdot \color{green}{1} + \color{green}{0}}
 * {\color{red}{0} \cdot \color{blue}{0} + \color{blue}{1}} =
 * \frac{\color{green}{0}}{\color{blue}{1}} = 0 \\
 * <p>
 * \frac{h_1}{k_1} &amp; = \frac{a_1h_0 + h_{-1}}{a_1k_0 + k_{-1}} =
 * \frac{\color{red}{1} \cdot \color{green}{0} + \color{green}{1}}
 * {\color{red}{1} \cdot \color{blue}{1} + \color{blue}{0}} =
 * \frac{\color{green}{1}}{\color{blue}{1}} = 1 \\
 * <p>
 * \frac{h_2}{k_2} &amp; = \frac{a_2h_1 + h_0}{a_2k_1 + k_0} =
 * \frac{\color{red}{2} \cdot \color{green}{1} + \color{green}{0}}
 * {\color{red}{2} \cdot \color{blue}{1} + \color{blue}{1}} =
 * \frac{\color{green}{2}}{\color{blue}{3}} \approx 0.66666666666666666667 \\
 * <p>
 * \frac{h_3}{k_3} &amp; = \frac{a_3h_2 + h_1}{a_3k_2 + k_1} =
 * \frac{\color{red}{6} \cdot \color{green}{2} + \color{green}{1}}
 * {\color{red}{6} \cdot \color{blue}{3} + \color{blue}{1}} =
 * \frac{\color{green}{13}}{\color{blue}{19}} \approx 0.68421052631578947368 \\
 * <p>
 * \frac{h_4}{k_4} &amp; = \frac{a_4h_3 + h_2}{a_4k_3 + k_2} =
 * \frac{\color{red}{2} \cdot \color{green}{13} + \color{green}{2}}
 * {\color{red}{2} \cdot \color{blue}{19} + \color{blue}{3}} =
 * \frac{\color{green}{28}}{\color{blue}{41}} \approx 0.68292682926829268293 \\
 * <p>
 * \frac{h_5}{k_5} &amp; = \frac{a_5h_4 + h_3}{a_5k_4 + k_3} =
 * \frac{\color{red}{6} \cdot \color{green}{28} + \color{green}{13}}
 * {\color{red}{6} \cdot \color{blue}{41} + \color{blue}{19}} =
 * \frac{\color{green}{181}}{\color{blue}{265}} \approx 0.68301886792452830189 \\\
 * <p>
 * \frac{h_6}{k_6} &amp; = \frac{a_6h_5 + h_4}{a_6k_5 + k_4} =
 * \frac{\color{red}{2} \cdot \color{green}{181} + \color{green}{28}}
 * {\color{red}{2} \cdot \color{blue}{265} + \color{blue}{41}} =
 * \frac{\color{green}{390}}{\color{blue}{571}} \approx 0.68301225919439579685 \\
 * ...
 * \end{align}
 *
 * <br><br>Implementation note: The procedure of finding the period among calculated coefficients \(a_0, a_1, a_2, ...\)
 * is limited to the {@value COEFFICIENTS_LIMIT} coefficients.
 * <br>If the period is not found among those coefficients,
 * then all of them are considered non-periodic and there are no periodic coefficients. This is done in order to
 * prevent too long object creation.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 * <p>
 * <br>NOTE 2: Immutable.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Periodic_continued_fraction">Wikipedia - Periodic continued fraction</a>
 */
public class PeriodicContinuedFraction extends ContinuedFraction {
    /**
     * Maximum number of coefficients that are calculated. It is equal to {@value COEFFICIENTS_LIMIT}.
     * <br>If the period is not found among those coefficients, then all of them are considered non-periodic
     * and there are no periodic coefficients.
     */
    public static final int COEFFICIENTS_LIMIT = 1000000;
    private final PolygonalNumberChecker polygonalNumberChecker = new PolygonalNumberChecker();

    private final BigInteger p;
    private final BigInteger d;
    private final BigInteger q;

    private List<BigInteger> nonPeriodicCoefficients;
    private List<BigInteger> periodicCoefficients;

    /**
     * Creates p periodic continued fraction representing the given quadratic irrational value
     * \(\frac{p + \sqrt{d}}{q}\), where \(p, d, q\) are integers, \(d\) is p positive non-square integer
     * and \(q \neq 0\).
     * <br>For more information about this kind of continued fractions, see {@link PeriodicContinuedFraction}.
     *
     * <br><br>This constructor calculates periodic coefficients, non-periodic coefficients and convergents straight
     * away and stores them.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param p value of \(p\)
     * @param d value of \(d\)
     * @param q value of \(q\)
     *
     * @throws IllegalArgumentException when \(d &lt; 2\), \(d\) is p square number or when \(q = 0\)
     * @see <a href="https://en.wikipedia.org/wiki/Periodic_continued_fraction">Wikipedia - Periodic continued
     * fraction</a>
     */
    public PeriodicContinuedFraction( long p, long d, long q ) {
        if( d < 2 || polygonalNumberChecker.isSquare( d ) ) {
            throw new IllegalArgumentException( "Value d must be p positive non-square number." );
        }
        else if( q == 0 ) {
            throw new IllegalArgumentException( "Value q cannot be equal to zero." );
        }

        this.p = BigInteger.valueOf( p );
        this.d = BigInteger.valueOf( d );
        this.q = BigInteger.valueOf( q );

        calculateCoefficients( this.p, this.d, this.q );
    }

    private void calculateCoefficients( BigInteger p, BigInteger d, BigInteger q ) {
        List<BigInteger> coefficients = new ArrayList<>(); //coefficients a_i

        //Procedure for calculating periodic (a_0, a_1, a_2, a_{j - 1}, ...) and non-periodic coefficients
        //(a_j, a_{j + 1}, a_{j + 2}, ..., a_{i - 1}) of continued fraction representing the value of quadratic
        //irrational (p + sqrt(d))/q, where p, d, q are integers, d is a positive non-square integer and q != 0 is
        //the following:

        //P_0 = |q|p
        //Q_0 = |q|q
        //D = dq^2
        //memory = empty
        //for i = 0, 1, 2, 3, ...
        //  if memory contains P_i, Q_i as the values for j-th iteration, then stop with a_0, a_1, ..., a_{j − 1} as the
        //  non-periodic coefficients and a_j, a_{j + 1}, ..., a{i − 1} as the periodic coefficients
        //
        //  put P_i, Q_i in the memory as the values for the i-th iteration
        //  a_i = floor( (P_i + sqrt(D))/Q_i )
        //  P_{i + 1} = a_iQ_i - P_i
        //  Q_{i + 1}  = (D - P^2_{i + 1})(Q_i)

        BigInteger P_i = q.abs().multiply( p );       //P_i initialized with P_0 = |q|p
        BigInteger Q_i = q.abs().multiply( q );       //Q_i initialized with Q_0 = |q|q
        BigInteger D = d.multiply( q ).multiply( q );   //D = dq^2
        //Now it is guaranteed that D - P_i^2 = 0 mod Q_i which is what we need

        BigDecimal sqrtD = new BigDecimal( q.abs() ).multiply( new BigDecimal( sqrt( d.longValueExact() ) ) )
                                                    .setScale( 20, BigDecimal.ROUND_HALF_UP );

        Map<List<BigInteger>, Integer> memory = new HashMap<>(); //empty memory with key = P_i, Q_i, value = i
        for( int i = 0; ; i++ ) {
            if( i == COEFFICIENTS_LIMIT ) {
                //If the coefficients limit has been reached, then stop and set all found coefficients as the
                //non-periodic ones with periodic coefficients set to null
                nonPeriodicCoefficients = coefficients;
                periodicCoefficients = null;
                break;
            }

            if( memory.keySet().contains( asList( P_i, Q_i ) ) ) {
                //If memory contains P_i, Q_i as the values for j-th iteration,
                //then stop with a_0, a_1, ..., a_{j − 1} as the non-periodic coefficients
                //and a_j, a_{j + 1}, ..., a{i − 1} as the periodic coefficients

                int j = memory.get( asList( P_i, Q_i ) );

                //a_0, a_1, ..., a_{j − 1} as the non-periodic coefficients
                nonPeriodicCoefficients = coefficients.subList( 0, j );

                //a_j, a_{j + 1}, ..., a{i − 1} as the periodic coefficients
                periodicCoefficients = coefficients.subList( j, i );
                break;
            }

            memory.put( asList( P_i, Q_i ), i ); //put P_i, Q_i in the memory as the values for the i-th iteration

            //a_i = floor( (P_i + sqrt(D))/Q_i )
            BigInteger a_i = ( new BigDecimal( P_i ).add( sqrtD ) )
                    .divide( new BigDecimal( Q_i ), 0, RoundingMode.FLOOR ).toBigIntegerExact();
            coefficients.add( a_i );

            BigInteger P_i_1 = ( a_i.multiply( Q_i ) ).subtract( P_i ); //P_{i + 1} = a_iQ_i - P_i
            BigInteger Q_i_1 = ( D.subtract( P_i_1.multiply( P_i_1 ) ) )
                    .divide( Q_i ); //Q_{i + 1}  = (D - P^2_{i + 1}) / Q_i

            //Adjusting the values for the next iteration
            P_i = P_i_1;
            Q_i = Q_i_1;
        }
    }

    /**
     * Returns the non-periodic coefficients of this continued fraction.
     * <br>For more information on how the non-periodic coefficients are calculated, see
     * {@link PeriodicContinuedFraction}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @return {@link List} of non-periodic coefficients of this continued fraction.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Periodic_continued_fraction">Wikipedia - Periodic continued
     * fraction</a>
     */
    public List<BigInteger> getNonPeriodicCoefficients() {
        return nonPeriodicCoefficients;
    }

    /**
     * Returns the periodic coefficients of this continued fraction.
     * <br>For more information on how the periodic coefficients are calculated, see {@link PeriodicContinuedFraction}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @return {@link List} of periodic coefficients of this continued fraction or null if the period was not found
     * among the first {@value COEFFICIENTS_LIMIT} coefficients.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Periodic_continued_fraction">Wikipedia - Periodic continued
     * fraction</a>
     */
    public List<BigInteger> getPeriodicCoefficients() {
        return periodicCoefficients;
    }

    @Override
    public Iterable<BigInteger> getCoefficients() {
        return () -> new CoefficientsIterator();
    }

    @Override
    public BigDecimal getValue( int decimalDigits ) {
        return ( new BigDecimal( p ).add( new BigDecimal( sqrt( d.longValueExact() ) ) ) )
                .divide( new BigDecimal( q ), decimalDigits, RoundingMode.HALF_UP );
    }

    @Override
    public int hashCode() {
        return Objects.hash( p, d, q );
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) {
            return true;
        }
        if( o == null || getClass() != o.getClass() ) {
            return false;
        }

        //Continued fractions are equal when they represent the same value
        PeriodicContinuedFraction that = (PeriodicContinuedFraction) o;
        return Objects.equals( p, that.p ) && Objects.equals( d, that.d ) && Objects.equals( q, that.q );
    }

    /**
     * Creates p string representation of this continued fraction.
     * <br>It has p form of "\((p + sqrt(d))/q = [a_0;a_1,a_2,a_3,...,a_{j - 1},(a_j,a_{j + 1},...,a_{j + m - 1})]\)",
     * where \(j\) is the number of non-periodic coefficients of this continued fraction and \(m\) is the number of
     * periodic coefficients.
     * <br>\(\frac{p + \sqrt{d}}{q}\) is the quadratic irrational value represented by this
     * continued fraction.
     *
     * @return "\((p + sqrt(d))/q = [a_0;a_1,a_2,a_3,...,a_{j - 1},(a_j,a_{j + 1},...,a_{j + m - 1})]\)"
     * e.g. "(23 + sqrt(62))/91 = [0;2,1,18,(29,5,7,5,3,4,2,3,5,3,2,48,1,61,3,19)] = [0;6,1,1,2]", "sqrt(122)/55 =
     * [0;4,(1,47,1,2,3,48,3,2,1,47,1,8)]", "sqrt(6) = [2;(2,4)]", "(1 + sqrt(3))/2 = [(1;2)]".
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder( "" );

        if( !p.equals( BigInteger.ZERO ) ) {
            if( !q.equals( BigInteger.ONE ) ) {
                builder.append( '(' );
            }

            builder.append( p );
            builder.append( " + " );
        }

        builder.append( "sqrt(" );
        builder.append( d );
        builder.append( ')' );

        if( !p.equals( BigInteger.ZERO ) && !q.equals( BigInteger.ONE ) ) {
            builder.append( ')' );
        }

        if( !q.equals( BigInteger.ONE ) ) {
            builder.append( "/" );
            builder.append( q );
        }

        builder.append( " = [" );

        if( nonPeriodicCoefficients.isEmpty() && ( periodicCoefficients != null && !periodicCoefficients.isEmpty() ) ) {
            builder.append( '(' );
            builder.append( periodicCoefficients.get( 0 ) );
            builder.append( ';' );

            for( int i = 1; i < periodicCoefficients.size(); i++ ) {
                builder.append( periodicCoefficients.get( i ) );
                builder.append( ',' );
            }

            builder.deleteCharAt( builder.length() - 1 );
            builder.append( ")]" );
        }
        else if( !nonPeriodicCoefficients.isEmpty() ) {
            builder.append( nonPeriodicCoefficients.get( 0 ) );
            builder.append( ';' );

            for( int i = 1; i < nonPeriodicCoefficients.size(); i++ ) {
                builder.append( nonPeriodicCoefficients.get( i ) );
                builder.append( ',' );
            }

            builder.append( '(' );

            if( periodicCoefficients != null ) {
                for( BigInteger periodicCoefficient : periodicCoefficients ) {
                    builder.append( periodicCoefficient );
                    builder.append( ',' );
                }
            }

            builder.deleteCharAt( builder.length() - 1 );
            builder.append( ")]" );
        }
        //else { there is no possibility to have no nonperiodic coefficients and no periodic coefficients }

        return builder.toString();
    }



    private class CoefficientsIterator implements Iterator<BigInteger> {
        //We have a_0, a_1, ..., a_{j − 1} as the non-periodic coefficients
        //and a_j, a_{j + 1}, ..., a{i − 1} as the periodic coefficients
        //It means that we first return the non-periodic coefficients and then we cycle through the periodic ones

        private int n = 0; //keeps track of how many coefficients were returned so far

        @Override
        public boolean hasNext() {
            return periodicCoefficients == null ? n < nonPeriodicCoefficients.size() : true;
        }

        @Override
        public BigInteger next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            if( n < nonPeriodicCoefficients.size() ) {
                //The non-periodic coefficients part
                return nonPeriodicCoefficients.get( n++ );
            }
            else {
                //The periodic coefficients part - cycle through the periodicCoefficients
                int index = ( n++ - nonPeriodicCoefficients.size() ) % periodicCoefficients.size();
                return periodicCoefficients.get( index );
            }
        }
    }
}