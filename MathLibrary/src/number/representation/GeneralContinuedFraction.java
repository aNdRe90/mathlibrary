package number.representation;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.*;

/**
 * A continued fraction representation of any value \(x\). For more information about continued fractions see
 * {@link ContinuedFraction}.
 *
 * <br><br>Example of calculating the coefficients \(\color{red}{a_0, a_1, a_2, a_3, ...}\) for a continued fraction
 * representing the value \(x = x_0 = 0.123\):
 * \begin{align}
 * a_0 &amp; = \lfloor x_0 \rfloor = \lfloor 0.123 \rfloor = \color{red}{0} \\
 * t &amp; = x_0 - a_0 = 0.123 - \color{red}{0} = 0.123 \neq 0 \\
 * x_1 &amp; = \frac{1}{t} = \frac{1}{0.123} \approx 8.130081 \\
 * \\
 * a_1 &amp; = \lfloor x_1 \rfloor = \lfloor 8.130081 \rfloor = \color{red}{8} \\
 * t &amp; = x_1 - a_1 = 8.130081 - \color{red}{8} = 0.130081 \neq 0 \\
 * x_2 &amp; = \frac{1}{t} = \frac{1}{0.130081} \approx 7.687518 \\
 * \\
 * a_2 &amp; = \lfloor x_2 \rfloor = \lfloor 7.687518 \rfloor = \color{red}{7} \\
 * t &amp; = x_2 - a_2 = 7.687518 - \color{red}{7} = 0.687518 \neq 0 \\
 * x_3 &amp; = \frac{1}{t} = \frac{1}{0.687518} \approx 1.454507 \\
 * \\
 * a_3 &amp; = \lfloor x_3 \rfloor = \lfloor 1.454507 \rfloor = \color{red}{1} \\
 * t &amp; = x_3 - a_3 = 1.454507 - \color{red}{1} = 0.454507 \neq 0 \\
 * x_4 &amp; = \frac{1}{t} = \frac{1}{0.454507} \approx 2.200186 \\
 * \\
 * a_4 &amp; = \lfloor x_4 \rfloor = \lfloor 2.200186 \rfloor = \color{red}{2} \\
 * t &amp; = x_4 - a_4 = 2.200186 - \color{red}{2} = 0.200186 \neq 0 \\
 * x_5 &amp; = \frac{1}{t} = \frac{1}{0.200186} \approx 4.995354 \\
 * \\
 * \color{purple}{a_5} &amp; \color{purple}{= \lfloor x_5 \rfloor = \lfloor 4.995354 \rfloor = 4} \\
 * ... &amp;
 * \end{align}
 *
 * <br>In the example above, calculated \(\color{purple}{a_5 = 4}\) is incorrect because, in fact,
 * \(\frac{123}{1000} = [\color{red}{0; 8, 7, 1, 2, 5}]\). We got it wrong because we used a scale of 6 during the
 * \(\frac{1}{x}\) calculation.
 * <br>There is a bigger scale used by this class, both for doing the inverse and comparing \(x\) to zero
 * (read the note below), but it is not guaranteed to get the exact coefficients sometimes.
 * <br>For the case of rational values use {@link RationalContinuedFraction} and for the case of values of form
 * \(\frac{a + \sqrt{b}}{c}\) (\(a, b, c\) are integers and \(c \neq 0\)), use {@link PeriodicContinuedFraction}.
 *
 * <br><br>Implementation note: The value \(x\) is represented by the {@link BigDecimal} and it uses:
 * <ul><li>Scale equal to {@value #INVERSE_SCALE}, rounding mode {@link RoundingMode#HALF_UP}
 * for calculating \(\frac{1}{x}\). It means that the result of \(\frac{1}{x}\) has {@value #INVERSE_SCALE} decimal
 * digits.
 * </li></ul>
 * <ul><li>Scale equal to {@value #EQUAL_TO_ZERO_COMPARISON_SCALE}, rounding mode {@link RoundingMode#HALF_UP}
 * for checking if \(x = 0\). It means that \(x\) is considered to be zero if it is equal to zero when rounded to
 * {@value #EQUAL_TO_ZERO_COMPARISON_SCALE} decimal digits.
 * </li></ul>
 *
 * <br>Example of calculating the convergents of \(0.123 = [\color{red}{0; 8, 7, 1, 2, 5}]\)
 * (\(h_i\) in \(\color{green}{\text{green}}\), \(k_i\) in \(\color{blue}{\text{blue}}\)):
 * \begin{align}
 * \frac{h_{-2}}{k_{-2}} &amp; = \frac{\color{green}{0}}{\color{blue}{1}} \\
 *
 * \frac{h_{-1}}{k_{-1}} &amp; = \frac{\color{green}{1}}{\color{blue}{0}} \\
 *
 * \frac{h_0}{k_0} &amp; = \frac{a_0h_{-1} + h_{-2}}{a_0k_{-1} + k_{-2}} =
 * \frac{\color{red}{0} \cdot \color{green}{1} + \color{green}{0}}
 * {\color{red}{0} \cdot \color{blue}{0} + \color{blue}{1}} =
 * \frac{\color{green}{0}}{\color{blue}{1}} = 0 \\
 *
 * \frac{h_1}{k_1} &amp; = \frac{a_1h_0 + h_{-1}}{a_1k_0 + k_{-1}} =
 * \frac{\color{red}{8} \cdot \color{green}{0} + \color{green}{1}}
 * {\color{red}{8} \cdot \color{blue}{1} + \color{blue}{0}} =
 * \frac{\color{green}{1}}{\color{blue}{8}} = 0.125 \\
 *
 * \frac{h_2}{k_2} &amp; = \frac{a_2h_1 + h_0}{a_2k_1 + k_0} =
 * \frac{\color{red}{7} \cdot \color{green}{1} + \color{green}{0}}
 * {\color{red}{7} \cdot \color{blue}{8} + \color{blue}{1}} =
 * \frac{\color{green}{7}}{\color{blue}{57}} \approx 0.12280701754385964912 \\
 *
 * \frac{h_3}{k_3} &amp; = \frac{a_3h_2 + h_1}{a_3k_2 + k_1} =
 * \frac{\color{red}{1} \cdot \color{green}{7} + \color{green}{1}}
 * {\color{red}{1} \cdot \color{blue}{57} + \color{blue}{8}} =
 * \frac{\color{green}{8}}{\color{blue}{65}} \approx 0.12307692307692307692 \\
 *
 * \frac{h_4}{k_4} &amp; = \frac{a_4h_3 + h_2}{a_4k_3 + k_2} =
 * \frac{\color{red}{2} \cdot \color{green}{8} + \color{green}{7}}
 * {\color{red}{2} \cdot \color{blue}{65} + \color{blue}{57}} =
 * \frac{\color{green}{23}}{\color{blue}{187}} \approx 0.12299465240641711230 \\
 *
 * \frac{h_5}{k_5} &amp; = \frac{a_5h_4 + h_3}{a_5k_4 + k_3} =
 * \frac{\color{red}{5} \cdot \color{green}{23} + \color{green}{8}}
 * {\color{red}{5} \cdot \color{blue}{187} + \color{blue}{65}} =
 * \frac{\color{green}{123}}{\color{blue}{1000}} = 0.123
 * \end{align}
 *
 * <p>
 * <br>NOTE: Overflow safe.
 * <p>
 * <br>NOTE 2: Immutable.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Continued_fraction">Wikipedia - Continued fraction</a>
 */
public class GeneralContinuedFraction extends ContinuedFraction {
    /**
     * Number of coefficients calculated at the start (in the constructor) and used for
     * {@link GeneralContinuedFraction#toString()}. It is equal to {@value #NUMBER_OF_STARTING_COEFFICIENTS}.
     */
    public static final int NUMBER_OF_STARTING_COEFFICIENTS = 6;

    /**
     * Scale for comparing decimal value \(x\) to zero. It is equal to {@value #EQUAL_TO_ZERO_COMPARISON_SCALE}.
     * <br>If \(x\) rounded to {@value #EQUAL_TO_ZERO_COMPARISON_SCALE} decimal digits using the
     * {@link RoundingMode#HALF_UP} rounding mode is equal to zero then it is considered a zero.
     *
     * <br><br>e.g. When the scale is \(5\), then:
     * <br>\(x = 0.00001 \neq 0\)
     * <br>\(x = 0.000005 \approx 0.00001 \neq 0\)
     * <br>\(x = 0.000001 \approx 0.00000 = 0\)
     * <br>\(x = 0.0000049 \approx 0.00000 = 0\)
     */
    public static final int EQUAL_TO_ZERO_COMPARISON_SCALE = 20;

    /**
     * Scale for calculating the inverse \(\frac{1}{x}\). It is equal to {@value #INVERSE_SCALE}.
     * <br>The division uses the {@link RoundingMode#HALF_UP} rounding mode.
     *
     * <br><br>e.g. When the scale is \(3\), then:
     * <br>\(\frac{1}{x} = \frac{1}{0.00012345} = 8100.446\)
     * <br>\(\frac{1}{x} = \frac{1}{0.3333} = 3.000\)
     */
    public static final int INVERSE_SCALE = 100;

    private BigDecimal x; //value represented by this continued fraction

    //First coefficients stored in the constructor for the purpose of toString() method
    private List<BigInteger> startingCoefficients;

    //Holds the info whether or not there are more coefficients than NUMBER_OF_STARTING_COEFFICIENTS so that the
    //toString() method does not calculate the first coefficients every time to know that
    //Initialize with true because it is set to false during the starting coefficients calculations if needed
    private boolean hasMoreThanStartingCoefficients = true;

    /**
     * Creates a continued fraction representing the given value.
     * <br>For more information about this kind of continued fractions, see {@link GeneralContinuedFraction}.
     *
     * <br><br>This constructor calculates the first {@value #NUMBER_OF_STARTING_COEFFICIENTS} coefficients straight
     * away and stores them, unless it is an exact representation or an exact one in terms of decimal rounding used -
     * then it stores less than {@value #NUMBER_OF_STARTING_COEFFICIENTS} and no more coefficients can be calculated.
     *
     * <br><br>Examples:
     * <br>\(0.5 = [0; 2]\) - exact representation
     * <br>\(3.87654 = [3; 1, 7, 10, 47, 2, ...]\) - has more coefficients
     * <br>\(0.19 = [0; 5, 3, 1, 4]\) - is exact representation in terms of decimal rounding, because
     * \(0.19 = 0 + 1 / (5 + 1 / (3 + 1 / (1 + 1 / (4 + 1 / (1.28 \cdot 10^{-98} )))))\). The code assumes that
     * \(1.28 \cdot 10^{-98} = 0\) and stops the calculation of more coefficients.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param x value represented by the continued fraction
     *
     * @see <a href="https://en.wikipedia.org/wiki/Continued_fraction">Wikipedia - Continued fraction</a>
     */
    public GeneralContinuedFraction( double x ) {
        this.x = BigDecimal.valueOf( x );
        startingCoefficients = getStartingCoefficients( this.x );
    }

    private List<BigInteger> getStartingCoefficients( BigDecimal x_i ) {
        //Procedure for calculating coefficients a_0, a_1, a_2, a_3, ... for continued fraction representing the
        //value x = x_0 is the following:
        //for i = 0, 1, 2, 3, ...
        //  a_i = floor(x_i)
        //  t = x_i − a_i
        //  if t = 0
        //      stop
        //  else
        //      x_{i + 1} = 1 / t

        List<BigInteger> startingCoefficients = new ArrayList<>( NUMBER_OF_STARTING_COEFFICIENTS );

        for( int i = 0; i < NUMBER_OF_STARTING_COEFFICIENTS; i++ ) {
            BigInteger a_i = floor( x_i ); //a_i = floor(x_i)
            startingCoefficients.add( a_i );

            BigDecimal t = x_i.subtract( new BigDecimal( a_i ) ); //t = x_i − a_i

            if( isEqualToZero( t ) ) { //if t = 0, then stop
                hasMoreThanStartingCoefficients = false;
                break;
            }

            BigDecimal x_i_1 = inverse( t ); //else, x_{i + 1} = 1 / t
            x_i = x_i_1; //adjusting the value for the next iteration
        }

        return startingCoefficients;
    }

    private BigInteger floor( BigDecimal value ) {
        return value.setScale( 0, RoundingMode.FLOOR ).toBigInteger();
    }

    private boolean isEqualToZero( BigDecimal value ) {
        return value.setScale( EQUAL_TO_ZERO_COMPARISON_SCALE, RoundingMode.HALF_UP ).compareTo( BigDecimal.ZERO ) == 0;
    }

    private BigDecimal inverse( BigDecimal x ) {
        return BigDecimal.ONE.divide( x, INVERSE_SCALE, RoundingMode.HALF_UP );
    }

    /**
     * Creates a continued fraction representing the given {@link BigDecimal} value.
     * <br>For more information see {@link GeneralContinuedFraction#GeneralContinuedFraction(double)}.
     *
     * @param x {@link BigDecimal} value represented by the continued fraction
     */
    public GeneralContinuedFraction( BigDecimal x ) {
        this.x = x;
        startingCoefficients = getStartingCoefficients( this.x );
    }

    @Override
    public Iterable<BigInteger> getCoefficients() {
        return () -> new CoefficientsIterator();
    }

    @Override
    public BigDecimal getValue( int decimalDigits ) {
        return x.setScale( decimalDigits, RoundingMode.HALF_UP );
    }

    @Override
    public int hashCode() {
        return Objects.hash( x );
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) {
            return true;
        }
        if( o == null || getClass() != o.getClass() ) {
            return false;
        }

        //Continued fractions are equal when they represent the same value
        return this.x.compareTo( ( (GeneralContinuedFraction) o ).x ) == 0;
    }

    /**
     * Creates a string representation of this continued fraction.
     * It has a form of "value = [\(a_0;a_1,a_2,a_3,...]\)".
     *
     * @return <ul><li>
     * "value = [\(a_0\)]" if value is an integer e.g. "5 = [5]"
     * </li></ul>
     * <ul><li>
     * "value = [\(a_0;a_1,...,a_{n - 1}\)]" if there are \(n &lt; \) {@value NUMBER_OF_STARTING_COEFFICIENTS}
     * coefficients e.g. "3.75 = [3;1,3]"
     * </li></ul>
     * <ul><li>
     * "value = [\(a_0;a_1,...,a_{j - 1},...\)]" if there are \(n &gt; j = \)
     * {@value NUMBER_OF_STARTING_COEFFICIENTS} coefficients e.g. "3.87654 = [3;1,7,10,47,2,...]"
     * </li></ul>
     */
    @Override
    public String toString() {
        if( startingCoefficients.size() == 1 ) {
            return x + " = [" + startingCoefficients.get( 0 ) + "]";
        }

        StringBuilder builder = new StringBuilder( x + " = [" );
        builder.append( startingCoefficients.get( 0 ) );
        builder.append( ';' );

        for( int i = 1; i < startingCoefficients.size(); i++ ) {
            builder.append( startingCoefficients.get( i ) );
            builder.append( ',' );
        }

        if( hasMoreThanStartingCoefficients ) {
            builder.append( "...]" );
        }
        else {
            builder.deleteCharAt( builder.length() - 1 ); //removing the comma at the end, because it is not needed
            //when we have a finite number of coefficients less than the NUMBER_OF_STARTING_COEFFICIENTS to display

            builder.append( ']' );
        }

        return builder.toString();
    }



    private class CoefficientsIterator implements Iterator<BigInteger> {
        //Procedure for calculating coefficients a_0, a_1, a_2, a_3, ... for continued fraction representing the
        //value x = x_0 is the following:
        //for i = 0, 1, 2, 3, ...
        //  a_i = floor(x_i)
        //  t = x_i − a_i
        //  if t = 0
        //      stop
        //  else
        //      x_{i + 1} = 1 / t

        private BigDecimal x_i = x; //current value x_i, initialized with x_0 = x
        private BigInteger a_i = floor( x_i ); //current value a_i, initialized with a_0 = floor(x_0)

        @Override
        public boolean hasNext() {
            return a_i != null;
        } //next coefficient exists if a_i != null

        @Override
        public BigInteger next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            BigInteger coefficientToReturn = a_i;

            BigDecimal t = x_i.subtract( new BigDecimal( a_i ) ); //t = x_i − a_i

            //Coefficient a_{i + 1} exists only if t != 0
            if( isEqualToZero( t ) ) {
                BigInteger a_i_1 = null; //if t = 0, then coefficient a_{i + 1} does not exist

                //Adjusting values for the next iteration
                x_i = null;
                a_i = a_i_1;
            }
            else {
                BigDecimal x_i_1 = inverse( t ); //x_{i + 1} = 1 / t
                BigInteger a_i_1 = floor( x_i_1 ); //a_{i + 1} = floor(x_{i + 1}) is the next coefficient

                //Adjusting values for the next iteration
                x_i = x_i_1;
                a_i = a_i_1;
            }

            return coefficientToReturn;
        }
    }
}
