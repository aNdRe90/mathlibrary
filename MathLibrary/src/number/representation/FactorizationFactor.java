package number.representation;

import java.util.Objects;

/**
 * Every integer greater than \(1\) either is a prime number or can be represented as the product of prime numbers:
 * <br>\(n = p_0^{e_0} \cdot p_1^{e_1} \cdot p_2^{e_2} \cdot ... \cdot p_{k - 1}^{e_{k - 1}}\), where \(p_i &gt;
 * p_{i - 1}, e_i &gt; 0\).
 *
 * <br><br>This class represents the factorization factor \(p_i^{e_i}\) containing the value of the prime \(p_i\) and
 * the exponent \(e_i\).
 *
 * <p>
 * <br>NOTE: Overflow safe.
 * <p>
 * <br>NOTE 2: Immutable.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Factorization">Wikipedia - Factorization</a>
 */
public class FactorizationFactor {
    private long prime;
    private int exponent;

    /**
     * Creates the factorization factor \(p^e\) with the given prime \(p\) and the exponent \(e\).
     *
     * <p>
     * <br>NOTE: Value of \(p\) is not checked here whether it is actually a prime or not. The same goes with \(e\)
     * which is not checked whether it is a positive number.
     *
     * @param p value of \(p\)
     * @param e value of \(e\)
     *
     * @see <a href="https://en.wikipedia.org/wiki/Factorization">Wikipedia - Factorization</a>
     */
    public FactorizationFactor( long p, int e ) {
        prime = p;
        exponent = e;
    }

    /**
     * Returns the prime value.
     *
     * @return Prime value \(p\) from factorization factor \(p^e\).
     */
    public long getPrime() {
        return prime;
    }

    /**
     * Returns the exponent value.
     *
     * @return Exponent value \(e\) from factorization factor \(p^e\).
     */
    public int getExponent() {
        return exponent;
    }

    @Override
    public int hashCode() {
        return Objects.hash( prime, exponent );
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) {
            return true;
        }
        if( o == null || getClass() != o.getClass() ) {
            return false;
        }

        FactorizationFactor that = (FactorizationFactor) o;

        if( prime != that.prime ) {
            return false;
        }
        return exponent == that.exponent;
    }

    @Override
    public String toString() {
        return prime + "^" + exponent;
    }
}
