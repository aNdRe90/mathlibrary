package number.converter;

import java.util.*;

import static java.lang.Math.multiplyExact;
import static java.util.Arrays.asList;

/**
 * Roman numbers are represented by 7 different letters: I, V, X, L, C, D and M. They represent the numbers 1, 5, 10,
 * 50, 100, 500 and 1000 accordingly.
 *
 * <br><br>The original pattern for Roman numerals used the letters I, V, and X to write the following:
 * <br>1 = I, 2 = II, 3 = III, 4 = IIII, 5 = V, 6 = VI, 7 = VII, 8 = VIII, 9 = VIIII, 10 = X
 * <br>The numerals for 4 (IIII) and 9 (VIIII) proved problematic as they were easily confused with III and VIII.
 *
 * <br><br>To resolve this problem, subtractive notation was introduced. It allows to write 4 as IV (5 - 1) and 9 as
 * IX (10 - 1). However, the previous representations are still valid. They are just not the shortest ones.
 * <br>Only one I, X, and C can be used as the leading numeral in part of a subtractive pair.
 * <ul><li>
 * I can only be placed before V and X
 * </li></ul>
 * <ul><li>
 * X can only be placed before L and C
 * </li></ul>
 * <ul><li>
 * C can only be placed before D and M
 * </li></ul>
 *
 * <br><br>All the valid roman number denominations are:
 * <br>I = 1, IV = 4, V = 5, IX = 9, X = 10, XL = 40, L = 50, XC = 90, C = 100, CD = 400, D = 500, CM = 900, M = 1000
 *
 * <br><br>So, the values 1- 10 are:
 * <br>1 = I, 2 = II, 3 = III, 4 = IV, 5 = V, 6 = VI, 7 = VII, 8 = VIII, 9 = IX, 10 = X
 *
 * <br><br>The same pattern repeats for tens and hundreds:
 * <br>10 = X, 20 = XX, 30 = XXX, 40 = XL, 50 = L, 60 = LX, 70 = LXX, 80 = LXXX, 90 = XC
 * <br>100 = C, 200 = CC, 300 = CCC, 400 = CD, 500 = D, 600 = DC, 700 = DCC, 800 = DCCC, 900 = CM
 *
 * <br><br>Thousands are written using only letter M:
 * <br>1000 = M, 2000 = MM, 3000 = MMM, 4000 = MMMM etc.
 *
 * <br><br>In order to represent a positive integer value as a roman number (there is no representation for negative
 * values or zero), we need to split it into thousands, hundreds, tens and ones:
 * <br>1284 = 1000 + 200 + 80 + 4 = M + CC + LXXX + IV = MCCLXXXIV
 *
 * <br><br>Other examples are:
 * <br>72 = LXXII
 * <br>101 = CI
 * <br>945 = CMXLV
 * <br>3057 = MMMLVII
 *
 * <br><br>The roman numbers presented above are the numbers in minimal form. They contain the least number of
 * letters to represent a value and they use subtractive notation to achieve that.
 * <br>However, minimal form was not a rule and there still remain in Rome many examples where economy of numerals has
 * not been employed. For example, in the famous Colosseum the number above the 49th entrance is written as XXXXVIIII
 * rather than XLIX.
 *
 * <br><br>There are certain rules for the roman number to be valid: *
 * <ul><li>
 * <b>Rule 1:</b> Numerals must be arranged in descending order of size.
 * <br>Examples of numbers violating this rule:
 * <ul><li>IL - value 50 follows the value 1</li></ul>
 * <ul><li>DM - value 1000 follows the value 500</li></ul>
 * <ul><li>IVX - value 10 follows the value 4</li></ul>
 * <ul><li>IXX - value 10 follows the value 9</li></ul>
 * </li></ul>
 *
 * <ul><li>
 * <b>Rule 2:</b> M, C, and X cannot be equalled or exceeded by smaller denominations.
 * <br>Examples of numbers violating this rule:
 * <ul><li>CCCCCCCCCC - value 1000 represented by 10x100</li></ul>
 * <ul><li>XXXXXXXXXX - value 100 represented by 10x10</li></ul>
 * <ul><li>LXXXXX - value 100 represented by 50 + 5x10</li></ul>
 * <ul><li>IXIX - value 18 represented by 9 + 9</li></ul>
 * <ul><li>MLIIIIIIIIIIIIIIII - value 1066 represented by 1000 + 50 + 16x1</li></ul>
 * </li></ul>
 *
 * <ul><li>
 * <b>Rule 3:</b> D, L, and V can each only appear once.
 * <br>Examples of numbers violating this rule (notice that they do not violate <b>Rule 2</b>):
 * <ul><li>XLXL</li></ul>
 * <ul><li>MDCD</li></ul>
 * <ul><li>VVVI</li></ul>
 * </li></ul>
 *
 * <br><br>Example of valid roman numbers that are not in minimal form:
 * <br>4 = IIII (minimal form = IV)
 * <br>9 = VIIII (minimal form = IX)
 * <br>41 = XXXXI (minimal form = XLI)
 * <br>75 = XXXXXXXV (minimal form = LXXV)
 * <br>144 = CXXXXIIII (minimal form = CXLIV)
 * <br>412 = CCCCXII (minimal form = CDXII)
 * <br>1414 = MCCCCXIV (minimal form = MCDXIV)
 * <br>2639 = MMDCXXXVIIII (minimal form = MMDCXXXIX)
 *
 * <br><br>This class does not allow to convert values greater than {@value MAX_ALLOWED_VALUE} to a roman number.
 * <br>The same thing applies to parsing values - cannot parse roman numbers representing a value greater than
 * {@value MAX_ALLOWED_VALUE}.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Roman_numerals">Wikipedia - Roman numerals</a>
 * @see <a href="https://projecteuler.net/about=roman_numerals">Project Euler - How to write roman numerals</a>
 */
public class RomanNumberConverter {
    /**
     * Maximum allowed integer value. It is equal to {@value MAX_ALLOWED_VALUE}.
     */
    public static final int MAX_ALLOWED_VALUE = 1000000;

    private static final String[] HUNDREDS_SYMBOLS = new String[]{ "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC",
            "CM" };
    private static final String[] TENS_SYMBOLS = new String[]{ "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX",
            "XC" };
    private static final String[] ONES_SYMBOLS = new String[]{ "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII",
            "IX" };

    private static final Map<String, Integer> VALUES_PER_ORDERED_ROMAN_DENOMINATIONS = new LinkedHashMap<>();
    private static final Set<Character> ALLOWED_CHARACTERS = new HashSet<>();

    static {
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "M", 1000 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "CM", 900 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "D", 500 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "CD", 400 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "C", 100 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "XC", 90 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "L", 50 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "XL", 40 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "X", 10 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "IX", 9 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "V", 5 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "IV", 4 );
        VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.put( "I", 1 );

        for( String romanDenomination : VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.keySet() ) {
            for( int i = 0; i < romanDenomination.length(); i++ ) {
                ALLOWED_CHARACTERS.add( romanDenomination.charAt( i ) );
            }
        }
    }

    /**
     * Parses the integer value from the given roman number. It does not need to be in the minimal form.
     * <br>For more information about the roman numbers, see {@link RomanNumberConverter}.
     *
     * <br><br>NOTE: This method does not remove whitespaces from the given input and treats them as invalid characters.
     *
     * @param romanNumber roman number that will be parsed (does not need to be in the minimal form)
     *
     * @return Integer value represented by the given {@code romanNumber}.
     *
     * @throws IllegalArgumentException when {@code romanNumber} is null, empty, contains unallowed characters
     *                                  (whitespaces included) or when it is an invalid roman number (violates one of
     *                                  the rules described in {@link RomanNumberConverter})
     * @see <a href="https://en.wikipedia.org/wiki/Roman_numerals">Wikipedia - Roman numerals</a>
     * @see <a href="https://projecteuler.net/about=roman_numerals">Project Euler - How to write roman numerals</a>
     */
    public int parseValue( String romanNumber ) {
        if( romanNumber == null || romanNumber.isEmpty() ) {
            throw new IllegalArgumentException( romanNumber + " cannot be null or empty." );
        }
        if( !containsOnlyAllowedCharacters( romanNumber ) ) {
            throw new IllegalArgumentException(
                    romanNumber + " is not a correct roman number - contains incorrect characters (only I, V, X, L, " +
                    "C, D, M are allowed)." );
        }

        //Rule 1: Numerals must be arranged in descending order of size.
        if( !followsRule1( romanNumber ) ) {
            throw new IllegalArgumentException( romanNumber + " is not a correct roman number - violates rule 1." );
        }
        //Rule 3: D, L, and V can each only appear once.
        if( !followsRule3( romanNumber ) ) {
            throw new IllegalArgumentException( romanNumber + " is not a correct roman number - violates rule 3." );
        }

        Map<String, Integer> romanDenominationsUsage = getRomanDenominationsUsage( romanNumber );
        int valueParsedFromRomanNumber = calculateValue( romanDenominationsUsage );

        //Rule 2: M, C, and X cannot be equalled or exceeded by smaller denominations.
        if( !followsRule2( romanDenominationsUsage, valueParsedFromRomanNumber ) ) {
            throw new IllegalArgumentException( romanNumber + " is not a correct roman number - violates rule 2." );
        }

        return valueParsedFromRomanNumber;
    }

    private boolean containsOnlyAllowedCharacters( String romanNumber ) {
        for( int i = 0; i < romanNumber.length(); i++ ) {
            if( !ALLOWED_CHARACTERS.contains( romanNumber.charAt( i ) ) ) {
                return false;
            }
        }

        return true;
    }

    private boolean followsRule1( String romanNumber ) {
        //Rule 1: Numerals must be arranged in descending order of size.

        int previousValue = -1;
        for( int i = 0; i < romanNumber.length(); ) {
            String currentRomanDenomination = getRomanDenomination( romanNumber, i );
            int currentValue = VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.get( currentRomanDenomination );

            if( previousValue != -1 && currentValue > previousValue ) {
                return false;
            }

            i += currentRomanDenomination.length();
            previousValue = currentValue;
        }

        return true;
    }

    private boolean followsRule3( String romanNumber ) {
        //Rule 3: D, L, and V can each only appear once.

        int numbersOfDs = 0;
        int numbersOfLs = 0;
        int numbersOfVs = 0;

        for( int i = 0; i < romanNumber.length(); i++ ) {
            if( romanNumber.charAt( i ) == 'D' ) {
                numbersOfDs++;
            }
            if( romanNumber.charAt( i ) == 'L' ) {
                numbersOfLs++;
            }
            if( romanNumber.charAt( i ) == 'V' ) {
                numbersOfVs++;
            }

            if( numbersOfDs > 1 || numbersOfLs > 1 || numbersOfVs > 1 ) {
                return false;
            }
        }

        return true;
    }

    private Map<String, Integer> getRomanDenominationsUsage( String romanNumber ) {
        Map<String, Integer> romanDenominationsUsage = new HashMap<>();

        //Initializing with usage = 0 for all the allowed roman denominations
        for( String romanDenomination : VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.keySet() ) {
            romanDenominationsUsage.put( romanDenomination, 0 );
        }

        for( int i = 0; i < romanNumber.length(); ) {
            String currentRomanDenomination = getRomanDenomination( romanNumber, i );
            romanDenominationsUsage
                    .put( currentRomanDenomination, romanDenominationsUsage.get( currentRomanDenomination ) + 1 );
            i += currentRomanDenomination.length();
        }

        return romanDenominationsUsage;
    }

    private int calculateValue( Map<String, Integer> romanDenominationsUsage ) {
        //Simply adding the denominationOccurences * denominationValue values
        //No need to validate anything here as it is done by rule 3

        int value = 0;
        for( String romanDenomination : romanDenominationsUsage.keySet() ) {
            int denominationOccurences = romanDenominationsUsage.get( romanDenomination );
            int denominationValue = VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.get( romanDenomination );

            int addend = multiplyExact( denominationOccurences, denominationValue );
            if( value > MAX_ALLOWED_VALUE - addend ) {
                throw new IllegalArgumentException( "Only values up to " + MAX_ALLOWED_VALUE + " are allowed." );
            }

            value += addend;
        }

        return value;
    }

    private boolean followsRule2( Map<String, Integer> romanDenominationsUsage, int value ) {
        //Rule 2: M, C, and X cannot be equalled or exceeded by smaller denominations.

        List<String> romanDenominationsThatCannotBeExceeded = asList( "M", "C", "X" );

        //It is very important to have roman denominations ordered in below iteration from the greatest to the lowest
        //value

        //When iterating over e.g. M = 1000, we subtract the 1000s occurred by using the letter M

        //If the value is still >= 1000, then it means that the value 1000 was formed by other lower denominators and
        //this is a rule 2 violation

        //C, X are handled the same way

        for( String romanDenomination : VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.keySet() ) {
            value -= VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.get( romanDenomination ) *
                     romanDenominationsUsage.get( romanDenomination ); //overflow safe

            for( String romanDenominationThatCannotBeExceeded : romanDenominationsThatCannotBeExceeded ) {
                if( romanDenomination.equals( romanDenominationThatCannotBeExceeded ) &&
                    value >= VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.get( romanDenominationThatCannotBeExceeded ) ) {
                    return false;
                }
            }
        }

        return true;
    }

    private String getRomanDenomination( String romanNumber, int position ) {
        for( String denomination : VALUES_PER_ORDERED_ROMAN_DENOMINATIONS.keySet() ) {
            int indexStart = position;
            int indexEnd = position + denomination.length() > romanNumber.length() ? romanNumber.length() :
                    position + denomination.length();

            if( romanNumber.substring( indexStart, indexEnd ).equals( denomination ) ) {
                return denomination;
            }
        }

        throw new IllegalArgumentException(
                "Unknown denomination found in " + romanNumber + " at position " + position );
    }

    /**
     * Checks if the given roman number is valid. It does not need to be in the minimal form, but it needs to follow
     * all the rules for being valid.
     * <br>For more information about the roman numbers, see {@link RomanNumberConverter}.
     *
     * @param romanNumber roman number that will be validated
     *
     * @return true if the given {@code romanNumber} is a valid roman number, false otherwise
     */
    public boolean isValid( String romanNumber ) {
        if( romanNumber == null || romanNumber.isEmpty() || !containsOnlyAllowedCharacters( romanNumber ) ) {
            return false;
        }

        //Rule 1: Numerals must be arranged in descending order of size.
        if( !followsRule1( romanNumber ) ) {
            return false;
        }

        //Rule 3: D, L, and V can each only appear once.
        if( !followsRule3( romanNumber ) ) {
            return false;
        }

        //Rule 2: M, C, and X cannot be equalled or exceeded by smaller denominations.
        Map<String, Integer> romanDenominationsUsage = getRomanDenominationsUsage( romanNumber );
        if( !followsRule2( romanDenominationsUsage, calculateValue( romanDenominationsUsage ) ) ) {
            return false;
        }

        return true;
    }

    /**
     * Checks if the given roman number is valid and in minimal form.
     * <br>For more information about the roman numbers, see {@link RomanNumberConverter}.
     *
     * @param romanNumber roman number that will be validated and checked if it is in minimal form
     *
     * @return true if the given {@code romanNumber} is a valid roman number in minmal form, false otherwise
     */
    public boolean isValidMinimalForm( String romanNumber ) {
        if( romanNumber == null || romanNumber.isEmpty() || !containsOnlyAllowedCharacters( romanNumber ) ) {
            return false;
        }

        //Rule 1: Numerals must be arranged in descending order of size.
        if( !followsRule1( romanNumber ) ) {
            return false;
        }

        //Rule 3: D, L, and V can each only appear once.
        if( !followsRule3( romanNumber ) ) {
            return false;
        }

        //Rule 2: M, C, and X cannot be equalled or exceeded by smaller denominations.
        Map<String, Integer> romanDenominationsUsage = getRomanDenominationsUsage( romanNumber );
        int valueParsedFromRomanNumber = calculateValue( romanDenominationsUsage );

        if( !followsRule2( romanDenominationsUsage, valueParsedFromRomanNumber ) ) {
            return false;
        }

        String romanNumberMinimalForm = convertToRomanNumber( valueParsedFromRomanNumber );
        return romanNumber.equals( romanNumberMinimalForm );
    }

    /**
     * Converts the given value to the roman number in the minimal form.
     * <br>For more information about the roman numbers, see {@link RomanNumberConverter}.
     *
     * @param value value that will be converted to the roman number
     *
     * @return Roman number representing the given integer {@code value}. It is always in the minimal form.
     *
     * @throws IllegalArgumentException when {@code value} &lt; 1 or {@code value} &gt; {@value MAX_ALLOWED_VALUE}
     * @see <a href="https://en.wikipedia.org/wiki/Roman_numerals">Wikipedia - Roman numerals</a>
     * @see <a href="https://projecteuler.net/about=roman_numerals">Project Euler - How to write roman numerals</a>
     */
    public String convertToRomanNumber( int value ) {
        if( value < 1 ) {
            throw new IllegalArgumentException( "Only positive values can be converted to roman number." );
        }
        if( value > MAX_ALLOWED_VALUE ) {
            throw new IllegalArgumentException( "Only values up to " + MAX_ALLOWED_VALUE + " can be converted." );
        }

        StringBuffer romanNumber = new StringBuffer();

        //1000s
        for( int i = 0; i < value / 1000; i++ ) {
            romanNumber.append( "M" );
        }
        value = value % 1000;

        //100s
        romanNumber.append( HUNDREDS_SYMBOLS[value / 100] );
        value = value % 100;

        //10s
        romanNumber.append( TENS_SYMBOLS[value / 10] );
        value = value % 10;

        //1s
        romanNumber.append( ONES_SYMBOLS[value] );

        return romanNumber.toString();
    }
}
