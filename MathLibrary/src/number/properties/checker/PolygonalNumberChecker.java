package number.properties.checker;

import java.math.BigInteger;

import static java.lang.Math.*;

/**
 * A polygon is a two-dimensional geometrical figure which is made by joining up straight lines together.
 * In a  regular polygon, all the sides and angles are equal.
 * <br>Examples of regular polygons are : triangle (\(3\) line segments), square (\(4\) line segments),
 * pentagon (\(5\) line segments), hexagon (\(6\) line segments) etc.
 *
 * <br><br>Polygonal number is a number which is represented in the form of dots arranged in such a way that it makes
 * a shape of regular polygon. Each of these dots are imagined as a unit.
 * <br>If an integer \(s &gt; 2\) is a number of sides of the polygonal and \(n &gt; 0 \) is the number of dots each
 * side has, then the \(n\)-th \(s\)-gonal number is written as \(P(s, n)\) and it is the total number of dots in the
 * figure.
 *
 * <br><br>Examples of polygonal numbers represented as dots:
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/PolygonalNumbers_Examples.png" alt = "Polygonal
 * numbers - examples"></p>
 *
 * <br><br>The general formula for \(P(s, n)\) is based on the idea of dividing the whole figure into triangles.
 * The picture below contains the \(6\)-th \(s\)-gonal numbers, where \(s = \{6, 7, 8\}\):
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/PolygonalNumbers_GeneralFormula.png" alt = "Polygonal
 * numbers - general formula"></p>
 *
 * <br><br>We can see that each figure contains \(6\) white dots and \(s - 2\) triangles (marked with different colors)
 * with \(6 - 1 = 5\) dots per side. Each triangle containing \(k\) dots in its side has the total of
 * \(1 + 2 + 3 + ... + k = \frac{k(k + 1)}{2}\) dots.
 * <br>Thus the general formula for calculating \(P(s, n)\) is the following:
 * <br>$$P(s, n) = n + (s - 2) \cdot \frac{(n - 1)n}{2} = \frac{2n + (n - 1)n \cdot (s - 2)}{2}
 * = \frac{(n^2 - n)(s - 2) + 2n}{2} = \frac{sn^2 - 2n^2 - sn + 2n + 2n}{2} = \frac{n^2(s - 2) - n(s - 4)}{2}$$
 *
 * <br><br>In order for an integer \(x &gt; 0\) to be the \(n\)-th \(s\)-gonal number, the value \(n\) needs to be a
 * positive integer in the following equation:
 * \begin{align}
 * \frac{n^2(s - 2) - n(s - 4)}{2} &amp; = x \\
 * n^2(s - 2) - n(s - 4) - 2x &amp; = 0 \rightarrow
 * \Delta = (-(s - 4))^2 - 4 \cdot (-2x) \cdot (s - 2) = (s - 4)^2 + 8x(s - 2) \\
 * n &amp; = \frac{(s - 4) \pm \sqrt{\Delta}}{2(s - 2)} = \frac{(s - 4) \pm \sqrt{(s - 4)^2 + 8x(s - 2)}}{2(s - 2)}
 * \end{align}
 *
 * <br>We can see that \(n = \frac{(s - 4) \pm \sqrt{(s - 4)^2 + 8x(s - 2)}}{2(s - 2)}
 * = \frac{s - 4}{2(s - 2)} \pm \frac{\sqrt{(s - 4)^2 + 8x(s - 2)}}{2(s - 2)}\).
 * Moreover \(\frac{s - 4}{2(s - 2)} = \frac{s - 4}{2s - 4} &lt; 1\) and
 * \(\frac{\sqrt{(s - 4)^2 + 8x(s - 2)}}{2(s - 2)} &gt; 0\) for \(s &gt; 2\).
 * <br>It means that we can use '\(+\)' sign instead of '\(\pm\)', because the '\(+\)' guarantees that integer \(n\)
 * is positive, whereas '\(-\)' for integer \(n\) means it is negative.
 *
 * <br><br>We also need to make sure that \(\Delta \geq 0\) (because we are calculating \(\sqrt{\Delta}\)).
 * <br>\(\Delta = (s - 4)^2 + 8x(s - 2) \geq 0\) for \(x \geq 0\) because \((s - 4)^2 \geq 0\) by definition and for
 * \(s &gt; 2\) we have \(8(s - 2) &gt; 0\). So for our initial assumptions that \(x &gt; 0, s &gt; 2\) we have
 * \(\Delta = (s - 4)^2 + 8x(s - 2) \geq 0\).
 *
 * <br><br>The final conditions for an integer \(x &gt; 0\) to be the \(n\)-th \(s\)-gonal number \(P(s, n)\) are:
 * <ul><li>
 * \(\sqrt{\Delta} = \sqrt{(s - 4)^2 + 8x(s - 2)}\) is an integer.
 * </li></ul>
 * <ul><li>
 * \(n = \frac{(s - 4) + \sqrt{\Delta}}{2(s - 2)}\) is an integer.
 * </li></ul>
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @see <a href="https://en.wikipedia.org/wiki/Polygonal_number">Wikipedia - Polygonal number</a>
 * @see <a href="http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Figurate/figurate.html">University of Surrey, Dr
 * Ron Knott - Polygonal numbers</a>
 */
public class PolygonalNumberChecker {
    /**
     * Checks if the given integer \(x\) is a triangular number.
     * <br>For the detailed description of the implementation, see {@link PolygonalNumberChecker}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param x value of \(x\)
     *
     * @return True if \(x\) is a triangular number, false otherwise.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Polygonal_number">Wikipedia - Polygonal number</a>
     * @see <a href="http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Figurate/figurate.html">University of Surrey,
     * Dr Ron Knott - Polygonal numbers</a>
     */
    public boolean isTriangular( long x ) {
        return isPolygonalNumber( 3, x );
    }

    private boolean isPolygonalNumber( int s, long x ) { //s > 2
        if( x < 1 ) {
            return false;
        }

        //In order for x > 0 to be the n-th polygonal number P(s, n), where s > 2:
        //1) sqrt(delta) is an integer -> delta = (s - 4)^2 + 8x(s - 2) must be a square number
        //2) n = ( (s - 4) + sqrt(delta) ) / 2(s - 2) must be an integer

        //Need to use BigIntegers when 8x(s - 2) > Long.MAX if x > Long.MAX / 8(s - 2)
        //floor(Long.MAX / 8(s - 2)) is enough here as well
        if( x > Long.MAX_VALUE / ( 8L * ( s - 2L ) ) ) { //overflow safe
            return isPolygonalNumberUsingBigIntegers( s, x );
        }

        return isPolygonalNumberUsingLong( s, x );
    }

    private boolean isPolygonalNumberUsingBigIntegers( int s, long x ) {
        BigInteger sMinus4 = BigInteger.valueOf( s ).subtract( BigInteger.valueOf( 4 ) );
        BigInteger sMinus2 = BigInteger.valueOf( s ).subtract( BigInteger.valueOf( 2 ) );

        //delta = (s - 4)^2 + 8x(s - 2)
        BigInteger delta = ( sMinus4.multiply( sMinus4 ) )
                .add( BigInteger.valueOf( 8 ).multiply( BigInteger.valueOf( x ) ).multiply( sMinus2 ) );
        BigInteger[] deltaSqrtWithRemainder = delta.sqrtAndRemainder();

        //1) delta must be a square number
        if( !deltaSqrtWithRemainder[1].equals( BigInteger.ZERO ) ) { //sqrt remainder = 0
            return false;
        }

        BigInteger sqrtDelta = deltaSqrtWithRemainder[0];
        BigInteger[] nWithRemainder = ( sMinus4.add( sqrtDelta ) )
                .divideAndRemainder( BigInteger.valueOf( 2 ).multiply( sMinus2 ) );

        //2) n = ( (s - 4) + sqrt(delta) / 2(s - 2) must be an integer
        return nWithRemainder[1].equals( BigInteger.ZERO ); //division remainder = 0
    }

    private boolean isPolygonalNumberUsingLong( int s, long x ) {
        //delta = (s - 4)^2 + 8x(s - 2)
        long delta = addExact( multiplyExact( subtractExact( s, 4 ), subtractExact( s, 4 ) ),
                               multiplyExact( 8, multiplyExact( x, subtractExact( s, 2 ) ) ) );
        long sqrtDelta = (long) sqrt( delta );

        //1) delta must be a square number
        if( sqrtDelta * sqrtDelta != delta ) {
            return false;
        }

        //2) n = ( (s - 4) + sqrt(delta) / 2(s - 2) must be an integer
        long nNumerator = addExact( subtractExact( s, 4 ), sqrtDelta );
        long nDenominator = multiplyExact( 2, subtractExact( s, 2 ) );

        return nNumerator % nDenominator == 0;
    }

    /**
     * Checks if the given integer \(x\) is a square number.
     * <br>For the detailed description of the implementation, see {@link PolygonalNumberChecker}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param x value of \(x\)
     *
     * @return True if \(x\) is a square number, false otherwise.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Polygonal_number">Wikipedia - Polygonal number</a>
     * @see <a href="http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Figurate/figurate.html">University of Surrey,
     * Dr Ron Knott - Polygonal numbers</a>
     */
    public boolean isSquare( long x ) {
        return isPolygonalNumber( 4, x );
    }

    /**
     * Checks if the given integer \(x\) is a pentagonal number.
     * <br>For the detailed description of the implementation, see {@link PolygonalNumberChecker}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param x value of \(x\)
     *
     * @return True if \(x\) is a pentagonal number, false otherwise.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Polygonal_number">Wikipedia - Polygonal number</a>
     * @see <a href="http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Figurate/figurate.html">University of Surrey,
     * Dr Ron Knott - Polygonal numbers</a>
     */
    public boolean isPentagonal( long x ) {
        return isPolygonalNumber( 5, x );
    }

    /**
     * Checks if the given integer \(x\) is a hexagonal number.
     * <br>For the detailed description of the implementation, see {@link PolygonalNumberChecker}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param x value of \(x\)
     *
     * @return True if \(x\) is a hexagonal number, false otherwise.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Polygonal_number">Wikipedia - Polygonal number</a>
     * @see <a href="http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Figurate/figurate.html">University of Surrey,
     * Dr Ron Knott - Polygonal numbers</a>
     */
    public boolean isHexagonal( long x ) {
        return isPolygonalNumber( 6, x );
    }

    /**
     * Checks if the given integer \(x\) is a heptagonal number.
     * <br>For the detailed description of the implementation, see {@link PolygonalNumberChecker}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param x value of \(x\)
     *
     * @return True if \(x\) is a heptagonal number, false otherwise.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Polygonal_number">Wikipedia - Polygonal number</a>
     * @see <a href="http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Figurate/figurate.html">University of Surrey,
     * Dr Ron Knott - Polygonal numbers</a>
     */
    public boolean isHeptagonal( long x ) {
        return isPolygonalNumber( 7, x );
    }

    /**
     * Checks if the given integer \(x\) is an octagonal number.
     * <br>For the detailed description of the implementation, see {@link PolygonalNumberChecker}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param x value of \(x\)
     *
     * @return True if \(x\) is an octagonal number, false otherwise.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Polygonal_number">Wikipedia - Polygonal number</a>
     * @see <a href="http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Figurate/figurate.html">University of Surrey,
     * Dr Ron Knott - Polygonal numbers</a>
     */
    public boolean isOctagonal( long x ) {
        return isPolygonalNumber( 8, x );
    }
}
