package number.properties.checker;

import algorithm.FactorizationAlgorithm;
import number.representation.FactorizationFactor;

import static java.lang.Math.incrementExact;
import static util.BasicOperations.power;
import static util.ModuloOperations.powerModulo;

/**
 * An integer \(a\) is called a quadratic residue modulo an integer \(m &gt; 0\) if it is congruent
 * to a perfect square modulo \(m\), which means that there exists an integer \(x\) such that
 * \(x^2 \equiv a \text{ mod } m\).
 *
 * <br>\(a\) is a quadratic residue modulo \(m\) only if it is a quadratic residue modulo \(p_i^{e_i}\) for
 * \(0 \leq i &lt; k\), where \(m = p_0^{e_0} \cdot p_1^{e_1} \cdot  \text{ ... } \cdot p_{k - 1}^{e_{k - 1}}\),
 * \(e^i &gt; 0\) is a prime factorization of \(m\).
 * <br>For each case let`s set \(a' \equiv a \text{ mod } p_i^{e_i}\). It guarantees that
 * \(0 \leq a' &lt; p_i^{e_i}\) and means that if \(a'\) is a quadratic residue modulo \(p_i^{e_i}\) then so is \(a\).
 *
 * <br><br>In order to check if there exists an \(x\) solving \(x^2 \equiv a' \text{ mod } p_i^{e_i}\) we need to
 * consider
 * the following cases:
 * <ul><li>
 * If \(a' = 0\), then \(a'\) is a quadratic residue \(\text{mod } p_i^{e_i}\).
 * </li></ul>
 * <ul><li>
 * If \(a' = np_i^r\) where \(\text{gcd}(n, p_i) = 1\) and \(0 &lt; r &lt; e_i\), then \(a'\) is a quadratic residue
 * \(\text{mod } p_i^{e_i}\) only if \(r \equiv 0 \text{ mod } 2\) and \(n\) is a quadratic residue
 * \(\text{mod } p_i^{e_i - r}\).
 * <br>It is because for integers \(A,B,C,M\) if \(AC \equiv BC \text{ mod }MC\), then \(A \equiv B \text{ mod }M\).
 * <br>\(x^2 \equiv np_i^r \text{ mod } p_i^{e_i}\) can be transformed to
 * \(y^2 = \left(\frac{x}{p_i^{r/2}}\right)^2 \equiv n \text{ mod } p_i^{e_i - r}\) only if
 * \(r \equiv 0 \text{ mod } 2\).
 * Then we need to check if \(n\) is a quadratic residue modulo \(p_i^{e_i - r}\) (both \(x\) and \(y\) integers
 * exist if it is). Look below for how to do that as \(\text{gcd}(n, p_i) = 1\).
 * </li></ul>
 * <ul><li>
 * If \(\text{gcd}(a', p_i) = 1\), then:
 * <ul><li>
 * If \(p_i = 2\), then \(a'\) is a quadratic residue modulo \(p_i^{e_i}\) when:
 * <ul><li>
 * \(e_i = 1\).
 * </li></ul>
 * <ul><li>
 * \(e_i = 2\) and \(a' \equiv 1 \text{ mod } 4\).
 * </li></ul>
 * <ul><li>
 * \(e_i \geq 3\) and \(a' \equiv 1 \text{ mod } 8\).
 * </li></ul>
 * </li></ul>
 * <ul><li>
 * If \(p_i \neq 2\), then \(a'\) is a quadratic residue modulo \(p_i^{e_i}\) only if it is a
 * quadratic residue modulo \(p_i\). By Euler`s criterion \(a'\) is a quadratic residue modulo \(p_i\) (\(\text{gcd}
 * (a', p_i) = 1\)) when \(a'^{\frac{p_i - 1}{2}} \equiv 1 \text{ mod } p_i\).
 * </li></ul>
 * </li></ul>
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Quadratic_residue">Wikipedia - Quadratic residue</a>
 * @see <a href="https://en.wikipedia.org/wiki/Euler%27s_criterion">Wikipedia - Euler's criterion</a>
 * @see <a href="https://www.johndcook.com/blog/quadratic_congruences/">John D. Cook - Quadratic congruences modulo
 * power of 2</a>
 */
public class QuadraticResidueChecker {
    private FactorizationAlgorithm factorizationAlgorithm = new FactorizationAlgorithm();

    /**
     * Checks if \(a\) is a quadratic residue modulo \(m\). Zero is considered to be a quadratic residue.
     * <br>For the detailed description of the implementation, see {@link QuadraticResidueChecker}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param m modulo \(m &gt; 0\)
     *
     * @return True if \(a\) is a quadratic residue modulo \(m\), false otherwise (zero is considered to be a quadratic
     * residue).
     *
     * @throws IllegalArgumentException when \(m &lt; 1\)
     * @see <a href="https://en.wikipedia.org/wiki/Quadratic_residue">Wikipedia - Quadratic residue</a>
     * @see <a href="https://en.wikipedia.org/wiki/Euler%27s_criterion">Wikipedia - Euler's criterion</a>
     * @see <a href="https://www.johndcook.com/blog/quadratic_congruences/">John D. Cook - Quadratic congruences modulo
     * power of 2</a>
     */
    public boolean isQuadraticResidue( long a, long m ) {
        if( m < 1 ) {
            throw new IllegalArgumentException( "modulo must be > 0" );
        }
        if( m == 1 ) {
            //If modulo is 1, then every number is a quadratic residue
            return true;
        }

        //a is a quadratic residue modulo m = p_0^e_0 * p_1^e_1 * ... * p_k^e_k
        //when it is a quadratic residue modulo every p_i^e_i
        for( FactorizationFactor factor : factorizationAlgorithm.getFactorization( m ) ) {
            long p_i = factor.getPrime();
            int e_i = factor.getExponent();

            if( !isQuadraticResidue( a, p_i, e_i ) ) {
                //If a is a quadratic non-residue modulo p_i^e_i,
                //then it is a quadratic non-residue modulo m
                return false;
            }
        }

        return true;
    }

    private boolean isQuadraticResidue( long a, long p_i, int e_i ) {
        a = a % power( p_i, e_i ); //a' = a mod p_i^e_i

        if( a == 0 ) {
            //If a' = 0, then a' is a quadratic residue modulo p_i^e_i
            return true;
        }

        if( a % p_i == 0 ) {
            //If a' = np_i^r, where gcd(n, p_i) = 1 and 0 < r < e_i,
            //then a' is a quadratic residue modulo p_i^e_i
            //if r % 2 == 0 and n is a quadratic residue modulo p_i^{e_i - r}

            long n = a;
            int r = 0;

            do {
                n /= p_i;
                r = incrementExact( r );
            } while( n % p_i == 0 );

            return r % 2 == 0 && isQuadraticResidue( n, p_i, e_i - r );
        }

        //gcd(a', p_i) = 1
        if( p_i == 2 ) {
            //a' is odd here
            //Every odd a' is a quadratic residue modulo 2^1
            //Only a' of form 4j + 1 is a quadratic residues mod 2^2
            //Only a' of form 8j + 1 is a quadratic residues mod 2^e_i where e_i > 2
            return e_i == 1 || ( e_i == 2 && a % 4 == 1 ) || ( e_i > 2 && a % 8 == 1 );
        }
        else {
            //a' is a quadratic residue modulo p_i^e_i when it is a quadratic residue modulo p_i
            //For p_i != 2 use Euler`s criterion to check that
            return isQuadraticResidueByEulersCriterion( a, p_i );
        }
    }

    private boolean isQuadraticResidueByEulersCriterion( long a, long p_i ) {
        //Euler`s criterion states that for a, p_i if gcd(a, p_i) = 1
        //then a is a quadratic residue modulo p_i if a^( (p_i - 1)/2 ) = 1 mod p_i
        return powerModulo( a, ( ( p_i - 1 ) / 2 ), p_i ) == 1;
    }
}
