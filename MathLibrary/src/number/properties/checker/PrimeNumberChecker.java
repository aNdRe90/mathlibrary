package number.properties.checker;

import algorithm.FactorizationAlgorithm;

import static java.lang.Math.addExact;
import static java.lang.Math.sqrt;

/**
 * A prime number number \(n\) is an integer \(n &gt; 1\) that has only two divisors - \(1\) and \(n\). Numbers that
 * are not prime are called composite (except for the number \(1\) which is neither prime nor composite).
 * <br>Primes are central in number theory because of the fundamental theorem of arithmetic which states that every
 * integer greater than 1 is either a prime itself or can be factorized as a product of primes that is unique up to
 * their order (see {@link FactorizationAlgorithm} for more details about factorization).
 * <br>The first prime numbers are: \(2, 3, 5, 7, 11, 13, 17, 19, 23, ...\).
 *
 * <br><br>An algorithm that determines whether or not a given integer \(n &gt; 1\) is prime is called a primality
 * test.
 * <br>The simplest primality test is called trial division. If the number \(n\) has \(k\) divisors
 * \(d_0, d_1, d_2, ..., d_{k - 1}\), where \(1 \leq d_i \leq n\), then we can find all of them by looking among
 * the values \(1, 2, 3, ..., \sqrt{n}\).
 *
 * <br><br>Trial division uses this fact and follows the idea that if any of the integers \(i = 2, 3, 4, 5, ...,
 * \sqrt{n}\) divides \(n\), then \(n\) is not prime.
 * <br>In fact, the condition can be narrowed to \(i\) being a prime value itself because for example, we need \(2\)
 * to divide \(n\) in order for \(4\) to divide \(n\) (\(2\) is a prime number and \(4 = 2^2\)).
 *
 * <br>We can notice that a prime number \(i &gt; 3\) needs to satisfy \(i \equiv \pm 1 \text{ mod } 6\), which means
 * that \(i = 6n \pm 1\). We can easily explain this by checking that for an integer \(n\) the following values are not
 * prime:
 * \begin{align}
 * 6n &amp; = 2 \cdot 3 \cdot n \\
 * 6n + 2 &amp; = 2 \cdot (3n + 1) \\
 * 6n + 3 &amp; = 3 \cdot (2n + 1) \\
 * 6n + 4 &amp; = 2 \cdot (3n + 2) \\
 * \end{align}
 *
 * <br>The above fact is used to optimize the trial division method. Instead of looking among \(i = 2, 3, 4, 5, 6, 7,
 * 8, 9, 10, 11, ..., \sqrt{n}\), we can search only the values \(i = 2, 3, 5, 7, 11, 13, 17, 19, 23, 25, 29, 31, ... \)
 * until \(i \leq \sqrt{n}\).
 *
 * <br><br>The final procedure of performing the primality test on \(n &gt; 1\) using the trial division method is the
 * following:
 * <br>1) If \(n = 2\) or \(n = 3\), then \(n\) is a prime number.
 * <br>2) If \(n\) is divisible by \(2\) or \(3\), then \(n\) is not a prime number.
 * <br>3) For \(i = 5, 7, 11, 13, 17, 19, 23, 25, 29, 31, 35, ...\) and \(i \leq \sqrt{n}\):
 * <ul><li>
 * If \(n\) is divisible by \(i\), then \(n\) is not a prime number.
 * </li></ul>
 * <br>4) \(n\) is a prime number.
 *
 * <br><br>Example of trial division primality test on \(n = 493\) (\(\sqrt{n} = \sqrt{493} \approx 22.204\)):
 * \begin{align}
 * &amp; 493 \neq 2, \text{  } 493 \neq 3 \\
 * &amp; 2 \not|493,  \text{  } 3 \not|493 \\
 * &amp; 5 \not|493, \text{  } 7 \not|493, 11 \not|493, \text{  } 13 \not|493, 17|493 \text{  } (\frac{493}{17} = 29)
 * \end{align}
 * \(n = 493\) is not a prime number.
 *
 * <br><br>Example of trial division primality test on \(n = 599\) (\(\sqrt{n} = \sqrt{599} \approx 24.474\)):
 * \begin{align}
 * &amp; 599 \neq 2, \text{  } 599 \neq 3 \\
 * &amp; 2 \not|599,  \text{  } 3 \not|599 \\
 * &amp; 5 \not|599, \text{  } 7 \not|599, 11 \not|599, \text{  } 13 \not|599, 17 \not|599, \text{  } 19 \not|599,
 * 23 \not|599, \text{  } 25 &gt; \sqrt{n} \approx 24.474
 * \end{align}
 * \(n = 599\) is a prime number.
 *
 * <br><br>The trial division primality test is a simple and fast way of determining whether or not a relatively
 * small number \(n &gt; 1\) is prime. This class uses this method for \(n \leq\) {@value TRIAL_DIVISION_LIMIT}.
 * <br>For greater values of \(n\) a deterministic version of Miller-Rabin primality test is used instead (see
 * {@link MillerRabinDeterministicPrimeNumberChecker} for more details on how it is done).
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Prime_number">Wikipedia - Prime number</a>
 * @see <a href="https://en.wikipedia.org/wiki/Fundamental_theorem_of_arithmetic">Wikipedia - Fundamental theorem of
 * arithmetic</a>
 * @see <a href="https://en.wikipedia.org/wiki/Primality_test">Wikipedia - Primality test</a>
 */
public class PrimeNumberChecker {
    /**
     * Limit for the trial division primality test. It is equal to {@value TRIAL_DIVISION_LIMIT}.
     * <br>Every \(n &gt; 1\) that is smaller than or equal to this value will be checked using the trial division
     * primality test.
     */
    public static final long TRIAL_DIVISION_LIMIT = 1000000000;

    private MillerRabinDeterministicPrimeNumberChecker millerRabinPrimeNumberChecker
            = new MillerRabinDeterministicPrimeNumberChecker();

    /**
     * Checks if the given integer \(n\) is prime.
     * <br>For the detailed description of the implementation, see {@link PrimeNumberChecker}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param n value of \(n\)
     *
     * @return True if \(n\) is a prime number, false otherwise.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Prime_number">Wikipedia - Prime number</a>
     * @see <a href="https://en.wikipedia.org/wiki/Fundamental_theorem_of_arithmetic">Wikipedia - Fundamental
     * theorem of arithmetic</a>
     * @see <a href="https://en.wikipedia.org/wiki/Primality_test">Wikipedia - Primality test</a>
     */
    public boolean isPrime( long n ) {
        if( n < 2 ) {
            return false; //all negative values, as well as 0 and 1 are not prime numbers
        }

        if( n <= TRIAL_DIVISION_LIMIT ) {
            return isPrimeUsingTrialDivision( n ); //safe casting
        }

        return millerRabinPrimeNumberChecker.isPrime( n );
    }

    private boolean isPrimeUsingTrialDivision( long n ) {
        //Procedure of performing the primality test on n > 1 using the trial division method is the following:
        //1) If n = 2 or n = 3, then n is a prime number.
        //2) If n is divisible by 2 or 3, then n is not a prime number.
        //3) For i = 5, 7, 11, 13, 17, 19, 23, 25, 29, 31, 35, ... and i <= sqrt(n)
        //      If n is divisible by i, then n is not a prime number.
        //4) n is a prime number.

        //1) If n = 2 or n = 3, then n is a prime number.
        if( n == 2 || n == 3 ) {
            return true;
        }

        //2)If n is divisible by 2 or 3, then n is not a prime number.
        if( n % 2 == 0 || n % 3 == 0 ) {
            return false;
        }

        double sqrtX = sqrt( n );

        //3) For i = 5, 7, 11, 13, 17, 19, 23, 25, 29, 31, 35, ... and i <= sqrt(n)
        for( long i = 5; ; i = addExact( i, 6 ) ) {
            if( i > sqrtX ) {
                break;
            }
            //Checking if n is divisible by i of form 6n - 1
            if( n % i == 0 ) {
                return false;
            }

            if( addExact( i, 2 ) > sqrtX ) {
                break;
            }
            //Checking if  n is divisible by (i + 2) of form 6n + 1
            if( n % addExact( i, 2 ) == 0 ) {
                return false;
            }
        }

        //4) n is a prime number.
        return true;
    }
}
