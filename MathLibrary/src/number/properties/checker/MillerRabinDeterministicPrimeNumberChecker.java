package number.properties.checker;

import algorithm.ModularSquareRootAlgorithm;

import java.util.List;

import static java.util.Arrays.asList;
import static util.ModuloOperations.multiplyModulo;
import static util.ModuloOperations.powerModulo;

/**
 * Miller-Rabin primality test is a probabilistic algorithm used for determining whether the given odd integer
 * \(n &gt; 2\) is a prime number. For more information about prime numbers see {@link PrimeNumberChecker}.
 *
 * <br><br>This probability test is based on the fact that if \(x^2 \equiv 1 \text{ mod } n\) has only the primitive
 * solutions \(x \equiv \pm 1 \text{ mod } n\), then \(n\) is a prime number.
 * <br>It is because \(x^2 \equiv 1 \text{ mod } n \Leftrightarrow x^2 - 1 \equiv 0 \text{ mod } n
 * \Leftrightarrow (x - 1)(x + 1) \equiv 0 \text{ mod } n\), which means that \(n\) divides \((x - 1)(x + 1)\).
 * <ul><li>
 * If \(n\) is a composite number, then there are non-trivial solutions (other than
 * \(x \equiv \pm 1 \text{ mod } n\)). Let`s say that \(x - 1 = d_1, x + 1 = d_2\), where \(1 &lt; d_1 &lt; \sqrt{n}
 * &lt; d_2 &lt; n\) and \(d_1, d_2\) are divisors of \(n\). Then \(x = d_1 + 1 = d_2 - 1\) is a non-trivial solution
 * to \(x^2 \equiv 1 \text{ mod } n\).
 * <br>For example, for \(n = 8 = 2 \cdot 4\), \(x = 3\) is a non-trivial solution (\(3^2 = 9 \equiv 1 \text{ mod }
 * 8\)).
 * </li></ul>
 * <ul><li>
 * If \(n\) is a prime number, then \(n\) divides \((x - 1)(x + 1)\) only if \(x \equiv \pm 1 \text{ mod } n\).
 * </li></ul>
 *
 * <br>Miller-Rabin primality test seeks for non-trivial solutions to \(x^2 \equiv 1 \text{ mod } n\) in order to
 * determine whether or not \(n\) is a prime number. If it finds such solution, then \(n\) for sure is a composite
 * number. If not, then \(n\) is probably a prime number.
 *
 * <br><br>Let`s look at the Fermat`s Little Theorem, which states that for relatively prime numbers \(a, n\) (\(a\)
 * cannot be zero, because zero is not relatively prime to any other number), we have
 * \(a^{n - 1} \equiv 1 \text{ mod } n\) if \(n\) is a prime number.
 * <br>It helps us with calculating the solutions to \(x^2 \equiv 1 \text{ mod } n\) because we can assume that
 * \(x^2 = a^{n - 1} \equiv 1 \text{ mod } n\) and \(x = \sqrt{x^2} = \sqrt{a^{n - 1}}\).
 * <br>Notice that \(a \equiv \pm 1 \text{ mod } n\) are trivial examples (because the power \(n - 1\) is an even
 * number), so we should not pick such values of \(a\).
 *
 * <br><br>If \(n - 1 = d2^s\), where \(d\) is an odd number and \(s &gt; 0\) (because \(n &gt; 2\) is an odd integer),
 * then \(x = \sqrt{a^{n - 1}} = \sqrt{a^{d2^s}} = a^{d2^{s - 1}}\). Now we have one of the three possible outcomes:
 * <ul><li>
 * \(x = a^{d2^{s - 1}} \not\equiv \pm 1 \text{ mod } n\) means that \(n\) for sure is not prime (\(x\) is a non-trivial
 * solution).
 * </li></ul>
 * <ul><li>
 * \(x = a^{d2^{s - 1}} \equiv -1 \text{ mod } n\) means that \(n\) may be prime but we cannot investigate further
 * for current \(a\).
 * </li></ul>
 * <ul><li>
 * \(x = a^{d2^{s - 1}} \equiv 1 \text{ mod } n\) means that \(n\) may be prime and we have another equation
 * \(y^2 = x \equiv 1 \text{ mod } n\) which we can check for non-trivial solutions the same way we did with
 * \(x^2 \equiv 1 \text{ mod } n\).
 * <br>In fact, this can be repeated many times until the square root can be done by simply subtracting from the power
 * of \(2\). Otherwise, it is not an easy task (see {@link ModularSquareRootAlgorithm}) and we should stop.
 * <br>In other words, we start by solving \(x_{s - 1}^2 = a^{d2^{s - 1}} \equiv 1 \text{ mod } n\). If it yields
 * \(x_{s - 1} \equiv 1 \text{ mod } n\), then we start to solve
 * \(x_{s - 2}^2 = a^{d2^{s - 2}} \equiv 1 \text{ mod } n\). If it yields \(x_{s - 2} \equiv 1 \text{ mod } n\), then
 * we need to check \(x_{s - 3}^2 = a^{d2^{s - 3}} \equiv 1 \text{ mod } n\) etc. until the case
 * \(x_0^2 = a^d \equiv 1 \text{ mod } n\) which cannot be easily solved.
 * </li></ul>
 *
 * <br>In order for \(x_s = a^{d2^s}\) to be congruent to \(1 \text{ mod } n\) (Fermat`s Little Theorem condition), we
 * need to have \(x_{s - 1} \equiv \{-1, 1, K\} \text{ mod } n\), where \(K\) is 'some other value'
 * (\(1 &lt; K &lt; n - 1\)).
 * <ul><li>
 * \(x_{s - 1} \equiv -1 \text{ mod } n\) yields \(x_{s - 2} \equiv K \text{ mod } n\).
 * </li></ul>
 * <ul><li>
 * \(x_{s - 1} \equiv 1 \text{ mod } n\) yields \(x_{s - 2} \equiv \{-1, 1, K\} \text{ mod } n\).
 * </li></ul>
 * <ul><li>
 * \(x_{s - 1} \equiv K \text{ mod } n\) yields \(x_{s - 2} \equiv K \text{ mod } n\).
 * </li></ul>
 *
 * In general:
 * <ul><li>
 * \(x_i \equiv -1 \text{ mod } n\) yields \(x_{i - 1} \equiv K \text{ mod } n\).
 * </li></ul>
 * <ul><li>
 * \(x_i \equiv 1 \text{ mod } n\) yields \(x_{i - 1} \equiv \{-1, 1, K\} \text{ mod } n\).
 * </li></ul>
 * <ul><li>
 * \(x_i \equiv K \text{ mod } n\) yields \(x_{i - 1} \equiv K \text{ mod } n\).
 * </li></ul>
 *
 * <br>To illustrate this rule, look at the tree below, for \(s = 3\).
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/MillerRabin_Example.png" alt = "Miller-Rabin - example">
 * </p>
 *
 * <br>All the \(K\)s marked in \(\color{red}{\text{red}}\) circles are the cases of non-trivial solutions to
 * \(x_i^2 \equiv 1 \text{ mod } n\) and thus being a proof of \(n\) being a composite number.
 * <br>All the values marked in \(\color{green}{\text{green}}\) circles are the cases of claiming that \(n\) is prime.
 *
 * <br><br>Remember that \(x_i = a^{d2^i} = (a^{d2^{i - 1}})^2 = x_{i - 1}^2\).
 *
 * <br><br>We can see that, if we check the values in the order of \(x_0, x_1, x_2, ..., x_{s - 1}\), then we can
 * claim \(n\) to be prime for the currently used value of \(a\) when \(x_0 = a^d \equiv \pm 1 \text{ mod } n\) or when
 * \(x_i = a^{d2^i} \equiv -1 \text{ mod } n\) for \(i = 1, 2, 3, ..., s - 1\). We call such value \(a\) the
 * <i>Miller-Rabin nonwitness for</i> \(n\).
 * <br>If none of the above conditions are satisfied, then the current value \(a\) has just confirmed that \(n\) is for
 * sure a composite number. We call such value \(a\) the <i>Miller-Rabin witness for</i> \(n\).
 *
 * <br><br>Finally, the procedure for Miller-Rabin primality test to say if the odd integer \(n &gt; 2\) is prime, is
 * the following:
 * <br>1) Find the value of \(s &gt; 0\) and an odd \(d\) such that \(n - 1 = d \cdot 2^s\).
 * <br>2) Repeat for selected (or random) values of \(a\), such that \(1 &lt; a &lt; n - 1\) and \(a\) being relatively
 * prime to \(n\):
 * <ul><li>
 * Set \(x_0 = a^d\).
 * </li></ul>
 * <ul><li>
 * If \(x_0 \equiv \pm 1 \text{ mod } n\), then \(n\) is considered prime for this value of \(a\).
 * </li></ul>
 * <ul><li>
 * For \(i = 1, 2, ..., s - 1\):
 * <ul><li>
 * Set \(x_i = x_{i - 1}^2\).
 * </li></ul>
 * <ul><li>
 * If \(x_i \equiv -1 \text{ mod } n\), then \(n\) is considered prime for this value of \(a\).
 * </li></ul>
 * If there is no such \(i = 1, 2, ..., s - 1\) that \(x_i \equiv -1 \text{ mod } n\), then \(n\) is for sure a
 * composite number and we can stop the whole test at this point.
 * </li></ul>
 * 3) If for all tested values of \(a\), we considered \(n\) to be prime, then we claim that \(n\) is prime.
 *
 * <br><br>The accuracy of the Miller-Rabin primality test is equal to \(4^{-k}\), where \(k\) is the number of
 * values \(a\) that we have tested. It means that the probability of \(n\) being actually a prime number, after
 * performing the test on \(k\) values of \(a\) and claiming \(n\) to be prime in each of them, is
 * \(P = 1 - \frac{1}{4^k}\).
 *
 * <br><br>This algorithm can be made deterministic by trying all possible values of \(a\) below a certain
 * limit.
 * <br>It has been verified that:
 * <ul><li>
 * If \(n &lt; 2,047\), then it is enough to test \(a = 2\).
 * </li></ul>
 * <ul><li>
 * If \(n &lt; 1,373,653\), then it is enough to test \(a = 2, 3\).
 * </li></ul>
 * <ul><li>
 * If \(n &lt; 9,080,191\), then it is enough to test \(a = 31, 73\).
 * </li></ul>
 * <ul><li>
 * If \(n &lt; 25,326,001\), then it is enough to test \(a = 2, 3, 5\).
 * </li></ul>
 * <ul><li>
 * If \(n &lt; 3,215,031,751\), then it is enough to test \(a = 2, 3, 5, 7\).
 * </li></ul>
 * <ul><li>
 * If \(n &lt; 4,759,123,141\), then it is enough to test \(a = 2, 7, 61\).
 * </li></ul>
 * <ul><li>
 * If \(n &lt; 1,122,004,669,633\), then it is enough to test \(a = 2, 13, 23, 1662803\).
 * </li></ul>
 * <ul><li>
 * If \(n &lt; 2,152,302,898,747\), then it is enough to test \(a = 2, 3, 5, 7, 11\).
 * </li></ul>
 * <ul><li>
 * If \(n &lt; 3,474,749,660,383\), then it is enough to test \(a = 2, 3, 5, 7, 11, 13\).
 * </li></ul>
 * <ul><li>
 * If \(n &lt; 341,550,071,728,321\), then it is enough to test \(a = 2, 3, 5, 7, 11, 13, 17\).
 * </li></ul>
 * <ul><li>
 * If \(n &lt; 3,825,123,056,546,413,051\), then it is enough to test \(a = 2, 3, 5, 7, 11, 13, 17, 19, 23\).
 * </li></ul>
 * <ul><li>
 * If \(n &lt; 18,446,744,073,709,551,616 = 2^64\), then it is enough to test \(a = 2, 3, 5, 7, 11, 13, 17, 19, 23, 29,
 * 31, 37\).
 * </li></ul>
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test">Wikipedia - Miller–Rabin primality
 * test</a>
 * @see <a href="https://www.coursera.org/lecture/mathematical-foundations-cryptography/miller-rabin-MZtah">
 * Coursera - Miller–Rabin primality test</a>
 */
public class MillerRabinDeterministicPrimeNumberChecker {
    /**
     * Checks if the given integer \(x\) is prime using the determinitic version of Miller–Rabin primality test.
     * <br>For the detailed description of the implementation, see {@link MillerRabinDeterministicPrimeNumberChecker}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param n value of \(x\)
     *
     * @return True if \(x\) is a prime number, false otherwise.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test">Wikipedia - Miller–Rabin
     * primality test</a>
     * @see <a href="https://www.coursera.org/lecture/mathematical-foundations-cryptography/miller-rabin-MZtah">
     * Coursera - Miller–Rabin primality test</a>
     */
    public boolean isPrime( long n ) {
        //Miller-Rabin primality test is used for odd values of integer n > 2 thus the prechecks:
        if( n < 2 ) {
            return false;
        }
        if( n == 2 ) {
            return true;
        }
        if( n % 2 == 0 ) {
            return false;
        }

        return isPrimeUsingMillerRabinDeterministicAlgorithm( n );
    }

    private boolean isPrimeUsingMillerRabinDeterministicAlgorithm( long n ) {
        //The procedure for Miller-Rabin primality test to say if the odd integer n > 2 is prime:
        //1) Find the value of s > 0 and an odd d such that n − 1 = d*2^s
        //2) Repeat for selected (or random) values of a, such that 1 < a < n − 1 and a being relatively prime to n:
        //      Set x_0 = a^d.
        //      If x_0 = +-1 mod n, then n is considered prime for this value of a.
        //      For i = 1, 2, ..., s − 1:
        //          Set x_i = (x_{i - 1})^2.
        //          If x_i = −1 mod n, then n is considered prime for this value of a.
        //      If there is no such i = 1, 2, ..., s − 1 that x_i = −1 mod n, then n is for sure a composite number
        //      and we can stop the whole test at this point.
        //3) If for all tested values of a, we considered n to be prime, then we claim that n is prime.

        //1) Find the value of s > 0 and an odd d such that n − 1 = d*2^s
        int s = 0;
        long d = n - 1;
        while( d % 2 == 0 ) {
            s++;
            d /= 2;
        }

        //2) Repeat for selected (or random) values of a, such that 1 < a < n − 1 and a being relatively prime to n:
        loopA:
        for( long a : getTestValuesOfA( n ) ) {
            //Since all the values a are primes and they smaller than the value n they are returned for,
            //gcd(a, n) > 1 = a only if a divides n and thus n is not prime
            if( n % a == 0 ) {
                return false;
            }

            //From here we are sure that a,n are relatively prime

            //Set x_0 = a^d.
            long x_0 = powerModulo( a, d, n );

            if( x_0 == 1 || x_0 == ( n - 1 ) ) {
                //If x_0 = +-1 mod n, then n is considered prime for this value of a.
                //We continue testing for another value of a.
                continue loopA;
            }

            long x_i_1 = x_0; //x_{i - 1} initialized with x_0

            //For i = 1, 2, ..., s − 1:
            for( int i = 1; i < s; i++ ) {
                //Set x_i = (x_{i - 1})^2.
                long x_i = multiplyModulo( x_i_1, x_i_1, n );

                if( x_i == ( n - 1 ) ) {
                    //If x_i = −1 mod n, then n is considered prime for this value of a.
                    //We continue testing for another value of a.
                    continue loopA;
                }

                //Adjusting value for the next iteration
                x_i_1 = x_i;
            }

            //If there is no such i = 1, 2, ..., s − 1 that x_i = −1 mod n, then n is for sure a composite number
            //and we can stop the whole test at this point.
            return false;
        }

        //3) If for all tested values of a, we considered n to be prime, then we claim that n is prime.
        return true;
    }

    private List<Long> getTestValuesOfA( long n ) {
        //It has been verified that:
        if( n < 2047 ) {
            //If n < 2,047, then it is enough to test a = 2.
            return asList( 2L );
        }
        if( n < 1373653 ) {
            //If n < 1,373,653, then it is enough to test a = 2, 3.
            return asList( 2L, 3L );
        }
        if( n < 9080191 ) {
            //If n < 9,080,191, then it is enough to test a = 31, 73.
            return asList( 31L, 73L );
        }
        if( n < 25326001 ) {
            //If n < 25,326,001, then it is enough to test a = 2, 3, 5.
            return asList( 2L, 3L, 5L );
        }
        if( n < 3215031751L ) {
            //If n < 3,215,031,751, then it is enough to test a = 2, 3, 5, 7.
            return asList( 2L, 3L, 5L, 7L );
        }
        if( n < 4759123141L ) {
            //If n < 4,759,123,141, then it is enough to test a = 2, 7, 61.
            return asList( 2L, 7L, 61L );
        }
        if( n < 1122004669633L ) {
            //If n < 1,122,004,669,633, then it is enough to test a = 2, 13, 23, 1662803.
            return asList( 2L, 13L, 23L, 1662803L );
        }
        if( n < 2152302898747L ) {
            //If n < 2,152,302,898,747, then it is enough to test a = 2, 3, 5, 7, 11.
            return asList( 2L, 3L, 5L, 7L, 11L );
        }
        if( n < 3474749660383L ) {
            //If n < 3,474,749,660,383, then it is enough to test a = 2, 3, 5, 7, 11, 13.
            return asList( 2L, 3L, 5L, 7L, 11L, 13L );
        }
        if( n < 341550071728321L ) {
            //If n < 341,550,071,728,321, then it is enough to test a = 2, 3, 5, 7, 11, 13, 17.
            return asList( 2L, 3L, 5L, 7L, 11L, 13L, 17L );
        }
        if( n < 3825123056546413051L ) {
            //If n < 3,825,123,056,546,413,051, then it is enough to test a = 2, 3, 5, 7, 11, 13, 17, 19, 23.
            return asList( 2L, 3L, 5L, 7L, 11L, 13L, 17L, 19L, 23L );
        }

        //If n < 18,446,744,073,709,551,616 = 2^64, then it is enough to test
        //a = 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37.
        return asList( 2L, 3L, 5L, 7L, 11L, 13L, 17L, 19L, 23L, 29L, 31L, 37L );
    }
}
