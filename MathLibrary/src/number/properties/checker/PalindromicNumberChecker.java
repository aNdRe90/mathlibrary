package number.properties.checker;

import java.math.BigInteger;

/**
 * A palindromic number is a non-negative number that reads the same both ways e.g. \(5, 22, 737, 12321, 112211, ...\).
 * <br>So if \(x\) is the \(n\)-digit number \(d_0d_1d_2...d_{n - 1}\), then \(x\) is palindromic if \(d_0 = d_{n - 1},
 * d_1 = d_{n - 2}, d_2 = d_{n - 3}, ...\).
 * <br>More formally, the number \(d_0d_1d_2...d_{n - 1}\) is palindromic if
 * for \(0 \leq i &lt; \lfloor \frac{n}{2} \rfloor\) we have \(d_i = d_{n - 1 - i}\).
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Palindromic_number">Wikipedia - Palindromic number</a>
 */
public class PalindromicNumberChecker {
    /**
     * Checks if given number \(x\) is palindromic.
     * <br>For the detailed description of the implementation, see {@link PalindromicNumberChecker}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param x value of \(x\)
     *
     * @return True if \(x\) is a palindromic number, false otherwise.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Palindromic_number">Wikipedia - Palindromic number</a>
     */
    public boolean isPalindromic( long x ) {
        return isPalindromicString( Long.toString( x ) );
    }

    private boolean isPalindromicString( String number ) {
        //The n-digit number d_0d_1d_2...d_{n - 1} is palindromic if
        //for 0 <= i < floor(n/2) we have d_i = d_{n - 1 - i}.

        int n = number.length();
        for( int i = 0; i < n / 2; i++ ) {
            if( number.charAt( i ) != number.charAt( n - 1 - i ) ) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if given {@link BigInteger} \(x\) is palindromic.
     * <br>For the detailed description of the implementation, see {@link PalindromicNumberChecker}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param x value of \(x\)
     *
     * @return True if \(x\) is a palindromic number, false otherwise.
     *
     * @throws IllegalArgumentException when \(x\) is null
     * @see <a href="https://en.wikipedia.org/wiki/Palindromic_number">Wikipedia - Palindromic number</a>
     */
    public boolean isPalindromic( BigInteger x ) {
        if( x == null ) {
            throw new IllegalArgumentException( "Cannot check if null is a palindromic number." );
        }

        return isPalindromicString( x.toString() );
    }
}
