package function;

import number.representation.big.BigFraction;

import java.math.BigInteger;

import static java.lang.Math.incrementExact;
import static java.lang.Math.subtractExact;
import static number.Constants.E;

/**
 * The binomial theorem gives the coefficients of the expansion of powers of binomial expressions.
 * A binomial expression is simply the sum of two terms, such as \(x + y\) (the terms can be products of constants
 * and variables, but that does not concern us here).
 *
 * <br><br>When \((x + y)^3 = (x + y)(x + y)(x + y)\) is expanded  all products of a term in the first sum,
 * a term in the second sum, and a term in the third sum are added. Terms of the form \(x^3, x^2y, xy^2, y^3\) arise.
 * <br> To obtain a term of the form \(x^3\) , an \(x\) must be chosen in each of the sums, and this can be done in
 * only one way. Thus, the \(x^3\) term in the product has a coefficient of \(1\).
 * <br>To obtain a term of the form \(x^2y\), an \(x\) must be chosen in two of the three sums
 * (and consequently \(y\) in the other sum).
 *
 * <br><br>Binomial theorem states that if we define \({n \choose k}\) as a number of ways to pick \(k\) values from
 * \(n\) values, where order does not matter, then:
 * $$(x + y)^n = {n \choose 0}x^n + {n \choose 1}x^{n - 1}y + {n \choose 2}x^{n - 2}y^2 + ... +
 * {n \choose n - 1}xy^{n - 1} + {n \choose n}y^n = \sum_{i = 0}^{n} {n \choose i}x^{n - i}y^i$$
 *
 * <br>Binomial coefficient is any of the positive integers that occurs as a coefficient in the binomial theorem.
 * It is defined by \({n \choose k} = \frac{n!}{k!(n - k)!}\).
 *
 * <br><br>To calculate this value efficiently, the implementation uses the fact that:
 * $${n \choose k} = \frac{n!}{k!(n - k)!} =
 * \frac{n \cdot (n - 1) \cdot (n - 2) \cdot ... \cdot (n - k + 1)}{1 \cdot 2 \cdot 3 \cdot ... \cdot k} =
 * \prod_{i = 1}^{k} \frac{n - i + 1}{i}$$
 *
 * <br><br>The value of \({n \choose k}\) can be very big, even for {@link Integer} values of \(n, k\). That is why,
 * a limit is set in the implementation to prevent the calculations that will last for too long.
 * <br>This limit is set on the number of digits the binomial coefficient has and uses the approximation:
 * \({n \choose k} \leq (\frac{n \cdot e}{k})^k \).
 *
 * <br><br>Number of digits of \(x\) is equal to \(\lfloor \text{ log }_{10}{x} \rfloor + 1\), so the number of
 * digits of
 * \({n \choose k}\) is at most \(\lfloor \text{ log }_{10} {\frac{n \cdot e}{k})^k} \rfloor + 1 =
 * \lfloor k \cdot \text{ log }_{10}{ \frac{n \cdot e}{k} } \rfloor + 1\).
 * <br>\({n \choose k}\) will not be calculated by this class if
 * \(\lfloor k \cdot \text{ log }_{10}{ \frac{n \cdot e}{k} } \rfloor + 1\)
 * &gt; {@value BINOMIAL_COEFFICIENT_DIGITS_UPPER_LIMIT}.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Binomial_coefficient">Binomial coefficient</a>
 * @see <a href="https://en.wikipedia.org/wiki/Binomial_theorem">Binomial theorem</a>
 */
public class BinomialCoefficientFunction {
    /**
     * Upper limit for binomial coefficient digits. It is equal to {@value BINOMIAL_COEFFICIENT_DIGITS_UPPER_LIMIT}.
     */
    public static final int BINOMIAL_COEFFICIENT_DIGITS_UPPER_LIMIT = 20000;

    /**
     * Calculates the binomial coefficient value \({n \choose k}\).
     * <br>For the detailed description of the implementation, see {@link BinomialCoefficientFunction}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param n value of \(n\)
     * @param k value of \(k\)
     *
     * @return {@link BigInteger} value of \({n \choose k}\) or null if its upper approximation
     * \((\frac{n \cdot e}{k})^k\) has more than {@value BINOMIAL_COEFFICIENT_DIGITS_UPPER_LIMIT} digits.
     *
     * @throws IllegalArgumentException when \(n &lt; 0\) or \(n &lt; k\) or \(k &lt; 0\)
     * @see <a href="https://en.wikipedia.org/wiki/Binomial_coefficient">Binomial coefficient</a>
     * @see <a href="https://en.wikipedia.org/wiki/Binomial_theorem">Binomial theorem</a>
     */
    public BigInteger getBinomialCoefficient( int n, int k ) {
        if( n < k ) {
            throw new IllegalArgumentException( "n needs to be bigger than or equal to k" );
        }
        if( n < 0 || k < 0 ) {
            throw new IllegalArgumentException( "n and k must not be negative" );
        }
        if( k == 0 || n == k ) {
            return BigInteger.ONE;
        }
        if( ( n - 1 ) == k || k == 1 ) {
            return BigInteger.valueOf( n );
        }

        //binomialCoefficient(n, k) = binomialCoefficient(n, n - k)
        //Adjusting to make sure the second value is smaller (less than or equal to floor(n / 2))
        //The later number of iterations performed depends on it
        if( k > ( n / 2 ) ) {
            k = subtractExact( n, k );
        }

        //Setting a limit for allowed binomial coefficient value
        //The limit is the maximum number of digits the binomial coefficient can have
        //Checking this condition is based on the approximation: binomialCoefficient(n, k) <= ( n*e / k )^k

        //Formula for getting the number of digits:
        //floor(log10( ( n*e / k )^k )) + 1 = floor( k*log10( n*e / k ) ) + 1
        int binomialCoefficientDigitsUpperLimitApproximation = incrementExact(
                (int) ( k * Math.log10( ( E.doubleValue() * n ) / k ) ) );
        if( binomialCoefficientDigitsUpperLimitApproximation > BINOMIAL_COEFFICIENT_DIGITS_UPPER_LIMIT ) {
            //Calculation is forbidden if number of digits of ( n*e / k )^k exceeds the limit
            return null;
        }

        return calculateBinomialCoefficient( n, k );
    }

    private BigInteger calculateBinomialCoefficient( long n, long k ) {
        //binomialCoefficient(n, k) = n! / ( (k!(n - k)! ) = ( n*(n - 1)*(n - 2)*...*(n - k + 1) ) / k! =
        //= n/1 * (n - 1)/2 * (n - 2)/3 * ... * (n - k + 1)/k = prod_{i = 1}^{k}( (n - i + 1)/i )

        BigFraction bigFraction = BigFraction.ONE;
        for( long i = 1; i <= k; i++ ) {
            //Overflow safe because using long n, k to store integers
            bigFraction = bigFraction
                    .multiply( new BigFraction( BigInteger.valueOf( n - i + 1 ), BigInteger.valueOf( i ) ) );
        }

        bigFraction = bigFraction.reduce(); //Dividing numerator and denominator by gcd(numerator, denominator)
        return bigFraction.getNumerator(); //Denominator = 1
    }
}