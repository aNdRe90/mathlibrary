package function;

import algorithm.FactorizationAlgorithm;
import number.representation.FactorizationFactor;

import java.math.BigInteger;
import java.util.List;

import static java.lang.Math.incrementExact;
import static java.lang.Math.multiplyExact;
import static util.BasicOperations.abs;

/**
 * Divisor function is an arithmetic function related to the divisors of an integer \(n\) (divisors include \(1, n\)).
 * <br>The sum of positive divisors \(\sigma_x(n)\), for a real or complex number \(x\), is defined as the sum of the
 * \(x\)-th powers of the positive divisors of \(n\):
 * $$\sigma_x(n) = \sum_{d|n}{d^x}$$
 *
 * <br>This class allows for calculating \(\sigma_x(n)\) for non-negative integer values of \(x\). For \(x = 0\) we
 * get the number of divisors of \(n\) and for \(x = 1\) we get their sum.
 *
 * <br><br>In order to calculate the divisor function value we first need to factorize the number \(n\) which is to
 * represent it as a product of prime numbers: \(n = p_0^{e_0} \cdot p_1^{e_1} \cdot p_2^{e_2} \cdot ...
 * \cdot p_{k - 1}^{e_{k - 1}}\), where \(p_i &gt; p_{i - 1}, e_i &gt; 0\) (see {@link FactorizationAlgorithm} for more
 * details on how it is done).
 *
 * <br>The formula is then the following:
 * $$
 * \sigma_x(n) =
 * \begin{cases}
 * \prod_{i = 0}^{k - 1}(e_i + 1) \text{ for } x = 0 \\
 * \prod_{i = 0}^{k - 1}\frac{p_i^{(e_i + 1)x} - 1}{p_i^x - 1} \text{ for } x &gt; 0
 * \end{cases}
 * $$
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Divisor">Wikipedia - Divisor</a>
 * @see <a href="https://en.wikipedia.org/wiki/Divisor_function">Wikipedia - Divisor function</a>
 */
public class DivisorFunction {
    private FactorizationAlgorithm factorizationAlgorithm = new FactorizationAlgorithm();

    /**
     * Calculates the sum of divisors of given number \(n\). Divisors include \(1, n\) values. Uses the divisor function
     * \(\sigma_x(n) = \sum_{d|n}{d^x}\) for \(x = 1\).
     * <br>For the detailed description of the implementation, see {@link DivisorFunction}.
     *
     * @param n value of \(n\)
     *
     * @return Sum of divisors of \(n\).
     *
     * @throws IllegalArgumentException when \(n = 0\)
     * @see <a href="https://en.wikipedia.org/wiki/Divisor">Wikipedia - Divisor</a>
     * @see <a href="https://en.wikipedia.org/wiki/Divisor_function">Wikipedia - Divisor function</a>
     */
    public long getSumOfDivisors( long n ) {
        return getDivisorFunctionValue( n, 1 ).longValueExact();
    }

    /**
     * Calculates the divisor function \(\sigma_x(n) = \sum_{d|n}{d^x}\). Divisors include \(1, n\) values.
     * <br>For the detailed description of the implementation, see {@link DivisorFunction}.
     *
     * @param n value of \(n\)
     * @param x value of \(x\)
     *
     * @return {@link BigInteger} value of divisor function \(\sigma_x(n)\).
     *
     * @throws IllegalArgumentException when \(n = 0\) or \(x &lt; 0\)
     * @see <a href="https://en.wikipedia.org/wiki/Divisor">Wikipedia - Divisor</a>
     * @see <a href="https://en.wikipedia.org/wiki/Divisor_function">Wikipedia - Divisor function</a>
     */
    public BigInteger getDivisorFunctionValue( long n, int x ) {
        if( x < 0 ) {
            throw new IllegalArgumentException( "Cannot calculate divisor function value for negative exponent." );
        }

        if( n == 1 || n == -1 ) {
            //Handled separately because the result of divisor function is always 1 for n = 1 and factorization later on
            //cannot be done for n < 2
            return BigInteger.ONE;
        }
        if( x == 0 ) {
            //Formula for divisor function used here cannot be applied for x = 0 so use the one applied for getting
            //number of divisors
            return BigInteger.valueOf( getNumberOfDivisors( n ) );
        }

        //Divisor function sigma_x (n) =
        //(p_0^{(e_0 + 1)x} - 1) / (p_0^x - 1) * (p_1^{(e_1 + 1)x} - 1) / (p_1^x - 1) *
        //(p_2^{(e_2 + 1)x} - 1) / (p_2^x - 1) * ... * (p_{k - 1}^{(e_{k - 1} + 1)x} - 1) / (p_{k - 1}^x - 1),
        //where n = p_0^e_0 * p_1^e_1 * p_2^e_2 * ... * p_{k - 1}^e_{k - 1} is a prime factorization of n
        List<FactorizationFactor> factorization = factorizationAlgorithm.getFactorization( abs( n ) );

        BigInteger divisorFunction = BigInteger.ONE;
        for( FactorizationFactor factor : factorization ) {
            BigInteger p_i = BigInteger.valueOf( factor.getPrime() );
            int e_i = factor.getExponent();

            //multiplying by (p_i^{(e_i + 1)x} - 1) / (p_i^x - 1)
            divisorFunction = divisorFunction.multiply(
                    p_i.pow( multiplyExact( incrementExact( e_i ), x ) ).subtract( BigInteger.ONE )
                       .divide( p_i.pow( x ).subtract( BigInteger.ONE ) ) );
        }

        return divisorFunction;
    }

    /**
     * Calculates the number of divisors of given number \(n\). Divisors include \(1, n\) values. Uses the divisor
     * function \(\sigma_x(n) = \sum_{d|n}{d^x}\) for \(x = 0\).
     * <br>For the detailed description of the implementation, see {@link DivisorFunction}.
     *
     * @param n value of \(n\)
     *
     * @return Number of divisors of \(n\).
     *
     * @throws IllegalArgumentException when \(n = 0\)
     * @see <a href="https://en.wikipedia.org/wiki/Divisor">Wikipedia - Divisor</a>
     * @see <a href="https://en.wikipedia.org/wiki/Divisor_function">Wikipedia - Divisor function</a>
     */
    public long getNumberOfDivisors( long n ) {
        if( n == 1 || n == -1 ) {
            //Handled separately because 1 has only one divisor equal to 1 and factorization later on
            //cannot be done for n < 2
            return 1;
        }

        //The case where x = 0 so divisor function sigma_0 (n) represents number of divisors of value n
        //The formula for that is sigma_0 (n) = (e_0 + 1) * (e_1 + 1) * (e_2 + 1) * ... * (e_{k - 1} + 1),
        //where n = p_0^e_0 * p_1^e_1 * p_2^e_2 * ... * p_{k - 1}^e_{k - 1} is a prime factorization of n
        List<FactorizationFactor> factorization = factorizationAlgorithm.getFactorization( abs( n ) );

        long numberOfDivisors = 1;
        for( FactorizationFactor factor : factorization ) {
            int e_i = factor.getExponent();
            numberOfDivisors = multiplyExact( numberOfDivisors, incrementExact( e_i ) );
        }

        return numberOfDivisors;
    }
}
