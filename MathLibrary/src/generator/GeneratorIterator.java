package generator;

import java.util.Iterator;

/**
 * Interface of the {@link Iterator} used by the {@link Generator} instance.
 *
 * @param <T> type of object that is being iterated
 *
 * @author Andrzej Walkowiak
 */
public interface GeneratorIterator<T> extends Iterator<T> {
    /**
     * Initializes the iterator so that next iteration can be done in exactly the same way as the first one.
     */
    void initialize();
}
