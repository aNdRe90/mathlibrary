package generator;

import java.util.Iterator;

/**
 * The abstract class for the generators. Defines their general behaviour which is to be .
 * Generators are {@link Iterable} objects thus can be use in for each loops. They allows for iterating through
 * sequence of values created on the fly.
 *
 * @param <T> type of object that is being generated
 *
 * @author Andrzej Walkowiak
 */
public abstract class Generator<T> implements Iterable<T> {
    private GeneratorIterator<T> iterator = createIterator();

    @Override
    public Iterator<T> iterator() {
        iterator.initialize();
        return iterator;
    }

    /**
     * Creates the instance of {@link GeneratorIterator} that will be used by this generator. The potential setup of
     * the iterator should not be done in its constructor but in the {@link GeneratorIterator#initialize()} method.
     *
     * @return {@link GeneratorIterator} instance
     */
    protected abstract GeneratorIterator<T> createIterator();
}
