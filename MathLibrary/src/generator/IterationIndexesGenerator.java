package generator;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static java.lang.Math.incrementExact;

/**
 * Generator of iteration indexes. It basically serves the purpose of performing multiple iterations dynamically.
 *
 * <br><br>For example \(n\) iterations are coded statically in this way:
 *
 * <br>for( int \(i_0 = 0\); \(i_0 &lt; r_0\); \(i_0\)++ ) {
 * <br>&emsp;for( int \(i_1 = 0\); \(i_1 &lt; r_1\); \(i_1\)++ ) {
 * <br>&emsp; &emsp;for( int \(i_2 = 0\); \(i_2 &lt; r_2\); \(i_2\)++ ) {
 * <br>&emsp; &emsp; &emsp; ...
 * <br>&emsp; &emsp; &emsp; &emsp; for( int \(i_{n - 1} = 0\); \(i_{n - 1} &lt; r_{n - 1}\); \(i_{n - 1}\)++ ) { ... }
 * <br>&emsp; &emsp; }
 * <br>&emsp; }
 * <br>}
 *
 * <br><br>
 * Using this generator you can make the same dynamically. The single iteration over this {@link Iterable} generator
 * will return a {@link List} of values \([i_0, i_1, i_2, ..., i_{n - 1}]\) for the given ranges
 * \(r_0, r_1, r_2, ..., r_{n - 1}\).
 * <br>Consequently, the number of generator iterations is equal to
 * \(r_0 \cdot r_1 \cdot r_2 \cdot ... \cdot r_{n - 1}\).
 *
 * <br>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 */
public class IterationIndexesGenerator extends Generator<List<Integer>> {
    private int[] ranges;

    /**
     * Creates the iteration generator for the given ranges.
     * <br>For the detailed description of the implementation, see {@link IterationIndexesGenerator}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param ranges ranges \(r_0, r_1, r_2, ..., r_{n - 1}\)
     *
     * @throws IllegalArgumentException when {@code ranges} is null, empty or if any value in {@code ranges} is less
     *                                  than \(1\)
     */
    public IterationIndexesGenerator( int... ranges ) {
        if( ranges == null || ranges.length == 0 ) {
            throw new IllegalArgumentException( "Given ranges are null or empty." );
        }

        for( int range : ranges ) {
            if( range < 1 ) {
                throw new IllegalArgumentException( "Range must be a positive value." );
            }
        }

        this.ranges = ranges;
    }

    @Override
    protected GeneratorIterator<List<Integer>> createIterator() {
        return new IterationIndexesGeneratorIterator();
    }

    private class IterationIndexesGeneratorIterator implements GeneratorIterator<List<Integer>> {
        private List<Integer> currentIndexes; //holds the indexes [i_0, i_1, i_2, ..., i_{n - 1}]
        private boolean hasNext = true; //true because ranges are not empty and values in it are > 0, so at least one
        //generator iteration is guaranteed

        @Override
        public void initialize() {
            //Initialized with indexes [0, 0, 0, ..., -1] so that the first returned indexes are [0, 0, 0, ..., 0]
            currentIndexes = new ArrayList<>( ranges.length );
            for( int i = 0; i < ranges.length - 1; i++ ) {
                currentIndexes.add( 0 );
            }
            currentIndexes.add( -1 );

            hasNext = true; //true because ranges are not empty and values in it are > 0, so at least one generator
            //iteration is guaranteed
        }

        @Override
        public boolean hasNext() {
            return hasNext;
        }

        @Override
        public List<Integer> next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            for( int j = currentIndexes.size() - 1; j >= 0; j-- ) {
                currentIndexes.set( j, incrementExact( currentIndexes.get( j ) ) );

                if( currentIndexes.get( j ) == ranges[j] ) {
                    //If index i_j was already of max value (r_j - 1), reset it and move to increasing index i_{j - 1}
                    currentIndexes.set( j, 0 );
                }
                else {
                    //Stop if i_j has not yet reached its max value after the incrementation
                    break;
                }
            }

            hasNext = !isLastIndexes( currentIndexes );
            return currentIndexes;
        }


        private boolean isLastIndexes( List<Integer> indexes ) {
            //Last indexes [i_0, i_1, i_2, ..., i_{n - 1}] are equal to [r_0 - 1, r_1 - 1, r_2 - 1, ..., r_{n - 1} - 1]

            for( int i = 0; i < indexes.size(); i++ ) {
                if( indexes.get( i ) != ranges[i] - 1 ) {
                    return false;
                }
            }

            return true;
        }
    }
}
