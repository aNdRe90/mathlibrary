package generator;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static java.lang.Math.*;
import static util.BasicOperations.factorial;

/**
 * A permutation of a set \(S\) containing distinct objects is an ordered arrangement of all these objects.
 * <br>e.g. permutations of set \(S = \{0, 1, 2\}\) are the following: \((0, 1, 2), (0, 2, 1), (1, 0, 2), (1, 2, 0),
 * (2, 0, 1), (2, 1, 0)\).
 * <br>The total number of permutations for a set of \(n\) objects is equal to \(n!\).
 *
 * <br><br>The example above listed the permutations in lexicographic order. In such order, the first permutation of
 * set \(S = \{0, 1, 2, 3, ..., n - 1\}\) is \((0, 1, ..., n − 1)\) and the last permutation is \((n − 1, ..., 1, 0)\).
 * A permutation \(A = (a_0, a_1, a_2, ..., a_{n - 1})\) precedes the permutation \(B = (b_0, b_1, b_2, ...,
 * b_{n - 1})\) if for some \(0 \leq k &lt; n\), we have \(a_0 = b_0, a_1 = b_1, ..., a_{k - 1} = b_{k - 1}\)
 * and \(a_k &lt; b_k\).
 * <br>The permutation \((1, 2, 3, 0, 4)\) of the set \(S = \{0, 1, 2, 3, 4\}\) precedes the permutation
 * \((1, 2, 4, 0, 3)\), \((3, 0, 4, 2, 1)\) precedes \((4, 1, 0, 3, 2)\), \((2, 0, 3, 4, 1)\) precedes
 * \((2, 4, 0, 1, 3)\) etc.
 *
 * <br><br>The procedure for generating permutations \((a_0, a_1, a_2, ..., a_{n - 1})\) of a set \(S = \{0, 1,
 * 2, ..., n - 1\}\), in lexicographic order, is the following:
 * <br>1) Create a first permutation \(A = \{0, 1, 2, ..., n - 1\}\).
 * <br>2) Loop:
 * <ul><li>Find the last element \(a_i\) in the sequence such that \(a_i &lt; a_{i + 1}\) and \(a_{i + 1} &gt;
 * a_{i + 2} &gt; a_{i + 3} &gt; ... &gt; a_{n - 1}\). If there is no such element, stop the loop.
 * </li></ul>
 * <ul><li>
 * Find the smallest \(a_j &gt; a_i\) among \(a_{i + 1}, a_{i + 2}, ..., a_{n - 1}\).
 * </li></ul>
 * <ul><li>
 * Swap the values \(a_i\) and \(a_j\). \(a_j\) value is now at \(i\)-th position and \(a_i\) value at \(j\)-th
 * position.
 * </li></ul>
 * <ul><li>
 * List the values \(a_{i + 1}, a_{i + 2}, ..., a_{n - 1}\) in increasing order.
 * </li></ul>
 * <ul><li>
 * \(A\) is now the next permutation.
 * </li></ul>
 *
 * <br><br>Let us show an example of generating all permutations of a set \(S = \{0, 1, 2, 3\}\).
 * Left side contains permutations before changing to the next one, right side contains the next permutation.
 * <br>We will mark the position of last \(a_i\) in the sequence such that \(a_i &lt; a_{i + 1}\) in
 * \(\color{blue}{\text{blue}}\).
 * Smallest \(a_j &gt; a_i\) among \(a_{i + 1}, a_{i + 2}, ..., a_{n - 1}\) will be marked \(\color{red}{\text{red}}\)
 * and the values \(a_{i + 1}, a_{i + 2}, ..., a_{n - 1}\) in next permutations will be marked
 * \(\color{green}{\text{green}}\).
 *
 * \begin{align}
 * \{0, 1, \color{blue}{2}, \color{red}{3}\} &amp; \rightarrow \{0, 1, \color{blue}{3}, \color{green}{2}\} \\
 * \{0, \color{blue}{1}, 3, \color{red}{2}\} &amp; \rightarrow \{0, \color{blue}{2}, \color{green}{1},
 * \color{green}{3}\} \\
 * \{0, 2, \color{blue}{1}, \color{red}{3}\} &amp; \rightarrow \{0, 2, \color{blue}{3}, \color{green}{1}\} \\
 * \{0, \color{blue}{2}, \color{red}{3}, 1\} &amp; \rightarrow \{0, \color{blue}{3}, \color{green}{1},
 * \color{green}{2}\} \\
 * \{0, 3, \color{blue}{1}, \color{red}{2}\} &amp; \rightarrow \{0, 3, \color{blue}{2}, \color{green}{1}\} \\
 * \{\color{blue}{0}, 3, 2, \color{red}{1}\} &amp; \rightarrow \{\color{blue}{1}, \color{green}{0}, \color{green}{2},
 * \color{green}{3}\} \\
 * \\
 * \{1, 0, \color{blue}{2}, \color{red}{3}\} &amp; \rightarrow \{1, 0, \color{blue}{3}, \color{green}{2}\} \\
 * \{1, \color{blue}{0}, 3, \color{red}{2}\} &amp; \rightarrow \{1, \color{blue}{2}, \color{green}{0},
 * \color{green}{3}\} \\
 * \{1, 2, \color{blue}{0}, \color{red}{3}\} &amp; \rightarrow \{1, 2, \color{blue}{3}, \color{green}{0}\} \\
 * \{1, \color{blue}{2}, \color{red}{3}, 0\} &amp; \rightarrow \{1, \color{blue}{3}, \color{green}{0},
 * \color{green}{2}\} \\
 * \{1, 3, \color{blue}{0}, \color{red}{2}\} &amp; \rightarrow \{1, 3, \color{blue}{2}, \color{green}{0}\} \\
 * \{\color{blue}{1}, 3, \color{red}{2}, 0\} &amp; \rightarrow \{\color{blue}{2}, \color{green}{0}, \color{green}{1},
 * \color{green}{3}\} \\
 * \\
 * \{2, 0, \color{blue}{1}, \color{red}{3}\} &amp; \rightarrow \{2, 0, \color{blue}{3}, \color{green}{1}\} \\
 * \{2, \color{blue}{0}, 3, \color{red}{1}\} &amp; \rightarrow \{2, \color{blue}{1}, \color{green}{0},
 * \color{green}{3}\} \\
 * \{2, 1, \color{blue}{0}, \color{red}{3}\} &amp; \rightarrow \{2, 1, \color{blue}{3},
 * \color{green}{0}\} \\
 * \{2, \color{blue}{1}, \color{red}{3}, 0\} &amp; \rightarrow \{2, \color{blue}{3}, \color{green}{0},
 * \color{green}{1}\} \\
 * \{2, 3, \color{blue}{0}, \color{red}{1}\} &amp; \rightarrow \{2, 3, \color{blue}{1},
 * \color{green}{0}\} \\
 * \{\color{blue}{2}, \color{red}{3}, 1, 0\} &amp; \rightarrow \{\color{blue}{3}, \color{green}{0}, \color{green}{1},
 * \color{green}{2}\} \\
 * \\
 * \{3, 0, \color{blue}{1}, \color{red}{2}\} &amp; \rightarrow \{3, 0, \color{blue}{2},
 * \color{green}{1}\} \\
 * \{3, \color{blue}{0}, 2, \color{red}{1}\} &amp; \rightarrow \{3, \color{blue}{1}, \color{green}{0},
 * \color{green}{2}\} \\
 * \{3, 1, \color{blue}{0}, \color{red}{2}\} &amp; \rightarrow \{3, 1, \color{blue}{2},
 * \color{green}{0}\} \\
 * \{3, \color{blue}{1}, \color{red}{2}, 0\} &amp; \rightarrow \{3, \color{blue}{2}, \color{green}{0},
 * \color{green}{1}\} \\
 * \{3, 2, \color{blue}{0}, \color{red}{1}\} &amp; \rightarrow \{3, 2, \color{blue}{1},
 * \color{green}{0}\}
 * \end{align}
 *
 * <br>The example above shows the creation of all permutations of a set \(4\)-element set \(S\). Total number of
 * permutations is equal to \(4! = 4 \cdot 3 \cdot 2 \cdot 1 = 24\). Indeed we showed 24 different
 * permutations generated in lexicographic order.
 *
 * <br><br>From the implementation point of view, this generator accepts a set \(S\) of size \(n\) in a form of a
 * {@link List} containing any type of element. The generator enables us to iterate through every permutation
 * {@link List} of \(S\).
 * <br>To do that, it generates (in lexicographic order) index permutations \(\{a_0, a_1, a_2, ..., a_{n - 1}\}\),
 * where \(0 \leq a_i &lt; n\).
 * <br>For example, for permutations of \(S = \{\text{"a"}, \text{"b"}, \text{"c"}\}\), the generator
 * generates the indexes \(\{0, 1, 2\}, \{0, 2, 1\}, \{1, 0, 2\}, \{1, 2, 0\}, \{2, 0, 1\}, \{2, 1, 0\}\) to return
 * \(\{\text{"a"}, \text{"b"}, \text{"c"}\}, \{\text{"a"}, \text{"c"}, \text{"b"}\},
 * \{\text{"b"}, \text{"a"}, \text{"c"}\}, \{\text{"b"}, \text{"c"}, \text{"a"}\},
 * \{\text{"c"}, \text{"a"}, \text{"b"}\}, \{\text{"c"}, \text{"b"}, \text{"a"}\}\) accordingly.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @param <T> type of elements included in set \(S\)
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Permutation">Wikipedia - Permutation</a>
 * @see <a href="http://www2.fiit.stuba.sk/~kvasnicka/Mathematics%20for%20Informatics/Rosen_Discrete_Mathematics_and_Its_Applications_7th_Edition.pdf">
 * Kenneth H. Rosen - "Discrete mathematics and its applications (7th edition)"</a>
 */
public class PermutationGenerator<T> extends Generator<List<T>> {
    private List<T> elements;
    private long totalNumberOfPermutations;
    private int n;

    /**
     * Creates the permutation generator for the given elements.
     * <br>A lot of permutations can sometimes be generated as their number is equal to
     * \(n! = 1 \cdot 2 \cdot 3 \cdot ... \cdot n\), where \(n\) is the size of {@code elements}.
     *
     * <br><br>For the detailed description of the implementation, see {@link PermutationGenerator}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param elements given elements, from which the permutation {@link List}s will be generated
     *
     * @throws IllegalArgumentException when {@code elements} is null or its size \(n\) does not satisfy
     *                                  \(1 &lt; n &lt; 21\)
     * @see <a href="https://en.wikipedia.org/wiki/Permutation">Wikipedia - Permutation</a>
     * @see <a href="http://www2.fiit.stuba.sk/~kvasnicka/Mathematics%20for%20Informatics/Rosen_Discrete_Mathematics_and_Its_Applications_7th_Edition.pdf">
     * Kenneth H. Rosen - "Discrete mathematics and its applications (7th edition)"</a>
     */
    public PermutationGenerator( List<T> elements ) {
        if( elements == null ) {
            throw new IllegalArgumentException( "Elements cannot be null." );
        }

        if( elements.size() < 2 || elements.size() > 20 ) {
            throw new IllegalArgumentException( "Incorrect size of given elements. Size n must satisfy 1 < n < 21." );
        }

        this.n = elements.size();
        this.totalNumberOfPermutations = factorial( n ).longValueExact();
        this.elements = elements;
    }

    @Override
    protected GeneratorIterator<List<T>> createIterator() {
        return new PermutationIterator();
    }

    private class PermutationIterator implements GeneratorIterator<List<T>> {
        private int[] indexPermutation;
        private long permutationsGenerated;

        @Override
        public void initialize() {
            indexPermutation = null;
            permutationsGenerated = 0;
        }

        @Override
        public boolean hasNext() {
            return permutationsGenerated < totalNumberOfPermutations;
        }

        @Override
        public List<T> next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            permutationsGenerated++;

            if( indexPermutation == null ) {
                indexPermutation = getStartingIndexPermutation();
            }
            else {
                transformToNextIndexPermutation( indexPermutation );
            }

            return getElementsPermutation( indexPermutation );
        }

        private void transformToNextIndexPermutation( int[] A ) {
            int n = A.length;

            //Generating next permutation from permutation A = {a_0, a_1, a_2, ..., a_{n - 1}}
            //of a set S = {0, 1, 2, ..., n - 1} is done in the the following way:
            //1) Find the last element a_i in the sequence such that a_i < a_{i + 1} and
            //a_{i + 1} > a_{i + 2} > a_{i + 3} > ... > a_{n - 1}.
            //2) Find the smallest a_j > a_i among a{i + 1}, a{i + 2}, ..., a{n - 1}.
            //3) Swap the values a_i and a_j. a_j value is now at i-th position and a_i value at j-th position.
            //4) List the values a{i + 1}, a{i + 2}, ..., a{n - 1} in increasing order.
            //More detailed description available in
            //Kenneth H. Rosen - "Discrete mathematics and its applications (7th edition)"
            //chapter "Generating Permutations and Combinations"

            //1) Find the last element a_i in the sequence such that a_i < a_{i + 1} and
            //a_{i + 1} > a_{i + 2} > a_{i + 3} > ... > a_{n - 1}.

            //We know that all the values in the arrays are different, so there is no worry about equality condition
            int i = subtractExact( n, 2 );
            while( A[i] > A[incrementExact( i )] ) {
                i = decrementExact( i );
            }

            //2) Find the smallest a_j > a_i among a{i + 1}, a{i + 2}, ..., a{n - 1}.

            //Keep in mind that a_{i + 1} > a_{i + 2} > a_{i + 3} > ... > a_{n - 1}, so the first value a_j > a_i
            //when iterating over j = n - 1, n - 2, n - 3, ... is the smallest one
            //We know that all the values in the arrays are different, so there is no worry about equality condition
            int j = decrementExact( n );
            while( A[i] > A[j] ) {
                j = decrementExact( j );
            }

            //3) Swap the values a_i and a_j. a_j value is now at i-th position and a_i value at j-th position.
            int temp = A[i];
            A[i] = A[j];
            A[j] = temp;

            //4) List the values a{i + 1}, a{i + 2}, ..., a{n - 1} in increasing order.

            //Even after swapping a_i with a_j in 3) new values a{i + 1}, a{i + 2}, ..., a{n - 1} satisfy:
            //a_{i + 1} > a_{i + 2} > a_{i + 3} > ... > a_{n - 1}
            //It is because we replaced a_j with smaller value a_i which was still greater than a_{j + 1}

            //Thanks to that, sorting current a_{i + 1} > a_{i + 2} > a_{i + 3} > ... > a_{n - 1} from smallest value to
            //greatest can be done by swapping values at positions:
            //i + 1 with n - 1
            //i + 2 with n - 2
            //etc.
            int s = incrementExact( i ); //i + 1 index
            int r = decrementExact( n ); //n - 1 index
            while( r > s ) {
                temp = A[s];
                A[s] = A[r];
                A[r] = temp;
                s = incrementExact( s );
                r = decrementExact( r );
            }
        }

        private List<T> getElementsPermutation( int[] indexPermutation ) {
            //Returns permutation of elements at the given index positions

            List<T> elementsPermutation = new ArrayList<>( indexPermutation.length );
            for( int i = 0; i < indexPermutation.length; i++ ) {
                elementsPermutation.add( elements.get( indexPermutation[i] ) );
            }
            return elementsPermutation;
        }

        private int[] getStartingIndexPermutation() {
            //First index permutation is equal to [0, 1, 2, ..., n - 1]

            int[] startingIndexPermutation = new int[n];
            for( int i = 0; i < n; i++ ) {
                startingIndexPermutation[i] = i;
            }
            return startingIndexPermutation;
        }
    }
}
