package generator;

import algorithm.NextPrimeAlgorithm;

import java.util.NoSuchElementException;

/**
 * Generator of prime numbers. It allows for generating primes in the specified interval.
 * It uses {@link NextPrimeAlgorithm} to obtain the first prime number coming after the given value.
 * <br>e.g. for interval \([2, 20]\) the following primes are generated: \(2, 3, 5, 7, 11, 13, 17, 19\).
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Prime_number">Wikipedia - Prime number</a>
 */
public class PrimeGenerator extends Generator<Long> {
    /**
     * Maximum prime value for {@link Long} data type.
     */
    public static final long MAX_LONG_PRIME = 9223372036854775783L;

    private long minimumPrime;
    private long maximumPrime;

    /**
     * Creates the infinite prime generator. It generates all the primes of {@link Long} data type:
     * \(2, 3, 5, 7, ..., \) {@value MAX_LONG_PRIME}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Prime_number">Wikipedia - Prime number</a>
     */
    public PrimeGenerator() {
        this( Long.MAX_VALUE );
    }

    /**
     * Creates the prime generator with the given upper limit. It generates all the primes from the interval
     * [2, {@code maximumPrime}].
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param maximumPrime maximum prime that can be generated
     *
     * @throws IllegalArgumentException when {@code maximumPrime} &lt; \(2\)
     * @see <a href="https://en.wikipedia.org/wiki/Prime_number">Wikipedia - Prime number</a>
     */
    public PrimeGenerator( long maximumPrime ) {
        this( 2, maximumPrime );
    }

    /**
     * Creates the prime generator with the given limits. It generates all the primes from the interval
     * [{@code minimumPrime}, {@code maximumPrime}].
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param minimumPrime minimum prime that can be generated
     * @param maximumPrime maximum prime that can be generated
     *
     * @throws IllegalArgumentException when {@code maximumPrime} &lt; {@code minimumPrime}
     * @see <a href="https://en.wikipedia.org/wiki/Prime_number">Wikipedia - Prime number</a>
     */
    public PrimeGenerator( long minimumPrime, long maximumPrime ) {
        if( maximumPrime < minimumPrime ) {
            throw new IllegalArgumentException( "Upper limit cannot be smaller than the lower limit." );
        }

        this.minimumPrime = minimumPrime;
        this.maximumPrime = maximumPrime;
    }

    @Override
    protected GeneratorIterator<Long> createIterator() {
        return new PrimeGeneratorIterator();
    }

    private class PrimeGeneratorIterator implements GeneratorIterator<Long> {
        private long nextPrime;
        private NextPrimeAlgorithm nextPrimeAlgorithm = new NextPrimeAlgorithm();

        @Override
        public void initialize() {
            nextPrime = getFirstIntervalPrime();
        }

        @Override
        public boolean hasNext() {
            //nextPrime == -1 if already all the Long primes were generated, we needs to stop then
            return nextPrime >= minimumPrime && nextPrime <= maximumPrime && nextPrime != -1;
        }

        private long getFirstIntervalPrime() {
            if( minimumPrime == Long.MIN_VALUE ) {
                return 2;
            }

            //Subtracting 1 because minimumPrime can be prime as well and then it is the first interval prime
            return getNextPrime( minimumPrime - 1 );
        }

        @Override
        public Long next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            long currentPrime = nextPrime;
            nextPrime = getNextPrime( nextPrime );
            return currentPrime;
        }


        private long getNextPrime( long n ) {
            if( n == MAX_LONG_PRIME ) {
                return -1; //reached maximum Long prime already, cannot generate more
            }

            return nextPrimeAlgorithm.getNextPrime( n );
        }
    }
}
