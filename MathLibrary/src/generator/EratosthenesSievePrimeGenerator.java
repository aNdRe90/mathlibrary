package generator;

import java.util.NoSuchElementException;

import static java.lang.Math.incrementExact;
import static java.lang.Math.sqrt;

/**
 * The Sieve of Eratosthenes is a simple and efficient method of generating prime numbers smaller than or equal to a
 * given number. It is one of the most efficient ways to find all of the smaller primes.
 * However it requires some memory space to work with, so its usage is limited.
 * <br>Finding primes less than or equal to \(x\) is done by creating a sieve of size \(x\). At the \(i\)-position
 * the sieve will hold the true/false value, depending on whether \(i\) is prime (sieve[\(i\)] = false) or not
 * (sieve[\(i\)] = true).
 *
 * <br><br>The sieve for generating primes less than or equal to \(x\) is initialized in a following way:
 * <br>1) Set sieve[1] = true.
 * <br>2) Set primeSearchStart = 1.
 * <br>3) Loop:
 * <ul><li>
 * Find first prime \(p\) which is the smallest \(n\) such that primeSearchStart &lt; \(n \leq \lfloor \sqrt{x}
 * \rfloor\) and sieve[\(n\)] = false. If there is none, stop the loop.
 * </li></ul>
 * <ul><li>
 * Set sieve[\(p \cdot j\)] = true for \(1 \leq j \leq \lfloor \frac{x}{p} \rfloor\).
 * </li></ul>
 * <ul><li>
 * Set primeSearchStart = \(p\).
 * </li></ul>
 * After that, if sieve[\(i\)] = false, then \(i\) is a prime number.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes">Sieve of Eratosthenes</a>
 */
public class EratosthenesSievePrimeGenerator extends Generator<Integer> {
    /**
     * Maximum sieve size. It is equal to {@value MAX_SIEVE_SIZE}.
     */
    public static final int MAX_SIEVE_SIZE = 100000000;

    private int sieveSize;

    /**
     * Creates the Eratosthenes Sieve generator with the given size. It is able to generate primes up to the value of
     * the sieve size.
     * <br>The sieve is not allocated and initialized in this constructor but in the
     * {@link GeneratorIterator#initialize()} method during the start of the iteration.
     * <br>There is a limit for the {@code sieveSize}, so that the generator does not take too much of a memory to
     * create the sieve. The maximum value is {@value MAX_SIEVE_SIZE}.
     *
     * <br><br>For the detailed description of the implementation, see {@link EratosthenesSievePrimeGenerator}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param sieveSize sieve size
     *
     * @throws IllegalArgumentException when {@code sieveSize} \(&lt; 2\) or
     *                                  {@code sieveSize} &gt; {@value MAX_SIEVE_SIZE}
     * @see <a href="https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes">Sieve of Eratosthenes</a>
     */
    public EratosthenesSievePrimeGenerator( int sieveSize ) {
        if( sieveSize < 2 || sieveSize > MAX_SIEVE_SIZE ) {
            throw new IllegalArgumentException( "Sieve size must be > 2 and <= " + MAX_SIEVE_SIZE );
        }

        this.sieveSize = sieveSize;
    }

    @Override
    protected GeneratorIterator<Integer> createIterator() {
        return new EratosthenesSievePrimeIterator();
    }

    private class EratosthenesSievePrimeIterator implements GeneratorIterator<Integer> {
        private boolean[] sieve;
        private int nextPrime;

        //Will make sure sieve is initialized only once for the iterator instance
        private boolean sieveInitialized = false;

        @Override
        public void initialize() {
            //Initializing the iterator for a new iteration requires updating the nextPrime and initializing sieve
            //values if it has not been done already for this instance

            nextPrime = 2;
            if( !sieveInitialized ) {
                initializeSieve();
            }
        }

        @Override
        public boolean hasNext() {
            return nextPrime != -1;
        }

        private void initializeSieve() {
            //Initializing the sieve so that all values i such that sieve[i] = false are prime numbers

            sieve = new boolean[incrementExact( sieveSize )];
            sieve[0] = sieve[1] = true;

            int sqrtSieveSize = (int) sqrt( sieveSize ); //Handling primes up to floor(sqrt(sieveSize))

            for( int prime = 2; prime <= sqrtSieveSize && prime != -1; prime = getNextPrime( prime ) ) {
                for( int i = prime + prime; i <= sieveSize; i += prime ) {
                    //Setting sieve[p*j] = true for indexes being multiples of prime p (not including p) to mark them
                    //as not prime
                    sieve[i] = true;
                }
            }

            sieveInitialized = true; //So that the sieve is filled once for one instance of the generator
        }

        @Override
        public Integer next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            int currentPrime = nextPrime;

            //Calculating one prime ahead so that hasNext() does not make any assignment
            nextPrime = getNextPrime( currentPrime );

            return currentPrime;
        }


        private int getNextPrime( int currentPrime ) {
            //First value i > currentPrime for which sieve[i] = false is the next prime number

            for( int i = currentPrime + 1; i <= sieveSize; i++ ) {
                if( !sieve[i] ) {
                    return i;
                }
            }

            return -1; //Returns -1 if no more primes available in the sieve. It will stop the iteration.
        }
    }
}
