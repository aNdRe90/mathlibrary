package generator;

import function.BinomialCoefficientFunction;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static java.lang.Math.*;

/**
 * A \(r\)-combination of elements of a set \(S\) is an unordered selection of \(r\) elements from the set.
 * In other words, it is a subset of the set \(S\) with \(r\) elements.
 * <br>The number of \(r\)-combinations of a set containing \(n\) distinct elements is denoted by \(C(n,r)\). \(C(n,r)\)
 * is also denoted by \({n \choose r}\) and is called a binomial coefficient. See {@link BinomialCoefficientFunction}
 * for more details.
 * <br>For example, there are \({4 \choose 2} = 6\) \(2\)-combinations of the set \(S = \{0, 1, 2, 3\}\), and they
 * are the following: \(\{0,1\}, \{0,2\}, \{0,3\}, \{1,2\}, \{1,3\}, \{2,3\}\).
 *
 * <br><br>The \(r\)-combinations can be listed using lexicographic order, which is what we did in the example above.
 * In the lexicographic ordering, the first \(r\)-combination of set \(S = \{0, 1, 2, 3, ..., n - 1\}\) is
 * \(\{0, 1, ..., r - 2, r − 1\}\) and the last \(r\)-combination is \(\{n − r, n − r + 1, ..., n - 2, n − 1\}\).
 * <br>A combination \(A = \{a_0, a_1, a_2, ..., a_{r - 1}\}\) precedes \(B = \{b_0, b_1, b_2, ..., b_{r - 1}\}\) if the
 * smallest value from \(A\) that does not appear in \(B\) is less than the smallest value from \(B\) that does not
 * appear in \(A\).
 * <br>e.g. Considering \(3\)-combinations of \(S = \{0, 1, 2, 3, 4, 5\}\), \(\{1, 3, 4\}\) precedes \(\{2, 4, 5\}\),
 * \(\{0, 3, 5\}\) precedes \(\{1, 2, 4\}\), \(\{0, 2, 3\}\) precedes \(\{0, 2, 5\}\) etc.
 *
 * <br><br>The procedure for generating \(r\)-combinations \(A = \{a_0, a_1, a_2, ..., a_{r - 1}\}\) of a set
 * \(S = \{0, 1, 2, ..., n - 1\}\), where \(r \leq n\), in lexicographic order, is the following:
 * <br>1) Create a first combination \(A = \{0, 1, 2, ..., r - 1\}\).
 * <br>2) Loop:
 * <ul><li>
 * Find the last element \(a_i\) in the sequence such that \(a_i \neq n - r + i\). If there is no such element,
 * stop the loop.
 * </li></ul>
 * <ul><li>
 * Set \(a_i = a_i + 1\).
 * </li></ul>
 * <ul><li>
 * For \(i &lt; j &lt; r\) set \(a_j = a_i + (j - i)\).
 * </li></ul>
 * <ul><li>
 * \(A\) is now the next combination.
 * </li></ul>
 *
 * <br><br>Let us show an example of generating \(4\)-combinations of a set \(S = \{0, 1, 2, 3, 4, 5\}\).
 * Left side contains combinations before changing to the next one, right side contains the next combination.
 * <br>We will mark the last element \(a_i\) in the sequence such that \(a_i \neq 6 - 4 + i\) (and its increased
 * value in next combination) in \(\color{blue}{\text{blue}}\).
 * Additionally, the values changed for \(i &lt; j &lt; r\) will be marked \(\color{red}{\text{red}}\).
 *
 * \begin{align}
 * \{0, 1, 2, \color{blue}{3}\} &amp; \rightarrow \{0, 1, 2, \color{blue}{4}\} \\
 * \{0, 1, 2, \color{blue}{4}\} &amp; \rightarrow \{0, 1, 2, \color{blue}{5}\} \\
 * \{0, 1, \color{blue}{2}, 5\} &amp; \rightarrow \{0, 1, \color{blue}{3}, \color{red}{4}\} \\
 * \{0, 1, 3, \color{blue}{4}\} &amp; \rightarrow \{0, 1, 3, \color{blue}{5}\} \\
 * \{0, 1, \color{blue}{3}, 5\} &amp; \rightarrow \{0, 1, \color{blue}{4}, \color{red}{5}\} \\
 * \{0, \color{blue}{1}, 4, 5\} &amp; \rightarrow \{0, \color{blue}{2}, \color{red}{3}, \color{red}{4}\} \\
 * \{0, 2, 3, \color{blue}{4}\} &amp; \rightarrow \{0, 2, 3, \color{blue}{5}\} \\
 * \{0, 2, \color{blue}{3}, 5\} &amp; \rightarrow \{0, 2, \color{blue}{4}, \color{red}{5}\} \\
 * \{0, \color{blue}{2}, 4, 5\} &amp; \rightarrow \{0, \color{blue}{3}, \color{red}{4}, \color{red}{5}\} \\
 * \{\color{blue}{0}, 2, 4, 5\} &amp; \rightarrow \{\color{blue}{1}, \color{red}{2}, \color{red}{3},
 * \color{red}{4}\} \\
 * \{1, 2, 3, \color{blue}{4}\} &amp; \rightarrow \{1, 2, 3, \color{blue}{5}\} \\
 * \{1, 2, \color{blue}{3}, 5\} &amp; \rightarrow \{1, 2, \color{blue}{4}, \color{red}{5}\} \\
 * \{1, \color{blue}{2}, 4, 5\} &amp; \rightarrow \{1, \color{blue}{3}, \color{red}{4}, \color{red}{5}\} \\
 * \{\color{blue}{1}, 3, 4, 5\} &amp; \rightarrow \{\color{blue}{2}, \color{red}{3}, \color{red}{4}, \color{red}{5}\}
 * \end{align}
 *
 * <br>The example above shows the creation of \(4\)-combinations of \(6\)-element set \(S\). Total number of
 * combinations is equal to \({6 \choose 4} = \frac{6!}{2! \cdot 4!} = 15\). Indeed we showed 15 different
 * combinations generated in lexicographic order.
 *
 * <br><br>From the implementation point of view, this generator accepts a set \(S\) of size \(n\) in a form of a
 * {@link List} containing any type of element. The generator enables us to iterate through every \(r\)-combination
 * {@link List} of \(S\).
 * <br>To do that, it generates (in lexicographic order) index combinations \(\{a_0, a_1, a_2, ..., a_{r - 1}\}\),
 * where \(0 \leq a_i &lt; r\).
 * <br>For example, for \(2\)-combinations of \(S = \{\text{"a"}, \text{"b"}, \text{"c"}\}\), the generator
 * generates the indexes \(\{0, 1\}, \{0, 2\}, \{1, 2\}\) to return \(\{\text{"a"}, \text{"b"}\}, \{\text{"a"},
 * \text{"c"}\}, \{\text{"b"}, \text{"c"}\}\) accordingly.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @param <T> type of elements included in set \(S\)
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Combination">Wikipedia - Combination</a>
 * @see <a href="http://www2.fiit.stuba.sk/~kvasnicka/Mathematics%20for%20Informatics/Rosen_Discrete_Mathematics_and_Its_Applications_7th_Edition.pdf">
 * Kenneth H. Rosen - "Discrete mathematics and its applications (7th edition)"</a>
 */
public class CombinationGenerator<T> extends Generator<List<T>> {
    private BinomialCoefficientFunction binomialCoefficientFunction = new BinomialCoefficientFunction();

    private List<T> elements;
    private long totalNumberOfCombinations;
    private int n;
    private int r;

    /**
     * Creates the combination generator for the given elements and combination size.
     * <br>A lot of combinations can sometimes be generated as their number is equal to \({n \choose r}\), where
     * \(n\) is the size of {@code elements} and \(r\) is the size of a single combination. See
     * {@link BinomialCoefficientFunction} for more details in that matter.
     *
     * <br><br>For the detailed description of the implementation, see {@link CombinationGenerator}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param elements given elements, from which the combination {@link List}s will be generated
     * @param r        size of a single combination
     *
     * @throws IllegalArgumentException when {@code elements} is null or empty, when \(r &lt; 1\) or \(r\) is greater
     *                                  than the size of {@code elements}
     * @see <a href="https://en.wikipedia.org/wiki/Combination">Wikipedia - Combination</a>
     * @see <a href="http://www2.fiit.stuba.sk/~kvasnicka/Mathematics%20for%20Informatics/Rosen_Discrete_Mathematics_and_Its_Applications_7th_Edition.pdf">
     * Kenneth H. Rosen - "Discrete mathematics and its applications (7th edition)"</a>
     */
    public CombinationGenerator( List<T> elements, int r ) {
        if( r < 1 ) {
            throw new IllegalArgumentException( "Size of combinations must be a positive number." );
        }

        if( elements == null || elements.isEmpty() ) {
            throw new IllegalArgumentException( "Elements cannot be null or empty." );
        }

        this.elements = elements;
        this.n = elements.size();
        this.r = r;

        if( n < r ) {
            throw new IllegalArgumentException(
                    "Size of elements needs to be greater than or equal to the combination size." );
        }

        //Calculating the total number of combinations, which is equal to binomialCoefficient(n, r)
        //If it does not fit in the Long range, then the generator will not work
        BigInteger binomialCoefficient = binomialCoefficientFunction.getBinomialCoefficient( n, r );
        if( binomialCoefficient == null || binomialCoefficient.compareTo( BigInteger.valueOf( Long.MAX_VALUE ) ) > 0 ) {
            throw new ArithmeticException(
                    "Cannot generate combinations when their total number is greater than " + Long.MAX_VALUE );
        }

        totalNumberOfCombinations = binomialCoefficient.longValueExact();
    }

    @Override
    protected GeneratorIterator<List<T>> createIterator() {
        return new CombinationIterator();
    }

    private class CombinationIterator implements GeneratorIterator<List<T>> {
        private int[] indexCombination;
        private long combinationsGenerated;

        @Override
        public void initialize() {
            indexCombination = null;
            combinationsGenerated = 0;
        }

        @Override
        public boolean hasNext() {
            return combinationsGenerated < totalNumberOfCombinations;
        }

        @Override
        public List<T> next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            combinationsGenerated++;

            if( indexCombination == null ) {
                indexCombination = getStartingIndexCombination();
            }
            else {
                transformToNextIndexCombination( indexCombination );
            }

            return getElementsCombination( indexCombination );
        }

        private void transformToNextIndexCombination( int[] A ) {
            //Generating next r-combination from combination A = {a_0, a_1, a_2, ..., a_{r - 1}}
            //of a set S = {0, 1, 2, ..., n - 1}, where 0 < r <= n, is done in the the following way:
            //1) Find the last element a_i such that a_i is not equal to (n - r + i). It means that i-th position
            //does not have the final value for it.
            //2) Set a_i = a_i + 1.
            //3) For i < j < r set a_j = a_i + (j - i).
            //More detailed description available in
            //Kenneth H. Rosen - "Discrete mathematics and its applications (7th edition)"
            //chapter "Generating Permutations and Combinations"

            //1) Find the last element a_i such that a_i is not equal to (n - r + i).
            int i = decrementExact( r );
            while( A[i] == addExact( subtractExact( n, r ), i ) ) {
                i = decrementExact( i );
            }
            //i >= 0 here, because i < 0 would happen only if we try to get next combination from the last
            //lexicographic one
            //It won`t happen because we do not allow this by tracking the number of combinations
            //generated already and comparing it to the total number of combinations in hasNext()

            //2) Set a_i = a_i + 1.
            A[i] = incrementExact( A[i] );

            //3) For i < j < r set a_j = a_i + (j - i).
            for( int j = incrementExact( i ); j < r; j++ ) {
                A[j] = addExact( A[i], subtractExact( j, i ) );
            }
        }

        private List<T> getElementsCombination( int[] indexCombination ) {
            //Returns combination of elements at the given index positions

            List<T> elementsCombination = new ArrayList<>( indexCombination.length );
            for( int i = 0; i < indexCombination.length; i++ ) {
                elementsCombination.add( elements.get( indexCombination[i] ) );
            }
            return elementsCombination;
        }

        private int[] getStartingIndexCombination() {
            //First index combination is equal to [0, 1, 2, ..., r - 1]

            int[] startingIndexCombination = new int[r];
            for( int i = 0; i < r; i++ ) {
                startingIndexCombination[i] = i;
            }
            return startingIndexCombination;
        }
    }
}
