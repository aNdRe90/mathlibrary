package util;

import java.math.BigInteger;
import java.util.Collection;

import static java.lang.Math.*;
import static number.Constants.FACTORIALS_LONG;

/**
 * Class containing the basic number operations.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 */
public final class BasicOperations {
    private BasicOperations() {
    }

    /**
     * Calculates the absolute value \(|a|\) for the given integer \(a\).
     * <br>\(|a| = -a\) for negative values of \(a\) and \(|a| = a\) otherwise.
     *
     * @param a value of \(a\)
     *
     * @return Absolute value \(|a|\).
     *
     * @throws ArithmeticException when \(a = \) {@link Long#MIN_VALUE} because of the overflow
     * @see <a href="https://en.wikipedia.org/wiki/Absolute_value">Wikipedia - Absolute value</a>
     */
    public static long abs( long a ) {
        if( a < 0 ) {
            //Makes sure that for Long.MIN_VALUE we don`t get a wrong result but an ArithmeticException instead
            return subtractExact( 0, a );
        }

        return a;
    }

    /**
     * Calculates the power \(a^b\) for the given base \(a\) and exponent \(b\). It uses a simple iteration to
     * multiply (\(a \cdot a \cdot ... \cdot a\)) \(b\) times.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param b value of \(b\)
     *
     * @return Value of power \(a^b\).
     *
     * @throws IllegalArgumentException when \(b &lt; 0\) or when \(a = 0, b = 0\) (\(0^0\) cannot be calculated)
     * @throws ArithmeticException      when overflow occurs (\(a^b\) does not fit in {@link Long} range)
     * @see <a href="https://en.wikipedia.org/wiki/Exponentiation">Wikipedia - Exponentiation</a>
     */
    public static long power( long a, long b ) {
        if( b < 0 ) {
            throw new IllegalArgumentException( "Cannot calculate negative powers: " + a + "^" + b );
        }
        if( a == 0 && b == 0 ) {
            throw new IllegalArgumentException( "Cannot calculate 0^0." );
        }

        long result = 1;
        for( long i = 0; i < b; i++ ) {
            result = multiplyExact( result, a );
        }

        return result;
    }

    /**
     * Calculates factorial \(n! = 1 \cdot 2 \cdot 3 \cdot ... \cdot n\) for the given non-negative integer \(n\).
     * <br>Factorial of \(0\) is defined as \(0! = 1\).
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param n value of \(n\)
     *
     * @return factorial \(n! = 1 \cdot 2 \cdot 3 \cdot ... \cdot n\)
     *
     * @throws IllegalArgumentException when \(n &lt; 0\)
     * @see <a href="https://en.wikipedia.org/wiki/Factorial">Wikipedia - Factorial</a>
     */
    public static BigInteger factorial( int n ) {
        if( n < 0 ) {
            throw new IllegalArgumentException( "Factorial of negative number cannot be calculated." );
        }
        if( n < FACTORIALS_LONG.length ) {
            return BigInteger.valueOf( FACTORIALS_LONG[n] );
        }

        BigInteger factorial = BigInteger.valueOf( FACTORIALS_LONG[FACTORIALS_LONG.length - 1] );

        for( int i = FACTORIALS_LONG.length; i <= n; i++ ) {
            factorial = factorial.multiply( BigInteger.valueOf( i ) );
        }

        return factorial;
    }

    /**
     * Calculates the sum \(s = n_0 + n_1 + ... + n_{k - 1}\) for the given values \((n_0, n_1, ..., n_{k - 1})\).
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param values values \((n_0, n_1, ..., n_{k - 1})\)
     *
     * @return Sum \(s = n_0 + n_1 + ... + n_{k - 1}\).
     *
     * @throws IllegalArgumentException when given values is null or empty
     * @throws ArithmeticException      when overflow occurs (sum does not fit in {@link Long} range)
     * @see <a href="https://en.wikipedia.org/wiki/Sum_(mathematics)">Wikipedia - Sum</a>
     */
    public static long sum( Collection<Long> values ) {
        if( values == null || values.isEmpty() ) {
            throw new IllegalArgumentException( "Either null or empty values was given." );
        }

        long sum = 0;
        for( long value : values ) {
            sum = addExact( sum, value );
        }

        return sum;
    }

    /**
     * Calculates the product \(p = n_0 \cdot n_1 \cdot ... \cdot n_{k - 1}\) for the given values
     * \((n_0, n_1, ..., n_{k - 1})\).
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param values values \((n_0, n_1, ..., n_{k - 1})\)
     *
     * @return Product \(p = n_0 \cdot n_1 \cdot ... \cdot n_{k - 1}\).
     *
     * @throws IllegalArgumentException when given values is null or empty
     * @throws ArithmeticException      when overflow occurs (product does not fit in {@link Long} range)
     * @see <a href="https://en.wikipedia.org/wiki/Product_(mathematics)">Wikipedia - Product</a>
     */
    public static long product( Collection<Long> values ) {
        if( values == null || values.isEmpty() ) {
            throw new IllegalArgumentException( "Either null or empty values was given." );
        }

        long product = 1;
        for( long value : values ) {
            product = multiplyExact( product, value );
        }

        return product;
    }
}
