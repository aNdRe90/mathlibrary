package util;

import java.math.BigInteger;

import static util.BasicOperations.abs;

/**
 * Class containing the basic modulo operations.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 */
public class ModuloOperations {
    private ModuloOperations() {
    }

    /**
     * Calculates the value \(x\) from \(x \equiv (a + b) \text{ mod } m\) for the given integers \(a, b, m\).
     * \(0 \leq x &lt; m\), even for \((a + b) &lt; 0\). It returns the correct result even if \(a + b\) exceeds the
     * {@link Long} range.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param b value of \(b\)
     * @param m value of \(m\)
     *
     * @return Value \(x\) from \(x \equiv (a + b) \text{ mod } m\), where \(0 \leq x &lt; m\). It returns the correct
     * result even if \(a + b\) exceeds the {@link Long} range.
     *
     * @throws IllegalArgumentException when \(m &lt; 1\)
     * @see <a href="https://en.wikipedia.org/wiki/Modular_arithmetic">Wikipedia - Modular arithmetic</a>
     */
    public static long addModulo( long a, long b, long m ) {
        if( m < 1 ) {
            throw new IllegalArgumentException( "Modulo must be greater than 0." );
        }

        //To reduce the potential need of BigInteger usage
        a = a % m;
        b = b % m;

        //Use BigInteger if (a + b) exceeds Long range:
        //1) a > 0, b > 0, a + b > Long.MAX_VALUE
        //2) a < 0, b < 0, a + b < Long.MIN_VALUE
        //3)(a - b) does not exceed the Long range for other cases

        if( ( a > 0 && b > 0 && a > Long.MAX_VALUE - b ) || ( a < 0 && b < 0 && ( a < Long.MIN_VALUE - b ) ) ) {
            return BigInteger.valueOf( a ).add( BigInteger.valueOf( b ) ).mod( BigInteger.valueOf( m ) )
                             .longValueExact();
        }

        return mod( a + b, m ); //(a + b) is in Long range here
    }

    /**
     * Calculates the value \(x\) from \(x \equiv a \text{ mod } m\) for the given integers \(a, m\).
     * \(0 \leq x &lt; m\), even for \(a &lt; 0\).
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param m value of \(m\)
     *
     * @return Value \(x\) from \(x \equiv a \text{ mod } m\), where \(0 \leq x &lt; m\).
     *
     * @throws IllegalArgumentException when \(m &lt; 1\)
     * @see <a href="https://en.wikipedia.org/wiki/Modular_arithmetic">Wikipedia - Modular arithmetic</a>
     */
    public static long mod( long a, long m ) {
        if( m < 1 ) {
            throw new IllegalArgumentException( "Modulo must be greater than zero." );
        }

        //When a < 0, then x = (a % m) <= 0
        //By adding m, x = (a % m) + m, we make it 0 < x <= m
        //We want 0 <= x < m though, so another modulo needs to be applied to change x = m into x = 0
        //So finally, x = ((a % m) + m) % m for a < 0
        //e.g. ((-5 % 3) + 3) % 3 = (-2 + 3) % 3 = 1 % 3 = 1
        //e.g. ((-10 % 2) + 2) % 2 = (0 + 2) % 2 = 2 % 2 = 0

        //When a >= 0, then x = a % m is enough to have 0 <= x < m

        return a < 0 ? ( ( a % m ) + m ) % m : a % m;
    }

    /**
     * Calculates the value \(x\) from \(x \equiv (a - b) \text{ mod } m\) for the given integers \(a, b, m\).
     * \(0 \leq x &lt; m\), even for \((a - b) &lt; 0\). It returns the correct result even if \(a - b\) exceeds the
     * {@link Long} range.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param b value of \(b\)
     * @param m value of \(m\)
     *
     * @return Value \(x\) from \(x \equiv (a - b) \text{ mod } m\), where \(0 \leq x &lt; m\). It returns the correct
     * result even if \(a - b\) exceeds the {@link Long} range.
     *
     * @throws IllegalArgumentException when \(m &lt; 1\)
     * @see <a href="https://en.wikipedia.org/wiki/Modular_arithmetic">Wikipedia - Modular arithmetic</a>
     */
    public static long subtractModulo( long a, long b, long m ) {
        if( m < 1 ) {
            throw new IllegalArgumentException( "Modulo must be greater than 0." );
        }

        //To reduce the potential need of BigInteger usage
        a = a % m;
        b = b % m;

        //Use BigInteger if (a - b) exceeds Long range:
        //1) a >= 0, b < 0, a - b > Long.MAX_VALUE
        //  (need to include a = 0 here because (0 - Long.MIN_VALUE) > Long.MAX_VALUE
        //2) a < 0, b > 0, a - b < Long.MIN_VALUE
        //  (no need to include a = 0 here because (0 - Long.MAX_VALUE) > Long.MIN_VALUE
        //3)(a - b) does not exceed the Long range for other cases

        if( ( a >= 0 && b < 0 && a > Long.MAX_VALUE + b ) || ( a < 0 && b > 0 && ( a < Long.MIN_VALUE + b ) ) ) {
            return BigInteger.valueOf( a ).subtract( BigInteger.valueOf( b ) ).mod( BigInteger.valueOf( m ) )
                             .longValueExact();
        }

        return mod( a - b, m ); //(a - b) is in Long range here
    }

    /**
     * Calculates the value \(x\) from \(x \equiv a^b \text{ mod } m\) for the given integers \(a, b, m\).
     * \(0 \leq x &lt; m\), even for \(a^b &lt; 0\). It returns the correct result even if \(a^b\) exceeds the
     * {@link Long} range.
     * <br>To have a good performance, even for big values of \(a, b\), it uses the exponentiation by squaring. In
     * this approach, we iterate through the bits of \(b\) and accumulate the result to \(x \equiv a^b \text{ mod } m\).
     *
     * <br><br>Consider the binary representation of \(b = B_{k - 1}B_{k - 2} ... B_2B_1B_0\) where each \(B_i\) is a
     * binary digit (\(B_i = 0\) or \(B_i = 1\)).
     * Then \(b = B_0 \cdot 2^0 + B_1 \cdot 2^1 + B_2 \cdot 2^2 + ... + B_{k - 2} \cdot 2^{k - 2} + B_{k - 1} \cdot
     * 2^{k - 1}\).
     * <br>Plugging this into \(a^b\) we get:
     * \begin{align}
     * a^b &amp; = a^{B_0 \cdot 2^0 + B_1 \cdot 2^1 + B_2 \cdot 2^2 + ... + B_{k - 2} \cdot 2^{k - 2} + B_{k - 1} \cdot
     * 2^{k - 1}} \\
     * &amp; = a^{B_0 \cdot 2^0} \cdot a^{B_1 \cdot 2^1} \cdot a^{B_2 \cdot 2^2} \cdot ... \cdot a^{B_{k - 2} \cdot
     * 2^{k - 2}} \cdot a^{B_{k - 1} \cdot 2^{k - 1}} \\
     * &amp; = (a^{2^0})^{B_0} \cdot (a^{2^1})^{B_1} \cdot (a^{2^2})^{B_2} \cdot ... (a^{2^{k - 2}})^{B_{k - 2}} \cdot
     * (a^{2^{k - 1}})^{B_{k - 1}}
     * \end{align}
     *
     * We can notice that the factor \(a^{2_{i}}\) disappears when \(B_i = 0\) and that \(x \equiv a^b \text{ mod } m
     * \Leftrightarrow x \equiv [(a^1)^{B_0} \cdot (a^2)^{B_1} \cdot (a^4)^{B_2} \cdot ... (a^{2^{k - 2}}) ^{B_{k - 2}}
     * \cdot (a^{2^{k - 1}})^{B_{k - 1}}] \text{ mod } m \Leftrightarrow x \equiv [((a^1)^{B_0} \text{ mod } m)
     * \cdot ((a^2)^{B_1} \text{ mod } m) \cdot ((a^4)^{B_2} \text{ mod } m) \cdot ... \cdot ((a^{2^{k - 2}})^
     * {B_{k - 2}} \text{ mod } m) \cdot ((a^{2^{k - 1}})^{B_{k - 1}} \text{ mod } m)] \text{ mod } m\).
     * <br>Value \(a^{2^i} \text{ mod } m\) can be easily calculated by squaring the value of
     * \(a^{2^{i - 1}} \text{ mod } m\) because \((a^{2^{i - 1}})^2 = a^{2^{i - 1} \cdot 2^1} = a^{2^i}\).
     * <br>So by iterating through \(i = 0, 1, 2, ..., k - 2, k - 1\), we can calculate \(a^{2^i} \text{ mod } m\) by
     * simply squaring the value from the previous iteration (\(a^{2^0} \text{ mod } m\) for the first iteration with
     * \(i = 0\) does not need to square anything).
     *
     * <br><br>To improve the calculations whenever the squaring of \(a^{2^{i - 1}} \text{ mod } m\) occurs, we
     * should perform it by using the {@link #multiplyModulo(long, long, long)} so that the value
     * \(a^{2^i} \text{ mod } m\) stays in range \([0; m)\).
     *
     * <br><br>Let`s look at the example of calculating \(114^{1800} \text{ mod } 67\).
     * <br>To start off \(114^{1800} \text{ mod } 67\) is equal to \(47^{1800} \text{ mod } 67\) because
     * \(47 \equiv 114 \text{ mod } 67\). \(1800 = 1024 + 512 + 256 + 8\) is equal to \(11100001000\) in binary.
     * \begin{align}
     * 47^{1800} \text{ mod } 67 &amp; = 47^{1024 + 512 + 256 + 8} \text{ mod } 67 = [(47^8 \text{ mod } 67) \cdot
     * (47^{256} \text{ mod } 67) \cdot (47^{512} \text{ mod } 67) \cdot (47^{1024} \text{ mod } 67)] \text{ mod } 67
     * \end{align}
     *
     * Now, we calculate the values \(47^{2^i} \text{ mod } 67\) for \(i = 0, 1, 2, ... 10\) (\(1024 = 2^{10}\)).
     * Current value \(47^{2^i} \text{ mod } 67\) is marked in \(\color{blue}{\text{blue}}\) and the value
     * \(47^{2^{i - 1}} \text{ mod } 67\) from previous iteration is marked in \(\color{red}{\text{red}}\).
     * Modulo values that are needed to calculate \(47^{1800} \text{ mod } 67\) are marked in
     * \(\color{green}{\text{green}}\).
     *
     * \begin{align}
     * 47^1 \text{ mod } 67 &amp; = &amp; &amp; &amp; &amp; &amp; &amp; &amp;
     * \color{blue}{47} \\
     *
     * 47^2 \text{ mod } 67 &amp; = &amp;
     * (47^1 \text{ mod } 67)^2 \text{ mod } 67 &amp; = &amp;
     * \color{red}{47}^2 \text{ mod } 67 &amp; = &amp;
     * 2209 \text{ mod } 67 &amp; = &amp;
     * \color{blue}{65} \\
     *
     * 47^4 \text{ mod } 67 &amp; = &amp;
     * (47^2 \text{ mod } 67)^2 \text{ mod } 67 &amp; = &amp;
     * \color{red}{65}^2 \text{ mod } 67 &amp; = &amp;
     * 4225 \text{ mod } 67 &amp; = &amp;
     * \color{blue}{4} \\
     *
     * \color{green}{47^8 \text{ mod } 67} &amp; = &amp;
     * (47^4 \text{ mod } 67)^2 \text{ mod } 67 &amp; = &amp;
     * \color{red}{4}^2 \text{ mod } 67 &amp; = &amp;
     * 16 \text{ mod } 67 &amp; = &amp;
     * \color{blue}{16} \\
     *
     * 47^{16} \text{ mod } 67 &amp; = &amp;
     * (47^8 \text{ mod } 67)^2 \text{ mod } 67 &amp; = &amp;
     * \color{red}{16}^2 \text{ mod } 67 &amp; = &amp;
     * 256 \text{ mod } 67 &amp; = &amp;
     * \color{blue}{55} \\
     *
     * 47^{32} \text{ mod } 67 &amp; = &amp;
     * (47^{16} \text{ mod } 67)^2 \text{ mod } 67 &amp; = &amp;
     * \color{red}{55}^2 \text{ mod } 67 &amp; = &amp;
     * 3025 \text{ mod } 67 &amp; = &amp;
     * \color{blue}{10} \\
     *
     * 47^{64} \text{ mod } 67 &amp; = &amp;
     * (47^{32} \text{ mod } 67)^2 \text{ mod } 67 &amp; = &amp;
     * \color{red}{10}^2 \text{ mod } 67 &amp; = &amp;
     * 100 \text{ mod } 67 &amp; = &amp;
     * \color{blue}{33} \\
     *
     * 47^{128} \text{ mod } 67 &amp; = &amp;
     * (47^{64} \text{ mod } 67)^2 \text{ mod } 67 &amp; = &amp;
     * \color{red}{33}^2 \text{ mod } 67 &amp; = &amp;
     * 1089 \text{ mod } 67 &amp; = &amp;
     * \color{blue}{17} \\
     *
     * \color{green}{47^{256} \text{ mod } 67} &amp; = &amp;
     * (47^{128} \text{ mod } 67)^2 \text{ mod } 67 &amp; = &amp;
     * \color{red}{17}^2 \text{ mod } 67 &amp; = &amp;
     * 289 \text{ mod } 67 &amp; = &amp;
     * \color{blue}{21} \\
     *
     * \color{green}{47^{512} \text{ mod } 67} &amp; = &amp;
     * (47^{256} \text{ mod } 67)^2 \text{ mod } 67 &amp; = &amp;
     * \color{red}{21}^2 \text{ mod } 67 &amp; = &amp;
     * 441 \text{ mod } 67 &amp; = &amp;
     * \color{blue}{39} \\
     *
     * \color{green}{47^{1024} \text{ mod } 67} &amp; = &amp;
     * (47^{512} \text{ mod } 67)^2 \text{ mod } 67 &amp; = &amp;
     * \color{red}{39}^2 \text{ mod } 67 &amp; = &amp;
     * 1521 \text{ mod } 67 &amp; = &amp;
     * \color{blue}{47} \\
     * \end{align}
     *
     * Finally, \(47^{1800} \text{ mod } 67 = [\color{green}{(47^8 \text{ mod } 67)} \cdot
     * \color{green}{(47^{256} \text{ mod } 67)} \cdot \color{green}{(47^{512} \text{ mod } 67)} \cdot
     * \color{green}{(47^{1024} \text{ mod } 67)}] \text{ mod } 67 = \color{blue}{16} \cdot \color{blue}{21} \cdot
     * \color{blue}{39} \cdot \color{blue}{47} \text{ mod } 67 = 615888 \text{ mod } 67 = 24\).
     * <br>Indeed, \(114^{1800} \text{ mod } 67 = 47^{1800} \text{ mod } 67 = 24\).
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param b value of \(b\)
     * @param m value of \(m\)
     *
     * @return Value \(x\) from \(x \equiv a^b \text{ mod } m\), where \(0 \leq x &lt; m\). It returns the
     * correct result even if \(a^b\) exceeds the {@link Long} range.
     *
     * @throws IllegalArgumentException when \(b &lt; 0\) or \(m &lt; 1\) or when \(a = 0, b = 0\)
     *                                  (\(0^0\) cannot be calculated)
     * @see <a href="https://en.wikipedia.org/wiki/Modular_exponentiation">Wikipedia - Modular exponentiation</a>
     * @see <a href="https://en.wikipedia.org/wiki/Exponentiation_by_squaring">Wikipedia - Exponentiation by
     * squaring</a>
     * @see <a href="https://mpark.github.io/programming/2014/08/18/exponentiation-by-squaring/">Michael Park blog -
     * Exponentiation by squaring</a>
     */
    public static long powerModulo( long a, long b, long m ) {
        if( b < 0 ) {
            throw new IllegalArgumentException( "Exponent cannot be negative." );
        }
        if( m < 1 ) {
            throw new IllegalArgumentException( "Modulo must be greater than 0." );
        }
        if( a == 0 && b == 0 ) {
            throw new IllegalArgumentException( "0^0 is undefined." );
        }

        if( m == 1 ) {
            //Handling this separately because otherwise the case a^0 mod 1 will incorrectly return 1
            return 0;
        }

        //m > 1 from here

        //b = B_{k - 1}B_{k - 2}...B_2B_1B_0 is a binary representation of the exponent b, where B_i = 0 or B_i = 1

        //a^b = a^{B_0*2^0 + B_1*2^1 + B_2*2^2 + ... + B{k − 2}*2^{k − 2} + B_{k − 1}*2^{k − 1}} =
        //a^{B_0*2^0} * a^{B_1*2^1} * a^{B_2*2^2} * ... * a^{B_{k - 2}*2^{k - 2}} * a^{B_{k - 1}*2^{k - 1}} =
        //(a^{2^0})^B_0 * (a^{2^1})^B_1 * (a^{2^2})^B_2 * ... * (a^{2^{k - 2}})^B_{k - 2} * (a^{2^{k - 1}})^B_{k - 1}

        //a^b mod m = [ ((a^{2^0})^B_0 mod m) * ((a^{2^1})^B_1 mod m) * ((a^{2^2})^B_2 mod m) * ... *
        //            (a^{2^{k - 2}})^B_{k - 2} mod m) * (a^{2^{k - 1}})^B_{k - 1} mod m) ] mod m

        //If B_i = 0, then (a^{2^i})^B_i mod m = 1, so multiplying by 1 can be omitted
        //If B_i = 1, then (a^{2^i})^B_i mod m = a^{2^i} mod m

        long powerModulo = 1; //result variable, instantiated with 1 because we will be multiplying it by a^{2^i} mod m
        long a_2_i = mod( a, m ); //value of a^{2^i} mod m, first instantiated with a^{2^0} mod m = a mod m

        while( b > 0 ) {
            //This is the iteration over the values i = 0, 1, 2, ..., k - 2, k - 1
            //where b = B_{k - 1}B_{k - 2}...B_2B_1B_0 is a binary representation of the exponent b (B_i = 0 or B_i = 1)

            long B_i = b % 2; //the least significant bit of b

            if( B_i == 1 ) {
                //If B_i = 1, then ((a^{2^i})^B_i mod m) = a^{2^i} mod m
                //So multiplying the result by a^{2^i} mod m
                powerModulo = multiplyModulo( powerModulo, a_2_i, m );
            } //else if B_i = 0, then ((a^{2^i})^B_i mod m) = 1, so multiplying by 1 can be ommited

            //Squaring the value a^{2^i} mod m to obtain the next a^{2^{i + 1}} mod m
            //a^{2^i}^2 mod m = a^{2^i * 2^1} mod m = a^{2^{i + 1}} mod m
            a_2_i = multiplyModulo( a_2_i, a_2_i, m );
            b /= 2; //shifts the variable b so that (b % 2) gives the next bit B_{i + 1} in the next iteration
        }

        return powerModulo;
    }

    /**
     * Calculates the value \(x\) from \(x \equiv (a \cdot b) \text{ mod } m\) for the given integers \(a, b, m\).
     * \(0 \leq x &lt; m\), even for \((a \cdot b) &lt; 0\). It returns the correct result even if \(a \cdot b\) exceeds
     * the {@link Long} range.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param b value of \(b\)
     * @param m value of \(m\)
     *
     * @return Value \(x\) from \(x \equiv (a \cdot b) \text{ mod } m\), where \(0 \leq x &lt; m\). It returns the
     * correct result even if \(a \cdot b\) exceeds the {@link Long} range.
     *
     * @throws IllegalArgumentException when \(m &lt; 1\)
     * @see <a href="https://en.wikipedia.org/wiki/Modular_arithmetic">Wikipedia - Modular arithmetic</a>
     */
    public static long multiplyModulo( long a, long b, long m ) {
        if( m < 1 ) {
            throw new IllegalArgumentException( "Modulo must be greater than 0." );
        }

        //To reduce the potential need of BigInteger usage
        a = a % m;
        b = b % m;

        if( a == 0 || b == 0 ) {
            return 0;
        }

        //Use BigInteger if |ab| = |a|*|b| exceeds Long range:
        //|a|*|b| > Long.MAX_VALUE
        if( abs( a ) >
            Long.MAX_VALUE / abs( b ) ) { //no need to use doubles, |a| > floor(Long.MAX_VALUE / |b|) is the same
            //This way Long.MIN_VALUE * 1 will be handled here but this one case not being handled in Long values,
            //when it could, is not what we should be worried about
            return BigInteger.valueOf( a ).multiply( BigInteger.valueOf( b ) ).mod( BigInteger.valueOf( m ) )
                             .longValueExact();
        }

        return mod( a * b, m );
    }

}
