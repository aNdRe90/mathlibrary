package solver;

import algorithm.ChineseRemainderTheoremAlgorithm;
import algorithm.FactorizationAlgorithm;
import algorithm.GreatestCommonDivisorAlgorithm;
import algorithm.ModularSquareRootAlgorithm;
import generator.IterationIndexesGenerator;
import number.representation.FactorizationFactor;
import number.representation.ModularCongruence;
import number.representation.big.BigModularCongruence;

import java.math.BigInteger;
import java.util.*;

import static java.lang.Math.decrementExact;
import static java.lang.Math.multiplyExact;
import static java.util.Collections.*;
import static util.BasicOperations.power;
import static util.ModuloOperations.*;

/**
 * Solver for the quadratic congruence of form:
 * \(ax^2 + bc + c \equiv 0 \text{ mod } m\), where \(a, b, c, m\) are integers and \(m &gt; 0\).
 * <br>First thing we can do is to use the fact that for integers \(A,B,C,M\) we have:
 * If \(AC \equiv BC \text{ mod }MC\), then \(A \equiv B \text{ mod }M\).
 * Let`s assume then that we are trying to solve \(ax^2 + bc + c \equiv 0 \text{ mod } m\), where
 * \(\text{gcd}(a,b,c,m) = 1\).
 *
 * <br><br>
 * In order to solve the general case we first need to split it to the set of cases:
 * \begin{align}
 * ax^2 + bx + c &amp; \equiv 0 \text{ mod } p_0^{e_0} \\
 * ax^2 + bx + c &amp; \equiv 0 \text{ mod } p_1^{e_1} \\
 * &amp; \text{ ... } \\
 * ax^2 + bx + c &amp; \equiv 0 \text{ mod } p_{k - 1}^{e_{k - 1}}
 * \end{align}
 * <br>Where \(m = p_0^{e_0} \cdot p_1^{e_1} \cdot  \text{ ... } \cdot p_{k - 1}^{e_{k - 1}}\), \(e^i &gt; 0\) is a
 * prime factorization of \(m\).
 * <br>If any of the cases \(ax^2 + bx + c \equiv 0 \text{ mod } p_i^{e_i}\) has no solutions then the whole
 * \(ax^2 + bx + c \equiv 0 \text{ mod } m\) has no solutions.
 *
 * <br><br>Next step is solving every case by:
 * <ul><li>
 * Solving \(ax^2 + bx + c \equiv 0 \text{ mod } p_i\), where \(p_i\) is a prime number.
 * </li></ul>
 * <ul><li>
 * Lifting the solutions of \(ax^2 + bx + c \equiv 0 \text{ mod } p_i\) so that we obtain the solutions for
 * \(ax^2 + bx + c \equiv 0 \text{ mod } p_i^{e_i}\).
 * </li></ul>
 *
 * <br><br>In order to solve \(ax^2 + bx + c \equiv 0 \text{ mod } p_i\) we should first use the fact that
 * \(ax^2 + bx + c \equiv 0 \text{ mod } p_i\) is equivalent to \(a'x^2 + b'x + c' \equiv 0 \text{ mod } p_i\), where
 * \(a' \equiv a \text{ mod } p_i, b' \equiv b \text{ mod } p_i, c' \equiv c \text{ mod } p_i\).
 * <br>By doing so we guarantee that the coefficients \(a', b', c'\) are either equal to \(0\) and the corresponding
 * polynomial part "disappears" or that they are coprime with \(p_i\) (\(\text{gcd}(a', p_i) = 1, \text{gcd}(b', p_i)
 * = 1, \text{gcd}(c', p_i) = 1\)).
 * <br>If \(a' = 0\), then we have a linear congruence to solve: \(bx + c \equiv 0 \text{ mod } p_i\).
 * The algorithm for solving linear congruences is described in {@link LinearCongruenceSolver}.
 * <br>Next, for quadratic congruence case where \(a' \neq 0\), we can apply the formula for quadratic equation
 * solution \(x = \frac{-b' \pm \sqrt{\Delta}}{2a'}\), where \(\Delta = b'^2 - 4a'c'\).
 * We need to do it "in the modulo world" though. Let`s define then, how to calculate square root value and perform
 * a division in such case.
 *
 * <br><br>Square root of value \(a \text{ mod } m\) is such integer \(x\) that \(x^2 \equiv a \text{ mod } m\).
 * More details about how to solve this is described in {@link ModularSquareRootAlgorithm}.
 * <br>Finding the result of division \(x \equiv \frac{r}{s} \text{ mod } m\) is equivalent to solving \(x\) in
 * \(sx \equiv r \text{ mod } m\). Such equation has solutions only when \(\text{gcd}(s, m) = 1\). For more details,
 * please see the description of {@link LinearCongruenceSolver}.
 * <br>In our case \(s = 2a', m = p_i\) so \(\text{gcd}(2a', p_i) = 1\). We already established that \(a' \neq 0\)
 * (linear congruence is being solved for this case) and that \(0 \leq a' &lt; p_i\) (because
 * \(a' \equiv a \text{ mod } p_i\)). It means that in order for \(\text{gcd}(2a', p_i) = 1\) to be true \(p_i \neq 2\)
 * so a different approach is needed for the case when \(p_i = 2\).
 *
 * <br><br>Final algorithm of solving \(a'x^2 + b'x + c' \equiv 0 \text{ mod } p_i\), where \(0 &lt; a' &lt; p_i\)
 * is the following:
 * <ul><li>
 * If \(p_i \neq 2\), then solve \(d^2 = \Delta \text{ mod } p_i\), where \(\Delta = b'^2 - 4a'c\). For each modular
 * square root \(d\) (it may happen that there will be none) find the solution values of \(x\) by solving
 * \(2a'x \equiv -b' \pm d \text{ mod } p_i\).
 * </li></ul>
 * <ul><li>
 * If \(p_i = 2\), then the only possible solutions are \(0 \text{ mod } 2\) and \(1 \text{ mod } 2\). Check them
 * manually by putting \(x = 0\) and \(x = 1\) into the equation.
 * </li></ul>
 *
 * <br><br>If it happens that in case \(ax^2 + bx + c \equiv 0 \text{ mod } p_i^{e_i}\) we have \(e_i &gt; 1\)
 * then we need to apply a lifting process which is based on the Hensel`s lemma (\(f(x) = ax^2 + bx + c\)).
 * <br>Let \(p_i\) be a prime and let \(k\) be an arbitrary positive integer, and suppose that \(x_k\) is a solution of
 * \(f(x) \equiv 0 \text{ mod } p_i^k, k \geq 1\).
 * <ul><li>
 * If \(p_i \not| f'(x_k)\), then there is precisely one solution \(x_{k+1}\) of
 * \(f(x) \equiv 0 \text{ mod } p_i^{k + 1}\) such that \(x_{k+1} \equiv x_k \text{ mod } p_i^k\).
 * The solution is given by \(x_{k+1} = x_k + p_i^kt\), where \(t\) is the unique solution of
 * \(f'(x_k)t \equiv \frac{-f(x_k)}{p_i^k} \text{ mod } p_i\).
 * </li></ul>
 * <ul><li>
 * If \(p_i | f'(x_k)\) and \(p_i^{k + 1} | f(x_k)\), then there are \(p_i\) solutions of the congruence
 * \(f(x) \equiv 0 \text{ mod } p_i^{k + 1}\) that are congruent to a modulo \(p_i^k\).
 * These solutions are \(x_{k+1} = x_k + p_i^kj\) for \(j = 0, 1, 2, ..., p_i - 1\).
 * </li></ul>
 * <ul><li>
 * If \(p_i | f'(x_k)\) and \(p_i^{k + 1} \not| f(x_k)\), then there are no solutions of the congruence
 * \(f(x) \equiv 0 \text{ mod } p_i^{k + 1}\) that are congruent to a modulo \(p_i^k\).
 * </li></ul>
 *
 * <br>Note that, if we have a solution \(x_1\) to \(f(x) \equiv 0 \text{ mod } p_i^1\), then if \(x_2\) solves
 * \(f(x) \equiv 0 \text{ mod } p_i^2\), we have \(x_2 \equiv x_1 \text{ mod } p_i^1\). It also means that
 * \(f'(x_2) \equiv f'(x_1) \text{ mod } p_i^1\). By the same reason, \(f'(x_k) \equiv f'(x_1) \text{ mod } p_i^{k - 1}
 * \text{ for }k \geq 2\).
 * <br>This fact makes it easier for us as we only need to calculate \(f'(x_1)\) and if it is equal to zero, then
 * \(f'(x_k) \equiv 0 \text{ mod } p_i^{k - 1} \text{ for }k \geq 2\).
 *
 * <br><br>Having all the cases \(ax^2 + bx + c \equiv 0 \text{ mod } p_i^{e_i}\) solved, Chinese Remainder Theorem
 * needs to be applied in order to form a final solution to \(ax^2 + bx + c \equiv 0 \text{ mod } m\), where
 * \(m = p_0^{e_0} \cdot p_1^{e_1} \cdot  \text{ ... } \cdot p_{k - 1}^{e_{k - 1}}\), \(e^i &gt; 0\).
 * <br>e.g. If for m = 10 the solutions are [1 mod 2] and [3 mod 5, 4 mod 5], then we need to use Chinese Remainder
 * Theorem for two inputs: [1 mod 2, 3 mod 5] and [1 mod 2, 4 mod 5].
 * <br>For more details on how the Chinese Remainder Theorem works, see {@link ChineseRemainderTheoremAlgorithm}.
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="http://www.cs.xu.edu/math/math302/08f/16_QuadraticCongruences.pdf">XAVIER UNIVERSITY Math 302
 * Number Theory materials - Quadratic congruences</a>
 * @see <a href="http://www2.math.uu.se/~lal/kompendier/Talteori.pdf">"Lectures on Number Theory" - Lars-Ake Lindahl,
 * Polynomial Congruences with Prime Power Moduli</a>
 * @see <a href="https://en.wikipedia.org/wiki/Hensel%27s_lemma#Hensel_lifting">Wikipedia - Hensel lifting</a>
 */
public class QuadraticCongruenceSolver {
    private ChineseRemainderTheoremAlgorithm chineseRemainderTheoremAlgorithm = new ChineseRemainderTheoremAlgorithm();
    private FactorizationAlgorithm factorizationAlgorithm = new FactorizationAlgorithm();
    private LinearCongruenceSolver linearCongruenceSolver = new LinearCongruenceSolver();
    private ModularSquareRootAlgorithm modularSquareRootAlgorithm = new ModularSquareRootAlgorithm();
    private GreatestCommonDivisorAlgorithm gcdAlgorithm = new GreatestCommonDivisorAlgorithm();
    private ModularCongruenceComparator modularCongruenceComparator = new ModularCongruenceComparator();

    /**
     * Solves the quadratic congruence of form: \(ax^2 + bc + c \equiv 0 \text{ mod } m\),
     * where \(a, b, c, m\) are integers and \(m &gt; 0\).
     * <br>For the detailed description of the implementation, see {@link QuadraticCongruenceSolver}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param b value of \(b\)
     * @param c value of \(c\)
     * @param m modulo \(m &gt; 0\)
     *
     * @return {@link List} of {@link ModularCongruence} solving given quadratic congruence or empty {@link List} if no
     * solution exists.
     * <br>The list has elements with the same modulo value, ordered from the smallest to greatest
     * residue value. The solution modulo is not necessarily equal to input \(m\) (but is always the same
     * for every output congruence) as the result is returned in its simplest form
     * e.g. \(x^2 + 2x + 1 \equiv 0 \text{ mod } 4\) gives \([1 \text{ mod } 2]\) as a result instead of
     * \([1 \text{ mod } 4, 3 \text{ mod } 4]\).
     *
     * @throws IllegalArgumentException when \(m &lt; 1\)
     * @see <a href="http://www.cs.xu.edu/math/math302/08f/16_QuadraticCongruences.pdf">XAVIER UNIVERSITY Math 302
     * Number Theory materials - Quadratic congruences</a>
     * @see <a href="http://www2.math.uu.se/~lal/kompendier/Talteori.pdf">"Lectures on Number Theory" - Lars-Ake
     * Lindahl,
     * Polynomial Congruences with Prime Power Moduli</a>
     * @see <a href="https://en.wikipedia.org/wiki/Hensel%27s_lemma#Hensel_lifting">Wikipedia - Hensel lifting</a>
     */
    public List<ModularCongruence> getSolutions( long a, long b, long c, long m ) {
        if( m < 1 ) {
            throw new IllegalArgumentException( "Modulo cannot must be greater than zero." );
        }

        //We use the following equality:
        //For integers a, b, c, m the congurence ac = bc mod mc is equivalent to a = b mod m
        //We can simplify the congruence ax^2 + bx + c = 0 mod m by dividing a,b,c,m by gcd(a,b,c,m)

        //We only do this because we want the final result to have a minimal modulo value e.g.
        //for 2x^2 + 4x + 2 mod 8 we want to return [1 mod 2], instead of [1 mod 8, 3 mod 8, 5 mod 8, 7 mod 8]
        //Without this reduction the result is still correct but it is not the minimal one
        //Probably further code implementation can be changed so this reduction is not needed here but it is always
        //nice to reduce the values if we can

        long gcd = gcdAlgorithm.getGreatestCommonDivisor( a, b, c, m );
        a /= gcd;
        b /= gcd;
        c /= gcd;
        m /= gcd;

        //If modulo is 1, then every number is a solution
        if( m == 1 ) {
            return singletonList( new ModularCongruence( 0, 1 ) );
        }

        return calculateSolutions( a, b, c, m );
    }

    private List<ModularCongruence> calculateSolutions( long a, long b, long c, long m ) {
        //gcd(a, b, c, m) = 1

        //Holds the solutions for each case ax^2 + bx + c = 0 mod p_i^e_i
        List<List<ModularCongruence>> allCasesSolutions = new ArrayList<>();

        for( FactorizationFactor factor : factorizationAlgorithm.getFactorization( m ) ) {
            //Handling the case ax^2 + bx + c = 0 mod p_i^e_i
            long p_i = factor.getPrime();
            int e_i = factor.getExponent();

            List<ModularCongruence> currentCaseSolutions;
            if( p_i == 2 ) {
                //Special case for p_i = 2
                currentCaseSolutions = calculateSolutionsForModuloPowerOfTwo( a, b, c, e_i );
            }
            else {
                //Case for odd primes p_i (p_i != 2)
                currentCaseSolutions = calculateSolutionsForModuloOddPrimePower( a, b, c, p_i, e_i );
            }

            if( currentCaseSolutions.isEmpty() ) {
                //If any of the cases ax^2 + bx + c = 0 mod p_i^e_i has no solution then the whole
                //ax^2 + bx + c = 0 mod m has no solution
                return emptyList();
            }

            allCasesSolutions.add( currentCaseSolutions );
        }

        //Chinese Remainder Theorem needs to be applied now to obtain the final result for ax^2 + bx + c = 0 mod m
        return calculateFinalSolutionsUsingChineseRemainderTheorem( allCasesSolutions );
    }

    private List<ModularCongruence> calculateSolutionsForModuloPowerOfTwo( long a, long b, long c, int e_i ) {
        List<ModularCongruence> solutionsForModuloTwo = new ArrayList<>( 2 );

        //Case ax^2 + bx + c = 0 mod 2^e_i
        //Checking manually the only possible solutions: 0 mod 2 and 1 mod 2

        if( addModulo( a, addModulo( b, c, 2 ), 2 ) == 0 ) {
            //Checking if x = 1 mod 2 is the solution
            //It is when a + b + c = 0 mod 2
            solutionsForModuloTwo.add( new ModularCongruence( 1, 2 ) );
        }
        if( c % 2 == 0 ) {
            //Checking if x = 0 mod 2 is the solution
            //It is when c = 0 mod 2
            solutionsForModuloTwo.add( new ModularCongruence( 0, 2 ) );
        }

        if( e_i == 1 && solutionsForModuloTwo.size() == 2 ) {
            //If there is no lifting of solutions to mod 2^k, k>1 needed,
            //and both 0 mod 2 and 1 mod 2 are the solution,
            //then return the reduced form 0 mod 1 (every number is a solution)
            return singletonList( new ModularCongruence( 0, 1 ) );
        }

        //If the exponent is 1, no lifting is needed
        if( e_i == 1 ) {
            return solutionsForModuloTwo;
        }

        //If the exponent is greater than 1, apply the lifting process
        return liftSolutions( a, b, c, solutionsForModuloTwo, 2, e_i );
    }

    private List<ModularCongruence> calculateSolutionsForModuloOddPrimePower( long a, long b, long c, long p_i,
            int e_i ) {
        //Calculate solution for ax^2 + bx + c = 0 mod p_i
        List<ModularCongruence> solutionsForModuloOddPrime = calculateSolutionsForModuloOddPrime( a, b, c, p_i );

        //If the exponent is 1, no lifting is needed
        if( e_i == 1 ) {
            return solutionsForModuloOddPrime;
        }

        //If the exponent is greater than 1, apply the lifting process
        return liftSolutions( a, b, c, solutionsForModuloOddPrime, p_i, e_i );
    }

    private List<ModularCongruence> calculateSolutionsForModuloOddPrime( long a, long b, long c, long p_i ) {
        a = mod( a, p_i ); //a = a mod p_i
        b = mod( b, p_i ); //b = b mod p_i
        c = mod( c, p_i ); //c = c mod p_i
        //After this, 0 <= a < p_i, 0 <= b < p_i, 0 <= c < p_i
        //So according to this class` javadoc, {a,b,c} is now {a', b', c'}

        if( a == 0 ) {
            //The case of bx + c = 0 mod p_i or bx = -c mod p_i is a linear congruence
            ModularCongruence linearCongruenceSolutionForModuloPrimePower = linearCongruenceSolver
                    .getSolution( b, subtractModulo( 0, c, p_i ), p_i );
            if( linearCongruenceSolutionForModuloPrimePower == null ) {
                //Null returned means there are no solution for the case bx = -c mod p_i
                //We need to return empty list here because we treat empty lists as a signal for no solution for the
                //i-th case
                return emptyList();
            }

            //Linear congruence has one solution in the minimal modulo
            return singletonList( linearCongruenceSolutionForModuloPrimePower );
        }

        //If a != 0, then we find a solution to ax^2 + bx + c = 0 mod p_i by:
        //1) Solving d from d^2 = delta mod p_i, where delta = b^2 - 4ac
        //2) Solving x from 2ax = -b +-d mod p_i

        //1)
        long delta = subtractModulo( multiplyModulo( b, b, p_i ), multiplyModulo( 4, multiplyModulo( a, c, p_i ), p_i ),
                                     p_i ); //delta = (b^2 - 4ac) mod p_i

        //Solving d^2 = delta mod p_i
        List<ModularCongruence> dValues = modularSquareRootAlgorithm.getSquareRoots( delta, p_i );
        //ModularCongruences on this list have modulo = p_i because this is a prime number

        Set<ModularCongruence> solutionsForModuloOddPrime = new HashSet<>( dValues.size() );

        for( ModularCongruence dCongruence : dValues ) {
            long d = dCongruence.getResidue();
            //dCongruence.getModulo() is always equal to p_i here

            //2) Solving x from 2ax = -b +-d mod p_i
            List<ModularCongruence> xValues = calculateSolutionsForDeltaSquareRoot( a, b, d, p_i );

            solutionsForModuloOddPrime.addAll( xValues );
        }

        //If no d solving d^2 = delta mod p_i is found, the result list is empty
        return new ArrayList<>( solutionsForModuloOddPrime );
    }

    private List<ModularCongruence> calculateSolutionsForDeltaSquareRoot( long a, long b, long d, long p_i ) {
        List<ModularCongruence> quadraticCongruenceSolutions = new ArrayList<>();

        //Solving x from 2ax = -b +-d mod p_i

        //Case 2ax = d - b mod p_i
        ModularCongruence quadraticCongruenceSolution = linearCongruenceSolver
                .getSolution( multiplyModulo( 2, a, p_i ), subtractModulo( d, b, p_i ), p_i );
        if( quadraticCongruenceSolution != null ) { //If solution exists
            quadraticCongruenceSolutions.add( quadraticCongruenceSolution );
        }

        if( d == 0 ) {
            //If d = 0, then there is no need to check the 2ax = -d - b mod p_i case as it would give the same result
            //as 2ax = d - b mod p_i
            return quadraticCongruenceSolutions;
        }

        //Case 2ax = -d - b mod p_i
        quadraticCongruenceSolution = linearCongruenceSolver
                .getSolution( multiplyModulo( 2, a, p_i ), subtractModulo( subtractModulo( 0, d, p_i ), b, p_i ), p_i );

        if( quadraticCongruenceSolution != null ) { //If solution exists
            quadraticCongruenceSolutions.add( quadraticCongruenceSolution );
        }

        return quadraticCongruenceSolutions;
    }

    private List<ModularCongruence> liftSolutions( long a, long b, long c,
            List<ModularCongruence> solutionsForPrimeModulo, long p_i, int e_i ) {
        //solutionsForPrimeModulo holds solutions to ax^2 + bx + c = 0 mod p_i

        List<ModularCongruence> allLiftedSolutions = new ArrayList<>();

        for( ModularCongruence solutionForPrimeModulo : solutionsForPrimeModulo ) {
            //f(x) = 0 mod p_i
            long x = solutionForPrimeModulo.getResidue();

            //f'(x) mod p_i = 2ax + b mod p_i
            long fDerivativeModPrime = addModulo( multiplyModulo( 2, multiplyModulo( a, x, p_i ), p_i ), b, p_i );

            //If a_k solves f(x) = 0 mod p_i^k then f'(a_k) = f'(a_1) mod p_i^{k - 1} for k >= 2
            //So we need to calculate the derivative only once
            //We also did it for "mod p_i" straight away because it was easier, would not cause overflow or need to use
            //BigInteger and it will be used in "mod p_i" equation for the first time anyway

            if( fDerivativeModPrime == 0 ) {
                //This means that p_i | f'(x)
                List<ModularCongruence> liftedSolutions = liftedSolutionForDerivativeZero( a, b, c,
                                                                                           solutionForPrimeModulo, p_i,
                                                                                           e_i );
                allLiftedSolutions.addAll( liftedSolutions );
            }
            else {
                //This means that p_i does not divide f'(x)
                ModularCongruence liftedSolution = liftSolutionForNonZeroDerivative( BigInteger.valueOf( a ),
                                                                                     BigInteger.valueOf( b ),
                                                                                     BigInteger.valueOf( c ),
                                                                                     solutionForPrimeModulo,
                                                                                     fDerivativeModPrime, p_i, e_i );
                if( liftedSolution != null ) {
                    allLiftedSolutions.add( liftedSolution );
                }
            }
        }

        return allLiftedSolutions;
    }

    private List<ModularCongruence> liftedSolutionForDerivativeZero( long a, long b, long c,
            ModularCongruence solutionForPrimeModulo, long p_i, int e_i ) {
        //Case "If p_i | f'(X)" where X is a solution to f(X) = 0 mod p_i^{k - 1}
        //solutionForPrimeModulo holds solution to ax^2 + bx + c = 0 mod p_i

        //Holds solution to ax^2 + bx + c = 0 mod p_i^{k - 1}
        List<ModularCongruence> liftedSolutions = new ArrayList<>();
        liftedSolutions.add( solutionForPrimeModulo );

        //This implementation naming differs from the javadoc description because instead of thinking of
        //knowing solutions to ax^2 + bx + c = 0 mod p_i^k and getting solutions to ax^2 + bx + c = 0 mod p_i^{k + 1} we
        //assume we know solutions to ax^2 + bx + c = 0 mod p_i^{k - 1} and are solving ax^2 + bx + c = 0 mod p_i^k
        for( int k = 2; k <= e_i; k++ ) {
            long currentModulo = power( p_i, k ); //p_i^k

            //Holds solution to ax^2 + bx + c = 0 mod p_i^k
            List<ModularCongruence> newLiftedSolutions = new ArrayList<>();

            for( ModularCongruence liftedSolution : liftedSolutions ) {
                for( long X = liftedSolution.getResidue(); X < currentModulo; X += liftedSolution.getModulo() ) {
                    //For every solution to ax^2 + bx + c = 0 mod p_i^{k - 1}

                    //f(X) = aX^2 + bX + c mod p_i^k
                    long f = addModulo(
                            addModulo( multiplyModulo( a, multiplyModulo( X, X, currentModulo ), currentModulo ),
                                       multiplyModulo( b, X, currentModulo ), currentModulo ), c, currentModulo );
                    if( f == 0 ) {
                        //If p_i | f'(X) and p_i^k | f(X) then there are p solutions of the congruence
                        //f(x) = 0 mod p_i^k that are congruent to mod p_i^{k - 1}
                        //These are X + jp_i^{k - 1} for j = 0, 1, 2, ..., p_i - 1

                        //We add only X mod p_i^k because these all solutions can be written as that
                        //and we care about minimal form of modular solutions
                        newLiftedSolutions.add( new ModularCongruence( X, currentModulo ) );
                    }
                    //Else there are no solutions congruent to p^{k - 1}
                }
            }

            if( !newLiftedSolutions.isEmpty() ) {
                //Newly lifted solutions are often not in its minimal modulo e.g. [x = 0 mod 49, x = 7 mod 49,
                //x = 14 mod 49, x = 21 mod 49, x = 28 mod 49, x = 35 mod 49, x = 42 mod 49]
                //The reduction below makes it so that we have just x = 0 mod 7 instead
                //It reduces the number of iterations for the next values of k (modulo powers)
                newLiftedSolutions = reduceSolutions( newLiftedSolutions, p_i );
            }

            liftedSolutions = newLiftedSolutions;
        }

        return liftedSolutions;
    }

    private List<ModularCongruence> reduceSolutions( List<ModularCongruence> solutions, long p_i ) {
        //Needs sorting from lowest to greatest residue before it starts the reduction
        sort( solutions, modularCongruenceComparator );

        List<ModularCongruence> reducedSolutions = new ArrayList<>( solutions );
        long originalModulo = solutions.iterator().next().getModulo(); //equal to p_i^currentK

        //Reduced solutions can have modulo p_i^y where 1 < y < currentK
        for( long newModulo = originalModulo / p_i; newModulo > 1; newModulo /= p_i ) {
            if( reducedSolutions.size() % p_i != 0 ) {
                //It means that the reduction is not possible
                //Only size = 0 mod p_i means there is equivalent representation for given solutions
                //containing modulo of the same base p_i but with lower power
                break;
            }

            //Establishing new solution list size considering we reduce the power of modulo by 1
            int newReducedSolutionsSize = (int) ( reducedSolutions.size() / p_i );
            List<ModularCongruence> newReducedSolutions = new ArrayList<>( newReducedSolutionsSize );

            int i = 0;
            //Adding the number of solutions until they exceed the new modulo
            for( ModularCongruence reducedSolution : reducedSolutions ) {
                if( i++ == newReducedSolutionsSize ) {
                    break;
                }

                newReducedSolutions.add( new ModularCongruence( reducedSolution.getResidue(), newModulo ) );
            }

            if( areEquivalentSolutions( newReducedSolutions, reducedSolutions ) ) {
                //If solutions represented by mod p_i^{k - 1} are equal to the ones represented by mod p_i^k
                //then replace them by the simpler ones
                reducedSolutions = newReducedSolutions;
            }
            else {
                //Stop if solutions represented by mod p_i^{k - 1} are not the same as the ones required and
                //represented by mod p_i^k
                break;
            }
        }

        return reducedSolutions;
    }

    private boolean areEquivalentSolutions( List<ModularCongruence> newSolutions,
            List<ModularCongruence> originalSolutions ) {
        //Checks if given two lists contain the same modular values e.g.
        //[1 mod 8, 3 mod 8, 5 mod 8, 7 mod 8] is equal to [1 mod 4, 3 mod 4] and to [1 mod 2]

        //newSolutions is never empty here
        long newModulo = newSolutions.get( 0 ).getModulo();

        for( int originalSolutionsIndex = 0;
             originalSolutionsIndex < originalSolutions.size(); originalSolutionsIndex++ ) {
            int newSolutionsIndex = originalSolutionsIndex % newSolutions.size();
            long originalSolutionResidue = originalSolutions.get( originalSolutionsIndex ).getResidue();
            long newSolutionResidue = newSolutions.get( newSolutionsIndex ).getResidue();

            if( originalSolutionResidue % newModulo != newSolutionResidue ) {
                return false;
            }
        }

        return true;
    }

    private ModularCongruence liftSolutionForNonZeroDerivative( BigInteger a, BigInteger b, BigInteger c,
            ModularCongruence solutionForPrimeModulo, long fDerivativeModPrime, long p_i, int e_i ) {
        //Case "If p_i does not divide f'(X)" where X is a solution to f(x) = 0 mod p_i^{k - 1}
        //Then there is precisely one solution to f(x) = 0 mod p_i^k equal to X + tp_i^{k - 1} where X is a solution to
        //f(x) = 0 mod p_i^{k - 1} and t is the unique solution of f'(X)t = -f(X)/p_i^{k - 1} mod p_i

        //This implementation naming differs from the javadoc description because instead of thinking of
        //knowing solutions to ax^2 + bx + c = 0 mod p_i^k and getting solutions to ax^2 + bx + c = 0 mod p_i^{k + 1} we
        //assume we know solutions to ax^2 + bx + c = 0 mod p_i^{k - 1} and are solving ax^2 + bx + c = 0 mod p_i^k

        //Initialized with solution to ax^2 + bx + c = 0 mod p_i
        ModularCongruence liftedSolution = solutionForPrimeModulo;

        long currentModulo = p_i;
        for( int k = 2; k <= e_i; k++ ) {
            BigInteger X = BigInteger.valueOf( liftedSolution.getResidue() );
            BigInteger f = a.multiply( X.pow( 2 ) ).add( b.multiply( X ) ).add( c ); //f(X) = aX^2 + bX + c

            BigInteger bigPrimeModulo = BigInteger.valueOf( p_i );

            //Solving for t: f`(X)t = -f(X)/p^{k - 1} mod p_i
            ModularCongruence t = linearCongruenceSolver.getSolution( fDerivativeModPrime, BigInteger.ZERO
                    .subtract( f.divide( bigPrimeModulo.pow( decrementExact( k ) ) ) ).mod( bigPrimeModulo )
                    .longValueExact(), p_i );

            currentModulo = multiplyExact( currentModulo, p_i );
            long liftedSolutionResidue = addModulo( X.longValueExact(), multiplyModulo(
                    powerModulo( p_i, decrementExact( k ), currentModulo ), t.getResidue(), currentModulo ),
                                                    currentModulo ); //(X + tp_i^{k - 1}) mod p^k

            liftedSolution = new ModularCongruence( liftedSolutionResidue, currentModulo );
        }

        return liftedSolution;
    }

    private List<ModularCongruence> calculateFinalSolutionsUsingChineseRemainderTheorem(
            List<List<ModularCongruence>> allCasesSolutions ) {
        //Having all solutions to every case:
        //ax^2 + bx + c mod p_0^e_0 -> [x0_0, x0_1, ...
        //ax^2 + bx + c mod p_1^e_1 -> [x1_0, x1_1, ...
        //...
        //ax^2 + bx + c mod p_{k - 1}^e_{k - 1} -> [x{k-1}_0, x{k-1}_1, ...

        //We need to form inputs for chinese remainder theorem by creating every possible combination of solutions
        //e.g. [x0_0, x1_0, ..., x{k-1}_0] is the first one, [x0_0, x1_0, ..., x{k-1}_1] is the second one etc.

        //For case mod 10 (mod 2, mod 5) if the solutions are [1 mod 2], [3 mod 5, 4 mod 5], then we need to apply
        //Chineses Remainder Theorem for two inputs: [1 mod 2, 3 mod 5] and [1 mod 2, 4 mod 5].
        //Two results are obtained that way

        List<ModularCongruence> finalSolutions = new ArrayList<>();

        int[] caseSolutionsSizes = getCaseSolutionsSizes( allCasesSolutions );
        IterationIndexesGenerator iterationIndexesGenerator = new IterationIndexesGenerator( caseSolutionsSizes );

        //Iterates through all combinations of chinese remainder theorem inputs
        for( List<Integer> primeFactorSolutionsIndexes : iterationIndexesGenerator ) {
            List<ModularCongruence> chineseRemainderTheoremInput = getChineseRemainderTheoremInput(
                    primeFactorSolutionsIndexes, allCasesSolutions );

            BigModularCongruence solution = chineseRemainderTheoremAlgorithm
                    .getSolution( chineseRemainderTheoremInput );
            finalSolutions.add( convertToModularCongruence( solution ) );
        }

        sort( finalSolutions, modularCongruenceComparator );
        return finalSolutions;
    }

    private int[] getCaseSolutionsSizes( List<List<ModularCongruence>> allCasesSolutions ) {
        int[] caseSolutionsSizes = new int[allCasesSolutions.size()];

        for( int i = 0; i < allCasesSolutions.size(); i++ ) {
            caseSolutionsSizes[i] = allCasesSolutions.get( i ).size();
        }

        return caseSolutionsSizes;
    }

    private List<ModularCongruence> getChineseRemainderTheoremInput( List<Integer> factorSolutionsIndexes,
            List<List<ModularCongruence>> allCasesSolutions ) {
        List<ModularCongruence> chineseRemainderTheoremInput = new ArrayList<>( factorSolutionsIndexes.size() );

        for( int i = 0; i < factorSolutionsIndexes.size(); i++ ) {
            chineseRemainderTheoremInput.add( allCasesSolutions.get( i ).get( factorSolutionsIndexes.get( i ) ) );
        }

        return chineseRemainderTheoremInput;
    }

    private ModularCongruence convertToModularCongruence( BigModularCongruence modularCongruence ) {
        return new ModularCongruence( modularCongruence.getResidue().longValueExact(),
                                      modularCongruence.getModulo().longValueExact() );
    }

    //Comparator for modular congruences which can be used only for values with the same modulo
    //It compares the residue values and can be used in sorting by ascending residue values
    private class ModularCongruenceComparator implements Comparator<ModularCongruence> {

        @Override
        public int compare( ModularCongruence modularCongruence1, ModularCongruence modularCongruence2 ) {
            return Long.compare( modularCongruence1.getResidue(), modularCongruence2.getResidue() );
        }
    }
}
