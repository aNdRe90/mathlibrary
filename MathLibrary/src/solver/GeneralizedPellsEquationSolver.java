package solver;

import algorithm.DivisorsAlgorithm;
import algorithm.ModularSquareRootAlgorithm;
import number.properties.checker.PolygonalNumberChecker;
import number.representation.GeneralContinuedFraction;
import number.representation.ModularCongruence;
import number.representation.PeriodicContinuedFraction;

import java.math.BigInteger;
import java.util.*;

import static java.lang.Math.*;
import static java.util.Arrays.asList;
import static util.BasicOperations.abs;

/**
 * Solver for generalized Pell`s equation - the equation of form: \(x^2 - Dy^2 = N\).
 * <br>\(x, y, D, N\) are integers and \(x \geq  0, y \geq 0\).
 *
 * <br><br>1) \(D = 0\), \(x^2 - 0\cdot y^2 = N \Leftrightarrow x^2 = N\)
 * <ul><li>
 * \(N \geq 0\) and \(N\) is a square number (\(N = n^2\), \(n \geq 0\)). Then \(x = n\)
 * and we have infinite number of solutions:
 * \([x,y] = [n, 0], [n, 1], [n, 2], [n, 3], [n, 4], ...\) .
 * </li></ul>
 * <ul><li>
 * \(N &lt; 0\) or \(N &gt; 0\) and is not a square number. No solution exists.
 * </li></ul>
 *
 * <br>2) \(N = 0\), \(x^2 - Dy^2 = 0 \Leftrightarrow x^2 = Dy^2\)
 * <ul><li>
 * \(D &lt; 0\). The only solution is \([0,0]\).
 * </li></ul>
 * <ul><li>
 * \(D &gt; 0\) and \(D\) is a square number \((D = d^2)\). Then \(x^2 = (dy)^2\) and \(x = dy\).
 * </li></ul>
 * <ul><li>
 * \(D &gt; 0\) and \(D\) is not a square number. No solution exists.
 * </li></ul>
 *
 * <br>3) \(D &lt; 0\), \(x^2 - Dy^2 = N \Leftrightarrow x = \sqrt{N + Dy^2}\)
 * <br>In this case, there is a finite number of solutions and as we have a constant value of \(N\) in {@link Long}
 * range and we are dealing with squares, simple iteration is enough to solve this.
 * <br>Because \(y &lt; x\), we will iterate over the values of \(y\) and match the values of \(x\).
 * Knowing that \(N + Dy^2 \geq 0\) (value under square root) we have \(N \geq -Dy^2\) which will be our limit
 * to stop the iteration.
 *
 * <br><br>4) \(D\) &gt; 0 and \(D\) is a square number (\(D = d^2\))
 * <br>\(x^2 - Dy^2 = N \Leftrightarrow x^2 - (dy)^2 = N \Leftrightarrow (x - dy)(x + dy) = N\)
 *
 * <br><br>Divisors of \(N\) need to be found in this case.
 * If \(N = \text{divisor}_1 \cdot \text{divisor}_2\) and \(\text{divisor}_1 \leq \text{divisor}_2\) then:
 * \begin{align}
 * x - dy &amp; = \text{divisor}_1 \\
 * x + dy &amp; = \text{divisor}_2
 * \end{align}
 * <br>Solving this we get:
 * \begin{align}
 * x &amp; = \text{divisor}_1 + dy \\
 * y &amp; = \frac{\text{divisor}_2 - \text{divisor}_1}{2d}
 * \end{align}
 * Additionally, if \(N &lt; 0\) then \(\text{divisor}_1 &lt; 0\).
 *
 * <br><br>5) In order to present how finding the solution of \(x^2 - Dy^2 = N\) where \(D\) is a positive nonsquare
 * integer and \(N \neq 0\) is implemented let`s introduce the \(PQa\) algorithm
 * <br>which is an algorithm computing the continued fraction expansion of the quadratic irrational
 * \(\frac{P_0 + \sqrt{D}}{Q_0}\).
 * <br>Let \(P_0, Q_0, D\) be integers so that \(Q_0 \neq 0, D &gt; 0\) is not a square number and
 * \(P_0^2 \equiv D \text{ mod } |Q_0|\).
 * We will be calculating \(P_k, Q_k, a_i, A_i, B_i, G_i\) where \(i \geq 0, k \geq 1\) by using the following formulas:
 * \begin{align}
 * a_i &amp; = \left\lfloor\frac{P_i + \sqrt{D}}{Q_i}\right\rfloor \\\\
 * A_{-2} &amp; = 0 \\
 * A_{-1} &amp; = 1 \\
 * A_i &amp; = a_iA_{i-1} + A_{i-2} \\\\
 * B_{-2} &amp; = 1 \\
 * B_{-1} &amp; = 0 \\
 * B_i &amp; = a_iB_{i-1} + B_{i-2} \\\\
 * G_{-2} &amp; = -P_0 \\
 * G_{-1} &amp; = Q_0 \\
 * G_i &amp; = a_iG_{i - 1} + G_{i-2} \\\\
 * P_i &amp; = a_{i - 1}Q_{i - 1} - P_{i - 1} \\
 * Q_i &amp; = \frac{D - P_i^2}{Q_{i - 1}} \\
 * \end{align}
 *
 * <br>In order to understand the \(PQa\) algorithm, please see {@link PeriodicContinuedFraction} (value \(a_i\)
 * is a coefficient and \(\frac{A_i}{B_i}\) is a convergent of the periodic continued fraction).
 *
 * <br><br>Important properties of calculated values:
 * <ul><li>
 * \(a_i\) is the \(i\)-th continued fraction`s coefficient (\(a_0\) can be negative, \(a_k &gt; 0\) for \(k \geq 1\)).
 * </li></ul>
 * <ul><li>
 * Each of the sequences \(\{a_i\}, \{P_i\}, \{Q_i\}\) is eventually periodic. Specifically, there is a least
 * nonnegative integer \(i_0\) and a least positive integer \(L\), the length of the minimal period,
 * <br>so that for any integer \(i \geq i_0\) and \(k &gt; 0, a_{i + kL} = a_i, P_{i+kL} = P_i, Q_{i+kL} = Q_i\).
 * It means that \(i_0\) points to the start of the period so for \(P_0 = 0, Q_0 = 1\) \(i_0 = 1\).
 * </li></ul>
 * <ul><li>
 * For \(i \geq i_0, 0 &lt; P_i &lt; \sqrt{D}, 0 &lt; \sqrt{D} − P_i &lt; Q_i &lt; \sqrt{D} + P_i &lt; 2\sqrt{D}\)
 * (data limit for \(P_i\) and \(Q_i\)).
 * </li></ul>
 * <ul><li>
 * For \(i \geq i_0\), if \(Q_i \neq 1\) then \(a_i &lt; \sqrt{D}\),
 * while if \(Q_i = 1\) then \(\sqrt{D} &lt; a_i &lt; 2\sqrt{D}\) (data limit for \(a_i\) and \(Q_i\)).
 * </li></ul>
 * <ul><li>
 * \(\frac{A_i}{B_i}\) is the \(i\)-th convergent
 * (\(\frac{P_0 + \sqrt{D}}{Q_0}\) = \(\lim_{i\to\infty} \frac{A_i}{B_i}\)).
 * </li></ul>
 * <ul><li>
 * \(\\text{gcd}(A_i, B_i) = 1\) for \(i \geq -2\).
 * </li></ul>
 * <ul><li>
 * \(G_i^2 - DB_i^2 = (-1)^iQ_0Q_i\).
 * </li></ul>
 * <ul><li>
 * \(G_i = A_i\) for \(P_0 = 0, Q_0 = 1\).
 * </li></ul>
 *
 * <br><br>Pell`s equation case \(x^2 - Dy^2 = \pm 1\), where \(D &gt; 0\) and \(D\) is not a square number
 * <br>To understand how it is solved, getting familiar with continued fractions is required (see
 * {@link GeneralContinuedFraction} and {@link PeriodicContinuedFraction} for more details).
 *
 * <br><br>Generally speaking, continued fractions are a way of representing real value numbers.
 * <br>They have a  property which allows us to approximate the value of given real number with the rational value
 * \(\frac{A_i}{B_i}\) called the \(i\)-th (\(i \geq 0\)) convergent for which \(\text{gcd}(A_i, B_i) = 1\).
 * <br>The bigger the \(i\), the more precise the approximation is.
 *
 * <br><br>Turns out that if we create a continued fraction for \(\sqrt{D}\) some of its convergents solve the Pell`s
 * equation. More precisely, if \(\frac{A_i}{B_i}\) is a convergent estimating the value of \(\sqrt{D}\)
 * <br>then for some values of \(i\) \(A_i^2 - DB_i^2 = 1\) or \(A_i^2 - DB_i^2 = -1\), and it happens regularly
 * under certain conditions.
 *
 * <br><br>Let`s consider a continued fraction of form
 * $$a_0 + \frac{1}{a_1 + \frac{1}{a_2 + \frac{1}{a_3 + \frac{1}{a_4 + ...}}}}$$
 * <br>where \(a_i\) is called a continued fraction`s cofficient
 * <br>When the fraction is representing the value \(\frac{P_0 + \sqrt{D}}{Q_0}\), where \(P_0\) is any integer,
 * \(D\) is a positive nonsquare integer, \(Q_0\) is an integer and \(Q_0 \neq 0\), has periodical coefficients
 * \(a_i\), \(i \geq 1\).
 * <br>When \(P_0 = 0, D = D, Q_0 = 1\) (our case here) the period starts at \(a_1\) and ends when the coefficient
 * \(a_i = 2a_0\). Moreover, the coefficients` period is symmetrical and looks like this:
 * <br>$$\sqrt{D} = [a_0; \overline{a_1, a_2, a_3, ..., a_3, a_2, a_1, 2a_0}]$$
 *
 * <br><br>Now, in order to solve the Pell`s equation \(x^2 - Dy^2 = \pm 1\) first calculate the continued fraction for
 * \(\sqrt{D}\) and determine its coefficients` period. Let`s say that the period is of length \(L\) \((a_L = 2a_0)\).
 * <br>\(L\) can be an odd or even number. Let`s also call the \(\frac{A_i}{B_i}\) the \(i\)-th (\(i \geq 0\) convergent
 * of the continued fraction. It turns out that:
 *
 * <br><br>
 * When \(L\) is even:
 * <ul><li>
 * \(x^2 - Dy^2 = -1\) has no solution.
 * </li></ul>
 * <ul><li>
 * \(x^2 - Dy^2 = 1\) has infinite number of solutions equal to:
 *
 * \begin{align}
 * x &amp; = A_{kL - 1} &amp; \text{for } k = 1, 2, 3, 4, 5, ...\\
 * y &amp; = B_{kL - 1} &amp; \text{for } k = 1, 2, 3, 4, 5, ...
 * \end{align}
 * </li></ul>
 *
 * When L is odd:
 * <ul><li>
 * \(x^2 - Dy^2 = -1\) has infinite number of solutions equal to:
 * \begin{align}
 * x &amp; = A_{kL - 1} &amp; \text{for } k = 1, 3, 5, 7, 9, ...\\
 * y &amp; = B_{kL - 1} &amp; \text{for } k = 1, 3, 5, 7, 9, ...
 * \end{align}
 * </li></ul>
 * <ul><li>
 * \(x^2 - Dy^2 = 1\) has infinite number of solutions equal to:
 * \begin{align}
 * x &amp; = A_{kL - 1} &amp; \text{for } k = 2, 4, 6, 8, 10, ...\\
 * y &amp; = B_{kL - 1} &amp; \text{for } k = 2, 4, 6, 8, 10, ...
 * \end{align}
 * </li></ul>
 * Additionally, \(x^2 - Dy^2 = 1\) has always a trivial solution \([x,y] = [1,0]\) which will not be included in the
 * described procedure.
 *
 * <br><br>To implement that proces of solving the Pell`s equation \(x^2 - Dy^2 = \pm 1\) apply the \(PQa\) algorithm
 * with: \(P_0 = 0, Q_0 = 1, D = D\).
 * <br>There will be a smallest \(L\) with \(a_L = 2a_0\), which will also be the smallest \(L &gt; 0\) so that
 * \(Q_L = 1\). \(L\) is the length of the period of the continued fraction for \(\sqrt{D}\).
 *
 * <br><br>When \(L\) is even:
 * <ul><li>
 * \(x^2 - Dy^2 = -1\) has no solutions
 * </li></ul>
 * <ul><li>
 * \(x^2 - Dy^2 = 1\) has infinite number of solutions equal to:
 * \begin{align}
 * x_{-1} &amp; = 1 \\
 * y_{-1} &amp; = 0 \\\\
 * x_0 &amp; = G_{L - 1} \\
 * y_0 &amp; = B_{L - 1} \\
 * t &amp; = x_0 \\
 * u &amp; = y_0 \\\\
 * x_{n + 1} &amp; = tx_n + uy_nD &amp; \text{for } n \geq 0 \\
 * y_{n + 1} &amp; = ty_n + ux_n &amp; \text{for } n \geq 0
 * \end{align}
 * </li></ul>
 *
 * <br><br>When \(L\) is odd:
 * <ul><li>
 * \(x^2 - Dy^2 = \pm 1\) has infinite number of solutions equal to:
 * \begin{align}
 * x_{-1} &amp; = 1 \\
 * y_{-1} &amp; = 0 \\\\
 * x_0 &amp; = G_{L - 1} \\
 * y_0 &amp; = B_{L - 1} \\
 * t &amp; = x_0 \\
 * u &amp; = y_0 \\\\
 * x_{n + 1} &amp; = tx_n + uy_nD &amp; \text{for } n \geq 0 \\
 * y_{n + 1} &amp; = ty_n + ux_n &amp; \text{for } n \geq 0
 * \end{align}
 *
 * <br><br>If \(n \equiv 0 \text{ mod } 2\) then [\(x_n, y_n]\) solves \(x^2 - Dy^2 = -1\)
 * <br>If \(n \equiv 1 \text{ mod } 2\) then [\(x_n, y_n]\) solves \(x^2 - Dy^2 = 1\)
 * </li></ul>
 *
 * <br><br>
 * <br><br>Structure of solutions to \(x^2 - Dy^2 = N\) where \(D\) is a positive nonsquare integer, \(N \neq 0\) and
 * \(|N| \neq 1\):
 * <br>If \([x, y] = [r, s]\) is a solution to \(x^2 - Dy^2 = N\), and \([x, y] = [t, u]\) is any solution to
 * \(x^2 - Dy^2 = 1\), then \([x, y] = [rt + suD, ru + st]\) is also a solution to \(x^2 - Dy^2 = N\).
 * <br><br>It means that we need to find the fundamental solutions \([r, s]\) and by using the solutions to
 * \(x^2 - Dy^2 = 1\) we can obtain all the results for \(x^2 - Dy^2 = N\).
 * <br>We can say that each \([r, s]\) makes the different class of solutions. The trick will be just to order them all
 * so that they are returned by ascending values of \(x\).
 *
 * <br><br>Process of calculating the fundamental solutions \([r, s]\):
 * <ul><li>
 * \(1 &lt; N^2 &lt; D\):
 * <br>Apply the \(PQa\) algorithm with \(P_0 = 0, Q_0 = 1, D = D\) until the first \(k\) for which
 * \(G_{k - 1}^2 - DB_{k - 1}^2 = 1\) (\(Q_k = 1\) and \(k = L\) or \(k = 2L\)).
 * <br>For \(0 \leq i &lt; k\), if \(G_i^2 - DB_i^2 = \frac{N}{f^2}\) for some \(f &gt; 0\), add \([fG_i, fB_i]\)
 * to the list of fundamental solutions.
 * </li></ul>
 * <ul><li>
 * \(N^2 &gt; D\) (\(LMM\) algorithm):
 * <br>Make a list of \(f &gt; 0\) so that \(f^2\) divides \(N\). For each \(f\) in this list, set
 * \(m = \frac{N}{f^2}\).  Find all \(z\) so that \(\frac{−|m|}{2} &lt; z \leq \frac{|m|}{2}\) and
 * \(z^2 \equiv D \text{ mod } |m|\). For each such \(z\), apply the \(PQa\) algorithm with
 * \(P_0 = z, Q_0 = |m|, D = D\).
 * <br>Continue until either there is an \(i \geq 1\) with \(Q_i = \pm 1\), or, without
 * having reached an \(i\) with \(Q_i = \pm 1\), you reach the end of the first period for the sequence \(a_i\).
 * <br>In the latter case, there will not be any \(i\) with \(Q_i = \pm 1\). If you reached an \(i\) with \(Q_i = \pm
 * 1\), then look at \(r = G_{i − 1}, s = B_{i − 1}\). If \(r^2 − Ds^2 = m\), then add \([fr, fs]\) to the list of
 * fundamental solutions.
 * <br>Otherwise, \(r^2 −Ds^2 = −m\). If the equation \(t^2 −Du^2 = −1\) does not have solutions, test the next \(z\).
 * If it has solutions, let the minimal positive solution be \([t, u]\), and add \([f(rt+sud), f(ru+st)]\)
 * to the list of fundamental solutions.
 * </li></ul>
 *
 * <br><br>Important to note is that if \(N = n^2\) is a square number, then \(x^2 - Dy^2 = N\) has a first solution
 * equal to \([n, 0]\).
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/Pell%27s_equation">Wikipedia - Pell`s equation</a>
 * @see <a href="http://mathworld.wolfram.com/PellEquation.html">Mathworld Wolfram - Pell`s equation</a>
 * @see <a href="http://www.jpr2718.org/pell.pdf">John P. Robertson - "Solving the generalized Pell equation
 * \(x^2 - Dy^2 = N\)"</a>
 */
public class GeneralizedPellsEquationSolver {
    /**
     * Limit for coefficients period of continued fraction made for \(\sqrt{D}\) equal to {@value PERIOD_LIMIT}.
     * <br>It makes the algorithm stop if looking for period takes more than certain number of iterations. Used due to
     * the fact that longer iterations cause a very long computation time and take a lot of memory.
     * <br>Note that for case \(N^2 &gt; D\) there are multiple \(PQa\) algorithms executed so it will take even
     * longer than a single one when \(|N| = 1\) or when \(N^2 &lt; D\).
     */
    public static final long PERIOD_LIMIT = 100000;

    private PolygonalNumberChecker polygonalNumberChecker = new PolygonalNumberChecker();
    private ModularSquareRootAlgorithm modularSquareRootAlgorithm = new ModularSquareRootAlgorithm();
    private DivisorsAlgorithm divisorsAlgorithm = new DivisorsAlgorithm();

    /**
     * Solves the \(x^2 - Dy^2 = N\) equation where \(x, y, D, N\) are integers.
     * <br>For the detailed description of the implementation, see {@link GeneralizedPellsEquationSolver}.
     *
     * <br><br>Important to note here is that there is a limit set for the calculations so that they do not take
     * forever and use a huge amount of memory. The limit refers to the number of iterations in \(PQa\) algorithm or,
     * in other words, to the length of coefficients period of continued fraction for the value \(\sqrt{D}\). The
     * limit is set to {@value PERIOD_LIMIT}.
     *
     * <br><br>In case the limit is exceeded and the period is longer than the {@value PERIOD_LIMIT},
     * {@link ArithmeticException} is thrown when trying to invoke the {@link Iterable#iterator()} method from the
     * returned {@link Iterable} object.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param D value of \(D\)
     * @param N value of \(N\)
     *
     * @return {@link Iterable} solutions to \(x^2 - Dy^2 = N\) equation where \(x, y, D, N\) are integers. The result
     * array elements are \([x, y]\), where \(x \geq 0, y \geq 0\). The solutions are ordered by ascending values
     * of \(x\).
     *
     * @see <a href="https://en.wikipedia.org/wiki/Pell%27s_equation">Wikipedia - Pell`s equation</a>
     * @see <a href="http://mathworld.wolfram.com/PellEquation.html">Mathworld Wolfram - Pell`s equation</a>
     * @see <a href="http://www.jpr2718.org/pell.pdf">John P. Robertson - "Solving the generalized Pell equation
     * \(x^2 - Dy^2 = N\)"</a>
     */
    public Iterable<BigInteger[]> getSolutions( long D, long N ) {
        return () -> {
            if( D == 0 ) {
                //x^2 - 0y^2 = N -> x^2 = N
                return new DEqualsZero_SolutionsIterator( N );
            }

            if( N == 0 ) {
                //x^2 - Dy^2 = 0 -> x^2 = Dy^2
                return new NEqualsZero_SolutionsIterator( D );
            }

            if( D < 0 ) {
                //x^2 - Dy^2 = N -> x^2 + |D|y^2 = N
                return new NegativeD_SolutionsIterator( D, N );
            }

            if( polygonalNumberChecker.isSquare( D ) ) {
                //D = d^2
                //x^2 - Dy^2 = N -> x^2 - (dy)^2 = N -> (x - dy)(x + dy) = N
                return new SquarePositiveD_SolutionsIterator( D, N );
            }

            if( abs( N ) == 1 ) {
                //x^2 - Dy^2 = 1, x^2 - Dy^2 = -1
                return new ProperPellsEquation_SolutionsIterator( D, N );
            }

            if( abs( N ) < sqrt( D ) ) {
                //x^2 - Dy^2 = N, N^2 < D
                return new NSquaredLessThanD_SolutionsIterator( D, N );
            }

            //x^2 - Dy^2 = N, N^2 > D
            return new NSquaredGreaterThanD_SolutionsIterator( D, N );
        };
    }

    private class DEqualsZero_SolutionsIterator implements Iterator<BigInteger[]> {
        private final BigInteger xSolution;
        private BigInteger currentYSolution;

        public DEqualsZero_SolutionsIterator( long N ) {
            if( N == 0 ) {
                //Separate handling because the below method for determining square numbers does not accept 0
                xSolution = BigInteger.ZERO;
            }
            else if( polygonalNumberChecker.isSquare( N ) ) {
                xSolution = BigInteger.valueOf( (long) sqrt( N ) );
            }
            else {
                xSolution = null;
            }

            currentYSolution = BigInteger.ZERO;
        }

        @Override
        public boolean hasNext() {
            return xSolution != null;
        }

        @Override
        public BigInteger[] next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            //x^2 - 0y^2 = N -> x^2 = N
            //If N = 0 or is a positive square number N = n^2 then x = n (constant)
            //y values are irrelevant since D = 0 so we iterate through 0, 1, 2, 3, 4, ...
            BigInteger returnedY = currentYSolution;
            currentYSolution = currentYSolution.add( BigInteger.ONE );

            return new BigInteger[]{ xSolution, returnedY };
        }
    }



    private class NEqualsZero_SolutionsIterator implements Iterator<BigInteger[]> {
        private final long D;
        private final BigInteger d;
        private final boolean hasSolutionsForPositiveD;
        private boolean onlySolutionForNegativeDReturned;
        private BigInteger currentY;

        public NEqualsZero_SolutionsIterator( long D ) {
            this.D = D;
            hasSolutionsForPositiveD = polygonalNumberChecker.isSquare( D );
            d = BigInteger.valueOf( (long) sqrt( D ) );
            onlySolutionForNegativeDReturned = false;
            currentY = BigInteger.ZERO;
        }

        @Override
        public boolean hasNext() {
            //Case D = 0 is handled earlier by DEqualsZero_SolutionsIterator
            return D > 0 ? hasSolutionsForPositiveD : !onlySolutionForNegativeDReturned;
        }

        @Override
        public BigInteger[] next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            //Case D = 0 is handled earlier by DEqualsZero_SolutionsIterator
            if( D < 0 ) {
                onlySolutionForNegativeDReturned = true;

                //only [0,0] is the solution when x^2 - Dy^2 = 0 and D < 0
                return new BigInteger[]{ BigInteger.ZERO, BigInteger.ZERO };
            }

            //D is a square number (D = d^2) so x^2 - Dy^2 = 0 -> x^2 = (dy)^2 means that
            //x = 0, d, 2d, 3d, 4d, ...
            //y = 0, 1, 2, 3, 4, ....
            BigInteger[] currentResult = { d.multiply( currentY ), currentY };
            currentY = currentY.add( BigInteger.ONE );

            return currentResult;
        }
    }



    private class NegativeD_SolutionsIterator implements Iterator<BigInteger[]> {
        private final long D;
        private final long N;
        private Iterator<BigInteger[]> solutionsIterator;

        public NegativeD_SolutionsIterator( long D, long N ) {
            this.D = D;
            this.N = N;
            solutionsIterator = getSolutions().iterator();
        }

        private List<BigInteger[]> getSolutions() {
            List<BigInteger[]> solutions = new ArrayList<>();

            //x^2 - Dy^2 = N
            //x = sqrt(N + Dy^2), so N + Dy^2 >= 0
            //We will iterate over y because it is faster (max possible x >= max possible y)

            BigInteger bigN = BigInteger.valueOf( N );
            BigInteger bigD = BigInteger.valueOf( D );
            for( long y = 0; ; y++ ) {
                long y2 = multiplyExact( y, y );
                if( bigN.add( bigD.multiply( BigInteger.valueOf( y2 ) ) ).signum() == -1 ) { //N + Dy^2 < 0
                    break;
                }

                long x2 = addExact( N, multiplyExact( D, y2 ) );
                if( x2 == 0 || polygonalNumberChecker.isSquare( x2 ) ) {
                    long x = (long) sqrt( x2 );
                    solutions.add( new BigInteger[]{ BigInteger.valueOf( x ), BigInteger.valueOf( y ) } );
                }
            }

            //Iterating over y meant that values of y were growing but the values of x were decreasing
            //We want the results to be ordered from the smallest x so that is why we need to reverse the list
            Collections.reverse( solutions );
            return solutions;
        }

        @Override
        public boolean hasNext() {
            return solutionsIterator.hasNext();
        }

        @Override
        public BigInteger[] next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            return solutionsIterator.next();
        }


    }



    private class SquarePositiveD_SolutionsIterator implements Iterator<BigInteger[]> {
        private final long D;
        private final long N;

        private Iterator<BigInteger[]> solutionsIterator;

        public SquarePositiveD_SolutionsIterator( long D, long N ) {
            this.D = D;
            this.N = N;
            solutionsIterator = getSolutions().iterator();
        }

        private List<BigInteger[]> getSolutions() {
            List<BigInteger[]> solutions = new ArrayList<>();

            //When D > 0 and is a square number
            //x^2 - Dy^2 = N -> x^2 - (dy)^2 = N = (x - dy)(x + dy) = N
            //We need to find the divisors of N and solve it for x, dy
            //Additionally if N < 0 then x < dy (because we assume x > 0, y > 0)
            long d = (long) sqrt( D );
            Collection<Long> divisorsOfN = N > 0 ? divisorsAlgorithm.getDivisors( N ) :
                    divisorsAlgorithm.getDivisors( subtractExact( 0, N ) );

            //N = divisor1*divisor2         divisor1 <= divisor2
            //x - dy = divisor1             divisor1 + dy = divisor2 - dy
            //x + dy = divisor2             dy = (divisor2 - divisor1) / 2

            //y = (divisor2 - divisor1) / 2d
            //x = divisor1 + dy
            //Additionally if N < 0 then divisor1 < 0
            for( long divisor1 : divisorsOfN ) {
                if( N < 0 ) {
                    divisor1 = subtractExact( 0, divisor1 );
                }

                long divisor2 = N / divisor1;
                if( abs( divisor1 ) > divisor2 ) {
                    break;
                }

                if( subtractExact( divisor2, divisor1 ) % multiplyExact( 2, d ) == 0 ) {
                    long y = subtractExact( divisor2, divisor1 ) / multiplyExact( 2, d );
                    long x = addExact( divisor1, multiplyExact( d, y ) );
                    solutions.add( new BigInteger[]{ BigInteger.valueOf( x ), BigInteger.valueOf( y ) } );
                }
            }

            //divisor1 is growing during the above iterations and divisor2 = N / divisor1 is decreasing
            //so y = (divisor2 - divisor1) / 2d is decreasing, so is x = divisor1 + dy as well
            //We want the list of growing values of x, so we need to reverse the list of solutions
            Collections.reverse( solutions );
            return solutions;
        }

        @Override
        public boolean hasNext() {
            return solutionsIterator.hasNext();
        }

        @Override
        public BigInteger[] next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            return solutionsIterator.next();
        }


    }



    private class ProperPellsEquation_SolutionsIterator implements Iterator<BigInteger[]> {
        private final long D;
        private final long N;
        private final BigInteger t;
        private final BigInteger u;

        private BigInteger[] fundamentalSolution; //[t,u] = [x_0, y_0]
        private BigInteger currentX;
        private BigInteger currentY;

        public ProperPellsEquation_SolutionsIterator( long D, long N ) {
            this.D = D;
            this.N = N;

            fundamentalSolution = getFundamentalSolution();
            t = fundamentalSolution[0];
            u = fundamentalSolution[1];

            //If N = 1, then we need to first return the solution [x_{-1},y_{-1}] = [1,0]
            //in this case, the next solution [x_0, y_0] = [tx_{-1} + uy_{-1}D, ty_{-1} + ux_{-1}] = [t, u]
            //which is already the case for N = -1 so the rest of the implementation is the same for both cases
            currentX = N == 1 ? BigInteger.ONE : t;
            currentY = N == 1 ? BigInteger.ZERO : u;
        }

        private BigInteger[] getFundamentalSolution() {
            //PQa algorithm with P_0 = 0, Q_0 = 1, D = D
            long P = 0;
            long Q = 1;
            long a0 = (long) floor( ( P + sqrt( D ) ) / Q ); //a_0 = floor( (P_0 + sqrt(D)) / Q_0 )
            long a = a0;

            BigInteger previousB = BigInteger.ONE; //B_{-2} = 1
            BigInteger currentB = BigInteger.ZERO; //B_{-1} = 0
            BigInteger previousG = BigInteger.valueOf( P ).negate(); //G_{-2} = -P_0
            BigInteger currentG = BigInteger.valueOf( Q ); //G_{-1} = Q_0

            BigInteger temp = currentB;
            currentB = BigInteger.valueOf( a ).multiply( currentB ).add( previousB ); //B_0 = aB_{-1} + B_{-2}
            previousB = temp; //B_{-1} = 0

            temp = currentG;
            currentG = BigInteger.valueOf( a ).multiply( currentG ).add( previousG ); //G_0 = aG_{-1} + G_{-2}
            previousG = temp; //G_{-1} = Q_0

            long i = 1; //period index
            while( true ) {
                P = subtractExact( multiplyExact( a, Q ), P ); //P_i = a_{i - 1}Q_{i - 1} - P_{i - 1}
                Q = ( subtractExact( D, multiplyExact( P, P ) ) ) / Q; //Q_i = (D - P_i^2) / Q_{i - 1}
                a = (long) floor( ( P + sqrt( D ) ) / Q ); //a_i = floor( (P_i + sqrt(D)) / Q_i )

                if( Q == 1 && a == multiplyExact( 2, a0 ) ) { //end of period, i holds the period length
                    if( i % 2 == 0 ) { //period is even
                        if( N == 1 ) {
                            //[x0, y0] == [G_{i - 1}, B_{i - 1}]
                            return new BigInteger[]{ currentG, currentB };
                        }
                        else { // if (N == -1)
                            return new BigInteger[2]; //no solution
                        }
                    }
                    else { //i % 2 == 1, period is odd
                        if( N == 1 ) {
                            //[G_{i - 1}, B_{i - 1}] solves x0^2 - Dy^2 = -1 so we need to apply the recurrence:
                            //x_{n + 1} = tx_n + uy_nD
                            //y_{n + 1} = ty_n + ux_n
                            //where t = G_{i - 1}, u = B_{i - 1}

                            BigInteger t = currentG;
                            BigInteger u = currentB;
                            BigInteger x0 = t.multiply( currentG )
                                             .add( u.multiply( currentB ).multiply( BigInteger.valueOf( D ) ) );
                            BigInteger y0 = t.multiply( currentB ).add( u.multiply( currentG ) );
                            return new BigInteger[]{ x0, y0 };
                        }
                        else { //if (N == -1)
                            //[x0, y0] == [G_{i - 1}, B_{i - 1}]
                            return new BigInteger[]{ currentG, currentB };
                        }
                    }
                }

                //B_i = a_iB_{i - 1} + B_{i - 2}
                temp = currentB;
                currentB = BigInteger.valueOf( a ).multiply( currentB ).add( previousB );
                previousB = temp;

                //G_i = a_iG_{i - 1} + G_{i - 2}
                temp = currentG;
                currentG = BigInteger.valueOf( a ).multiply( currentG ).add( previousG );
                previousG = temp;

                i = incrementExact( i ); //Increasing the period index
                if( i > PERIOD_LIMIT ) {
                    //stop as the computation is already too long and we will unfortunately not return
                    //any result
                    throw new ArithmeticException(
                            "Cannot calculate solutions for x^2 - " + D + "y^2 = " + N + " because the " + "continued" +
                            " fraction for sqrt(D) value has a perdio longer than " + PERIOD_LIMIT );
                }
            }
        }

        @Override
        public boolean hasNext() {
            return currentX != null && currentY != null;
        }

        BigInteger[] peekNext() {
            return new BigInteger[]{ currentX, currentY };
        }

        @Override
        public BigInteger[] next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            BigInteger[] currentResult = new BigInteger[]{ currentX, currentY };
            calculateNextXY();
            return currentResult;
        }

        private void calculateNextXY() {
            BigInteger previousX = currentX;
            BigInteger previousY = currentY;

            //x_n = tx_{n - 1} + uy_{n - 1}D
            //y_n = ty_{n - 1} + ux_{n - 1}
            currentX = t.multiply( previousX ).add( u.multiply( previousY ).multiply( BigInteger.valueOf( D ) ) );
            currentY = t.multiply( previousY ).add( u.multiply( previousX ) );

            if( N == -1 ) {
                //For N = -1 the procedure needs to be applied twice
                previousX = currentX;
                previousY = currentY;
                currentX = t.multiply( previousX ).add( u.multiply( previousY ).multiply( BigInteger.valueOf( D ) ) );
                currentY = t.multiply( previousY ).add( u.multiply( previousX ) );
            }
        }


    }



    private abstract class GeneralizedPellsEquation_SolutionsIterator implements Iterator<BigInteger[]> {
        protected final long D;
        protected final long N;

        //Comparator for sorting [x, y] solution arrays by ascending values of x
        protected final Comparator<BigInteger[]> solutionsComparator = ( o1, o2 ) -> o1[0].compareTo( o2[0] );

        //In order to merge the solutions so that they are returned by ascending value of x we keep, we keep
        //the separate iterators of solutions to x^2 - Dy^2 = 1 (values of [t,u]) for each fundamental solution [r, s]
        //If [r, s] is a solution to x^2 −Dy^2 = N, and [t, u] is a solution to x^2 −Dy^2 = 1,
        //then [rt + suD, ru + s], is also a solution to x 2 − Dy 2 = N
        private List<BigInteger[]> fundamentalSolutions;
        private List<ProperPellsEquation_SolutionsIterator> nEqualsOneSolutionIterators;

        //We return the solutions in chunks which hold the [x,y] sorted by ascending values of x
        private List<BigInteger[]> solutionsChunk;

        //Chunk set is needed as there is a possibility that some solutions from previous chunk are included
        //in the next chunk
        //In that case they need to be removed or duplicates will be returned
        private Set<List<BigInteger>> solutionsChunkSet;
        private int solutionsChunkIndex; //index for returned solutions from the current chunk list

        //If N > 0 and N = n^2 is a square number then x^2 −Dy^2 = N has a first solution equal to [n, 0]
        private boolean shouldReturnTrivialSolution;

        public GeneralizedPellsEquation_SolutionsIterator( long D, long N ) {
            this.D = D;
            this.N = N;

            fundamentalSolutions = getSortedFundamentalSolutions();
            solutionsChunk = new ArrayList<>( fundamentalSolutions );
            solutionsChunkIndex = solutionsChunk.size();

            nEqualsOneSolutionIterators = getNEqualsOneSolutionIterators( fundamentalSolutions.size() );
            shouldReturnTrivialSolution = N > 0 && polygonalNumberChecker.isSquare( N );
        }

        protected abstract List<BigInteger[]> getSortedFundamentalSolutions();

        private List<ProperPellsEquation_SolutionsIterator> getNEqualsOneSolutionIterators(
                int fundamentalSolutionsSize ) {
            List<ProperPellsEquation_SolutionsIterator> properPellsEquationIterators = new ArrayList<>(
                    fundamentalSolutionsSize );

            GeneralizedPellsEquationSolver solver = new GeneralizedPellsEquationSolver();
            for( int i = 0; i < fundamentalSolutionsSize; i++ ) {
                properPellsEquationIterators
                        .add( (ProperPellsEquation_SolutionsIterator) solver.getSolutions( D, 1 ).iterator() );
            }

            return properPellsEquationIterators;
        }

        @Override
        public boolean hasNext() {
            return shouldReturnTrivialSolution || !fundamentalSolutions.isEmpty();
        }

        protected Collection<Long> getSquareDivisors( long N ) {
            Collection<Long> squareDivisorsOfN = new ArrayList<>();

            for( long i = 1; ; i++ ) {
                long i2 = multiplyExact( i, i );
                if( i2 > abs( N ) ) {
                    break;
                }

                if( N % i2 == 0 ) {
                    squareDivisorsOfN.add( i2 );
                }
            }

            return squareDivisorsOfN;
        }

        @Override
        public BigInteger[] next() {
            if( !hasNext() ) {
                throw new NoSuchElementException();
            }

            if( shouldReturnTrivialSolution ) {
                //If N > 0 and N = n^2 is a square number then x^2 −Dy^2 = N has a first solution equal to [n, 0]
                shouldReturnTrivialSolution = false;
                return new BigInteger[]{ BigInteger.valueOf( (long) sqrt( N ) ), BigInteger.ZERO };
            }

            if( solutionsChunkIndex == solutionsChunk.size() ) {
                //It may happen  that the current chunk has the same solutions as the previous one and as a result
                //it is emptied
                //It is for example due to the fact that we have one fundamental solution and different [t,u] generate
                //the same solution e.g. for D = 3, N = 6 which has only one fundamental solution [r,s] = [-3,1]
                //[t,u] = [1,0] generates [-3,1] and [t,u] = [2,1] generates the [-3,-1] solution which in term of
                //absolute values are the same
                do {
                    solutionsChunk = calculateNextSolutionsChunk();
                } while( solutionsChunk.isEmpty() );

                solutionsChunkIndex = 0;
            }

            return solutionsChunk.get( solutionsChunkIndex++ );
        }



        private List<BigInteger[]> calculateNextSolutionsChunk() {
            //Solution chunks are generated in the way that we take the biggest fundamental solution [r,s]
            //and we create one next solution [xMax, yMax] for it using the [t, u] from the iterator of solutions to
            //x^2 - Dy^2 = 1 assigned to it
            //Next, we iterate through the rest of fundamental solutions and generate all the solutions that have the
            //x value lower or equal than xMax (because it may happen that there will be more than one, that is why
            //we keep the separate iterators of solutions [t, u] to x^2 - Dy^2 = 1 for each fundamental solution
            Set<List<BigInteger>> nextSolutionsChunkSet = new HashSet<>();

            BigInteger[] biggestFundamentalSolution = fundamentalSolutions.get( fundamentalSolutions.size() - 1 );
            BigInteger r = biggestFundamentalSolution[0];
            BigInteger s = biggestFundamentalSolution[1];

            ProperPellsEquation_SolutionsIterator biggestFundamentalSolutionPellsEquationIterator
                    = nEqualsOneSolutionIterators.get( fundamentalSolutions.size() - 1 );
            BigInteger[] nextTU = biggestFundamentalSolutionPellsEquationIterator.next();
            BigInteger t = nextTU[0];
            BigInteger u = nextTU[1];

            BigInteger xMax = r.multiply( t ).add( s.multiply( u ).multiply( BigInteger.valueOf( D ) ) );
            BigInteger yMax = r.multiply( u ).add( s.multiply( t ) );
            nextSolutionsChunkSet.add( asList( xMax.abs(), yMax.abs() ) );

            for( int i = fundamentalSolutions.size() - 2; i >= 0; i-- ) {
                while( true ) {
                    BigInteger[] fundamentalSolution = fundamentalSolutions.get( i );
                    ProperPellsEquation_SolutionsIterator nEqualsOneSolutionIterator = nEqualsOneSolutionIterators
                            .get( i );
                    nextTU = nEqualsOneSolutionIterator.peekNext();
                    //We just peek in order to check if the next [t, u] generates the solution that is valid for this
                    //chunk
                    //In case it would not, the iterator cannot be moved

                    t = nextTU[0];
                    u = nextTU[1];
                    r = fundamentalSolution[0];
                    s = fundamentalSolution[1];

                    BigInteger x = r.multiply( t ).add( s.multiply( u ).multiply( BigInteger.valueOf( D ) ) );
                    BigInteger y = r.multiply( u ).add( s.multiply( t ) );

                    if( x.abs().compareTo( xMax.abs() ) <= 0 ) {
                        nextSolutionsChunkSet.add( asList( x.abs(), y.abs() ) );
                        //Solution is applied to current chunks so the iterator can be moved
                        nEqualsOneSolutionIterator.next();
                    }
                    else {
                        break; //Stop as the values of x are only growing
                    }
                }
            }

            if( solutionsChunkSet != null ) {
                //It may happen that some solutions from the previous chunks are included in the current one
                //In that case they need to be removed or otherwise duplicate solutions will be returned
                nextSolutionsChunkSet.removeAll( solutionsChunkSet );
            }
            solutionsChunkSet = nextSolutionsChunkSet;

            //Converting the set of solutions in current chunk to the result list
            List<BigInteger[]> nextSolutionsChunk = new ArrayList<>( nextSolutionsChunkSet.size() );
            for( List<BigInteger> solutionChunk : nextSolutionsChunkSet ) {
                nextSolutionsChunk.add( new BigInteger[]{ solutionChunk.get( 0 ), solutionChunk.get( 1 ) } );
            }

            //Set contains solutions in no specific order so the final chunk list needs to be sorted by ascending
            //values of x
            nextSolutionsChunk.sort( solutionsComparator );
            return nextSolutionsChunk;
        }


    }



    private class NSquaredLessThanD_SolutionsIterator extends GeneralizedPellsEquation_SolutionsIterator {
        public NSquaredLessThanD_SolutionsIterator( long D, long N ) {
            super( D, N );
        }

        @Override
        protected List<BigInteger[]> getSortedFundamentalSolutions() {
            List<BigInteger[]> fundamentalSolutions = new ArrayList<>();
            Collection<Long> squareDivisorsOfN = getSquareDivisors( N );

            //Calculate all possible integer values of N / f^2 to use during the PQa algorithm
            Set<Long> possibleRightSideValues = new HashSet<>();
            for( long f2 : squareDivisorsOfN ) {
                possibleRightSideValues.add( N / f2 );
            }

            //PQa algorithm with P_0 = 0, Q_0 = 1, D = D
            long P = 0;
            long Q = 1;
            long a = (long) floor( ( P + sqrt( D ) ) / Q ); //a_0 = floor( (P_0 + sqrt(D)) / Q_0 )
            long Q0 = Q;

            BigInteger previousB = BigInteger.ONE; //B_{-2} = 1
            BigInteger currentB = BigInteger.ZERO; //B_{-1} = 0
            BigInteger previousG = BigInteger.valueOf( P ).negate(); //G_{-2} = -P_0
            BigInteger currentG = BigInteger.valueOf( Q ); //G_{-1} = Q_0

            BigInteger temp = currentB;
            currentB = BigInteger.valueOf( a ).multiply( currentB ).add( previousB ); //B_0 = aB_{-1} + B_{-2}
            previousB = temp; //B_{-1} = 0

            temp = currentG;
            currentG = BigInteger.valueOf( a ).multiply( currentG ).add( previousG ); //G_0 = aG_{-1} + G_{-2}
            previousG = temp; //G_{-1} = Q_0

            BigInteger bigD = BigInteger.valueOf( D );

            long i = 1; //period index
            while( true ) {
                P = subtractExact( multiplyExact( a, Q ), P ); //P_i = a_{i - 1}Q_{i - 1} - P_{i - 1}
                Q = ( subtractExact( D, multiplyExact( P, P ) ) ) / Q; //Q_i = (D - P_i^2) / Q_{i - 1}
                a = (long) floor( ( P + sqrt( D ) ) / Q ); //a_i = floor( (P_i + sqrt(D)) / Q_i )

                //Using the fact that G_{i - 1}^2 - DB_{i - 1}^2 = (-1)^iQ_0Q_i because calculating
                //G_{i - 1}^2 - DB_{i - 1}^2 takes too much time for big values of G_{i - 1}, B_{i - 1}
                long rightSideValue = multiplyExact( Q0, Q );
                if( i % 2 == 1 ) {
                    rightSideValue = subtractExact( 0, rightSideValue );
                }

                //If G_i^2 - DB_i^2 = N / f^2 then add [fG_i, fB_i] to the list of fundamental solutions
                if( possibleRightSideValues.contains( rightSideValue ) ) {
                    long f = (long) ( sqrt( N / rightSideValue ) );
                    fundamentalSolutions.add( new BigInteger[]{ BigInteger.valueOf( f ).multiply( currentG ),
                            BigInteger.valueOf( f ).multiply( currentB ) } );
                }

                //Stop at first i for which G_i^2 - DB_i^2 = 1
                //What is important here is that we do this after checking if G_i^2 - DB_i^2 = N / f^2
                //as f^2 can be equal to N and it would produce a valid fundamental solution [fG_i, fB_i]
                if( rightSideValue == 1 ) {
                    break;
                }

                //B_i = a_iB_{i - 1} + B_{i - 2}
                temp = currentB;
                currentB = BigInteger.valueOf( a ).multiply( currentB ).add( previousB );
                previousB = temp;

                //G_i = a_iG_{i - 1} + G_{i - 2}
                temp = currentG;
                currentG = BigInteger.valueOf( a ).multiply( currentG ).add( previousG );
                previousG = temp;

                i = incrementExact( i ); //Increasing the period index
                if( i > PERIOD_LIMIT ) {
                    //stop as the computation is already too long and we will unfortunately not return
                    //any result
                    throw new ArithmeticException(
                            "Cannot calculate solutions for x^2 - " + D + "y^2 = " + N + " because the continued" + "" +
                            " fraction for sqrt(D) value has a perdio longer than " + PERIOD_LIMIT );
                }
            }

            //The solutions are sorted in ascending x order
            return fundamentalSolutions;
        }
    }



    private class NSquaredGreaterThanD_SolutionsIterator extends GeneralizedPellsEquation_SolutionsIterator {
        public NSquaredGreaterThanD_SolutionsIterator( long D, long N ) {
            super( D, N );
        }

        private long calculateStartingZ( long zMin, long zResidue, long zModulo ) {
            long zMinAddition = zResidue - ( ( zMin % zModulo ) + zModulo );
            if( zMinAddition < 0 ) {
                zMinAddition += zModulo;
            }

            return zMin + zMinAddition;
        }

        @Override
        protected List<BigInteger[]> getSortedFundamentalSolutions() {
            List<BigInteger[]> fundamentalSolutions = new ArrayList<>();

            for( long f2 : getSquareDivisors( N ) ) {
                long f = (long) sqrt( f2 );
                long m = N / f2;
                long absM = abs( m );

                long zMin = absM % 2 == 0 ? incrementExact( (int) ( -absM / 2 ) ) : ( (int) ( -absM / 2 ) );
                long zMax = (int) ( absM / 2 );

                //Iteration through -|m|/2 < z <= |m|/2 where z^2 = D mod |m|
                //zMin is the lowest integer > -|m|/2
                //zMax is the greatest integer <= |m|/2
                for( ModularCongruence zCongruence : modularSquareRootAlgorithm.getSquareRoots( D, absM ) ) {
                    long zResidue = zCongruence.getResidue();
                    long zModulo = zCongruence.getModulo();

                    //Calculates the zStart which is the first integer z that not only is
                    //in the range of zMin <= z <= zMax but also satisfies the z^2 = D mod |m|
                    long zStart = calculateStartingZ( zMin, zResidue, zModulo );

                    for( long z = zStart; z <= zMax; z += zModulo ) {
                        //Smart iteration using the modulo, skipping a
                        //lot of unnecessary z values that are not satisfying the z^2 = D mod |m|

                        //PQa algorithm with P_0 = z, Q_0 = |m|, D = D
                        long P = z;
                        long Q = absM;
                        long Q0 = Q;

                        //In order to notice the period end, keep track of all the {P_i, Q_i} values
                        Set<List<Long>> pqValues = new HashSet<>();
                        pqValues.add( asList( P, Q ) );

                        long a = (long) floor( ( P + sqrt( D ) ) / Q ); //a_0 = floor( (P_0 + sqrt(D)) / Q_0 )

                        BigInteger previousB = BigInteger.ONE; //B_{-2} = 1
                        BigInteger currentB = BigInteger.ZERO; //B_{-1} = 0
                        BigInteger previousG = BigInteger.valueOf( P ).negate(); //G_{-2} = -P_0
                        BigInteger currentG = BigInteger.valueOf( Q ); //G_{-1} = Q_0

                        BigInteger temp = currentB;
                        currentB = BigInteger.valueOf( a ).multiply( currentB )
                                             .add( previousB ); //B_0 = aB_{-1} + B_{-2}
                        previousB = temp; //B_{-1} = 0

                        temp = currentG;
                        currentG = BigInteger.valueOf( a ).multiply( currentG )
                                             .add( previousG ); //G_0 = aG_{-1} + G_{-2}
                        previousG = temp; //G_{-1} = Q_0

                        long i = 1; //period index
                        while( true ) {
                            P = subtractExact( multiplyExact( a, Q ), P ); //P_i = a_{i - 1}Q_{i - 1} - P_{i - 1}
                            Q = ( subtractExact( D, multiplyExact( P, P ) ) ) / Q; //Q_i = (D - P_i^2) / Q_{i - 1}
                            a = (long) floor( ( P + sqrt( D ) ) / Q ); //a_i = floor( (P_i + sqrt(D)) / Q_i )

                            List<Long> pqValue = asList( P, Q );
                            if( Q == 1 || Q == -1 ) {
                                BigInteger r = currentG;
                                BigInteger s = currentB;

                                //Using the fact that r^2 - Ds^2 = (-1)^iQ_0Q_i because calculating
                                //r^2 - Ds^2 takes too much time for big values of r, s
                                long rightSideValue = multiplyExact( Q0, Q );
                                if( i % 2 == 1 ) {
                                    rightSideValue = subtractExact( 0, rightSideValue );
                                }
                                if( rightSideValue == m ) {
                                    //If r^2 - Ds^2 = m then add [fr, fs] as a fundamental solution

                                    BigInteger x = BigInteger.valueOf( f ).multiply( r );
                                    BigInteger y = BigInteger.valueOf( f ).multiply( s );
                                    fundamentalSolutions.add( new BigInteger[]{ x, y } );
                                    break;
                                }
                                else { //If r^2 - Ds^2 = -m ...
                                    Iterator<BigInteger[]> nEqualsMinusOneSolutionIterator
                                            = new GeneralizedPellsEquationSolver().getSolutions( D, -1 ).iterator();
                                    if( nEqualsMinusOneSolutionIterator.hasNext() ) {
                                        //... and it has solutions to x^2 - Dy^2 = -1 then add
                                        //[f(rt + suD), f(ru + st)] as a fundamental solution
                                        BigInteger[] tu = nEqualsMinusOneSolutionIterator.next();
                                        BigInteger t = tu[0];
                                        BigInteger u = tu[1];

                                        BigInteger x = BigInteger.valueOf( f ).multiply( r.multiply( t )
                                                                                          .add( s.multiply( u )
                                                                                                 .multiply( BigInteger
                                                                                                                    .valueOf(
                                                                                                                            D ) ) ) );
                                        BigInteger y = BigInteger.valueOf( f )
                                                                 .multiply( r.multiply( u ).add( s.multiply( t ) ) );
                                        fundamentalSolutions.add( new BigInteger[]{ x, y } );
                                    }

                                    break;
                                }
                            }
                            else if( pqValues.contains( pqValue ) ) { //End of period, stop the iteration
                                break;
                            }

                            //B_i = a_iB_{i - 1} + B_{i - 2}
                            temp = currentB;
                            currentB = BigInteger.valueOf( a ).multiply( currentB ).add( previousB );
                            previousB = temp;

                            //G_i = a_iG_{i - 1} + G_{i - 2}
                            temp = currentG;
                            currentG = BigInteger.valueOf( a ).multiply( currentG ).add( previousG );
                            previousG = temp;

                            //Keep track of spotted {P_i, Q_i} values in order to notice the period end
                            pqValues.add( pqValue );

                            i = incrementExact( i ); //Increasing the period index
                            if( pqValues.size() > PERIOD_LIMIT ) {
                                //stop as the computation is already too long and we will unfortunately not return
                                //any result
                                throw new ArithmeticException(
                                        "Cannot calculate solutions for x^2 - " + D + "y^2 = " + N + " because the " +
                                        "continued fraction for sqrt(D) value has a perdio longer than " +
                                        PERIOD_LIMIT );
                            }
                        }
                    }
                }
            }

            //Adjust the fundamental solutions so that the negative values are not present
            //[x,y] -> [|x|,|y|}
            List<BigInteger[]> adjustedFundamentalSolutions = new ArrayList<>( fundamentalSolutions.size() );
            for( BigInteger[] fundamentalSolution : fundamentalSolutions ) {
                adjustedFundamentalSolutions
                        .add( new BigInteger[]{ fundamentalSolution[0].abs(), fundamentalSolution[1].abs() } );
            }

            //Sort the fundamental solutions in ascending x order
            adjustedFundamentalSolutions.sort( solutionsComparator );
            return adjustedFundamentalSolutions;
        }
    }
}
