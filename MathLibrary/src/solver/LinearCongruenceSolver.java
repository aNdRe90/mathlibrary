package solver;

import algorithm.ExtendedEuclideanAlgorithm;
import number.representation.ModularCongruence;

import static util.ModuloOperations.mod;
import static util.ModuloOperations.multiplyModulo;

/**
 * Solver for the linear congruence of form \(ax \equiv b \text{ mod } m\) where \(a, b, m\) are integers and
 * \(m &gt; 0\).
 * <br>If \(a \equiv 0 \text{ mod } m\) then every value of \(x\) is a solution if \(b \equiv 0 \text{ mod } m\) and
 * there are no solutions otherwise.
 *
 * <br><br>If \(a \not\equiv 0 \text{ mod } m\) then the linear congruence \(ax \equiv b \text{ mod } m\) can be
 * presented as \(ax = b + i \cdot m\) for an arbitrary integer \(i\) or as \(ax + my = b\) for an integer \(y\).
 * <ul><li>
 * If \(b \not\equiv 0 \text{ mod } \text{gcd}(a, m)\), then there are no solutions to \(ax \equiv b \text{ mod } m\)
 * (Bézout's lemma).
 * </li></ul>
 * <ul><li>
 * If \(b \equiv 0 \text{ mod } \text{gcd}(a, m)\), then \(b = k \cdot \text{gcd}(a, m)\) for an integer \(k\) and
 * instead of solving \(ax + my = b\) we will be solving \(a\frac{x}{k} + m\frac{y}{k} = \text{gcd}(a, m)
 * \Leftrightarrow ax' + my' = \text{gcd}(a, m)\) where \(x' = \frac{x}{k}\) and \(y' = \frac{y}{k}\).
 * <br>This kind of equation is solved for integer values of \((x', y')\) by the Extended Euclidean algorithm (please
 * see {@link ExtendedEuclideanAlgorithm} for more details on how it is done).
 * <br>When one pair of (\(x', y'\)) has been computed, all pairs can be represented in the
 * form \(\left(x' + j\frac{m}{\text{gcd}(a, m)}, y' - j\frac{a}{\text{gcd}(a, m)}\right)\), where \(j\) is
 * an arbitrary integer (see the proof of that in the description of {@link ExtendedEuclideanAlgorithm}).
 *
 * <br><br>This means that having the value of \(x'\) from the Extended Euclidean algorithm the final solution to
 * \(ax \equiv b \text{ mod } m\) is \(x = kx' \text { mod } \frac{m}{\text{gcd}(a, m)} =
 * \frac{b}{\text{gcd}(a, m)}x' \text { mod } \frac{m}{\text{gcd}(a, m)}\).
 * </li></ul>
 *
 * <p>
 * <br>NOTE: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="http://gauss.math.luc.edu/greicius/Math201/Fall2012/Lectures/linear-congruences.article.pdf">Chicago
 * Math Teachers' Circle - Linear Congruence Article</a>
 * @see <a href="http://mathworld.wolfram.com/LinearCongruenceEquation.html">Mathworld Wolfram - Linear congruences</a>
 * @see <a href="https://www.johndcook.com/blog/2008/12/10/solving-linear-congruences">John D. Cook - Solving linear
 * congruences</a>
 */
public class LinearCongruenceSolver {
    private ExtendedEuclideanAlgorithm extendedEuclideanAlgorithm = new ExtendedEuclideanAlgorithm();

    /**
     * Solves the linear congruence \(ax \equiv b \text{ mod } m\).
     * <br>For the detailed description of the implementation, see {@link QuadraticCongruenceSolver}.
     *
     * <p>
     * <br>NOTE: Overflow safe.
     *
     * @param a value of \(a\)
     * @param b value of \(b\)
     * @param m modulo \(m &gt; 0\)
     *
     * @return {@link ModularCongruence} solving this linear congruences or null if no solution exists.
     *
     * @throws IllegalArgumentException when \(m &lt; 1\)
     * @see <a href="http://gauss.math.luc.edu/greicius/Math201/Fall2012/Lectures/linear-congruences.article.pdf">Chicago
     * Math Teachers' Circle - Linear Congruence Article</a>
     * @see <a href="http://mathworld.wolfram.com/LinearCongruenceEquation.html">Mathworld Wolfram - Linear
     * congruences</a>
     * @see <a href="https://www.johndcook.com/blog/2008/12/10/solving-linear-congruences">John D. Cook - Solving
     * linear congruences</a>
     */
    public ModularCongruence getSolution( long a, long b, long m ) {
        if( m < 1 ) {
            throw new IllegalArgumentException( "Modulo must be a positive number." );
        }

        if( mod( a, m ) == 0 ) {
            //If a = 0 mod m then every x is a solution if b = 0 mod m and there is no solution otherwise
            return mod( b, m ) == 0 ? new ModularCongruence( 0, 1 ) : null;
        }

        //Solving ax' + my' = gcd(a, m)
        long[] bezoutsCoefficientsWithGCD = extendedEuclideanAlgorithm.getBezoutsCoefficientsWithGCD( a, m );
        long x = bezoutsCoefficientsWithGCD[0]; //x' solving ax' + my' = gcd(a, m)
        long gcdAM = bezoutsCoefficientsWithGCD[2]; //gcd(a, m)

        if( b % gcdAM != 0 ) {
            //Bézout's lemma: No solution to ax + my = b if b is not divisible by gcd(a, m)
            return null;
        }

        long modulo = m / gcdAM; //Result modulo is m / gcd(a, m)

        //Solution is (b / gcd(a, m))x' mod (m / gcd(a, m))
        return new ModularCongruence( multiplyModulo( b / gcdAM, x, modulo ), modulo );
    }
}
