package solver;

import number.properties.checker.PolygonalNumberChecker;
import org.junit.Test;

import java.math.BigInteger;

import static java.lang.Math.sqrt;
import static org.junit.Assert.*;

public class GeneralizedPellsEquationSolverMassTest {
    private PolygonalNumberChecker polygonalNumberChecker = new PolygonalNumberChecker();
    private GeneralizedPellsEquationSolver solver = new GeneralizedPellsEquationSolver();

    @Test
    public void test_MassTest_CorrectSolutions_WhenDIsZero() {
        for( long N = -100; N <= 100; N++ ) {
            assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( 0, N );
        }
    }

    private void assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( long D, long N ) {
        int expectedRepeats = 20;

        int repeats = 1;
        BigInteger[] previousSolution = null;
        for( BigInteger[] solution : solver.getSolutions( D, N ) ) {
            assertEquals( 2, solution.length );
            assertSolutionHasNonNegativeValues( solution );
            assertSatisfiesEquation( BigInteger.valueOf( D ), BigInteger.valueOf( N ), solution );
            assertCorrectSolutionsOrder( previousSolution, solution, D );
            previousSolution = solution;

            if( repeats++ == expectedRepeats ) {
                break;
            }
        }
    }

    private void assertSolutionHasNonNegativeValues( BigInteger[] solution ) {
        assertTrue( solution[0].signum() >= 0 );
        assertTrue( solution[1].signum() >= 0 );
    }

    private void assertSatisfiesEquation( BigInteger D, BigInteger N, BigInteger[] solution ) {
        BigInteger x = solution[0];
        BigInteger y = solution[1];
        assertTrue( x.multiply( x ).subtract( D.multiply( y ).multiply( y ) ).equals( N ) );
    }

    private void assertCorrectSolutionsOrder( BigInteger[] previousSolution, BigInteger[] solution, long D ) {
        if( previousSolution != null ) {
            if( D == 0 ) {
                assertTrue( previousSolution[0].compareTo( solution[0] ) <= 0 );
            }
            else {
                assertTrue( previousSolution[0].compareTo( solution[0] ) < 0 );
            }
        }
    }

    @Test
    public void test_MassTest_CorrectSolutions_WhenNIsZero() {
        for( long D = -100; D <= 100; D++ ) {
            if( D == 0 || polygonalNumberChecker.isSquare( D ) ) {
                continue;
            }

            assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( D, 0 );
        }

        for( long d = 2; d <= 100; d++ ) {
            assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( d * d, 0 );
        }
    }

    @Test
    public void test_MassTest_CorrectSolutions_WhenDIsPositiveSquare_AndNIsPositive() {
        for( long d = 2; d <= 20; d++ ) {
            for( long N = 4; N < 200; N += 13 ) {
                assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( d * d, N );
            }
        }
    }

    @Test
    public void test_MassTest_CorrectSolutions_WhenDIsPositiveSquare_AndNIsNegative() {
        for( long d = 2; d <= 20; d++ ) {
            for( long N = -200; N < 0; N += 11 ) {
                assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( d * d, N );
            }
        }
    }

    @Test
    public void test_MassTest_CorrectSolutions_WhenDIsNegative_AndNIsPositive() {
        for( long D = -48; D < 0; D += 2 ) {
            if( polygonalNumberChecker.isSquare( -D ) ) {
                continue;
            }

            for( long N = 169; N < 200; N += 11 ) {
                assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( D, N );
            }
        }
    }

    @Test
    public void test_MassTest_CorrectSolutions_WhenDIsNegative_AndNIsNegative() {
        for( long D = -50; D < 0; D += 2 ) {
            if( polygonalNumberChecker.isSquare( -D ) ) {
                continue;
            }

            for( long N = -200; N < 0; N += 11 ) {
                assertFalse( solver.getSolutions( D, N ).iterator().hasNext() );
            }
        }
    }

    @Test
    public void test_MassTest_CorrectSolutions_WhenNIsOne() {
        for( long D = 2; D < 2000; D++ ) {
            if( polygonalNumberChecker.isSquare( D ) ) {
                continue;
            }

            assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( D, 1 );
        }
    }

    @Test
    public void test_MassTest_CorrectSolutions_WhenNIsMinusOne() {
        for( long D = 2; D < 2000; D++ ) {
            if( polygonalNumberChecker.isSquare( D ) ) {
                continue;
            }

            assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( D, -1 );
        }
    }

    @Test
    public void test_MassTest_CorrectSolutions_WhenNSquaredIsLessThanPositiveD() {
        // takes about 2s to execute

        for( long D = 5; D < 100; D++ ) {
            if( polygonalNumberChecker.isSquare( D ) ) {
                continue;
            }

            for( long N = 2; N * N < D; N++ ) {
                assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( D, N );
                assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( D, -N );
            }
        }
    }

    @Test
    public void test_MassTest_CorrectSolutions_WhenNSquaredIsGreaterThanPositiveD() {
        //takes about 2-3s to execute

        for( long D = 2; D < 30; D++ ) {
            if( polygonalNumberChecker.isSquare( D ) ) {
                continue;
            }

            long sqrtD = (long) sqrt( D );
            for( long N = sqrtD + 1; N < sqrtD + 25; N++ ) {
                assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( D, N );
                assertNonNegativeSolutionsThatSatisfyTheEquationAndTheirOrderIsCorrect( D, -N );
            }
        }
    }
}
