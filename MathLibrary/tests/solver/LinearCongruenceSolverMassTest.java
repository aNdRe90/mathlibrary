package solver;

import number.representation.ModularCongruence;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static util.ModuloOperations.mod;
import static util.ModuloOperations.multiplyModulo;

public class LinearCongruenceSolverMassTest {
    private LinearCongruenceSolver solver = new LinearCongruenceSolver();

    @Test
    public void test_MassTest_CorrectSolutionsCalculated() {
        for( long a = -100; a < 100; a++ ) {
            for( long b = -50; b < 50; b++ ) {
                for( long modulo = 2; modulo < 30; modulo++ ) {
                    assertCorrectSolutionCalculated( a, b, modulo );
                }
            }
        }
    }

    private void assertCorrectSolutionCalculated( long a, long b, long modulo ) {
        ModularCongruence solution = solver.getSolution( a, b, modulo );
        if( solution == null ) {
            return;
        }

        int valuesToCheckPerCongruence = 10;

        int valuesChecked = 0;
        for( long x : solution ) {
            if( valuesChecked++ == valuesToCheckPerCongruence ) {
                break;
            }

            assertEquals( mod( b, modulo ), multiplyModulo( a, x, modulo ) );
        }
    }
}
