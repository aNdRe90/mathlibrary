package solver;

import number.representation.ModularCongruence;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static testutils.ExceptionAssert.assertException;

public class LinearCongruenceSolverTest {
    private LinearCongruenceSolver solver = new LinearCongruenceSolver();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, 3, -4, -5 ),
                Arguments.of( IllegalArgumentException.class, 7, 88, -1 ),
                Arguments.of( IllegalArgumentException.class, 1, 5, 0 ),
                Arguments.of( IllegalArgumentException.class, 0, 5, 0 ),
                Arguments.of( IllegalArgumentException.class, 0, 1, -6 ),
                Arguments.of( IllegalArgumentException.class, 0, -13, -567 )
        );
    }

    private static Stream<Arguments> noSolutionExistsData() {
        return Stream.of(
                //a = 0
                Arguments.of( 0, 1, 7 ),
                Arguments.of( 0, 634, 5345 ),
                Arguments.of( 0, 111, 7 ),
                Arguments.of( 0, -4, 53220 ),
                Arguments.of( 0, -2384, 53220 ),
                Arguments.of( 0, -45345222, 53220 ),

                //a mod m = 0
                Arguments.of( 8, 1, 4 ),
                Arguments.of( 500, 3, 25 ),
                Arguments.of( 51, -8, 17 ),
                Arguments.of( 341502, -443, 987 ),

                //gcd(a, m) does not divide b
                Arguments.of( 6, 3, 24 ),
                Arguments.of( 12, 1, 18 ),
                Arguments.of( 772, 7, 18 ),
                Arguments.of( 56, 20, 72 ),
                Arguments.of( 255, 16, 3383 ),
                Arguments.of( 49842, 34211, 442401 ),
                Arguments.of( 17336254, 987628, 134597157 ),
                Arguments.of( -3, 35, 333 ),
                Arguments.of( -51, 2000, 3978 ),
                Arguments.of( -2340, 4141, 7216 ),
                Arguments.of( -3298, 775, 12098 ),
                Arguments.of( -3400, 12366, 1230000 ),
                Arguments.of( -34083, 43598, 310011 ),
                Arguments.of( -10239872, 11717191, 23498712390L ),

                //b > m
                Arguments.of( 0, 9872, 7 ),
                Arguments.of( 0, -78, 7 ),
                Arguments.of( 15, 3790, 27 ),
                Arguments.of( 46521, -239487123, 3861 )
        );
    }

    private static Stream<Arguments> solutionsExistData() {
        return Stream.of(
                //a = 0
                Arguments.of( new ModularCongruence( 0, 1 ),
                              0, 0, 8 ),
                Arguments.of( new ModularCongruence( 0, 1 ),
                              0, 123, 123 ),
                Arguments.of( new ModularCongruence( 0, 1 ),
                              0, 2337, 123 ),
                Arguments.of( new ModularCongruence( 0, 1 ),
                              0, 0, 46457 ),
                Arguments.of( new ModularCongruence( 0, 1 ),
                              0, -46457, 46457 ),
                Arguments.of( new ModularCongruence( 0, 1 ),
                              0, -1068511, 46457 ),

                //a mod m = 0
                Arguments.of( new ModularCongruence( 0, 1 ),
                              18, 0, 6 ),
                Arguments.of( new ModularCongruence( 0, 1 ),
                              1100, 121, 11 ),
                Arguments.of( new ModularCongruence( 0, 1 ),
                              300, -20, 10 ),
                Arguments.of( new ModularCongruence( 0, 1 ),
                              39, -91, 13 ),

                //b = 0
                Arguments.of( new ModularCongruence( 0, 1 ),
                              1426, 0, 713 ),
                Arguments.of( new ModularCongruence( 0, 1 ),
                              -713, 0, 713 ),
                Arguments.of( new ModularCongruence( 0, 1 ),
                              0, 0, 713 ),
                Arguments.of( new ModularCongruence( 0, 713 ),
                              144, 0, 713 ),
                Arguments.of( new ModularCongruence( 0, 713 ),
                              56123, 0, 713 ),
                Arguments.of( new ModularCongruence( 0, 713 ),
                              -33, 0, 713 ),
                Arguments.of( new ModularCongruence( 0, 5 ),
                              -18, 0, 45 ),
                Arguments.of( new ModularCongruence( 0, 22 ),
                              -78, 0, 132 ),
                Arguments.of( new ModularCongruence( 0, 12 ),
                              -51291, 0, 15012 ),

                //b mod m = 0
                Arguments.of( new ModularCongruence( 0, 34 ),
                              13, 34, 34 ),
                Arguments.of( new ModularCongruence( 0, 56 ),
                              -50, 224, 112 ),
                Arguments.of( new ModularCongruence( 0, 4 ),
                              4, -32, 16 ),
                Arguments.of( new ModularCongruence( 0, 197 ),
                              13, -25413, 197 ),

                //gcd(a, m) = 1
                Arguments.of( new ModularCongruence( 5, 7 ),
                              3, 1, 7 ),
                Arguments.of( new ModularCongruence( 5, 14 ),
                              17, 1, 14 ),
                Arguments.of( new ModularCongruence( 6, 7 ),
                              6, 1, 7 ),
                Arguments.of( new ModularCongruence( 209, 222 ),
                              17, 1, 222 ),
                Arguments.of( new ModularCongruence( 9871, 24192 ),
                              14575, 1, 24192 ),
                Arguments.of( new ModularCongruence( 3, 22 ),
                              9871, 1, 22 ),
                Arguments.of( new ModularCongruence( 6810157, 8009933 ),
                              728112, 1, 8009933 ),
                Arguments.of( new ModularCongruence( 12920898443L, 72737455291L ),
                              99372862311L, 1, 72737455291L ),
                Arguments.of( new ModularCongruence( 6, 212 ),
                              -719239, 38, 212 ),
                Arguments.of( new ModularCongruence( 447, 7389 ),
                              -8345, 1230, 7389 ),
                Arguments.of( new ModularCongruence( 637, 1024 ),
                              -35, 233, 1024 ),
                Arguments.of( new ModularCongruence( 81, 1088 ),
                              -35001, 247, 1088 ),
                Arguments.of( new ModularCongruence( 441, 6454 ),
                              -723477, 133, 6454 ),
                Arguments.of( new ModularCongruence( 842362, 4892435 ),
                              -34511, 93788, 4892435 ),
                Arguments.of( new ModularCongruence( 165772, 198213 ),
                              -721, 827, 198213 ),
                Arguments.of( new ModularCongruence( 27, 89 ),
                              -345089734, 68795, 89 ),

                //gcd(a, m) > 1
                Arguments.of( new ModularCongruence( 2, 3 ),
                              3, 6, 9 ),
                Arguments.of( new ModularCongruence( 26, 37 ),
                              120, 12, 222 ),
                Arguments.of( new ModularCongruence( 2, 4 ),
                              77, 42, 28 ),
                Arguments.of( new ModularCongruence( 7, 13 ),
                              121, 132, 143 ),
                Arguments.of( new ModularCongruence( 10, 25 ),
                              784, 140, 350 ),
                Arguments.of( new ModularCongruence( 34, 47 ),
                              48994, 3406, 12314 ),
                Arguments.of( new ModularCongruence( 1, 3 ),
                              -17, 340, 51 ),
                Arguments.of( new ModularCongruence( 51200, 61550 ),
                              -235234, 100, 123100 ),
                Arguments.of( new ModularCongruence( 91363, 122467 ),
                              -169566, 139509, 367401 ),
                Arguments.of( new ModularCongruence( 3, 5 ),
                              -14739, 578, 1445 ),
                Arguments.of( new ModularCongruence( 51, 88 ),
                              -61887, 147, 4312 ),
                Arguments.of( new ModularCongruence( 669, 863 ),
                              -21492, 47760, 343474 ),

                //b > m
                Arguments.of( new ModularCongruence( 0, 1 ),
                              0, 94488, 124 ),
                Arguments.of( new ModularCongruence( 0, 1 ),
                              0, -94612, 124 ),
                Arguments.of( new ModularCongruence( 192, 199 ),
                              17, 345345, 199 ),
                Arguments.of( new ModularCongruence( 17, 199 ),
                              35, -2987, 199 )
        );
    }

    @ParameterizedTest( name = "{index}) {1} is an incorrect input and raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long a, long b, long m ) {
        assertException( throwableClass, () -> solver.getSolution( a, b, m ) );
    }

    @ParameterizedTest( name = "{index}) {0}x = {1} mod {2} has no solution" )
    @MethodSource( "noSolutionExistsData" )
    public void test_NullIsReturned_WhenNoSolutionExists( long a, long b, long m ) {
        assertNull( solver.getSolution( a, b, m ) );
    }

    @ParameterizedTest( name = "{index}) {1}x = {2} mod {3} has a solution {0}" )
    @MethodSource( "solutionsExistData" )
    public void test_NullIsReturned_WhenNoSolutionExists( ModularCongruence expectedSolution, long a, long b, long m ) {
        assertEquals( expectedSolution, solver.getSolution( a, b, m ) );
    }

    @Test
    public void test_OverflowDoesNotOccurForBigValues() {
        assertEquals( new ModularCongruence( 927734950183752496L, 934591233420837423L ),
                      solver.getSolution( 1111145471236182736L, 34748761273123123L, 934591233420837423L ) );
        assertNull( solver.getSolution( -2348762346723421111L, 4443234231923878784L, 8347598374823748222L ) );
    }
}
