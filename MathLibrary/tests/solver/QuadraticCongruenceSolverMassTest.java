package solver;

import number.representation.ModularCongruence;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static java.lang.Math.log;
import static org.junit.Assert.assertTrue;
import static util.BasicOperations.power;
import static util.ModuloOperations.addModulo;
import static util.ModuloOperations.multiplyModulo;

public class QuadraticCongruenceSolverMassTest {
    private QuadraticCongruenceSolver solver = new QuadraticCongruenceSolver();

    @Test
    public void test_MassTest_CorrectSolutions_WhenAIsZero() {
        long a = 0;

        for( long b = 53250; b <= 53290; b++ ) {
            for( long c = 450; c < 500; c++ ) {
                for( long modulo = 1000; modulo <= 1030; modulo++ ) {
                    List<ModularCongruence> solutions = solver.getSolutions( a, b, c, modulo );
                    assertTrue( areSolutionsCorrect( solutions, a, b, c, modulo ) );
                }
            }
        }
    }

    private boolean areSolutionsCorrect( List<ModularCongruence> solutions, long a, long b, long c, long modulo ) {
        for( ModularCongruence solution : solutions ) {
            Iterator<Long> iterator = solution.iterator();
            for( int i = 0; i < 5; i++ ) {
                if( iterator.hasNext() ) {
                    long x = iterator.next();
                    long f = addModulo( addModulo( multiplyModulo( a, multiplyModulo( x, x, modulo ), modulo ),
                                                   multiplyModulo( b, x, modulo ), modulo ), c, modulo );
                    if( f != 0 ) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    @Test
    public void test_MassTest_CorrectSolutions_WhenAIsOneAndBIsZero() {
        long a = 1;
        long b = 0;

        for( long c = 750; c < 850; c++ ) {
            for( long modulo = 2000; modulo <= 2050; modulo++ ) {
                List<ModularCongruence> solutions = solver.getSolutions( a, b, c, modulo );
                assertTrue( areSolutionsCorrect( solutions, a, b, c, modulo ) );
            }
        }
    }

    @Test
    public void test_MassTest_CorrectSolutions_ForGeneralCase() {
        for( long a = 350; a < 360; a++ ) {
            for( long b = 1150; b < 1160; b++ ) {
                for( long c = 500; c < 510; c++ ) {
                    for( long modulo = 3000; modulo <= 3010; modulo++ ) {
                        List<ModularCongruence> solutions = solver.getSolutions( a, b, c, modulo );
                        assertTrue( areSolutionsCorrect( solutions, a, b, c, modulo ) );
                    }
                }
            }
        }
    }

    @Test
    public void test_MassTest_CorrectSolutions_ForGeneralCase_WithModuloPrimeToHighPower() {
        long[] primes = { 2, 3, 5, 7, 11, 13 };

        for( long a = 870; a < 880; a++ ) {
            for( long b = 680; b < 690; b++ ) {
                for( long c = 420; c < 430; c++ ) {
                    for( long prime : primes ) {
                        int exponent = (int) ( log( 1e9 ) / log( prime ) );
                        long modulo = power( prime, exponent );

                        List<ModularCongruence> solutions = solver.getSolutions( a, b, c, modulo );
                        assertTrue( areSolutionsCorrect( solutions, a, b, c, modulo ) );
                    }
                }
            }
        }
    }
}
