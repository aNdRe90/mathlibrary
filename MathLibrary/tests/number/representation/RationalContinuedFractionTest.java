package number.representation;

import number.representation.big.BigFraction;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;
import static testutils.ExceptionAssert.assertException;

public class RationalContinuedFractionTest {
    private static Stream<Arguments> coefficientsData() {
        return Stream.of(
                //Integer value
                Arguments.of( bigIntegerList( -234 ), rationalContinuedFraction( -234, 1 ) ),
                Arguments.of( bigIntegerList( -13 ), rationalContinuedFraction( -13, 1 ) ),
                Arguments.of( bigIntegerList( 0 ), rationalContinuedFraction( 0, 1 ) ),
                Arguments.of( bigIntegerList( 87 ), rationalContinuedFraction( 87, 1 ) ),
                Arguments.of( bigIntegerList( 695 ), rationalContinuedFraction( 695, 1 ) ),

                //0 < value < 1
                Arguments.of( bigIntegerList( 0, 1, 3, 2 ),
                              rationalContinuedFraction( 7, 9 ) ),
                Arguments.of( bigIntegerList( 0, 1, 2, 1, 1, 2, 1, 2 ),
                              rationalContinuedFraction( 49, 68 ) ),
                Arguments.of( bigIntegerList( 0, 1, 2, 4, 3, 1, 2, 1, 10, 3, 1, 2, 2 ),
                              rationalContinuedFraction( 40911, 59221 ) ),
                Arguments.of( bigIntegerList( 0, 1, 1, 2, 1, 1, 3, 1, 3, 2, 74, 1, 3, 2, 3 ),
                              rationalContinuedFraction( 638883, 1098226 ) ),
                Arguments.of( bigIntegerList( 0, 118, 1, 3, 7, 2, 113, 1, 3, 3, 1, 5, 2, 1, 1, 5, 4, 2, 1, 2, 3, 1, 14,
                                              1,
                                              1, 7, 3, 3 ),
                              rationalContinuedFraction( 7892381011123L, 937283911278333L ) ),

                //1 < value
                Arguments.of( bigIntegerList( 1, 4, 1, 19 ),
                              rationalContinuedFraction( 119, 99 ) ),
                Arguments.of( bigIntegerList( 4, 2, 6, 7 ),
                              rationalContinuedFraction( 415, 93 ) ),
                Arguments.of( bigIntegerList( 1, 9, 2, 3, 1, 2 ),
                              rationalContinuedFraction( 2088, 1888 ) ),
                Arguments.of( bigIntegerList( 13, 32, 1, 26, 7, 2 ),
                              rationalContinuedFraction( 2097780, 160992 ) ),
                Arguments.of( bigIntegerList( 7172, 1, 2, 1, 46, 27, 2 ),
                              rationalContinuedFraction( 73829102, 10293 ) ),
                Arguments
                        .of( bigIntegerList( 5, 1, 2, 1, 1, 1, 1, 6, 1, 2, 1, 1, 30, 2, 1, 2, 1, 11, 1, 79, 24, 1, 3, 2,
                                             1, 1, 1, 7, 1, 2, 1, 2, 7, 1, 2, 2, 2 ),
                             rationalContinuedFraction( 40002934234231134L, 6990289387161232L ) ),

                //-1 < value < 0
                Arguments.of( bigIntegerList( -1, 1, 2, 2 ),
                              rationalContinuedFraction( -2, 7 ) ),
                Arguments.of( bigIntegerList( -1, 1, 2, 1, 5, 3, 2 ),
                              rationalContinuedFraction( -44, 169 ) ),
                Arguments.of( bigIntegerList( -1, 1, 22, 1, 4, 1, 1, 3 ),
                              rationalContinuedFraction( -390, 9290 ) ),
                Arguments.of( bigIntegerList( -1, 1, 150, 3, 1, 3, 1, 7, 1, 2, 2, 1, 1, 1, 2, 1, 3 ),
                              rationalContinuedFraction( -58903, 8909873 ) ),
                Arguments.of( bigIntegerList( -1, 1, 773, 1, 6, 6, 10, 1, 1, 4, 2, 1, 5, 3, 2, 5, 1, 3, 1, 2,
                                              1, 12, 1, 47, 1, 1, 1, 2, 2, 14, 2, 1, 2 ),
                              rationalContinuedFraction( -99098716231111L, 76787672349000133L ) ),

                //value < -1
                Arguments.of( bigIntegerList( -3, 1, 2, 1, 6 ),
                              rationalContinuedFraction( -61, 27 ) ),
                Arguments.of( bigIntegerList( -66, 1, 1, 3, 3, 5 ),
                              rationalContinuedFraction( -7983, 122 ) ),
                Arguments.of( bigIntegerList( -2, 1, 1, 1, 2, 1, 2, 3, 1, 2, 2, 2, 1, 3, 1, 5 ),
                              rationalContinuedFraction( -109235, 79944 ) ),
                Arguments.of( bigIntegerList( -8, 2, 33, 1, 1, 5, 168, 28, 2, 1, 2, 2 ),
                              rationalContinuedFraction( -509831052, 67910983 ) ),
                Arguments.of( bigIntegerList( -340, 1, 1, 5, 1, 6, 1, 1, 28, 1, 1, 1, 1, 15 ),
                              rationalContinuedFraction( -3389098231L, 9983771 ) ),
                Arguments.of( bigIntegerList( -33, 1, 2, 1, 1, 7, 1, 1, 2, 5, 1, 14, 1, 1, 3, 2, 6, 1, 2, 3, 1,
                                              2, 4, 1, 2, 4, 4, 3, 1, 2, 1, 1, 2, 1, 1, 6, 2, 1, 77 ),
                              rationalContinuedFraction( -284730938457277234L, 8819782656765123L ) )
        );
    }

    private static List<BigInteger> bigIntegerList( long... values ) {
        List<BigInteger> bigIntList = new ArrayList<>( values.length );

        for( Number value : values ) {
            if( value instanceof BigInteger ) {
                bigIntList.add( (BigInteger) value );
            }
            else {
                bigIntList.add( BigInteger.valueOf( value.longValue() ) );
            }
        }

        return bigIntList;
    }

    private static RationalContinuedFraction rationalContinuedFraction( long numerator, long denominator ) {
        return new RationalContinuedFraction( BigInteger.valueOf( numerator ), BigInteger.valueOf( denominator ) );
    }

    private static Stream<Arguments> convergentsData() {
        return Stream.of(
                //Integer value
                Arguments.of( singletonList( bigFraction( -8862, 1 ) ), rationalContinuedFraction( -8862, 1 ) ),
                Arguments.of( singletonList( bigFraction( -14, 1 ) ), rationalContinuedFraction( -14, 1 ) ),
                Arguments.of( singletonList( BigFraction.ZERO ), rationalContinuedFraction( 0, 1 ) ),
                Arguments.of( singletonList( bigFraction( 43, 1 ) ), rationalContinuedFraction( 43, 1 ) ),
                Arguments.of( singletonList( bigFraction( 7921, 1 ) ), rationalContinuedFraction( 7921, 1 ) ),

                //0 < value
                Arguments.of( asList( BigFraction.ZERO, bigFraction( 1, 3 ), bigFraction( 3, 10 ) ),
                              rationalContinuedFraction( 3, 10 ) ),
                Arguments.of( asList( bigFraction( 3, 1 ), bigFraction( 19, 6 ), bigFraction( 22, 7 ),
                                      bigFraction( 195, 62 ),
                                      bigFraction( 217, 69 ), bigFraction( 629, 200 ) ),
                              rationalContinuedFraction( 629, 200 ) ),
                Arguments.of( asList( BigFraction.ZERO, bigFraction( 1, 1447 ), bigFraction( 2, 2895 ),
                                      bigFraction( 21, 30397 ), bigFraction( 23, 33292 ), bigFraction( 90, 130273 ),
                                      bigFraction( 203, 293838 ), bigFraction( 699, 1011787 ),
                                      bigFraction( 902, 1305625 ),
                                      bigFraction( 16033, 23207412 ), bigFraction( 49001, 70927861 ) ),
                              rationalContinuedFraction( 49001, 70927861 ) ),

                //value < 0
                Arguments.of( asList( bigFraction( -1, 1 ), bigFraction( -1, 2 ), bigFraction( -12, 23 ),
                                      bigFraction( -13, 25 ),
                                      bigFraction( -51, 98 ) ),
                              rationalContinuedFraction( -51, 98 ) ),
                Arguments.of( asList( bigFraction( -13, 1 ), bigFraction( -12, 1 ), bigFraction( -49, 4 ),
                                      bigFraction( -110, 9 ),
                                      bigFraction( -709, 58 ), bigFraction( -2946, 241 ) ),
                              rationalContinuedFraction( -5892, 482 ) ),
                Arguments.of( asList( bigFraction( -1, 1 ), bigFraction( -21, 22 ), bigFraction( -22, 23 ),
                                      bigFraction( -3937, 4116 ), bigFraction( -3959, 4139 ),
                                      bigFraction( -7896, 8255 ),
                                      bigFraction( -27647, 28904 ), bigFraction( -35543, 37159 ),
                                      bigFraction( -98733, 103222 ) ),
                              rationalContinuedFraction( -98733, 103222 ) )
        );
    }

    private static BigFraction bigFraction( long numerator, long denominator ) {
        return new BigFraction( BigInteger.valueOf( numerator ), BigInteger.valueOf( denominator ) );
    }

    private static Stream<Arguments> getValueData() {
        return Stream.of(
                Arguments.of( new BigDecimal( "-112" ),
                              rationalContinuedFraction( -112, 1 ) ),
                Arguments.of( new BigDecimal( "0" ),
                              rationalContinuedFraction( 0, 1 ) ),
                Arguments.of( new BigDecimal( "20394782" ),
                              rationalContinuedFraction( 20394782, 1 ) ),
                Arguments.of( new BigDecimal( "-0.125" ),
                              rationalContinuedFraction( -1, 8 ) ),
                Arguments.of( new BigDecimal( "0.2142857143" ),
                              rationalContinuedFraction( 15, 70 ) ),
                Arguments.of( new BigDecimal( "2.4675" ),
                              rationalContinuedFraction( 987, 400 ) )
        );
    }

    private static Stream<Arguments> toStringData() {
        return Stream.of(
                Arguments.of( "415/93 = [4;2,6,7]", rationalContinuedFraction( 415, 93 ) ),
                Arguments.of( "3/1 = [3]", rationalContinuedFraction( 3, 1 ) ),
                Arguments.of( "-4/30 = [-1;1,6,2]", rationalContinuedFraction( -4, 30 ) )
        );
    }

    @ParameterizedTest( name = "{index}) Rational continued fraction {1} has coefficients = {0}" )
    @MethodSource( "coefficientsData" )
    public void test_CorrectCoefficients(
            List<BigInteger> expectedCoefficients, RationalContinuedFraction continuedFraction ) {
        Iterator<BigInteger> coefficientsIterator = continuedFraction.getCoefficients().iterator();

        for( BigInteger expectedCoefficient : expectedCoefficients ) {
            assertTrue( coefficientsIterator.hasNext() );
            assertEquals( expectedCoefficient, coefficientsIterator.next() );
        }

        assertFalse( coefficientsIterator.hasNext() );
    }

    @ParameterizedTest( name = "{index}) Rational continued fraction {1} has convergents = {0}" )
    @MethodSource( "convergentsData" )
    public void test_CorrectConvergents(
            List<BigFraction> expectedConvergents, RationalContinuedFraction continuedFraction ) {
        Iterator<BigFraction> convergentsIterator = continuedFraction.getConvergents().iterator();

        for( BigFraction expectedConvergent : expectedConvergents ) {
            assertTrue( convergentsIterator.hasNext() );
            assertEquals( expectedConvergent, convergentsIterator.next() );
        }

        assertFalse( convergentsIterator.hasNext() );
    }

    @ParameterizedTest( name = "{index}) Value of rational continued fraction {1} is equal to {0}" )
    @MethodSource( "getValueData" )
    public void test_GetValue( BigDecimal expectedValue, RationalContinuedFraction continuedFraction ) {
        int decimalDigits = 10;
        assertEquals( expectedValue.setScale( decimalDigits, RoundingMode.HALF_UP ),
                      continuedFraction.getValue( decimalDigits ) );
    }

    @ParameterizedTest( name = "{index}) \"{0}\" is the same string as \"{1}\"" )
    @MethodSource( "toStringData" )
    public void test_ToString( String s, RationalContinuedFraction continuedFraction ) {
        assertEquals( s, continuedFraction.toString() );
    }

    @Test
    public void test_RationalContinuedFraction_CannotBeCreated_ForInvalidInput() {
        assertException( IllegalArgumentException.class, () -> new RationalContinuedFraction( null ) );
        assertException( IllegalArgumentException.class, () -> rationalContinuedFraction( 123, 0 ) );
    }

    @Test
    public void test_InstancesCreatedByBothConstructorsAreEqual_IfFractionValueIsTheSame() {
        assertEquals( rationalContinuedFraction( 2, 9 ), new RationalContinuedFraction( bigFraction( 2, 9 ) ) );
        assertEquals( rationalContinuedFraction( 2, 9 ), new RationalContinuedFraction( bigFraction( 4, 18 ) ) );
    }

    @Test
    public void test_GetRationalValue_ReturnsNewInstanceOfFraction_ThatRepresentsTheValue_AndWasNotReduced() {
        assertEquals( bigFraction( 12, 48 ), rationalContinuedFraction( 12, 48 ).getRationalValue() );

        BigFraction fraction = bigFraction( 12, 48 );
        RationalContinuedFraction rationalContinuedFraction = new RationalContinuedFraction( fraction );

        assertEquals( rationalContinuedFraction.getRationalValue(), fraction );
        assertNotSame( rationalContinuedFraction.getRationalValue(), fraction );
        assertEquals( rationalContinuedFraction.getRationalValue().getNumerator(), fraction.getNumerator() );
        assertEquals( rationalContinuedFraction.getRationalValue().getDenominator(), fraction.getDenominator() );
    }

    @Test
    public void test_HashCode() {
        RationalContinuedFraction fraction = rationalContinuedFraction( 45, 90 );

        assertEquals( fraction.hashCode(), fraction.hashCode() );
        assertEquals( fraction.hashCode(), rationalContinuedFraction( 45, 90 ).hashCode() );
        assertNotEquals( fraction.hashCode(), rationalContinuedFraction( 1, 2 ).hashCode() );
        assertNotEquals( fraction.hashCode(), rationalContinuedFraction( 2, 3 ).hashCode() );
    }

    @Test
    public void test_Equals() {
        RationalContinuedFraction fraction = rationalContinuedFraction( 23, 47 );

        assertTrue( fraction.equals( fraction ) );
        assertFalse( fraction.equals( null ) );
        assertFalse( fraction.equals( new String() ) );

        assertTrue( fraction.equals( rationalContinuedFraction( 23, 47 ) ) );
        assertTrue( fraction.equals( rationalContinuedFraction( 46, 94 ) ) );
        assertFalse( fraction.equals( rationalContinuedFraction( 1, 2 ) ) );
    }
}
