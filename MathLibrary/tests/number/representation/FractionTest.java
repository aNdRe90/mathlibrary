package number.representation;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static java.lang.Math.sqrt;
import static org.junit.Assert.*;
import static testutils.ExceptionAssert.assertException;

public class FractionTest {
    private static final long maxLongSqrt = (long) sqrt( Long.MAX_VALUE );

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                //Zero in denominator
                Arguments.of( ArithmeticException.class, 4, 0 ),
                Arguments.of( ArithmeticException.class, 0, 0 ),
                Arguments.of( ArithmeticException.class, -12, 0 ),

                //Long.MIN_VALUE as numerator or denominator
                Arguments.of( IllegalArgumentException.class, Long.MIN_VALUE, 2 ),
                Arguments.of( IllegalArgumentException.class, Long.MIN_VALUE, -24 ),
                Arguments.of( IllegalArgumentException.class, 3, Long.MIN_VALUE ),
                Arguments.of( IllegalArgumentException.class, 51, Long.MIN_VALUE ),
                Arguments.of( IllegalArgumentException.class, 0, Long.MIN_VALUE )
        );
    }

    private static Stream<Arguments> reverseFractionsData() {
        return Stream.of(
                Arguments.of( new Fraction( 2, 2 ),
                              new Fraction( 2, 2 ) ),
                Arguments.of( new Fraction( 543, 12 ),
                              new Fraction( 12, 543 ) ),
                Arguments.of( new Fraction( 34, 114 ),
                              new Fraction( 114, 34 ) ),
                Arguments.of( new Fraction( -4, 52 ),
                              new Fraction( -52, 4 ) ),
                Arguments.of( new Fraction( -654, 98 ),
                              new Fraction( -98, 654 ) ),
                Arguments.of( new Fraction( -100, 83 ),
                              new Fraction( 83, -100 ) ),
                Arguments.of( new Fraction( -76, 432 ),
                              new Fraction( 432, -76 ) ),
                Arguments.of( new Fraction( 920, 98 ),
                              new Fraction( -98, -920 ) ),
                Arguments.of( new Fraction( 23, 643 ),
                              new Fraction( -643, -23 ) )
        );
    }

    private static Stream<Arguments> addFractionsData() {
        return Stream.of(
                //Includes zero
                Arguments.of( new Fraction( 3, 16 ),
                              Fraction.ZERO,
                              new Fraction( 3, 16 ) ),
                Arguments.of( new Fraction( 8, 4 ),
                              new Fraction( 8, 4 ),
                              Fraction.ZERO ),
                Arguments.of( Fraction.ZERO,
                              Fraction.ZERO,
                              Fraction.ZERO ),

                //Integer values
                Arguments.of( new Fraction( 10 ),
                              new Fraction( 4 ),
                              new Fraction( 6 ) ),

                //Same denominator
                Arguments.of( new Fraction( 98, 49 ),
                              new Fraction( 6, 7 ),
                              new Fraction( 8, 7 ) ),
                Arguments.of( new Fraction( 40, 100 ),
                              new Fraction( -4, 10 ),
                              new Fraction( 8, 10 ) ),
                Arguments.of( new Fraction( -22, 121 ),
                              new Fraction( 3, 11 ),
                              new Fraction( -5, 11 ) ),
                Arguments.of( new Fraction( -65, 25 ),
                              new Fraction( -9, 5 ),
                              new Fraction( -4, 5 ) ),

                //Different denominators
                Arguments.of( new Fraction( 62, 30 ),
                              new Fraction( 5, 3 ),
                              new Fraction( 4, 10 ) ),
                Arguments.of( new Fraction( 12, 20 ),
                              new Fraction( -9, 10 ),
                              new Fraction( 3, 2 ) ),
                Arguments.of( new Fraction( -51, 40 ),
                              new Fraction( 1, 8 ),
                              new Fraction( 7, -5 ) ),
                Arguments.of( new Fraction( -399, 105 ),
                              new Fraction( -12, 15 ),
                              new Fraction( -21, 7 ) )
        );
    }

    private static Stream<Arguments> addFractions_OverflowData() {
        return Stream.of(
                //Same denominator
                Arguments.of( new Fraction( 2, 2345 ),
                              new Fraction( Long.MAX_VALUE / 2345 + 3, 2345 ) ),
                Arguments.of( new Fraction( 5, 123 ),
                              new Fraction( Long.MIN_VALUE / 123 - 3, 123 ) ),
                Arguments.of( new Fraction( 6, maxLongSqrt + 2 ),
                              new Fraction( 14, maxLongSqrt + 2 ) ),
                Arguments.of( new Fraction( 8, -maxLongSqrt - 1 ),
                              new Fraction( 7, -maxLongSqrt - 1 ) ),

                //Different denominators
                Arguments.of( new Fraction( Long.MAX_VALUE / 2 + 1, 3 ),
                              new Fraction( 5, 2 ) ),
                Arguments.of( new Fraction( Long.MIN_VALUE / 5 - 5, 4 ),
                              new Fraction( 6, 5 ) ),
                Arguments.of( new Fraction( 634, maxLongSqrt + 3 ),
                              new Fraction( 74, maxLongSqrt + 12 ) ),
                Arguments.of( new Fraction( 98, -maxLongSqrt - 9 ),
                              new Fraction( 14, maxLongSqrt + 6 ) )
        );
    }

    private static Stream<Arguments> subtractFractionsData() {
        return Stream.of(
                //Includes zero
                Arguments.of( new Fraction( 11, 13 ),
                              new Fraction( 11, 13 ),
                              Fraction.ZERO ),
                Arguments.of( new Fraction( -6, 67 ),
                              Fraction.ZERO,
                              new Fraction( 6, 67 ) ),
                Arguments.of( Fraction.ZERO,
                              Fraction.ZERO,
                              Fraction.ZERO ),

                //Integer values
                Arguments.of( new Fraction( 15 ),
                              new Fraction( 25 ),
                              new Fraction( 10 ) ),

                //Same denominator
                Arguments.of( new Fraction( 12, 4 ),
                              new Fraction( 13, 2 ),
                              new Fraction( 7, 2 ) ),
                Arguments.of( new Fraction( -98, 196 ),
                              new Fraction( -4, 14 ),
                              new Fraction( 3, 14 ) ),
                Arguments.of( new Fraction( 216, 64 ),
                              new Fraction( 7, 8 ),
                              new Fraction( -20, 8 ) ),
                Arguments.of( new Fraction( -40, 25 ),
                              new Fraction( -18, 5 ),
                              new Fraction( -10, 5 ) ),

                //Different denominators
                Arguments.of( new Fraction( 106, 36 ),
                              new Fraction( 14, 4 ),
                              new Fraction( 5, 9 ) ),
                Arguments.of( new Fraction( -38, 30 ),
                              new Fraction( -6, 10 ),
                              new Fraction( 2, 3 ) ),
                Arguments.of( new Fraction( 124, 48 ),
                              new Fraction( 14, 8 ),
                              new Fraction( -5, 6 ) ),
                Arguments.of( new Fraction( -208, 187 ),
                              new Fraction( -20, 11 ),
                              new Fraction( -12, 17 ) )
        );
    }

    private static Stream<Arguments> subtractFractions_OverflowData() {
        return Stream.of(
                //Same denominator
                Arguments.of( new Fraction( Long.MAX_VALUE / 5421 + 5, 5421 ),
                              new Fraction( 3, 5421 ) ),
                Arguments.of( new Fraction( Long.MIN_VALUE / 124 - 5, 124 ),
                              new Fraction( 4, 124 ) ),
                Arguments.of( new Fraction( 54, maxLongSqrt + 3 ),
                              new Fraction( 11, maxLongSqrt + 3 ) ),
                Arguments.of( new Fraction( 42, -maxLongSqrt - 12 ),
                              new Fraction( 15, -maxLongSqrt - 12 ) ),

                //Different denominators
                Arguments.of( new Fraction( Long.MAX_VALUE / 7 + 1, 14 ),
                              new Fraction( 8, 9 ) ),
                Arguments.of( new Fraction( Long.MIN_VALUE / 5 + 1, 4 ),
                              new Fraction( 23, 5 ) ),
                Arguments.of( new Fraction( 54, maxLongSqrt + 1 ),
                              new Fraction( 11, maxLongSqrt + 13 ) ),
                Arguments.of( new Fraction( 643, -maxLongSqrt - 2 ),
                              new Fraction( 1, maxLongSqrt + 5 ) )
        );
    }

    private static Stream<Arguments> multiplyFractionsData() {
        return Stream.of(
                //Includes zero
                Arguments.of( Fraction.ZERO,
                              new Fraction( 112, 455 ),
                              Fraction.ZERO ),
                Arguments.of( Fraction.ZERO,
                              Fraction.ZERO,
                              new Fraction( 9, 41 ) ),
                Arguments.of( Fraction.ZERO,
                              Fraction.ZERO,
                              Fraction.ZERO ),

                //Integer values
                Arguments.of( new Fraction( 24 ),
                              new Fraction( 6 ),
                              new Fraction( 4 ) ),

                //Same denominator
                Arguments.of( new Fraction( 8, 81 ),
                              new Fraction( 4, 9 ),
                              new Fraction( 2, 9 ) ),
                Arguments.of( new Fraction( -45, 1024 ),
                              new Fraction( -9, 32 ),
                              new Fraction( 5, 32 ) ),
                Arguments.of( new Fraction( -240, 324 ),
                              new Fraction( 10, 18 ),
                              new Fraction( -24, 18 ) ),
                Arguments.of( new Fraction( 88, 3600 ),
                              new Fraction( -8, 60 ),
                              new Fraction( -11, 60 ) ),

                //Different denominators
                Arguments.of( new Fraction( 6, 6 ),
                              new Fraction( 1, 3 ),
                              new Fraction( 6, 2 ) ),
                Arguments.of( new Fraction( -48, 80 ),
                              new Fraction( -4, 10 ),
                              new Fraction( 12, 8 ) ),
                Arguments.of( new Fraction( -738, 462 ),
                              new Fraction( 41, 22 ),
                              new Fraction( -18, 21 ) ),
                Arguments.of( new Fraction( 90, 126 ),
                              new Fraction( -10, 18 ),
                              new Fraction( -9, 7 ) )
        );
    }

    private static Stream<Arguments> multiplyFractions_OverflowData() {
        return Stream.of(
                //Same denominator
                Arguments.of( new Fraction( Long.MAX_VALUE / 4, 3 ),
                              new Fraction( 5, 3 ) ),
                Arguments.of( new Fraction( Long.MIN_VALUE / 8, 11 ),
                              new Fraction( 35, 11 ) ),
                Arguments.of( new Fraction( 16, Long.MAX_VALUE / 7 ),
                              new Fraction( 43, Long.MAX_VALUE / 7 ) ),
                Arguments.of( new Fraction( 22, Long.MIN_VALUE / 11 ),
                              new Fraction( 3, Long.MIN_VALUE / 11 ) ),

                //Different denominators
                Arguments.of( new Fraction( Long.MAX_VALUE / 6, 13 ),
                              new Fraction( 7, 54 ) ),
                Arguments.of( new Fraction( Long.MIN_VALUE / 2, 72 ),
                              new Fraction( 9, 13 ) ),
                Arguments.of( new Fraction( 77, Long.MAX_VALUE / 4 ),
                              new Fraction( 14, 8 ) ),
                Arguments.of( new Fraction( 62, Long.MIN_VALUE / 15 ),
                              new Fraction( 4, 20 ) )
        );
    }

    private static Stream<Arguments> divideFractionsData() {
        return Stream.of(
                //Includes zero
                Arguments.of( Fraction.ZERO,
                              Fraction.ZERO,
                              new Fraction( 24 ) ),

                //Integer values
                Arguments.of( new Fraction( 5 ),
                              new Fraction( 50 ),
                              new Fraction( 10 ) ),

                //Same denominator
                Arguments.of( new Fraction( 6, 30 ),
                              new Fraction( 1, 6 ),
                              new Fraction( 5, 6 ) ),
                Arguments.of( new Fraction( -270, 60 ),
                              new Fraction( -18, 15 ),
                              new Fraction( 4, 15 ) ),
                Arguments.of( new Fraction( -56, 112 ),
                              new Fraction( 7, 8 ),
                              new Fraction( -14, 8 ) ),
                Arguments.of( new Fraction( 18, 20 ),
                              new Fraction( -9, 2 ),
                              new Fraction( -10, 2 ) ),

                //Different denominators
                Arguments.of( new Fraction( 28, 30 ),
                              new Fraction( 4, 5 ),
                              new Fraction( 6, 7 ) ),
                Arguments.of( new Fraction( -4, 143 ),
                              new Fraction( -1, 11 ),
                              new Fraction( 13, 4 ) ),
                Arguments.of( new Fraction( -80, 63 ),
                              new Fraction( 8, 7 ),
                              new Fraction( -9, 10 ) ),
                Arguments.of( new Fraction( 132, 70 ),
                              new Fraction( -22, 14 ),
                              new Fraction( -5, 6 ) )
        );
    }

    private static Stream<Arguments> divideFractions_OverflowData() {
        return Stream.of(
                //Same denominator
                Arguments.of( new Fraction( Long.MAX_VALUE / 11, 13 ),
                              new Fraction( 2, 13 ) ),
                Arguments.of( new Fraction( Long.MIN_VALUE / 5, 6 ),
                              new Fraction( 123, 6 ) ),
                Arguments.of( new Fraction( 5, Long.MAX_VALUE / 10 ),
                              new Fraction( 11, Long.MAX_VALUE / 10 ) ),
                Arguments.of( new Fraction( 21, Long.MIN_VALUE / 5 ),
                              new Fraction( 33, Long.MIN_VALUE / 5 ) ),

                //Different denominators
                Arguments.of( new Fraction( Long.MAX_VALUE / 4, 42 ),
                              new Fraction( 3, 5 ) ),
                Arguments.of( new Fraction( Long.MIN_VALUE / 2, 2 ),
                              new Fraction( 53, 3 ) ),
                Arguments.of( new Fraction( 8, Long.MAX_VALUE / 41 ),
                              new Fraction( 44, 2 ) ),
                Arguments.of( new Fraction( 81, Long.MIN_VALUE / 23 ),
                              new Fraction( 25, 12 ) )
        );
    }

    private static Stream<Arguments> powerFractionsData() {
        return Stream.of(
                //Powers of zero
                Arguments.of( Fraction.ZERO,
                              Fraction.ZERO,
                              1 ),
                Arguments.of( Fraction.ZERO,
                              Fraction.ZERO,
                              4 ),

                //Powers of +-1
                Arguments.of( Fraction.ONE,
                              Fraction.ONE,
                              17 ),
                Arguments.of( Fraction.ONE,
                              Fraction.ONE,
                              42 ),
                Arguments.of( new Fraction( -1 ),
                              new Fraction( -1 ),
                              17 ),
                Arguments.of( Fraction.ONE,
                              new Fraction( -1 ),
                              42 ),

                //Integer values
                Arguments.of( new Fraction( 64 ),
                              new Fraction( 4 ),
                              3 ),

                //Exponent = 0
                Arguments.of( Fraction.ONE,
                              new Fraction( 7, 10 ),
                              0 ),
                Arguments.of( Fraction.ONE,
                              new Fraction( -11, 5 ),
                              0 ),
                Arguments.of( Fraction.ONE,
                              new Fraction( 92, -104 ),
                              0 ),
                Arguments.of( Fraction.ONE,
                              new Fraction( -42, -33 ),
                              0 ),

                //Positive exponents
                Arguments.of( new Fraction( 32, 243 ),
                              new Fraction( 2, 3 ),
                              5 ),
                Arguments.of( new Fraction( 196, 324 ),
                              new Fraction( 14, 18 ),
                              2 ),
                Arguments.of( new Fraction( -2187, 16384 ),
                              new Fraction( -3, 4 ),
                              7 ),
                Arguments.of( new Fraction( 625, 2401 ),
                              new Fraction( 5, -7 ),
                              4 ),

                //Negative exponents
                Arguments.of( new Fraction( 1728, 64 ),
                              new Fraction( 4, 12 ),
                              -3 ),
                Arguments.of( new Fraction( 1771561, 46656 ),
                              new Fraction( 6, 11 ),
                              -6 ),
                Arguments.of( new Fraction( -59049, 161051 ),
                              new Fraction( -11, 9 ),
                              -5 ),
                Arguments.of( new Fraction( 9765625, 1048576 ),
                              new Fraction( 4, -5 ),
                              -10 )
        );
    }

    private static Stream<Arguments> powerFractions_OverflowData() {
        return Stream.of(
                Arguments.of( new Fraction( 0, 11 ),
                              55 ),
                Arguments.of( new Fraction( 3, 4 ),
                              133 ),
                Arguments.of( new Fraction( -2, 5 ),
                              99 )
        );
    }

    private static Stream<Arguments> reduceFractionsData() {
        return Stream.of(
                //Includes zero
                Arguments.of( Fraction.ZERO,
                              Fraction.ZERO ),
                Arguments.of( Fraction.ZERO,
                              new Fraction( 0, 5 ) ),

                //Integer values
                Arguments.of( new Fraction( 55 ),
                              new Fraction( 55 ) ),

                //Already in the reduced form
                Arguments.of( new Fraction( 6, 13 ),
                              new Fraction( 6, 13 ) ),
                Arguments.of( new Fraction( -41, 42 ),
                              new Fraction( -41, 42 ) ),

                //Not in the reduced form
                Arguments.of( new Fraction( 1, 4 ),
                              new Fraction( 24, 96 ) ),
                Arguments.of( new Fraction( 41467, 50111 ),
                              new Fraction( 82934, 100222 ) ),
                Arguments.of( new Fraction( -1, 4 ),
                              new Fraction( 20, -80 ) ),

                Arguments.of( new Fraction( -3, 50 ),
                              new Fraction( -6, 100 ) ),
                Arguments.of( new Fraction( 2, 11 ),
                              new Fraction( -34, -187 ) ),
                Arguments.of( new Fraction( 6941321249L, 171539216633471L ),
                              new Fraction( 90237176237L, 2230009816235123L ) )
        );
    }

    private static Stream<Arguments> signumOfFractionsData() {
        return Stream.of(
                //Integer values
                Arguments.of( 1,
                              new Fraction( 3 ) ),
                Arguments.of( 0,
                              new Fraction( 0 ) ),
                Arguments.of( 0,
                              Fraction.ZERO ),
                Arguments.of( -1,
                              new Fraction( -8 ) ),

                //Rational values
                Arguments.of( 1,
                              new Fraction( 1, 2 ) ),
                Arguments.of( 1,
                              new Fraction( -3, -5 ) ),
                Arguments.of( 0,
                              new Fraction( 0, 5 ) ),
                Arguments.of( -1,
                              new Fraction( -7, 8 ) ),
                Arguments.of( -1,
                              new Fraction( 1, -14 ) ),

                //Big values
                Arguments.of( 1,
                              new Fraction( Long.MAX_VALUE - 1, Long.MAX_VALUE - 5 ) ),
                Arguments.of( 1,
                              new Fraction( Long.MIN_VALUE + 2, Long.MIN_VALUE + 4 ) ),
                Arguments.of( 0,
                              new Fraction( 0, Long.MAX_VALUE ) ),
                Arguments.of( 0,
                              new Fraction( 0, Long.MIN_VALUE + 1 ) ),
                Arguments.of( -1,
                              new Fraction( Long.MAX_VALUE - 7, Long.MIN_VALUE + 12 ) ),
                Arguments.of( -1,
                              new Fraction( Long.MIN_VALUE + 6, Long.MAX_VALUE - 10 ) )
        );
    }

    private static Stream<Arguments> floorOfFractionsData() {
        return Stream.of(
                //Integer values
                Arguments.of( -16,
                              new Fraction( -16 ) ),
                Arguments.of( -1,
                              new Fraction( -1 ) ),
                Arguments.of( 0,
                              Fraction.ZERO ),
                Arguments.of( 4,
                              new Fraction( 4 ) ),
                Arguments.of( 133,
                              new Fraction( 133 ) ),

                //Rational values
                Arguments.of( -5,
                              new Fraction( -33, 8 ) ),
                Arguments.of( -11,
                              new Fraction( 94, -9 ) ),
                Arguments.of( -1,
                              new Fraction( -1, 2 ) ),
                Arguments.of( 0,
                              new Fraction( 1, 5 ) ),
                Arguments.of( 2,
                              new Fraction( 146, 70 ) ),
                Arguments.of( 16,
                              new Fraction( 33, 2 ) ),

                //Big values
                Arguments.of( Long.MAX_VALUE,
                              new Fraction( Long.MAX_VALUE ) ),
                Arguments.of( 0,
                              new Fraction( Long.MAX_VALUE - 1, Long.MAX_VALUE ) ),
                Arguments.of( 1,
                              new Fraction( Long.MAX_VALUE, Long.MAX_VALUE - 1 ) ),
                Arguments.of( 4611686018427387903L,
                              new Fraction( Long.MAX_VALUE, 2 ) ),
                Arguments.of( 0,
                              new Fraction( 0, Long.MAX_VALUE ) ),
                Arguments.of( Long.MIN_VALUE + 1,
                              new Fraction( Long.MIN_VALUE + 1 ) ),
                Arguments.of( 0,
                              new Fraction( Long.MIN_VALUE + 2, Long.MIN_VALUE + 1 ) ),
                Arguments.of( 1,
                              new Fraction( Long.MIN_VALUE + 1, Long.MIN_VALUE + 2 ) ),
                Arguments.of( 0,
                              new Fraction( 0, Long.MIN_VALUE + 1 ) ),
                Arguments.of( -3074457345618258603L,
                              new Fraction( Long.MIN_VALUE + 1, 3 ) )
        );
    }

    private static Stream<Arguments> ceilOfFractionsData() {
        return Stream.of(
                //Integer values
                Arguments.of( -33,
                              new Fraction( -33 ) ),
                Arguments.of( -2,
                              new Fraction( -2 ) ),
                Arguments.of( 0,
                              Fraction.ZERO ),
                Arguments.of( 5,
                              new Fraction( 5 ) ),
                Arguments.of( 244,
                              new Fraction( 244 ) ),

                //Rational values
                Arguments.of( -4,
                              new Fraction( -25, 6 ) ),
                Arguments.of( -11,
                              new Fraction( 92, -8 ) ),
                Arguments.of( -1,
                              new Fraction( -3, 2 ) ),
                Arguments.of( 0,
                              new Fraction( -1, 5 ) ),
                Arguments.of( 3,
                              new Fraction( 126, 62 ) ),
                Arguments.of( 16,
                              new Fraction( 46, 3 ) ),

                //Big values
                Arguments.of( Long.MAX_VALUE,
                              new Fraction( Long.MAX_VALUE ) ),
                Arguments.of( 1,
                              new Fraction( Long.MAX_VALUE - 1, Long.MAX_VALUE ) ),
                Arguments.of( 2,
                              new Fraction( Long.MAX_VALUE, Long.MAX_VALUE - 1 ) ),
                Arguments.of( 4611686018427387904L,
                              new Fraction( Long.MAX_VALUE, 2 ) ),
                Arguments.of( 0,
                              new Fraction( 0, Long.MAX_VALUE ) ),
                Arguments.of( Long.MIN_VALUE + 1,
                              new Fraction( Long.MIN_VALUE + 1 ) ),
                Arguments.of( 1,
                              new Fraction( Long.MIN_VALUE + 2, Long.MIN_VALUE + 1 ) ),
                Arguments.of( 2,
                              new Fraction( Long.MIN_VALUE + 1, Long.MIN_VALUE + 2 ) ),
                Arguments.of( 0,
                              new Fraction( 0, Long.MIN_VALUE + 1 ) ),
                Arguments.of( -3074457345618258602L,
                              new Fraction( Long.MIN_VALUE + 1, 3 ) )
        );
    }

    private static Stream<Arguments> properFractionsData() {
        return Stream.of(
                Arguments.of( new Fraction( 1, 18 ) ),
                Arguments.of( new Fraction( 2, 4 ) ),
                Arguments.of( new Fraction( 5, 7 ) ),
                Arguments.of( new Fraction( 1234, 6756756 ) ),
                Arguments.of( new Fraction( -4, -9 ) ),
                Arguments.of( new Fraction( -534, -1111 ) ),
                Arguments.of( new Fraction( -643, -15345345 ) )
        );
    }

    private static Stream<Arguments> notProperFractionsData() {
        return Stream.of(
                Arguments.of( Fraction.ZERO ),
                Arguments.of( new Fraction( -3, 7 ) ),
                Arguments.of( new Fraction( 13, -22 ) ),
                Arguments.of( new Fraction( 5, 4 ) ),
                Arguments.of( new Fraction( 12, 12 ) ),
                Arguments.of( new Fraction( -4, -2 ) )
        );
    }

    private static Stream<Arguments> reducedFractionsData() {
        return Stream.of(
                //Values = 0
                Arguments.of( Fraction.ZERO ),
                Arguments.of( new Fraction( 0, 2 ) ),
                Arguments.of( new Fraction( 0, -13 ) ),
                Arguments.of( new Fraction( 5, 7 ) ),

                //Values != 0
                Arguments.of( Fraction.ONE ),
                Arguments.of( new Fraction( -13, 41 ) ),
                Arguments.of( new Fraction( 22, -101 ) ),
                Arguments.of( new Fraction( -3, -8 ) )
        );
    }

    private static Stream<Arguments> notReducedFractionsData() {
        return Stream.of(
                Arguments.of( new Fraction( 15, 55 ) ),
                Arguments.of( new Fraction( -3, 9 ) ),
                Arguments.of( new Fraction( 12, -100 ) ),
                Arguments.of( new Fraction( -11, -121 ) )
        );
    }

    private static Stream<Arguments> fractionsToDoubleData() {
        return Stream.of(
                Arguments.of( 14.0,
                              new Fraction( 14 ) ),
                Arguments.of( 1.0 / 4.0,
                              new Fraction( 1, 4 ) ),
                Arguments.of( 25.0 / 5.0,
                              new Fraction( 25, 5 ) ),
                Arguments.of( 1.0 / 100.0,
                              new Fraction( -6, -600 ) ),
                Arguments.of( 20.0 / 4.0,
                              new Fraction( -20, -4 ) ),
                Arguments.of( -3.0 / 2.0,
                              new Fraction( -3, 2 ) ),
                Arguments.of( 1.0 / -8.0,
                              new Fraction( 1, -8 ) )
        );
    }

    private static Stream<Arguments> equalFractionsData() {
        return Stream.of(
                //Integer values
                Arguments.of( new Fraction( 51 ),
                              new Fraction( 51 ) ),
                Arguments.of( new Fraction( -17 ),
                              new Fraction( -17 ) ),

                //Fraction reduced forms are equal
                Arguments.of( new Fraction( 2, 7 ),
                              new Fraction( 26, 91 ) ),
                Arguments.of( new Fraction( -11, 25 ),
                              new Fraction( -44, 100 ) ),
                Arguments.of( new Fraction( 207, 9 ),
                              new Fraction( -621, -27 ) )
        );
    }

    private static Stream<Arguments> notEqualFractionsData() {
        return Stream.of(
                //Integer values
                Arguments.of( new Fraction( -92 ),
                              new Fraction( 92 ) ),
                Arguments.of( new Fraction( 162 ),
                              new Fraction( -162 ) ),

                //Fraction reduced forms are not equal
                Arguments.of( new Fraction( 2, 7 ),
                              new Fraction( 26, 104 ) ),
                Arguments.of( new Fraction( -11, 12 ),
                              new Fraction( -66, 100 ) ),
                Arguments.of( new Fraction( 1, 3 ),
                              new Fraction( -15, -30 ) ),

                //The same value but different sign
                Arguments.of( new Fraction( -12, 13 ),
                              new Fraction( 12, 13 ) ),
                Arguments.of( new Fraction( 12, 13 ),
                              new Fraction( 12, -13 ) ),
                Arguments.of( new Fraction( -1, 2 ),
                              new Fraction( 5, 10 ) ),
                Arguments.of( new Fraction( 1, 2 ),
                              new Fraction( -5, 10 ) )
        );
    }

    private static Stream<Arguments> toStringData() {
        return Stream.of(
                Arguments.of( "142/1",
                              new Fraction( 142 ) ),
                Arguments.of( "345/8912",
                              new Fraction( 345, 8912 ) )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot create fraction with {1} as numerator and {2} as denominator, " +
                               "raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long numerator,
            long denominator ) {
        assertException( throwableClass, () -> new Fraction( numerator, denominator ) );
    }

    @ParameterizedTest( name = "{index}) reverse({0}) = {1} and reverse({1}) = {0}" )
    @MethodSource( "reverseFractionsData" )
    public void test_CorrectlyReverseFraction( Fraction expectedReversedFraction, Fraction fraction ) {
        Fraction reversedFraction = fraction.reverse();
        assertEquals( expectedReversedFraction, reversedFraction );
        assertNotSame( fraction, reversedFraction );

        assertEquals( fraction, reversedFraction.reverse() );
    }

    @ParameterizedTest( name = "{index}) add({1}, {2}) = {0} and add({2}, {1}) = {0}" )
    @MethodSource( "addFractionsData" )
    public void test_CorrectlyAddFractions( Fraction expectedSum, Fraction a, Fraction b ) {
        Fraction sum1 = a.add( b );
        Fraction sum2 = b.add( a );

        assertEquals( expectedSum, sum1 );
        assertEquals( expectedSum, sum2 );

        assertNotSame( a, sum1 );
        assertNotSame( b, sum1 );
        assertNotSame( a, sum2 );
        assertNotSame( b, sum2 );
    }

    @ParameterizedTest( name = "{index}) add({0}, {1}) causes overflow" )
    @MethodSource( "addFractions_OverflowData" )
    public void test_AddingFractions_CausesOverflow( Fraction a, Fraction b ) {
        assertException( ArithmeticException.class, () -> a.add( b ) );
    }

    @ParameterizedTest( name = "{index}) subtract({1}, {2}) = {0} and subtract({2}, {1}) != {0}" )
    @MethodSource( "subtractFractionsData" )
    public void test_CorrectlySubtractFractions( Fraction expectedDifference, Fraction a, Fraction b ) {
        Fraction difference1 = a.subtract( b );
        Fraction difference2 = b.subtract( a );

        assertEquals( expectedDifference, difference1 );
        if( !a.equals( b ) ) {
            assertNotEquals( expectedDifference, difference2 );
        }

        assertNotSame( a, difference1 );
        assertNotSame( b, difference1 );
        assertNotSame( a, difference2 );
        assertNotSame( b, difference2 );
    }

    @ParameterizedTest( name = "{index}) subtract({0}, {1}) causes overflow" )
    @MethodSource( "subtractFractions_OverflowData" )
    public void test_SubtractingFractions_CausesOverflow( Fraction a, Fraction b ) {
        assertException( ArithmeticException.class, () -> a.subtract( b ) );
    }

    @ParameterizedTest( name = "{index}) multiply({1}, {2}) = {0} and multiply({2}, {1}) = {0}" )
    @MethodSource( "multiplyFractionsData" )
    public void test_CorrectlyMultiplyFractions( Fraction expectedProduct, Fraction a, Fraction b ) {
        Fraction product1 = a.multiply( b );
        Fraction product2 = b.multiply( a );

        assertEquals( expectedProduct, product1 );
        assertEquals( expectedProduct, product2 );

        assertNotSame( a, product1 );
        assertNotSame( b, product1 );
        assertNotSame( a, product2 );
        assertNotSame( b, product2 );
    }

    @ParameterizedTest( name = "{index}) multiply({0}, {1}) causes overflow" )
    @MethodSource( "multiplyFractions_OverflowData" )
    public void test_MultiplyingFractions_CausesOverflow( Fraction a, Fraction b ) {
        assertException( ArithmeticException.class, () -> a.multiply( b ) );
    }

    @ParameterizedTest( name = "{index}) divide({1}, {2}) = {0} and divide({2}, {1}) != {0}" )
    @MethodSource( "divideFractionsData" )
    public void test_CorrectlyDivideFractions( Fraction expectedQuotient, Fraction a, Fraction b ) {
        Fraction quotient1 = a.divide( b );
        Fraction quotient2 = a.equals( Fraction.ZERO ) ? null : b.divide( a );

        assertEquals( expectedQuotient, quotient1 );
        assertNotEquals( expectedQuotient, quotient2 );

        assertNotSame( a, quotient1 );
        assertNotSame( b, quotient1 );
        assertNotSame( a, quotient2 );
        assertNotSame( b, quotient2 );
    }

    @ParameterizedTest( name = "{index}) divide({0}, {1}) causes overflow" )
    @MethodSource( "divideFractions_OverflowData" )
    public void test_DividingFractions_CausesOverflow( Fraction a, Fraction b ) {
        assertException( ArithmeticException.class, () -> a.divide( b ) );
    }

    @ParameterizedTest( name = "{index}) ({1})^{2} = {0}" )
    @MethodSource( "powerFractionsData" )
    public void test_CorrectlyCalculateThePowerOfFractions( Fraction expectedPower, Fraction a, int exponent ) {
        Fraction power = a.pow( exponent );

        assertEquals( expectedPower, power );
        assertNotSame( a, power );
    }

    @ParameterizedTest( name = "{index}) ({1})^{2} causes overflow" )
    @MethodSource( "powerFractions_OverflowData" )
    public void test_RaisingFractionsToPower_CausesOverflow( Fraction a, int exponent ) {
        assertException( ArithmeticException.class, () -> a.pow( exponent ) );
    }

    @ParameterizedTest( name = "{index}) reduce({1}) = {0}" )
    @MethodSource( "reduceFractionsData" )
    public void test_CorrectlyReduceFractions( Fraction expectedReducedFraction, Fraction a ) {
        Fraction reducedFraction = a.reduce();

        assertEquals( expectedReducedFraction, reducedFraction );
        assertNotSame( a, reducedFraction );
    }

    @ParameterizedTest( name = "{index}) signum({1}) = {0}" )
    @MethodSource( "signumOfFractionsData" )
    public void test_CorrectlyCalculateSignumOfFractions( int expectedSignum, Fraction a ) {
        assertEquals( expectedSignum, a.signum() );
    }

    @ParameterizedTest( name = "{index}) floor({1}) = {0}" )
    @MethodSource( "floorOfFractionsData" )
    public void test_CorrectlyCalculateFloorOfFractions( long expectedFloor, Fraction a ) {
        assertEquals( expectedFloor, a.floor() );
    }

    @ParameterizedTest( name = "{index}) ceil({1}) = {0}" )
    @MethodSource( "ceilOfFractionsData" )
    public void test_CorrectlyCalculateCeilOfFractions( long expectedCeil, Fraction a ) {
        assertEquals( expectedCeil, a.ceil() );
    }

    @ParameterizedTest( name = "{index}) {0} is a proper fraction" )
    @MethodSource( "properFractionsData" )
    public void test_CorrectlyDeterminIfFractionIsProper( Fraction a ) {
        assertTrue( a.isProper() );
    }

    @ParameterizedTest( name = "{index}) {0} is not a proper fraction" )
    @MethodSource( "notProperFractionsData" )
    public void test_CorrectlyDeterminIfFractionIsNotProper( Fraction a ) {
        assertFalse( a.isProper() );
    }

    @ParameterizedTest( name = "{index}) {0} is a reduced fraction" )
    @MethodSource( "reducedFractionsData" )
    public void test_CorrectlyDeterminIfFractionIsReduced( Fraction a ) {
        assertTrue( a.isReduced() );
    }

    @ParameterizedTest( name = "{index}) {0} is not a reduced fraction" )
    @MethodSource( "notReducedFractionsData" )
    public void test_CorrectlyDeterminIfFractionIsNotReduced( Fraction a ) {
        assertFalse( a.isReduced() );
    }

    @ParameterizedTest( name = "{index}) {1} ~ {0}" )
    @MethodSource( "fractionsToDoubleData" )
    public void test_CorrectlyConvertFractionToDouble( double expectedDoubleValue, Fraction a ) {
        assertEquals( expectedDoubleValue, a.toDouble(), 0.0 );
    }

    @ParameterizedTest( name = "{index}) {0} is equal to {1}" )
    @MethodSource( "equalFractionsData" )
    public void test_Equals( Fraction a, Fraction b ) {
        assertEquals( a, b );
    }

    @ParameterizedTest( name = "{index}) {0} is not equal to {1}" )
    @MethodSource( "notEqualFractionsData" )
    public void test_NotEquals( Fraction a, Fraction b ) {
        assertNotEquals( a, b );
    }

    @ParameterizedTest( name = "{index}) \"{0}\" is the same string as \"{1}\"" )
    @MethodSource( "toStringData" )
    public void test_ToString( String s, Fraction a ) {
        assertEquals( s, a.toString() );
    }

    @Test
    public void test_CannotCreateFraction_WithLongMINValue_AsNumerator_WhenGivingOnlyTheNumerator() {
        assertException( IllegalArgumentException.class, () -> new Fraction( Long.MIN_VALUE ) );
    }

    @Test
    public void test_ConstantsHaveCorrectValue() {
        assertTrue( Fraction.ZERO.toDouble() == 0.0 );
        assertTrue( Fraction.ONE.toDouble() == 1.0 );
    }

    @Test
    public void test_FractionCreatedWithJustNumerator_HasOneAsDenominator() {
        assertEquals( 1, new Fraction( 4 ).getDenominator() );
        assertEquals( 1, new Fraction( 0 ).getDenominator() );
        assertEquals( 1, new Fraction( -31 ).getDenominator() );
    }

    @Test
    public void test_SignsAreChangedToPositive_WhenFractionIsCreatedWithNegativeNumeratorAndDenominator() {
        Fraction fraction = new Fraction( -4, -2 );

        assertEquals( 4, fraction.getNumerator() );
        assertEquals( 2, fraction.getDenominator() );
    }

    @Test
    public void test_CopyingConstructor_WorksCorrectly() {
        Fraction fraction = new Fraction( 6, 7 );
        Fraction newFraction = new Fraction( fraction );

        assertEquals( fraction.getNumerator(), newFraction.getNumerator() );
        assertEquals( fraction.getDenominator(), newFraction.getDenominator() );
    }

    @Test
    public void test_ForbiddenCalculations_RaiseExceptions() {
        //Reverting zero
        assertException( ArithmeticException.class, () -> Fraction.ZERO.reverse() );

        //Dividing by zero
        assertException( ArithmeticException.class, () -> new Fraction( 1, 5 ).divide( Fraction.ZERO ) );
        assertException( ArithmeticException.class, () -> Fraction.ZERO.divide( Fraction.ZERO ) );

        //Raising to the power of Long.MIN_VALUE
        assertException( IllegalArgumentException.class, () -> new Fraction( 8, 3 ).pow( Integer.MIN_VALUE ) );

        //Zero to the power of zero
        assertException( ArithmeticException.class, () -> Fraction.ZERO.pow( 0 ) );

        //Zero raised to negative power
        assertException( ArithmeticException.class, () -> Fraction.ZERO.pow( -1 ) );
        assertException( ArithmeticException.class, () -> Fraction.ZERO.pow( -14 ) );
        assertException( ArithmeticException.class, () -> Fraction.ZERO.pow( -98 ) );
    }

    @Test
    public void test_CompareTo() {
        assertTrue( new Fraction( 6 ).compareTo( new Fraction( 5 ) ) > 0 );
        assertTrue( new Fraction( 6 ).compareTo( new Fraction( 11 ) ) < 0 );
        assertTrue( new Fraction( 6 ).compareTo( new Fraction( 6 ) ) == 0 );

        assertTrue( new Fraction( 1, 4 ).compareTo( new Fraction( 2, 10 ) ) > 0 );
        assertTrue( new Fraction( 2, 10 ).compareTo( new Fraction( 1, 4 ) ) < 0 );
        assertTrue( new Fraction( 2, 10 ).compareTo( new Fraction( 1, 5 ) ) == 0 );

        assertTrue( new Fraction( -1, 4 ).compareTo( new Fraction( -2, 10 ) ) < 0 );
        assertTrue( new Fraction( -2, 10 ).compareTo( new Fraction( -1, 4 ) ) > 0 );
        assertTrue( new Fraction( -2, 10 ).compareTo( new Fraction( -1, 5 ) ) == 0 );
    }

    @Test
    public void test_Equals() {
        Fraction fraction = new Fraction( 45, 90 );

        assertTrue( fraction.equals( fraction ) );
        assertFalse( fraction.equals( null ) );
        assertFalse( fraction.equals( new String() ) );
        assertTrue( fraction.equals( new Fraction( 45, 90 ) ) );
        assertTrue( fraction.equals( new Fraction( 1, 2 ) ) );
    }

    @Test
    public void test_HashCode() {
        Fraction fraction = new Fraction( 45, 90 );

        assertEquals( fraction.hashCode(), fraction.hashCode() );
        assertEquals( fraction.hashCode(), new Fraction( 45, 90 ).hashCode() );
        assertNotEquals( fraction.hashCode(), new Fraction( 1, 2 ).hashCode() );
    }
}
