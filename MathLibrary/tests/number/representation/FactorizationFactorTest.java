package number.representation;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.*;

public class FactorizationFactorTest {
    private static Stream<Arguments> factorizationFactorData() {
        return Stream.of(
                //Negative "prime" value
                Arguments.of( -1022, -34 ),
                Arguments.of( -7, 0 ),

                //Zero as "prime value"
                Arguments.of( 0, 1 ),

                //Prime as "prime value"
                Arguments.of( 2, 3 ),
                Arguments.of( 997, 2 )
        );
    }

    @ParameterizedTest( name = "{index}) Factorization factor can be created with prime = {0} and exponent = {1}" )
    @MethodSource( "factorizationFactorData" )
    public void test_FactorizationFactor_CanBeCreatedWithAnyValues( long prime, int exponent ) {
        FactorizationFactor factorizationFactor = new FactorizationFactor( prime, exponent );

        assertEquals( prime, factorizationFactor.getPrime() );
        assertEquals( exponent, factorizationFactor.getExponent() );
    }

    @Test
    public void test_HashCode() {
        FactorizationFactor factor = new FactorizationFactor( 5, 3 );

        assertEquals( factor.hashCode(), factor.hashCode() );
        assertEquals( factor.hashCode(), new FactorizationFactor( 5, 3 ).hashCode() );
        assertNotEquals( factor.hashCode(), new FactorizationFactor( 7, 4 ).hashCode() );
    }

    @Test
    public void test_Equals() {
        FactorizationFactor factor = new FactorizationFactor( 5, 3 );

        assertTrue( factor.equals( factor ) );
        assertFalse( factor.equals( null ) );
        assertFalse( factor.equals( new String() ) );

        assertTrue( factor.equals( new FactorizationFactor( 5, 3 ) ) );
        assertFalse( factor.equals( new FactorizationFactor( 5, 2 ) ) );
        assertFalse( factor.equals( new FactorizationFactor( 7, 3 ) ) );
        assertFalse( factor.equals( new FactorizationFactor( 11, 11 ) ) );
    }

    @Test
    public void test_ToString() {
        assertEquals( "11^5", new FactorizationFactor( 11, 5 ).toString() );
    }
}
