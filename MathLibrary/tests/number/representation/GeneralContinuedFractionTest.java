package number.representation;

import number.representation.big.BigFraction;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static java.lang.Math.sqrt;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static number.Constants.E;
import static number.Constants.PI;
import static org.junit.Assert.*;
import static testutils.ExceptionAssert.assertException;

public class GeneralContinuedFractionTest {
    private static Stream<Arguments> exactCoefficientsData() {
        return Stream.of(
                //Integer values
                Arguments.of( bigIntegerList( -14377789345L ),
                              new GeneralContinuedFraction( -14377789345.0 ) ),
                Arguments.of( bigIntegerList( -1234 ),
                              new GeneralContinuedFraction( -1234.0 ) ),
                Arguments.of( bigIntegerList( 0 ),
                              new GeneralContinuedFraction( 0 ) ),
                Arguments.of( bigIntegerList( 987234 ),
                              new GeneralContinuedFraction( 987234.0 ) ),
                Arguments.of( bigIntegerList( 444444444444L ),
                              new GeneralContinuedFraction( 444444444444.0 ) ),

                //Rational values
                Arguments.of( bigIntegerList( 0, 2 ),
                              new GeneralContinuedFraction( 0.5 ) ),
                Arguments.of( bigIntegerList( 0, 5, 3, 1, 4 ),
                              new GeneralContinuedFraction( 0.19 ) ),
                Arguments.of( bigIntegerList( 1575, 1, 2, 33 ),
                              new GeneralContinuedFraction( 1575.67 ) ),
                Arguments.of( bigIntegerList( 768, 4 ),
                              new GeneralContinuedFraction( 768.25 ) ),
                Arguments.of( bigIntegerList( 82342834234L, 20 ),
                              new GeneralContinuedFraction( 82342834234.05 ) ),
                Arguments.of( bigIntegerList( -3, 2, 2 ),
                              new GeneralContinuedFraction( -2.6 ) ),
                Arguments.of( bigIntegerList( -4913, 1, 124 ),
                              new GeneralContinuedFraction( -4912.008 ) ),
                Arguments.of( bigIntegerList( -90003, 10 ),
                              new GeneralContinuedFraction( -90002.9 ) ),
                Arguments.of( bigIntegerList( -1000283111124L, 74, 13, 2 ),
                              new GeneralContinuedFraction( -1000283111123.9865 ) ),

                //Big coefficient included
                Arguments
                        .of( bigIntegerList( new BigInteger( "1110982387612344555555555844" ), 51, 1, 5, 3, 7, 8, 3, 14,
                                             1,
                                             1, 2, 4, 1, 1, 1, 2, 1, 8, 1, 3, 1, 75, 1, 21, 7, 8, 1, 4, 7, 1, 9, 5, 9,
                                             5,
                                             16, 1, 1, 1, 1, 1, 4, 5, 20, 1, 1, 1, 1, 1, 1, 4, 1, 18, 1, 1, 1, 3, 1 ),
                             new GeneralContinuedFraction(
                                     new BigDecimal( "1110982387612344555555555844.019289478712398723409836666666" ) ) )
        );
    }

    private static List<BigInteger> bigIntegerList( Number... values ) {
        List<BigInteger> bigIntList = new ArrayList<>( values.length );

        for( Number value : values ) {
            if( value instanceof BigInteger ) {
                bigIntList.add( (BigInteger) value );
            }
            else {
                bigIntList.add( BigInteger.valueOf( value.longValue() ) );
            }
        }

        return bigIntList;
    }

    private static Stream<Arguments> firstCoefficientsData() {
        return Stream.of(
                //Irrational values
                Arguments.of( bigIntegerList( 3, 7, 15, 1, 292, 1, 1, 1, 2, 1, 3, 1, 14 ),
                              new GeneralContinuedFraction( PI ) ),
                Arguments.of( bigIntegerList( 2, 1, 2, 1, 1, 4, 1, 1, 6, 1, 1, 8, 1, 1 ),
                              new GeneralContinuedFraction( E ) ),
                Arguments.of( bigIntegerList( 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ),
                              new GeneralContinuedFraction( sqrt( 2 ) ) ),
                Arguments.of( bigIntegerList( 8, 6, 6, 6, 6, 6, 6, 6, 6, 6 ),
                              new GeneralContinuedFraction( 5 + sqrt( 10 ) ) ),
                Arguments.of( bigIntegerList( 2, 4, 2, 1, 49, 2, 1, 6, 2, 11 ),
                              new GeneralContinuedFraction( ( 31 + sqrt( 22 ) ) / 16 ) )
        );
    }

    private static Stream<Arguments> exactConvergentsData() {
        return Stream.of(
                //Integer values
                Arguments.of( singletonList( bigFraction( "-8293421231231" ) ),
                              new GeneralContinuedFraction( -8293421231231.0 ) ),
                Arguments.of( singletonList( bigFraction( "-9281" ) ),
                              new GeneralContinuedFraction( -9281.0 ) ),
                Arguments.of( singletonList( BigFraction.ZERO ),
                              new GeneralContinuedFraction( 0.0 ) ),
                Arguments.of( singletonList( bigFraction( "1235666" ) ),
                              new GeneralContinuedFraction( 1235666.0 ) ),
                Arguments.of( singletonList( bigFraction( "910273612312311" ) ),
                              new GeneralContinuedFraction( 910273612312311.0 ) ),

                //Rational values
                Arguments.of( asList( BigFraction.ZERO, bigFraction( "1", "2" ) ),
                              new GeneralContinuedFraction( 0.5 ) ),
                Arguments.of( asList( bigFraction( "2" ), bigFraction( "5", "2" ) ),
                              new GeneralContinuedFraction( 2.5 ) ),
                Arguments.of( asList( BigFraction.ZERO, bigFraction( "1", "16" ), bigFraction( "7", "113" ),
                                      bigFraction( "8", "129" ), bigFraction( "23", "371" ),
                                      bigFraction( "31", "500" ) ),
                              new GeneralContinuedFraction( 0.062 ) ),
                Arguments.of( asList( bigFraction( "-1" ), BigFraction.ZERO, bigFraction( "-1", "135" ),
                                      bigFraction( "-7", "946" ), bigFraction( "-15", "2027" ),
                                      bigFraction( "-22", "2973" ),
                                      bigFraction( "-37", "5000" ) ),
                              new GeneralContinuedFraction( -0.0074 ) ),
                Arguments.of( asList( bigFraction( "-63550014", "1" ), bigFraction( "-8769901931", "138" ),
                                      bigFraction( "-8833451945", "139" ), bigFraction( "-79437517491", "1250" ) ),
                              new GeneralContinuedFraction( -63550013.9928 ) ),
                Arguments.of( asList( bigFraction( "899926" ), bigFraction( "2699779", "3" ),
                                      bigFraction( "3599705", "4" ),
                                      bigFraction( "89092699", "99" ), bigFraction( "270877802", "301" ),
                                      bigFraction( "359970501", "400" ) ),
                              new GeneralContinuedFraction( 899926.2525 ) ),

                //Big convergent included
                Arguments.of( asList( bigFraction( "17", "1" ),
                                      bigFraction( "69", "4" ),
                                      bigFraction( "86", "5" ),
                                      bigFraction( "37049", "2154" ),
                                      bigFraction( "481723", "28007" ),
                                      bigFraction( "518772", "30161" ),
                                      bigFraction( "1519267", "88329" ),
                                      bigFraction( "55212384", "3210005" ),
                                      bigFraction( "443218339", "25768369" ),
                                      bigFraction( "2714522418", "157820219" ),
                                      bigFraction( "3157740757", "183588588" ),
                                      bigFraction( "12187744689", "708585983" ),
                                      bigFraction( "15345485446", "892174571" ),
                                      bigFraction( "27533230135", "1600760554" ),
                                      bigFraction( "70411945716", "4093695679" ),
                                      bigFraction( "590828795863", "34350325986" ),
                                      bigFraction( "661240741579", "38444021665" ),
                                      bigFraction( "3235791762179", "188126412646" ),
                                      bigFraction( "7132824265937", "414696846957" ),
                                      bigFraction( "131626628549045", "7652669657872" ),
                                      bigFraction( "138759452814982", "8067366504829" ),
                                      bigFraction( "270386081364027", "15720036162701" ),
                                      bigFraction( "1220303778271090", "70947511155633" ),
                                      bigFraction( "11253120085803837", "654247636563398" ),
                                      bigFraction( "57485904207290275", "3342185693972623" ),
                                      bigFraction( "126224928500384387", "7338619024508644" ),
                                      bigFraction( "183710832707674662", "10680804718481267" ),
                                      bigFraction( "677357426623408373", "39381033179952445" ),
                                      bigFraction( "861068259331083035", "50061837898433712" ),
                                      bigFraction( "1538425685954491408", "89442871078386157" ),
                                      bigFraction( "2399493945285574443", "139504708976819869" ),
                                      bigFraction( "6337413576525640294", "368452289032025895" ),
                                      bigFraction( "8736907521811214737", "507956998008845764" ),
                                      bigFraction( "15074321098336855031", "876409287040871659" ),
                                      bigFraction( "23811228620148069768", "1384366285049717423" ),
                                      bigFraction( "157941692819225273639", "9182606997339176197" ),
                                      bigFraction( "181752921439373343407", "10566973282388893620" ),
                                      bigFraction( "1066706300016091990674", "62017473409283644297" ),
                                      bigFraction( "2315165521471557324755", "134601920100956182214" ),
                                      bigFraction( "3381871821487649315429", "196619393510239826511" ),
                                      bigFraction( "12460780985934505271042", "724460100631675661747" ),
                                      bigFraction( "103068119708963691483765", "5992300198563645120487" ),
                                      bigFraction( "115528900694898196754807", "6716760299195320782234" ),
                                      bigFraction( "449654821793658281748186", "26142581096149607467189" ),
                                      bigFraction( "7310006049393430704725783", "424998057837589040257258" ),
                                      bigFraction( "7759660871187088986473969", "451140638933738647724447" ),
                                      bigFraction( "84906614761264320569465473", "4936404447174975517501728" ),
                                      bigFraction( "1705891956096473500375783429", "99179229582433248997759007" ),
                                      bigFraction( "5202582483050684821696815760", "302474093194474722510778749" ),
                                      bigFraction( "152580783964566333329583440469", "8870927932222200201810342728" ),
                                      bigFraction( "157783366447617018151280256229", "9173402025416674924321121477" ),
                                      bigFraction( "468147516859800369632143952927", "27217731983055550050452585682" ),
                                      bigFraction( "625930883307417387783424209156", "36391134008472224974773707159" ),
                                      bigFraction( "1094078400167217757415568162083", "63608865991527775025226292841" ),
                                      bigFraction( "1720009283474635145198992371239",
                                                   "100000000000000000000000000000" ) ),
                              new GeneralContinuedFraction( new BigDecimal( "17.20009283474635145198992371239" ) ) )
        );
    }

    private static BigFraction bigFraction( String numerator ) {
        return bigFraction( numerator, "1" );
    }

    private static BigFraction bigFraction( String numerator, String denominator ) {
        return new BigFraction( new BigInteger( numerator ), new BigInteger( denominator ) );
    }

    private static Stream<Arguments> firstConvergentsData() {
        return Stream.of(
                //Irrational values
                Arguments.of( asList( bigFraction( "3", "1" ), bigFraction( "22", "7" ), bigFraction( "333", "106" ),
                                      bigFraction( "355", "113" ), bigFraction( "103993", "33102" ),
                                      bigFraction( "104348", "33215" ), bigFraction( "208341", "66317" ),
                                      bigFraction( "312689", "99532" ), bigFraction( "833719", "265381" ) ),
                              new GeneralContinuedFraction( PI ) ),
                Arguments.of( asList( bigFraction( "2", "1" ), bigFraction( "3", "1" ), bigFraction( "8", "3" ),
                                      bigFraction( "11", "4" ), bigFraction( "19", "7" ), bigFraction( "87", "32" ),
                                      bigFraction( "106", "39" ), bigFraction( "193", "71" ),
                                      bigFraction( "1264", "465" ),
                                      bigFraction( "1457", "536" ), bigFraction( "2721", "1001" ),
                                      bigFraction( "23225", "8544" ), bigFraction( "25946", "9545" ),
                                      bigFraction( "49171", "18089" ), bigFraction( "517656", "190435" ),
                                      bigFraction( "566827", "208524" ) ), new GeneralContinuedFraction( E ) ),
                Arguments.of( asList( bigFraction( "2", "1" ), bigFraction( "9", "4" ), bigFraction( "38", "17" ),
                                      bigFraction( "161", "72" ), bigFraction( "682", "305" ),
                                      bigFraction( "2889", "1292" ),
                                      bigFraction( "12238", "5473" ), bigFraction( "51841", "23184" ),
                                      bigFraction( "219602", "98209" ), bigFraction( "930249", "416020" ),
                                      bigFraction( "3940598", "1762289" ), bigFraction( "16692641", "7465176" ),
                                      bigFraction( "70711162", "31622993" ) ),
                              new GeneralContinuedFraction( sqrt( 5 ) ) ),
                Arguments.of( asList( bigFraction( "10", "1" ), bigFraction( "21", "2" ), bigFraction( "31", "3" ),
                                      bigFraction( "145", "14" ), bigFraction( "176", "17" ),
                                      bigFraction( "321", "31" ),
                                      bigFraction( "497", "48" ), bigFraction( "2309", "223" ),
                                      bigFraction( "2806", "271" ),
                                      bigFraction( "5115", "494" ), bigFraction( "7921", "765" ),
                                      bigFraction( "36799", "3554" ), bigFraction( "44720", "4319" ),
                                      bigFraction( "81519", "7873" ), bigFraction( "126239", "12192" ),
                                      bigFraction( "586475", "56641" ), bigFraction( "712714", "68833" ),
                                      bigFraction( "1299189", "125474" ), bigFraction( "2011903", "194307" ) ),
                              new GeneralContinuedFraction( 13 - sqrt( 7 ) ) ),
                Arguments.of( asList( bigFraction( "8", "1" ), bigFraction( "9", "1" ), bigFraction( "35", "4" ),
                                      bigFraction( "44", "5" ), bigFraction( "123", "14" ),
                                      bigFraction( "1028", "117" ),
                                      bigFraction( "2179", "248" ), bigFraction( "3207", "365" ),
                                      bigFraction( "11800", "1343" ),
                                      bigFraction( "15007", "1708" ), bigFraction( "41814", "4759" ),
                                      bigFraction( "349519", "39780" ), bigFraction( "740852", "84319" ),
                                      bigFraction( "1090371", "124099" ), bigFraction( "4011965", "456616" ),
                                      bigFraction( "5102336", "580715" ), bigFraction( "14216637", "1618046" ),
                                      bigFraction( "118835432", "13525083" ), bigFraction( "133052069", "15143129" ),
                                      bigFraction( "251887501", "28668212" ) ),
                              new GeneralContinuedFraction( ( 22 + sqrt( 19 ) ) / 3 ) )
        );
    }

    private static Stream<Arguments> getValueData() {
        return Stream.of(
                //Integer values
                Arguments.of( new BigDecimal( "234" ), new GeneralContinuedFraction( 234.0 ) ),
                Arguments.of( new BigDecimal( "0" ), new GeneralContinuedFraction( 0.0 ) ),
                Arguments.of( new BigDecimal( "-2" ), new GeneralContinuedFraction( -2.0 ) ),
                Arguments
                        .of( new BigDecimal( "-987276716723123" ), new GeneralContinuedFraction( -987276716723123.0 ) ),

                //Rational values
                Arguments.of( new BigDecimal( "3.9812" ), new GeneralContinuedFraction( new BigDecimal( "3.9812" ) ) ),
                Arguments.of( new BigDecimal( "-17.09834" ),
                              new GeneralContinuedFraction( new BigDecimal( "-17.09834" ) ) ),
                Arguments.of( new BigDecimal( "0.00023874" ),
                              new GeneralContinuedFraction( new BigDecimal( "0.00023874" ) ) )
        );
    }

    private static Stream<Arguments> toStringData() {
        return Stream.of(
                Arguments.of( "12.0 = [12]",
                              new GeneralContinuedFraction( 12.0 ) ),
                Arguments.of( "0.0 = [0]",
                              new GeneralContinuedFraction( 0.0 ) ),
                Arguments.of( "0.5 = [0;2]",
                              new GeneralContinuedFraction( 0.5 ) ),
                Arguments.of( "3.75 = [3;1,3]",
                              new GeneralContinuedFraction( 3.75 ) ),
                Arguments.of( 15.0 / 146.0 + " = [0;9,1,2,1,3,...]",
                              new GeneralContinuedFraction( 15.0 / 146.0 ) ),
                Arguments.of( "3.87654 = [3;1,7,10,47,2,...]",
                              new GeneralContinuedFraction( 3.87654 ) ),
                Arguments.of( 13.0 / 15.0 + " = [0;1,6,1,1,133333333333332,...]",
                              new GeneralContinuedFraction( 13.0 / 15.0 ) ),

                //1393/972 = 1.4331275720164609053497942386831 has exactly 6 coefficients: 1, 2, 3, 4, 5, 6
                Arguments.of( "1.4331275720164609053497942386831 = [1;2,3,4,5,6]",
                              new GeneralContinuedFraction( new BigDecimal( "1.4331275720164609053497942386831" ) ) )
        );
    }

    @ParameterizedTest( name = "{index}) Continued fraction {1} has the following coefficients: {0}" )
    @MethodSource( "exactCoefficientsData" )
    public void test_ExactCoefficientsCorrectlyCalculated_WhenContinuedFractionHasRationalValue(
            List<BigInteger> expectedExactCoefficients, GeneralContinuedFraction continuedFraction ) {
        Iterator<BigInteger> coefficientsIterator = continuedFraction.getCoefficients().iterator();

        for( BigInteger expectedCoefficient : expectedExactCoefficients ) {
            assertTrue( coefficientsIterator.hasNext() );
            assertEquals( expectedCoefficient, coefficientsIterator.next() );
        }

        assertFalse( coefficientsIterator.hasNext() );
        assertException( NoSuchElementException.class, () -> coefficientsIterator.next() );
    }

    @ParameterizedTest( name = "{index}) Continued fraction {1} has the following first coefficients: {0} , ..." )
    @MethodSource( "firstCoefficientsData" )
    public void test_FirstCoefficientsCorrectlyCalculated_WhenContinuedFractionHasIrrationalValue(
            List<BigInteger> expectedFirstCoefficients, GeneralContinuedFraction continuedFraction ) {
        Iterator<BigInteger> coefficientsIterator = continuedFraction.getCoefficients().iterator();

        for( BigInteger expectedCoefficient : expectedFirstCoefficients ) {
            assertTrue( coefficientsIterator.hasNext() );
            assertEquals( expectedCoefficient, coefficientsIterator.next() );
        }

        assertTrue( coefficientsIterator.hasNext() );
    }

    @ParameterizedTest( name = "{index}) Continued fraction {1} has the following convergents: {0}" )
    @MethodSource( "exactConvergentsData" )
    public void test_ExactConvergentsCorrectlyCalculated_WhenContinuedFractionHasRationalValue(
            List<BigFraction> expectedExactConvergents, GeneralContinuedFraction continuedFraction ) {
        Iterator<BigFraction> convergentsIterator = continuedFraction.getConvergents().iterator();

        for( BigFraction expectedConvergent : expectedExactConvergents ) {
            assertTrue( convergentsIterator.hasNext() );
            assertEquals( expectedConvergent, convergentsIterator.next() );
        }

        assertFalse( convergentsIterator.hasNext() );
        assertException( NoSuchElementException.class, () -> convergentsIterator.next() );
    }

    @ParameterizedTest( name = "{index}) Continued fraction {1} has the following first convergents: {0}, ..." )
    @MethodSource( "firstConvergentsData" )
    public void test_FirstConvergentsCorrectlyCalculated_WhenContinuedFractionHasIrrationalValue(
            List<BigFraction> expectedFirstConvergents, GeneralContinuedFraction continuedFraction ) {
        Iterator<BigFraction> convergentsIterator = continuedFraction.getConvergents().iterator();

        for( BigFraction expectedConvergent : expectedFirstConvergents ) {
            assertTrue( convergentsIterator.hasNext() );
            assertEquals( expectedConvergent, convergentsIterator.next() );
        }

        assertTrue( convergentsIterator.hasNext() );
    }

    @ParameterizedTest( name = "{index}) Value of continued fraction {1} is equal to {0}" )
    @MethodSource( "getValueData" )
    public void test_GetValue( BigDecimal expectedValue, GeneralContinuedFraction continuedFraction ) {
        int decimalDigits = 10;
        assertEquals( expectedValue.setScale( decimalDigits ), continuedFraction.getValue( decimalDigits ) );
    }

    @ParameterizedTest( name = "{index}) \"{0}\" is the same string as \"{1}\"" )
    @MethodSource( "toStringData" )
    public void test_ToString( String s, GeneralContinuedFraction continuedFraction ) {
        assertEquals( s, continuedFraction.toString() );
    }

    @Test
    public void test_GettingCoefficients_CoefficientsCanBeReused() {
        GeneralContinuedFraction continuedFraction = new GeneralContinuedFraction( 0.123123 );
        Iterable<BigInteger> coefficients = continuedFraction.getCoefficients();

        Iterator<BigInteger> coefficientsIterator1 = coefficients.iterator();
        Iterator<BigInteger> coefficientsIterator2 = coefficients.iterator();

        while( coefficientsIterator1.hasNext() ) {
            assertTrue( coefficientsIterator2.hasNext() );
            assertEquals( coefficientsIterator1.next(), coefficientsIterator2.next() );
        }

        assertFalse( coefficientsIterator1.hasNext() );
        assertFalse( coefficientsIterator2.hasNext() );
    }

    @Test
    public void test_GettingCoefficients_CoefficientsIteratorDoesNotChangeState_WhenHasNextIsInvoked() {
        Iterator<BigInteger> coefficientsIterator = new GeneralContinuedFraction( 0.5 ).getCoefficients().iterator();

        for( int i = 0; i < 10; i++ ) {
            assertTrue( coefficientsIterator.hasNext() );
        }

        while( coefficientsIterator.hasNext() ) {
            coefficientsIterator.next();
        }

        for( int i = 0; i < 10; i++ ) {
            assertFalse( coefficientsIterator.hasNext() );
        }
    }

    @Test
    public void test_GettingConvergents_ConvergentsCanBeReused() {
        GeneralContinuedFraction continuedFraction = new GeneralContinuedFraction( 0.123123 );
        Iterable<BigFraction> convergents = continuedFraction.getConvergents();

        Iterator<BigFraction> convergentsIterator1 = convergents.iterator();
        Iterator<BigFraction> convergentsIterator2 = convergents.iterator();

        while( convergentsIterator1.hasNext() ) {
            assertTrue( convergentsIterator2.hasNext() );
            assertEquals( convergentsIterator1.next(), convergentsIterator2.next() );
        }
    }

    @Test
    public void test_GettingConvergents_ConvergentsIteratorDoesNotChangeState_WhenHasNextIsInvoked() {
        Iterator<BigFraction> convergentsIterator = new GeneralContinuedFraction( 0.5 ).getConvergents().iterator();

        for( int i = 0; i < 10; i++ ) {
            assertTrue( convergentsIterator.hasNext() );
        }

        while( convergentsIterator.hasNext() ) {
            convergentsIterator.next();
        }

        for( int i = 0; i < 10; i++ ) {
            assertFalse( convergentsIterator.hasNext() );
        }
    }

    @Test
    public void test_HashCode() {
        GeneralContinuedFraction fraction = new GeneralContinuedFraction( 4.321 );

        assertEquals( fraction.hashCode(), fraction.hashCode() );
        assertEquals( fraction.hashCode(), new GeneralContinuedFraction( 4.321 ).hashCode() );
        assertNotEquals( fraction.hashCode(), new GeneralContinuedFraction( 4.3211 ).hashCode() );
    }

    @Test
    public void test_Equals() {
        GeneralContinuedFraction fraction = new GeneralContinuedFraction( 0.18 );

        assertTrue( fraction.equals( fraction ) );
        assertFalse( fraction.equals( null ) );
        assertFalse( fraction.equals( new String() ) );

        assertTrue( fraction.equals( new GeneralContinuedFraction( 0.18 ) ) );
        assertFalse( fraction.equals( new GeneralContinuedFraction( 0.19 ) ) );
        assertFalse( fraction.equals( new GeneralContinuedFraction( 0.0 ) ) );
        assertFalse( fraction.equals( new GeneralContinuedFraction( -89.212 ) ) );
    }
}
