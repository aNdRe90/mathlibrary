package number.representation;

import number.representation.big.BigFraction;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static java.lang.Math.sqrt;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static number.representation.PeriodicContinuedFraction.COEFFICIENTS_LIMIT;
import static org.junit.Assert.*;
import static testutils.ExceptionAssert.assertException;

public class PeriodicContinuedFractionTest {
    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                //Modulo < 1
                Arguments.of( IllegalArgumentException.class, 2, -3, 5 ),
                Arguments.of( IllegalArgumentException.class, 0, -1, 6 ),
                Arguments.of( IllegalArgumentException.class, 2, 4, 0 ),
                Arguments.of( IllegalArgumentException.class, 2, 13, 0 )
        );
    }

    private static Stream<Arguments> periodicCoefficientsData() {
        return Stream.of(
                //Purely periodic continued fractions

                //(1 + sqrt(3))/2 = [1; 2, 1, 2, 1, 2, 1, ... ] = [(1;2)]
                Arguments.of( bigIntegerList( 1, 2 ),
                              new PeriodicContinuedFraction( 1, 3, 2 ) ),
                //(10 + sqrt(112))/6 = [3; 2, 3, 10, 3, 2, 3, 10, 3, 2, ... ] = [(3; 2, 3, 10)]
                Arguments.of( bigIntegerList( 3, 2, 3, 10 ),
                              new PeriodicContinuedFraction( 10, 112, 6 ) ),
                //golden ration = (1 + sqrt(5))/2 = [1; 1, 1, 1, 1, 1, 1, ... ] = [(1)]
                Arguments.of( bigIntegerList( 1 ),
                              new PeriodicContinuedFraction( 1, 5, 2 ) ),

                //Continued fractions having both periodic and non-periodic coefficients

                //Positive values
                Arguments.of( bigIntegerList( 5, 10 ),
                              new PeriodicContinuedFraction( 1, 3, 6 ) ),
                Arguments.of( bigIntegerList( 1, 16, 1, 3, 2, 3 ),
                              new PeriodicContinuedFraction( -1, 77, 4 ) ),
                Arguments.of( bigIntegerList( 1, 7, 1, 1, 4, 2, 7, 2, 16, 2, 7, 2, 4, 1, 1, 7, 1, 2, 1, 2 ),
                              new PeriodicContinuedFraction( 93, 335, 62 ) ),
                Arguments.of( bigIntegerList( 1, 1, 15, 1, 1, 3, 7, 13, 1, 4, 1, 2, 1, 2, 1, 1, 28, 1, 24, 1, 16, 2, 1,
                                              1,
                                              1, 1, 5, 1, 1, 1, 2, 3, 4, 3, 16, 1, 4, 4, 11, 1, 1, 2, 9, 2, 1, 1, 3, 1,
                                              2,
                                              1, 5, 1, 2, 1, 6, 1, 1, 1, 4, 3, 3, 55, 1, 1, 4, 1, 1, 2, 3, 1, 1, 1, 4,
                                              1,
                                              13, 3, 1, 7, 1, 2, 1, 98, 2, 3, 2, 17, 1, 5, 1, 1, 1, 2, 17, 1, 5, 1, 1,
                                              1,
                                              2, 35, 2, 10, 1, 1, 8, 1, 3, 2, 1, 1, 13, 2, 22, 1, 19, 2, 1, 2, 1, 5, 2,
                                              10, 1, 6, 2, 1, 1, 6, 5, 1, 5, 8, 2, 7, 13, 1, 10, 1, 9, 2, 16, 1, 1, 1,
                                              3,
                                              1, 25, 1, 5, 3, 1, 1, 4, 1, 1, 9, 2, 1, 3, 2, 2, 1, 5, 3, 13, 1, 4, 1, 2,
                                              1,
                                              6, 1, 1, 1, 1, 1, 1, 1, 1, 5, 1, 2, 31, 3, 25, 1, 1, 2, 1, 2, 25, 21, 5,
                                              2,
                                              1, 2, 1, 1, 1, 1, 7, 2, 21, 5, 3, 1, 5, 1, 1, 2, 4, 1, 1, 1, 5, 1, 4, 1,
                                              30,
                                              1, 5, 1, 2, 2, 8, 1, 1, 1, 1, 5, 2, 2, 2, 1, 2, 1, 4, 1, 5, 4, 1, 3, 2,
                                              39,
                                              5, 14, 1, 2, 26, 3, 1, 1, 67, 1, 9, 2, 2, 2, 13, 1, 1, 1, 2, 1, 2, 1, 1,
                                              3,
                                              1, 4, 1, 3, 2, 8, 1, 6, 2, 2, 2, 1, 2, 1, 5, 2, 80, 1, 4, 1, 6, 1, 2, 1,
                                              1,
                                              1, 5, 5, 2, 7, 1, 1, 1, 6, 1, 8, 10, 1, 1, 2, 72, 1, 1, 25, 1, 3, 1, 1, 1,
                                              1, 3, 1, 2, 24, 2, 3, 2, 1, 1, 2, 4, 1, 2, 1, 2, 2 ),
                              new PeriodicContinuedFraction( 888, 13, 9999 ) ),
                Arguments.of( bigIntegerList( 3, 2, 71, 2, 11, 4, 1, 3, 3, 9, 1, 28, 1, 2, 2, 2, 3, 215, 2, 3, 2, 2, 11,
                                              1, 29, 1, 8, 1, 9, 4, 3, 1, 71, 6, 1, 7, 2, 3, 1, 1, 1, 9, 1, 1, 1, 2, 1,
                                              1,
                                              1, 2, 1, 2, 1, 11, 1, 23, 20, 1, 1, 1, 4, 2, 1, 4, 3, 4, 1, 2, 1, 5, 10,
                                              1,
                                              3, 3, 7, 1, 2, 6, 1, 1, 4, 1, 2, 4, 1, 2, 3, 2, 10, 1, 1, 32, 3, 3, 1, 1,
                                              1,
                                              8, 2, 5, 2, 8, 1, 1, 1, 3, 3, 32, 1, 1, 10, 2, 3, 2, 1, 4, 2, 1, 4, 1, 1,
                                              6,
                                              2, 1, 7, 3, 3, 1, 10, 5, 1, 2, 1, 4, 3, 4, 1, 2, 4, 1, 1, 1, 20, 23, 1,
                                              11,
                                              1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 9, 1, 1, 1, 3, 2, 7, 1, 6, 71, 1, 3, 4,
                                              9,
                                              1, 8, 1, 29, 1, 11, 2, 2, 3, 2, 215, 3, 2, 2, 2, 1, 28, 1, 9, 3, 3, 1, 4,
                                              11, 2, 71, 2, 3, 8, 88, 1, 2, 2, 3, 2, 1, 1, 1, 34, 2, 23, 2, 10, 2, 1, 2,
                                              29, 4, 4, 3, 5, 1, 1, 11, 6, 7, 1, 4, 1, 2, 1, 3, 1, 3, 1, 9, 12, 1, 2, 3,
                                              1, 1, 5, 1, 3, 18, 2, 1, 1, 1, 1, 4, 13, 2, 1, 1, 2, 1, 2, 3, 1, 8, 1, 5,
                                              1,
                                              1, 3, 1, 55, 6, 1, 1, 1, 40, 6, 8, 1, 3, 2, 1, 18, 1, 2, 167, 2, 4, 1, 1,
                                              13, 18, 2, 1, 12, 58, 503, 2, 1, 5, 1, 3, 1, 2, 5, 1, 3, 1, 1, 3, 1, 2,
                                              19,
                                              1510, 19, 2, 1, 3, 1, 1, 3, 1, 5, 2, 1, 3, 1, 5, 1, 2, 503, 58, 12, 1, 2,
                                              18, 13, 1, 1, 4, 2, 167, 2, 1, 18, 1, 2, 3, 1, 8, 6, 40, 1, 1, 1, 6, 55,
                                              1,
                                              3, 1, 1, 5, 1, 8, 1, 3, 2, 1, 2, 1, 1, 2, 13, 4, 1, 1, 1, 1, 2, 18, 3, 1,
                                              5,
                                              1, 1, 3, 2, 1, 12, 9, 1, 3, 1, 3, 1, 2, 1, 4, 1, 7, 6, 11, 1, 1, 5, 3, 4,
                                              4,
                                              29, 2, 1, 2, 10, 2, 23, 2, 34, 1, 1, 1, 2, 3, 2, 2, 1, 88, 8 ),
                              new PeriodicContinuedFraction( 12, 127, 67 ) ),

                //Negative values
                Arguments.of( bigIntegerList( 6, 1, 1 ),
                              new PeriodicContinuedFraction( -3, 2, 5 ) ),
                Arguments.of( bigIntegerList( 1, 1, 1, 2 ),
                              new PeriodicContinuedFraction( -6, 24, 3 ) ),
                Arguments
                        .of( bigIntegerList( 1, 4, 2, 1, 18, 3, 9, 2, 2, 1, 2, 5, 1, 2, 3, 6, 3, 1140, 3, 6, 3, 2, 1, 5,
                                             2, 1, 2, 2, 9, 3, 18, 1, 2, 4, 1, 2, 1, 2, 2, 2, 1, 2, 2, 1, 3, 1, 3, 2, 1,
                                             8, 1, 1, 2, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 1, 2, 1, 1, 8, 1, 2, 3, 1,
                                             3, 1, 2, 2, 1, 2, 2, 2, 1, 2 ),
                             new PeriodicContinuedFraction( 41, 901, -19 ) ),
                Arguments.of( bigIntegerList( 1, 2, 30, 2, 7, 2, 1, 17, 1, 4, 46, 462, 46, 4, 1, 17, 1, 2, 7, 2, 30, 2,
                                              1,
                                              76, 2, 1, 30, 7, 2, 2, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 4, 1, 1, 3, 1, 50, 1,
                                              1, 3, 1, 4, 2, 1, 4, 3, 1, 1, 2, 1, 7, 2, 1, 1, 2, 3, 26, 1, 7, 1, 1, 2,
                                              5,
                                              3, 4, 4, 1, 1, 1, 2, 1, 1, 11, 1, 1, 2, 1, 1, 1, 4, 4, 3, 5, 2, 1, 1, 7,
                                              1,
                                              26, 3, 2, 1, 1, 2, 7, 1, 2, 1, 1, 3, 4, 1, 2, 4, 1, 3, 1, 1, 50, 1, 3, 1,
                                              1,
                                              4, 1, 1, 2, 1, 2, 2, 1, 1, 1, 1, 2, 2, 7, 30, 1, 2, 76 ),
                              new PeriodicContinuedFraction( -5, 19, 53 ) ),
                Arguments
                        .of( bigIntegerList( 5, 18, 7, 33, 10, 3, 1, 8, 2, 3, 12, 2, 1, 3, 1, 2, 1, 1, 1, 1, 2, 6, 1, 2,
                                             8, 1, 2, 10, 3, 2, 1, 1, 5, 1, 2, 1, 1, 1, 1, 4, 1, 25, 2, 3, 1, 45, 10, 4,
                                             1, 2, 2, 1, 1, 1, 2, 1, 5, 2, 2, 1, 2, 4, 1, 4, 1, 1, 4, 4, 1, 1, 2, 1, 74,
                                             4, 1, 1, 1, 9, 5, 5, 2, 1, 1, 3, 8, 2, 1, 1, 9, 1, 2, 4, 8, 44, 3, 23, 6,
                                             6,
                                             1, 8, 1, 1, 1, 6, 1, 2, 3338, 2, 1, 6, 1, 1, 1, 8, 1, 6, 6, 23, 3, 44, 8,
                                             4,
                                             2, 1, 9, 1, 1, 2, 8, 3, 1, 1, 2, 5, 5, 9, 1, 1, 1, 4, 74, 1, 2, 1, 1, 4, 4,
                                             1, 1, 4, 1, 4, 2, 1, 2, 2, 5, 1, 2, 1, 1, 1, 2, 2, 1, 4, 10, 45, 1, 3, 2,
                                             25, 1, 4, 1, 1, 1, 1, 2, 1, 5, 1, 1, 2, 3, 10, 2, 1, 8, 2, 1, 6, 2, 1, 1,
                                             1,
                                             1, 2, 1, 3, 1, 2, 12, 3, 2, 8, 1, 3, 10, 33, 7, 18, 5, 4, 3, 1, 2, 4, 2, 1,
                                             1, 5, 2, 61, 1, 1, 1, 1, 1, 20, 5, 2, 2, 1, 6, 1, 1, 1, 1, 2, 1, 92, 52, 1,
                                             3, 1, 20, 1, 1, 1, 1, 14, 2, 3, 16, 1, 1, 3, 1, 20, 1, 2, 3, 85, 3, 4, 14,
                                             1, 40, 3, 1, 1, 12, 2, 1, 1, 1, 46, 1, 162, 3, 16, 1, 2, 1, 968, 1, 1, 4,
                                             4,
                                             4, 18, 7, 1, 1, 1, 3, 2, 98, 12, 1, 1, 1, 3, 143, 2, 143, 3, 1, 1, 1, 12,
                                             98, 2, 3, 1, 1, 1, 7, 18, 4, 4, 4, 1, 1, 968, 1, 2, 1, 16, 3, 162, 1, 46,
                                             1,
                                             1, 1, 2, 12, 1, 1, 3, 40, 1, 14, 4, 3, 85, 3, 2, 1, 20, 1, 3, 1, 1, 16, 3,
                                             2, 14, 1, 1, 1, 1, 20, 1, 3, 1, 52, 92, 1, 2, 1, 1, 1, 1, 6, 1, 2, 2, 5,
                                             20,
                                             1, 1, 1, 1, 1, 61, 2, 5, 1, 1, 2, 4, 2, 1, 3, 4 ),
                             new PeriodicContinuedFraction( -741, 33570, 82 ) )
        );
    }

    private static List<BigInteger> bigIntegerList( long... values ) {
        List<BigInteger> bigIntList = new ArrayList<>( values.length );

        for( Number value : values ) {
            if( value instanceof BigInteger ) {
                bigIntList.add( (BigInteger) value );
            }
            else {
                bigIntList.add( BigInteger.valueOf( value.longValue() ) );
            }
        }

        return bigIntList;
    }

    private static Stream<Arguments> nonPeriodicCoefficientsData() {
        return Stream.of(
                //Purely periodic continued fractions

                //(1 + sqrt(3))/2 = [1; 2, 1, 2, 1, 2, 1, ... ] = [(1;2)]
                Arguments.of( emptyList(), new PeriodicContinuedFraction( 1, 3, 2 ) ),
                //(10 + sqrt(112))/6 = [3; 2, 3, 10, 3, 2, 3, 10, 3, 2, ... ] = [(3; 2, 3, 10)]
                Arguments.of( emptyList(), new PeriodicContinuedFraction( 10, 112, 6 ) ),
                //golden ration = (1 + sqrt(5))/2 = [1; 1, 1, 1, 1, 1, 1, ... ] = [(1)]
                Arguments.of( emptyList(), new PeriodicContinuedFraction( 1, 5, 2 ) ),

                //Continued fractions having both periodic and non-periodic coefficients

                //Positive values
                Arguments.of( bigIntegerList( 0, 1 ), new PeriodicContinuedFraction( 1, 2, 3 ) ),
                Arguments.of( bigIntegerList( 0, 21 ), new PeriodicContinuedFraction( -2, 5, 5 ) ),
                Arguments.of( bigIntegerList( 0, 1, 4 ), new PeriodicContinuedFraction( 34, 12, 45 ) ),
                Arguments.of( bigIntegerList( 2 ), new PeriodicContinuedFraction( 3, 7, 2 ) ),
                Arguments.of( bigIntegerList( 3 ), new PeriodicContinuedFraction( -79, 987345, 233 ) ),
                Arguments.of( bigIntegerList( 0, 33776 ), new PeriodicContinuedFraction( -39, 2110, 234234 ) ),

                //Negative values
                Arguments.of( bigIntegerList( -1, 1, 4 ), new PeriodicContinuedFraction( -4, 2, 14 ) ),
                Arguments.of( bigIntegerList( -2, 1, 1 ), new PeriodicContinuedFraction( 77, 133, -66 ) ),
                Arguments.of( bigIntegerList( -1, 1, 1, 3 ), new PeriodicContinuedFraction( -41, 13, 87 ) ),
                Arguments.of( bigIntegerList( -4, 1, 1, 12 ), new PeriodicContinuedFraction( 111, 15, -33 ) ),
                Arguments.of( bigIntegerList( -34, 6 ), new PeriodicContinuedFraction( 1113, 15, -33 ) ),
                Arguments.of( bigIntegerList( -1, 1, 12 ), new PeriodicContinuedFraction( -56, 2221, 123 ) )
        );
    }

    private static Stream<Arguments> firstCoefficientsData() {
        return Stream.of(
                Arguments.of( bigIntegerList( 3, 4, 1, 4, 1, 4, 1, 4, 1 ),
                              new PeriodicContinuedFraction( 5, 2, 2 ) ),
                Arguments.of( bigIntegerList( 8, 1, 2, 1, 2, 1, 2, 1, 2, 1 ),
                              new PeriodicContinuedFraction( 7, 3, 1 ) ),
                Arguments.of( bigIntegerList( 0, 2, 3, 1, 2, 9, 11, 2, 3, 1, 2, 9, 11, 2, 3 ),
                              new PeriodicContinuedFraction( -16, 300, 3 ) ),
                Arguments
                        .of( bigIntegerList( 29, 14, 1, 1, 14, 58, 14, 1, 1, 14, 58, 14, 1, 1, 14, 58, 14, 1, 1, 14, 58,
                                             14 ),
                             new PeriodicContinuedFraction( 0, 845, 1 ) ),
                Arguments.of( bigIntegerList( 0, 5, 4, 3, 1, 8, 1, 26, 1, 8, 1, 3, 4, 6, 24, 6, 4, 3, 1, 8, 1,
                                              26, 1, 8, 1, 3, 4, 6, 24 ),
                              new PeriodicContinuedFraction( -5, 56, 13 ) ),
                Arguments.of( bigIntegerList( -4, 4, 2, 1, 1, 3, 2, 1, 1, 3, 2, 1, 1, 3, 2, 1, 1, 3, 2, 1, 1, 3 ),
                              new PeriodicContinuedFraction( 8, 11, -3 ) ),
                Arguments.of( bigIntegerList( 0, 6, 6, 1, 104, 1, 25, 2, 6, 2, 25, 1, 104, 1, 5, 1, 104, 1, 25, 2, 6, 2,
                                              25, 1, 104, 1, 5, 1, 104, 1, 25, 2, 6, 2, 25, 1, 104, 1, 5, 1, 104, 1, 25,
                                              2, 6, 2, 25, 1, 104, 1, 5, 1, 104, 1, 25, 2, 6, 2, 25, 1, 104, 1, 5, 1,
                                              104,
                                              1, 25, 2 ),
                              new PeriodicContinuedFraction( 17, 232, 198 ) ),
                Arguments.of( bigIntegerList( -1, 1, 1, 1, 2, 1, 1, 1, 3, 3, 1, 1, 6, 8, 1, 1, 1, 7, 15,
                                              2, 1, 1, 1, 4, 32, 3, 2, 2, 2, 1, 25, 1, 2, 2, 2, 3, 32, 4, 1, 1, 1, 2,
                                              15,
                                              7, 1, 1, 1, 8, 6, 1, 1, 3, 3, 1, 1, 2, 6, 1, 129, 5, 1, 4, 26, 4, 1, 5,
                                              129,
                                              1, 6, 2, 1, 1, 3, 3, 1, 1, 6, 8, 1, 1, 1, 7, 15 ),
                              new PeriodicContinuedFraction( -93, 12, 244 ) )
        );
    }

    private static Stream<Arguments> nonPeriodicCoefficientsAndNumberOfPeriodicCoefficientsData() {
        return Stream.of(
                Arguments.of( bigIntegerList( 19, 10, 1, 3 ), 14742,
                              new PeriodicContinuedFraction( 235235, 235235, 12346 ) ),
                Arguments.of( bigIntegerList( 0, 6, 8, 1, 2 ), 55626,
                              new PeriodicContinuedFraction( 9209872, 4120928, 56334144 ) ),
                Arguments.of( bigIntegerList( 0, 151299 ), 86760,
                              new PeriodicContinuedFraction( 8, 871, 5675672 ) )
        );
    }

    private static Stream<Arguments> firstConvergentsData() {
        return Stream.of(
                Arguments
                        .of( asList( bigFraction( 4 ), bigFraction( 9, 2 ), bigFraction( 22, 5 ), bigFraction( 53, 12 ),
                                     bigFraction( 128, 29 ), bigFraction( 309, 70 ) ),
                             new PeriodicContinuedFraction( 3, 2, 1 ) ),
                Arguments.of( asList( bigFraction( 6 ), bigFraction( 37, 6 ), bigFraction( 228, 37 ),
                                      bigFraction( 1405, 228 ), bigFraction( 8658, 1405 ) ),
                              new PeriodicContinuedFraction( 3, 10, 1 ) ),
                Arguments.of( asList( bigFraction( 42 ), bigFraction( 43 ), bigFraction( 3052, 71 ),
                                      bigFraction( 3095, 72 ), bigFraction( 269222, 6263 ), bigFraction( 272317, 6335 ),
                                      bigFraction( 813856, 18933 ), bigFraction( 1086173, 25268 ) ),
                              new PeriodicContinuedFraction( -7, 123123, 8 ) ),
                Arguments.of( asList( bigFraction( -2 ), bigFraction( -1 ), bigFraction( -3, 2 ), bigFraction( -10, 7 ),
                                      bigFraction( -23, 16 ), bigFraction( -79, 55 ), bigFraction( -102, 71 ),
                                      bigFraction( -181, 126 ), bigFraction( -464, 323 ), bigFraction( -645, 449 ),
                                      bigFraction( -1109, 772 ) ),
                              new PeriodicContinuedFraction( -9, 22, 3 ) ),
                Arguments.of( asList( BigFraction.ZERO, BigFraction.ONE, bigFraction( 4, 5 ), bigFraction( 17, 21 ),
                                      bigFraction( 72, 89 ), bigFraction( 305, 377 ), bigFraction( 1292, 1597 ) ),
                              new PeriodicContinuedFraction( 1, 5, 4 ) ),
                Arguments.of( asList( bigFraction( 3 ), bigFraction( 4 ), bigFraction( 7, 2 ), bigFraction( 25, 7 ),
                                      bigFraction( 82, 23 ), bigFraction( 271, 76 ), bigFraction( 353, 99 ),
                                      bigFraction( 977, 274 ), bigFraction( 1330, 373 ), bigFraction( 4967, 1393 ) ),
                              new PeriodicContinuedFraction( 15, 8, 5 ) ),
                Arguments.of( asList( BigFraction.ZERO, BigFraction.ONE, bigFraction( 4, 5 ), bigFraction( 5, 6 ),
                                      bigFraction( 14, 17 ), bigFraction( 173, 210 ), bigFraction( 360, 437 ),
                                      bigFraction( 533, 647 ), bigFraction( 1426, 1731 ), bigFraction( 1959, 2378 ),
                                      bigFraction( 48442, 58803 ), bigFraction( 50401, 61181 ),
                                      bigFraction( 149244, 181165 ), bigFraction( 199645, 242346 ),
                                      bigFraction( 548534, 665857 ), bigFraction( 6782053, 8232630 ) ),
                              new PeriodicContinuedFraction( 6, 2, 9 ) ),
                Arguments.of( asList( BigFraction.ZERO, bigFraction( 1, 49 ), bigFraction( 2, 99 ),
                                      bigFraction( 11, 544 ), bigFraction( 13, 643 ), bigFraction( 63, 3116 ),
                                      bigFraction( 76, 3759 ), bigFraction( 443, 21911 ), bigFraction( 962, 47581 ),
                                      bigFraction( 54315, 2686447 ), bigFraction( 109592, 5420475 ),
                                      bigFraction( 602275, 29788822 ), bigFraction( 711867, 35209297 ),
                                      bigFraction( 3449743, 170626010 ), bigFraction( 4161610, 205835307 ),
                                      bigFraction( 24257793, 1199802545 ) ),
                              new PeriodicContinuedFraction( -7, 90, 123 )
                )
        );
    }

    private static BigFraction bigFraction( long numerator ) {
        return bigFraction( numerator, 1 );
    }

    private static BigFraction bigFraction( long numerator, long denominator ) {
        return new BigFraction( BigInteger.valueOf( numerator ), BigInteger.valueOf( denominator ) );
    }

    private static Stream<Arguments> getValueData() {
        return Stream.of(
                Arguments.of( new BigDecimal( ( 1.0 + sqrt( 10.0 ) ) / 2.0 ),
                              new PeriodicContinuedFraction( 1, 10, 2 ) ),
                Arguments.of( new BigDecimal( sqrt( 87.0 ) / 3.0 ),
                              new PeriodicContinuedFraction( 0, 87, 3 ) ),
                Arguments.of( new BigDecimal( 14.0 + sqrt( 53.0 ) ),
                              new PeriodicContinuedFraction( 14, 53, 1 ) ),
                Arguments.of( new BigDecimal( sqrt( 99.0 ) ),
                              new PeriodicContinuedFraction( 0, 99, 1 ) )
        );
    }

    private static Stream<Arguments> toStringData() {
        return Stream.of(
                Arguments.of( "(23 + sqrt(62))/91 = [0;2,1,18,(29,5,7,5,3,4,2,3,5,3,2,48,1,61,3,19)]",
                              new PeriodicContinuedFraction( 23, 62, 91 ) ),
                Arguments.of( "sqrt(122)/55 = [0;4,(1,47,1,2,3,48,3,2,1,47,1,8)]",
                              new PeriodicContinuedFraction( 0, 122, 55 ) ),
                Arguments.of( "sqrt(6) = [2;(2,4)]",
                              new PeriodicContinuedFraction( 0, 6, 1 ) ),
                Arguments.of( "4 + sqrt(7) = [6;(1,1,1,4)]",
                              new PeriodicContinuedFraction( 4, 7, 1 ) ),
                Arguments.of( "(1 + sqrt(3))/2 = [(1;2)]",
                              new PeriodicContinuedFraction( 1, 3, 2 ) ),
                Arguments.of( "(1 + sqrt(5))/2 = [(1)]",
                              new PeriodicContinuedFraction( 1, 5, 2 ) ),
                Arguments.of( "(10 + sqrt(112))/6 = [(3;2,3,10)]",
                              new PeriodicContinuedFraction( 10, 112, 6 ) )
        );
    }

    @ParameterizedTest(
            name = "{index}) Cannot create periodic continued fraction represeting a value ({1} + sqrt({2}))/{3}, " +
                   "raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long p, long d, long q ) {
        assertException( throwableClass, () -> new PeriodicContinuedFraction( p, d, q ) );
    }

    @ParameterizedTest( name = "{index}) Continued fraction {1} has periodic coefficients = {0}" )
    @MethodSource( "periodicCoefficientsData" )
    public void test_CorrectPeriodicCoefficients( List<BigInteger> expectedPeriodicCoefficients,
            PeriodicContinuedFraction continuedFraction ) {
        assertEquals( expectedPeriodicCoefficients, continuedFraction.getPeriodicCoefficients() );
    }

    @ParameterizedTest( name = "{index}) Continued fraction {1} has non-periodic coefficients = {0}" )
    @MethodSource( "nonPeriodicCoefficientsData" )
    public void test_CorrectNonPeriodicCoefficients( List<BigInteger> expectedNonPeriodicCoefficients,
            PeriodicContinuedFraction continuedFraction ) {
        assertEquals( expectedNonPeriodicCoefficients, continuedFraction.getNonPeriodicCoefficients() );
    }

    @ParameterizedTest( name = "{index}) First coefficients of continued fraction {1} are {0}" )
    @MethodSource( "firstCoefficientsData" )
    public void test_CorrectFirstCoefficients(
            List<BigInteger> expectedFirstCoefficients, PeriodicContinuedFraction continuedFraction ) {
        Iterator<BigInteger> coefficientsIterator = continuedFraction.getCoefficients().iterator();

        for( BigInteger expectedCoefficient : expectedFirstCoefficients ) {
            assertEquals( expectedCoefficient, coefficientsIterator.next() );
        }

        assertTrue( coefficientsIterator.hasNext() );
    }

    @ParameterizedTest(
            name = "{index}) Continued fraction {1} has non-periodic coefficients = {0} and {1} periodic coefficients" )
    @MethodSource( "nonPeriodicCoefficientsAndNumberOfPeriodicCoefficientsData" )
    public void test_CorrectNonPeriodicCoefficients_AndNumberOfPeriodicCoefficients(
            List<BigInteger> expectedNonPeriodicCoefficients, int expectedNumberOfPeriodicCoefficients,
            PeriodicContinuedFraction continuedFraction ) {
        assertEquals( expectedNonPeriodicCoefficients, continuedFraction.getNonPeriodicCoefficients() );
        assertEquals( expectedNumberOfPeriodicCoefficients, continuedFraction.getPeriodicCoefficients().size() );
    }

    @ParameterizedTest( name = "{index}) First convergents of continued fraction {1} are {0}" )
    @MethodSource( "firstConvergentsData" )
    public void test_CorrectFirstConvergents( List<BigFraction> expectedFirstConvergents,
            PeriodicContinuedFraction continuedFraction ) {
        Iterator<BigFraction> convergentsIterator = continuedFraction.getConvergents().iterator();

        for( BigFraction expectedConvergent : expectedFirstConvergents ) {
            assertEquals( expectedConvergent, convergentsIterator.next() );
        }

        assertTrue( convergentsIterator.hasNext() );
    }

    @ParameterizedTest( name = "{index}) Value of continued fraction {1} is equal to {0}" )
    @MethodSource( "getValueData" )
    public void test_GetValue( BigDecimal expectedValue, PeriodicContinuedFraction continuedFraction ) {
        int decimalDigits = 10;
        assertEquals( expectedValue.setScale( decimalDigits, RoundingMode.HALF_UP ),
                      continuedFraction.getValue( decimalDigits ) );
    }

    @ParameterizedTest( name = "{index}) \"{0}\" is the same string as \"{1}\"" )
    @MethodSource( "toStringData" )
    public void test_ToString( String s, PeriodicContinuedFraction continuedFraction ) {
        assertEquals( s, continuedFraction.toString() );
    }

    @Test
    public void test_GettingCoefficients_CoefficientsCanBeReused() {
        PeriodicContinuedFraction continuedFraction = new PeriodicContinuedFraction( 1, 5, 4 );
        Iterable<BigInteger> coefficients = continuedFraction.getCoefficients();

        Iterator<BigInteger> coefficientsIterator1 = coefficients.iterator();
        Iterator<BigInteger> coefficientsIterator2 = coefficients.iterator();

        for( int i = 0; i < 3; i++ ) {
            assertEquals( coefficientsIterator1.hasNext(), coefficientsIterator2.hasNext() );
            assertEquals( coefficientsIterator1.next(), coefficientsIterator2.next() );
        }
    }

    @Test
    public void test_Convergents_ConvergentsCanBeReused() {
        PeriodicContinuedFraction continuedFraction = new PeriodicContinuedFraction( 1, 7, 11 );
        Iterable<BigFraction> convergents = continuedFraction.getConvergents();

        Iterator<BigFraction> convergentsIterator1 = convergents.iterator();
        Iterator<BigFraction> convergentsIterator2 = convergents.iterator();

        for( int i = 0; i < 3; i++ ) {
            assertEquals( convergentsIterator1.hasNext(), convergentsIterator2.hasNext() );
            assertEquals( convergentsIterator1.next(), convergentsIterator2.next() );
        }
    }

    @Test
    public void test_CheckingContinuedFraction_WhenCoefficientsLimitIsExceeded() {
        //Couple of checks in one test because instantiating such a continued fraction takes a long time
        PeriodicContinuedFraction continuedFraction = new PeriodicContinuedFraction( 1, 2, 8029342 );

        //Making sure that the calculated coefficients are correct
        assertEquals( COEFFICIENTS_LIMIT, continuedFraction.getNonPeriodicCoefficients().size() );
        assertNull( continuedFraction.getPeriodicCoefficients() );

        //Checking the string representation
        assertTrue(
                continuedFraction.toString().startsWith( "(1 + sqrt(2))/8029342 = [0;3325862,2,1,4,1,8,31,6,1,1,1" ) );

        //Checking the getCoefficients() method
        Iterator<BigInteger> coefficientsIterator = continuedFraction.getCoefficients().iterator();
        for( int i = 0; i < COEFFICIENTS_LIMIT; i++ ) {
            assertTrue( coefficientsIterator.hasNext() );
            coefficientsIterator.next();
        }

        assertFalse( coefficientsIterator.hasNext() );
        assertException( NoSuchElementException.class, () -> coefficientsIterator.next() );

        //Checking the getConvergents() method takes too long of a time
    }

    @Test
    public void test_HashCode() {
        PeriodicContinuedFraction fraction = new PeriodicContinuedFraction( 1, 2, 3 );

        assertEquals( fraction.hashCode(), fraction.hashCode() );
        assertEquals( fraction.hashCode(), new PeriodicContinuedFraction( 1, 2, 3 ).hashCode() );
        assertNotEquals( fraction.hashCode(), new PeriodicContinuedFraction( 2, 3, 1 ).hashCode() );
    }

    @Test
    public void test_Equals() {
        PeriodicContinuedFraction fraction = new PeriodicContinuedFraction( 2, 3, 4 );

        assertTrue( fraction.equals( fraction ) );
        assertFalse( fraction.equals( null ) );
        assertFalse( fraction.equals( new String() ) );

        assertTrue( fraction.equals( new PeriodicContinuedFraction( 2, 3, 4 ) ) );
        assertFalse( fraction.equals( new PeriodicContinuedFraction( 1, 2, 3 ) ) );
        assertFalse( fraction.equals( new PeriodicContinuedFraction( 2, 2, 3 ) ) );
        assertFalse( fraction.equals( new PeriodicContinuedFraction( 2, 3, 5 ) ) );
    }
}
