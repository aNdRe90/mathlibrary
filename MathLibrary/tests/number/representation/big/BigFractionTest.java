package number.representation.big;

import number.representation.Fraction;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.stream.Stream;

import static org.junit.Assert.*;
import static testutils.ExceptionAssert.assertException;

public class BigFractionTest {
    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                //Zero in denominator
                Arguments.of( ArithmeticException.class, "4259872341029382382394234234", "0" ),
                Arguments.of( ArithmeticException.class, "0", "0" ),
                Arguments.of( ArithmeticException.class, "-823684217612938467587236472", "0" )
        );
    }

    private static Stream<Arguments> reverseFractionsData() {
        return Stream.of(
                Arguments.of( bigFraction( "238172300012398673123", "823719238236456123123" ),
                              bigFraction( "823719238236456123123", "238172300012398673123" ) ),
                Arguments.of( bigFraction( "-723487236495696743567", "23498671233453452134234234234" ),
                              bigFraction( "-23498671233453452134234234234", "723487236495696743567" ) ),
                Arguments.of( bigFraction( "-4352379464356972323421", "2663254712382374687234234" ),
                              bigFraction( "2663254712382374687234234", "-4352379464356972323421" ) ),
                Arguments.of( bigFraction( "126723674623756732542234", "24056938409034950923084123" ),
                              bigFraction( "-24056938409034950923084123", "-126723674623756732542234" ) )
        );
    }

    private static BigFraction bigFraction( String numerator, String denominator ) {
        return new BigFraction( new BigInteger( numerator ), new BigInteger( denominator ) );
    }

    private static Stream<Arguments> addFractionsData() {
        return Stream.of(
                //Includes zero
                Arguments.of( bigFraction( "5345345", "1239129387823423423423" ),
                              BigFraction.ZERO,
                              bigFraction( "5345345", "1239129387823423423423" ) ),
                Arguments.of( bigFraction( "33452123134245132", "65234544562" ),
                              bigFraction( "33452123134245132", "65234544562" ),
                              BigFraction.ZERO ),
                Arguments.of( BigFraction.ZERO,
                              BigFraction.ZERO,
                              BigFraction.ZERO ),

                //Integer values
                Arguments.of( bigFraction( "1939578825866890029256972468468" ),
                              bigFraction( "1938768723476887638764234234234" ),
                              bigFraction( "810102390002390492738234234" ) ),

                //Same denominator
                Arguments.of( bigFraction( "2291234384497716465465" ),
                              bigFraction( "2291230897234234123123" ),
                              bigFraction( "3487263482342342" ) ),
                Arguments.of( bigFraction( "12905971856898312088734784529895957358244",
                                           "1515877601934119314907988917476034596" ),
                              bigFraction( "9999999923471726375623", "1231209812312312314" ),
                              bigFraction( "482349817276234123123", "1231209812312312314" ) ),
                Arguments.of( bigFraction( "45335397195831853657513688839781415863034",
                                           "191502861071239121707551436062022858837698549441749789566756" ),
                              bigFraction( "-53767234234", "437610398723841023978143234234" ),
                              bigFraction( "157364857435", "437610398723841023978143234234" ) ),
                Arguments.of( bigFraction( "-150740861880392368441205988373047326198217", "108930113261458129" ),
                              bigFraction( "873498783412312345345345", "330045623" ),
                              bigFraction( "-456727348172367123324234123423424", "330045623" ) ),
                Arguments.of( bigFraction( "-4984463733030185748157491831623896672332538",
                                           "827780136997727760138464842750618756" ),
                              bigFraction( "-33212039345972634234", "909824234123123234" ),
                              bigFraction( "-5478457628374623741923123", "909824234123123234" ) ),

                //Different denominators
                Arguments.of( bigFraction( "11150789396276646873432710969298", "32327080435352389326872022756" ),
                              bigFraction( "723847623478123123", "8347567345876234" ),
                              bigFraction( "1000002934823874", "3872634876234" ) ),
                Arguments.of( bigFraction( "6882771306354814939735559092584883625152",
                                           "6025959746073297294672104190350962469315982" ),
                              bigFraction( "-87368234", "728347182376126872634" ),
                              bigFraction( "9449849568475863451", "8273471624362545123123" ) ),
                Arguments.of( bigFraction( "6282861548989009480046557602948786400750196051373",
                                           "1867274760962143227578759211782" ),
                              bigFraction( "77345665263421231233412123", "22987234" ),
                              bigFraction( "-348767123234234", "81230945879010203123123" ) ),
                Arguments.of( bigFraction( "-308366733127436117152679693466406192632682",
                                           "89092516411270794560134693741593977052933586319653" ),
                              bigFraction( "-8987176123", "987340203481726123123123" ),
                              bigFraction( "-312319827637263423", "90234871523612312311111111" ) )
        );
    }

    private static BigFraction bigFraction( String numerator ) {
        return new BigFraction( new BigInteger( numerator ) );
    }

    private static Stream<Arguments> subtractFractionsData() {
        return Stream.of(
                //Includes zero
                Arguments.of( bigFraction( "8123876765236562", "1123876662366263646123123" ),
                              bigFraction( "8123876765236562", "1123876662366263646123123" ),
                              BigFraction.ZERO ),
                Arguments.of( bigFraction( "-701923972836478234234", "119812823040234234234" ),
                              BigFraction.ZERO,
                              bigFraction( "701923972836478234234", "119812823040234234234" ) ),
                Arguments.of( BigFraction.ZERO,
                              BigFraction.ZERO,
                              BigFraction.ZERO ),

                //Integer values
                Arguments.of( bigFraction( "81759640650414312201893" ),
                              bigFraction( "87249879123314432512123" ),
                              bigFraction( "5490238472900120310230" ) ),

                //Same denominator
                Arguments.of( bigFraction( "-76825988510864295075439444488000000", "97463278475254845239331235713129" ),
                              bigFraction( "438723467123123", "9872349187263123" ),
                              bigFraction( "7782374618723123123", "9872349187263123" ) ),
                Arguments.of( bigFraction( "-47795953881776115574892038741", "9649944538709329575361" ),
                              bigFraction( "-87812938172831111", "98234131231" ),
                              bigFraction( "398738459827348100", "98234131231" ) ),
                Arguments.of( bigFraction( "504737226966756537342083656213474058",
                                           "37544202340240504913562241715166846529" ),
                              bigFraction( "82374671283767123", "6127332400012301023" ),
                              bigFraction( "-38298781923", "6127332400012301023" ) ),
                Arguments.of( bigFraction( "-10999666124984187255399489709103750", "161885375467764072956409765625" ),
                              bigFraction( "-38273487612737123234", "402349817283125" ),
                              bigFraction( "-10934923874198237812", "402349817283125" ) ),

                //Different denominators
                Arguments.of( bigFraction( "3918750476788736936718941576657373", "60472714862309465611938991833730" ),
                              bigFraction( "349847892348768762", "5398723491209845" ),
                              bigFraction( "54345234123", "11201298781234234" ) ),
                Arguments.of( bigFraction( "-129794476848559142861105420687592425",
                                           "519808614064597523362285513995632553" ),
                              bigFraction( "-982342346152313", "2341233455345123123" ),
                              bigFraction( "55345345234234234", "222023400903423411" ) ),
                Arguments.of( bigFraction( "6648914690105214908447457017918848",
                                           "466274854353540709210860760376896396028" ),
                              bigFraction( "94831123123", "1101239234019232342" ),
                              bigFraction( "-6001204927394123", "423409228394234234234" ) ),
                Arguments.of( bigFraction( "-483261065315276836691112907278849562", "10012142958303476490787672070" ),
                              bigFraction( "-4309823498678234", "89290390" ),
                              bigFraction( "-701293719273912", "112130129102398102313" ) )
        );
    }

    private static Stream<Arguments> multiplyFractionsData() {
        return Stream.of(
                //Includes zero
                Arguments.of( BigFraction.ZERO,
                              bigFraction( "812371", "1231523512546735167235647123" ),
                              BigFraction.ZERO ),
                Arguments.of( BigFraction.ZERO,
                              BigFraction.ZERO,
                              bigFraction( "1238447234234234234", "612023455000234010230123" ) ),
                Arguments.of( BigFraction.ZERO,
                              BigFraction.ZERO,
                              BigFraction.ZERO ),

                //Integer values
                Arguments.of( bigFraction( "121597105325704204678337605729" ),
                              bigFraction( "423412321231223" ),
                              bigFraction( "287183672341223" ) ),

                //Same denominator
                Arguments.of( bigFraction( "242040121982786008545455062608", "72085395230328946465221324964" ),
                              bigFraction( "3451314435234", "268487234762342" ),
                              bigFraction( "70129837928364712", "268487234762342" ) ),
                Arguments.of( bigFraction( "-29391755754506801737683736332", "3620796915948488144906801075361" ),
                              bigFraction( "-238765163123412", "1902839172381231" ),
                              bigFraction( "123099012309911", "1902839172381231" ) ),
                Arguments.of( bigFraction( "-26016404214083531556451434616901", "160993440598738356259393273129" ),
                              bigFraction( "123962312378236487", "401239879123123" ),
                              bigFraction( "-209873498767123", "401239879123123" ) ),
                Arguments.of( bigFraction( "3157067886175656073256557326405", "65592380642502708802160721" ),
                              bigFraction( "-378190868234235", "8098912312311" ),
                              bigFraction( "-8347816278367423", "8098912312311" ) ),

                //Different denominators
                Arguments.of( bigFraction( "60112951155423275536072", "50013514998536417726013" ),
                              bigFraction( "8236516231", "9826371231" ),
                              bigFraction( "7298346712312", "5089723746723" ) ),
                Arguments.of( bigFraction( "-1894498022915428985185774", "51178200102907498336203653" ),
                              bigFraction( "-61263455234", "6382375667123" ),
                              bigFraction( "30923786712311", "8018675611111" ) ),
                Arguments.of( bigFraction( "-1390040183506510293496452252", "468280626570201236902976150505" ),
                              bigFraction( "2092394867123", "93987687123" ),
                              bigFraction( "-664329761723124", "4982361423123124435" ) ),
                Arguments.of( bigFraction( "13803016518567539764717139748", "7519035393940917389523555267789" ),
                              bigFraction( "-23480916723", "5409378234234123" ),
                              bigFraction( "-587839762876345676", "1389999934993543" ) )
        );
    }

    private static Stream<Arguments> divideFractionsData() {
        return Stream.of(
                //Includes zero
                Arguments.of( BigFraction.ZERO,
                              BigFraction.ZERO,
                              bigFraction( "237496438275687345234" ) ),

                //Integer values
                Arguments.of( bigFraction( "534523423423", "912308273459876123786234235" ),
                              bigFraction( "534523423423" ),
                              bigFraction( "912308273459876123786234235" ) ),

                //Same denominator
                Arguments.of( bigFraction( "59033696605734354408142117856704", "68552599265393633085567704186592" ),
                              bigFraction( "7091238976237234", "8324877613567456" ),
                              bigFraction( "8234667516753657", "8324877613567456" ) ),
                Arguments.of( bigFraction( "-4390405237881002439365418179476", "9455410415663629892333557476" ),
                              bigFraction( "-1092347986172357234", "4019236812314" ),
                              bigFraction( "2352538767234234", "4019236812314" ) ),
                Arguments.of( bigFraction( "-593490049158840937560120918150", "1051830079382716835996727018150" ),
                              bigFraction( "28349751651234", "20934576657323475" ),
                              bigFraction( "-50243675647234", "20934576657323475" ) ),
                Arguments.of( bigFraction( "5908117591456593941346349706429", "21389076914317903669765613203289" ),
                              bigFraction( "-77293849617523", "76437098432695823" ),
                              bigFraction( "-279825861432343", "76437098432695823" ) ),

                //Different denominators
                Arguments.of( bigFraction( "6987386189903928313893613874628", "14151616998427626145441728330" ),
                              bigFraction( "45675679868975436", "2094787236745" ),
                              bigFraction( "6755634534234234", "152978263486123" ) ),
                Arguments.of( bigFraction( "-15228289847595405461655352035", "97127551862820258403086308470" ),
                              bigFraction( "-1235674576457745", "98283487126823455" ),
                              bigFraction( "988238764234", "12323867576243" ) ),
                Arguments.of( bigFraction( "-432807466477424315496430425", "4902911836528783312813698900" ),
                              bigFraction( "78345987637845", "4459687698216348" ),
                              bigFraction( "-1099384568675", "5524309278965" ) ),
                Arguments.of( bigFraction( "169302000528527229005818", "105501639878446729109818415790" ),
                              bigFraction( "-4898263478234", "189782435676435" ),
                              bigFraction( "-555908345798234", "34563677777" ) )
        );
    }

    private static Stream<Arguments> powerFractionsData() {
        return Stream.of(
                //Powers of zero
                Arguments.of( BigFraction.ZERO,
                              BigFraction.ZERO,
                              1 ),
                Arguments.of( BigFraction.ZERO,
                              BigFraction.ZERO,
                              4 ),

                //Powers of +-1
                Arguments.of( BigFraction.ONE,
                              BigFraction.ONE,
                              11 ),
                Arguments.of( BigFraction.ONE,
                              BigFraction.ONE,
                              40 ),
                Arguments.of( bigFraction( "-1" ),
                              bigFraction( "-1" ),
                              13 ),
                Arguments.of( BigFraction.ONE,
                              bigFraction( "-1" ),
                              24 ),

                //Integer values
                Arguments.of( bigFraction(
                        "153899339938802847342210814700727379821510132674077624739758935683724239067693371" ),
                              bigFraction( "11" ),
                              77 ),

                //Exponent = 0
                Arguments.of( BigFraction.ONE,
                              bigFraction( "5532142354325", "99435982347617234" ),
                              0 ),
                Arguments.of( BigFraction.ONE,
                              bigFraction( "-5982634792341234543255", "8124392391826734" ),
                              0 ),
                Arguments.of( BigFraction.ONE,
                              bigFraction( "4243412234", "-525942987279834762387644" ),
                              0 ),
                Arguments.of( BigFraction.ONE,
                              bigFraction( "-24019473428768734", "-12303458796788787" ),
                              0 ),

                //Positive exponents
                Arguments.of( bigFraction( "48873677980689257489322752273774603865660850176",
                                           "585709328057096652672532224032955891605231761913348157820228102782976" ),
                              bigFraction( "6", "14" ),
                              60 ),
                Arguments.of( bigFraction( "5391030899743293631239539488528815119194426882613553319203",
                                           "3761581922631320025499956919111186169019729781670680068828005460090935230255126953125" ),
                              bigFraction( "3", "5" ),
                              121 ),

                Arguments.of( bigFraction(
                        "-904625697166532776746648320380374280103671755200316906558262375061821325312",
                        "41264301843800314543614325335974127508785598792350355289680619019776486311457119882106781005859375" ),
                              bigFraction( "-8", "15" ),
                              83 ),
                Arguments.of( bigFraction(
                        "2348542582773833227889480596789337027375682548908319870707290971532209025114608443463698998384768703031934976",
                        "131958595297047047613619485386963581036568211862815932772294629684698391367129360542894689994553947877251074157052481" ),
                              bigFraction( "32", "-41" ),
                              72 ),

                //Negative exponents
                Arguments.of( bigFraction(
                        "2596148429267413814265248164610048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                        "4443366189418546455356123568209062184833735738464729529085598664937297538206181887949649031784926231934960580287550545372037" ),
                              bigFraction( "13", "20" ),
                              -111 ),
                Arguments.of( bigFraction(
                        "884467056134897769177403028352204277531894146980966017349124212298943513062971345024451521058208349266478534208013045806724329989399740544099720742247653959918481338241",
                        "4205673275401404175166710417735035247499489682900933884871444658423398634704164322270089851852073082097860199586119819851506236827747844794351616" ),
                              bigFraction( "44", "81" ),
                              -88 ),
                Arguments.of( bigFraction(
                        "-16405899233741778361040045153096356207472096812507433541764131470686904904875680038576951706389130734028806769544716612204297715712",
                        "1494236912846185452088061826700837191902371265444315864511557082971899640911175229815885685929368287894101295104" ),
                              bigFraction( "-14", "22" ),
                              -97 ),
                Arguments.of( bigFraction(
                        "4474829878349144208999641276263521807023017567422608543949552633973642629628012698710524290126785855201146146883541931149148536013546558582716080284284892171837449455706085596168402170161208797290308528983582281313147177570499479770660400390625",
                        "18316774253714359260286652104797592498875155815140577826476924073024514250498449856229541717239796857646419378217258155071767922120354084555470472992660057764443072350571201" ),
                              bigFraction( "17", "-55" ),
                              -140 )
        );
    }

    private static Stream<Arguments> reduceFractionsData() {
        return Stream.of(
                //Integer values
                Arguments.of( bigFraction( "92349816235617623123" ),
                              bigFraction( "92349816235617623123" ) ),

                //Already in the reduced form
                Arguments.of( bigFraction( "2539812673486726384123", "349234823439819829334" ),
                              bigFraction( "2539812673486726384123", "349234823439819829334" ) ),
                Arguments.of( bigFraction( "-700102302349172302", "1121392358923987617" ),
                              bigFraction( "-700102302349172302", "1121392358923987617" ) ),

                //Not in the reduced form
                Arguments.of( bigFraction( "23423498167273", "429384278364" ),
                              bigFraction( "205260114439813299", "3762694431303732" ) ),
                Arguments.of( bigFraction( "4193867821", "4423871788812312313" ),
                              bigFraction( "89052589311114", "93936493563640639654242" ) ),
                Arguments.of( bigFraction( "8012309894123", "7713771267371" ),
                              bigFraction( "34356784825999424", "33076651194486848" ) ),

                Arguments.of( bigFraction( "-48208947", "1123094" ),
                              bigFraction( "2810068130482966731", "-65464417983173062" ) ),
                Arguments.of( bigFraction( "-29812948123", "64356009239828934" ),
                              bigFraction( "-506820118091", "1094052157077091878" ) ),
                Arguments.of( bigFraction( "5539874897", "234141" ),
                              bigFraction( "-23815821296223336749943331", "-1006567895087077142343" ) )
        );
    }

    private static Stream<Arguments> signumOfFractionsData() {
        return Stream.of(
                //Integer values
                Arguments.of( 1,
                              bigFraction( "598792983467768565745" ) ),
                Arguments.of( 0,
                              bigFraction( "0" ) ),
                Arguments.of( 0,
                              BigFraction.ZERO ),
                Arguments.of( -1,
                              bigFraction( "-4129197862736712637123" ) ),

                //Rational values
                Arguments.of( 1,
                              bigFraction( "59828493467172983235", "9843589234872437686782" ) ),
                Arguments.of( 1,
                              bigFraction( "-42823756123", "-5543291276846785" ) ),
                Arguments.of( 0,
                              bigFraction( "0", "453676745678236848716" ) ),
                Arguments.of( -1,
                              bigFraction( "-69384729672934234234", "5776637848753877828342" ) ),
                Arguments.of( -1,
                              bigFraction( "1", "-65443897762835672686234657" ) )
        );
    }

    private static Stream<Arguments> floorOfFractionsData() {
        return Stream.of(
                //Integer values
                Arguments.of( new BigInteger( "-564878347887234234234" ),
                              bigFraction( "-564878347887234234234" ) ),
                Arguments.of( new BigInteger( "-1" ),
                              bigFraction( "-1" ) ),
                Arguments.of( new BigInteger( "0" ),
                              BigFraction.ZERO ),
                Arguments.of( BigInteger.ONE,
                              BigFraction.ONE ),
                Arguments.of( new BigInteger( "9983892903863475776234234" ),
                              bigFraction( "9983892903863475776234234" ) ),

                //Rational values
                Arguments.of( new BigInteger( "-211041045" ),
                              bigFraction( "-9283478923412345345222", "43988973547878" ) ),
                Arguments.of( new BigInteger( "-650703" ),
                              bigFraction( "210987234123578568", "-324245346234" ) ),
                Arguments.of( new BigInteger( "-1" ),
                              bigFraction( "-11153452345", "88457839882374823425" ) ),
                Arguments.of( BigInteger.ZERO,
                              bigFraction( "583467786872342342", "45698456892938792834982" ) ),
                Arguments.of( new BigInteger( "601563518" ),
                              bigFraction( "675766278764876876123124", "1123349834524234" ) ),
                Arguments.of( new BigInteger( "9" ),
                              bigFraction( "78679675877268276834", "8298287963267235123" ) )
        );
    }

    private static Stream<Arguments> ceilOfFractionsData() {
        return Stream.of(
                //Integer values
                Arguments.of( new BigInteger( "-45398237647627312334111" ),
                              bigFraction( "-45398237647627312334111" ) ),
                Arguments.of( new BigInteger( "-1" ),
                              bigFraction( "-1" ) ),
                Arguments.of( new BigInteger( "0" ),
                              BigFraction.ZERO ),
                Arguments.of( BigInteger.ONE,
                              BigFraction.ONE ),
                Arguments.of( new BigInteger( "54730952893976349234234" ),
                              bigFraction( "54730952893976349234234" ) ),

                //Rational values
                Arguments.of( new BigInteger( "-1339206047" ),
                              bigFraction( "-789987234234356754456345", "589892224325345" ) ),
                Arguments.of( new BigInteger( "-35015920191535" ),
                              bigFraction( "343989208940981904072803754235", "-9823794635679345" ) ),
                Arguments.of( new BigInteger( "-1" ),
                              bigFraction( "14398821123112312", "-14398821123112212" ) ),
                Arguments.of( BigInteger.ZERO,
                              bigFraction( "-4597638762734234554", "8129872996716926984" ) ),
                Arguments.of( BigInteger.ONE,
                              bigFraction( "22987614872525235", "45603490432098578234" ) ),
                Arguments.of( new BigInteger( "7267688046281509507" ),
                              bigFraction( "18346287487234120034503450345", "2524363645" ) )
        );
    }

    private static Stream<Arguments> properFractionsData() {
        return Stream.of(
                Arguments.of( bigFraction( "5412323234312312", "9549838457872634762" ) ),
                Arguments.of( bigFraction( "-23478197862384523253", "-82347273618625654235235352" ) )
        );
    }

    private static Stream<Arguments> notProperFractionsData() {
        return Stream.of(
                Arguments.of( bigFraction( "-3286727836472", "93474387587696723477234" ) ),
                Arguments.of( bigFraction( "2382893697", "-21762615241134142365" ) ),
                Arguments.of( bigFraction( "54357236726374235342344234", "15349838497673654" ) ),
                Arguments.of( bigFraction( "3459348596723467672", "3459348596723467672" ) ),
                Arguments.of( bigFraction( "-3427364726374823874525634", "-224323988927887234" ) )
        );
    }

    private static Stream<Arguments> reducedFractionsData() {
        return Stream.of(
                //Values = 0
                Arguments.of( BigFraction.ZERO ),
                Arguments.of( bigFraction( "0", "43877634862568234" ) ),
                Arguments.of( bigFraction( "0", "-23494326756872354682" ) ),

                //Values != 0
                Arguments.of( bigFraction( "4353745682736487234211", "984573984598348953" ) ),
                Arguments.of( bigFraction( "-35488934677823674234", "2122903873459879834531" ) ),
                Arguments.of( bigFraction( "3402987287892235", "-45456034950394985345234" ) ),
                Arguments.of( bigFraction( "-234971628972422344", "-665709506989084564563" ) ),
                Arguments.of( BigFraction.ONE )
        );
    }

    private static Stream<Arguments> notReducedFractionsData() {
        return Stream.of(
                Arguments.of( bigFraction( "5439887239782342342340", "2354988923468792397866975" ) ),
                Arguments.of( bigFraction( "-23982634871562731236", "463894538673292343246340" ) ),
                Arguments.of( bigFraction( "2342343121131231231", "-23423431211312312310000" ) ),
                Arguments.of( bigFraction( "-435289023798723648237423458", "-234093427891238404502222" ) )
        );
    }

    private static Stream<Arguments> equalFractionsData() {
        return Stream.of(
                //Integer values
                Arguments.of( bigFraction( "534987634876287342341234235" ),
                              bigFraction( "534987634876287342341234235" ) ),
                Arguments.of( bigFraction( "-12130192384593874623746734" ),
                              bigFraction( "-12130192384593874623746734" ) ),

                //Fraction reduced forms are equal
                Arguments.of( bigFraction( "222222222222222222222222222", "33333333333333333333" ),
                              bigFraction( "444444444444444444444444444", "66666666666666666666" ) ),
                Arguments.of( bigFraction( "-1", "250000000000000000000000000000000000000" ),
                              bigFraction( "-4", "1000000000000000000000000000000000000000" ) ),
                Arguments.of( bigFraction( "11882859593649189710619", "171486182408811856" ),
                              bigFraction( "-23765719187298379421238", "-342972364817623712" ) )
        );
    }

    private static Stream<Arguments> notEqualFractionsData() {
        return Stream.of(
                //Integer values
                Arguments.of( bigFraction( "-2387342987636478876234234" ),
                              bigFraction( "2387342987636478876234234" ) ),
                Arguments.of( bigFraction( "354346436827987435345345" ),
                              bigFraction( "-354346436827987435345345" ) ),

                //Fraction reduced forms are not equal
                Arguments.of( bigFraction( "2348716314124", "328273491239123" ),
                              bigFraction( "57277667846782867", "548273847982312387682734" ) ),
                Arguments.of( bigFraction( "-4112323411113213", "8458699848467345" ),
                              bigFraction( "-88453872341465234634", "14345092398236746782348768235124" ) ),
                Arguments.of( bigFraction( "1111111111111111111111", "3333333333333333333333" ),
                              bigFraction( "-1111111111111111111111", "-3333333333333333333334" ) ),

                //The same value but different sign
                Arguments.of( bigFraction( "-34576436565236767234234", "77198492349823400102401" ),
                              bigFraction( "34576436565236767234234", "77198492349823400102401" ) ),
                Arguments.of( bigFraction( "34576436565236767234234", "77198492349823400102401" ),
                              bigFraction( "34576436565236767234234", "-77198492349823400102401" ) ),
                Arguments.of( bigFraction( "-1000000000000000000000000000", "200000000000000000000000000000000" ),
                              bigFraction( "3000000000000000000000000000", "600000000000000000000000000000000" ) ),
                Arguments.of( bigFraction( "1000000000000000000000000000", "200000000000000000000000000000000" ),
                              bigFraction( "-3000000000000000000000000000", "600000000000000000000000000000000" ) )
        );
    }

    private static Stream<Arguments> toStringData() {
        return Stream.of(
                Arguments.of( "7723412389345796784658723/1",
                              bigFraction( "7723412389345796784658723" ) ),
                Arguments.of( "6001239023409781234/55589245983489582347823487124",
                              bigFraction( "6001239023409781234", "55589245983489582347823487124" ) )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot create fraction with {1} as numerator and {2} as denominator, " +
                               "raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException(
            Class<Throwable> throwableClass, String numerator, String denominator ) {
        assertException( throwableClass, () -> bigFraction( numerator, denominator ) );
    }

    @ParameterizedTest( name = "{index}) reverse({0}) = {1} and reverse({1}) = {0}" )
    @MethodSource( "reverseFractionsData" )
    public void test_CorrectlyReverseFraction( BigFraction expectedReversedFraction, BigFraction fraction ) {
        BigFraction reversedFraction = fraction.reverse();
        assertEquals( expectedReversedFraction, reversedFraction );
        assertNotSame( fraction, reversedFraction );

        assertEquals( fraction, reversedFraction.reverse() );
    }

    @ParameterizedTest( name = "{index}) add({1}, {2}) = {0} and add({2}, {1}) = {0}" )
    @MethodSource( "addFractionsData" )
    public void test_CorrectlyAddFractions( BigFraction expectedSum, BigFraction a, BigFraction b ) {
        BigFraction sum1 = a.add( b );
        BigFraction sum2 = b.add( a );

        assertEquals( expectedSum, sum1 );
        assertEquals( expectedSum, sum2 );

        assertNotSame( a, sum1 );
        assertNotSame( b, sum1 );
        assertNotSame( a, sum2 );
        assertNotSame( b, sum2 );
    }

    @ParameterizedTest( name = "{index}) subtract({1}, {2}) = {0} and subtract({2}, {1}) != {0}" )
    @MethodSource( "subtractFractionsData" )
    public void test_CorrectlySubtractFractions( BigFraction expectedDifference, BigFraction a, BigFraction b ) {
        BigFraction difference1 = a.subtract( b );
        BigFraction difference2 = b.subtract( a );

        assertEquals( expectedDifference, difference1 );
        if( !a.equals( b ) ) {
            assertNotEquals( expectedDifference, difference2 );
        }

        assertNotSame( a, difference1 );
        assertNotSame( b, difference1 );
        assertNotSame( a, difference2 );
        assertNotSame( b, difference2 );
    }

    @ParameterizedTest( name = "{index}) multiply({1}, {2}) = {0} and multiply({2}, {1}) = {0}" )
    @MethodSource( "multiplyFractionsData" )
    public void test_CorrectlyMultiplyFractions( BigFraction expectedProduct, BigFraction a, BigFraction b ) {
        BigFraction product1 = a.multiply( b );
        BigFraction product2 = b.multiply( a );

        assertEquals( expectedProduct, product1 );
        assertEquals( expectedProduct, product2 );

        assertNotSame( a, product1 );
        assertNotSame( b, product1 );
        assertNotSame( a, product2 );
        assertNotSame( b, product2 );
    }

    @ParameterizedTest( name = "{index}) divide({1}, {2}) = {0} and divide({2}, {1}) != {0}" )
    @MethodSource( "divideFractionsData" )
    public void test_CorrectlyDivideFractions( BigFraction expectedQuotient, BigFraction a, BigFraction b ) {
        BigFraction quotient1 = a.divide( b );
        BigFraction quotient2 = a.equals( BigFraction.ZERO ) ? null : b.divide( a );

        assertEquals( expectedQuotient, quotient1 );
        assertNotEquals( expectedQuotient, quotient2 );

        assertNotSame( a, quotient1 );
        assertNotSame( b, quotient1 );
        assertNotSame( a, quotient2 );
        assertNotSame( b, quotient2 );
    }

    @ParameterizedTest( name = "{index}) ({1})^{2} = {0}" )
    @MethodSource( "powerFractionsData" )
    public void test_CorrectlyCalculateThePowerOfFractions( BigFraction expectedPower, BigFraction a, int exponent ) {
        BigFraction power = a.pow( exponent );

        assertEquals( expectedPower, power );
        assertNotSame( a, power );
    }

    @ParameterizedTest( name = "{index}) reduce({1}) = {0}" )
    @MethodSource( "reduceFractionsData" )
    public void test_CorrectlyReduceFractions( BigFraction expectedReducedFraction, BigFraction a ) {
        BigFraction reducedFraction = a.reduce();

        assertEquals( expectedReducedFraction, reducedFraction );
        assertNotSame( a, reducedFraction );
    }

    @ParameterizedTest( name = "{index}) signum({1}) = {0}" )
    @MethodSource( "signumOfFractionsData" )
    public void test_CorrectlyCalculateSignumOfFractions( int expectedSignum, BigFraction a ) {
        assertEquals( expectedSignum, a.signum() );
    }

    @ParameterizedTest( name = "{index}) floor({1}) = {0}" )
    @MethodSource( "floorOfFractionsData" )
    public void test_CorrectlyCalculateFloorOfFractions( BigInteger expectedFloor, BigFraction a ) {
        assertEquals( expectedFloor, a.floor() );
    }

    @ParameterizedTest( name = "{index}) ceil({1}) = {0}" )
    @MethodSource( "ceilOfFractionsData" )
    public void test_CorrectlyCalculateCeilOfFractions( BigInteger expectedCeil, BigFraction a ) {
        assertEquals( expectedCeil, a.ceil() );
    }

    @ParameterizedTest( name = "{index}) {0} is a proper fraction" )
    @MethodSource( "properFractionsData" )
    public void test_CorrectlyDeterminIfFractionIsProper( BigFraction a ) {
        assertTrue( a.isProper() );
    }

    @ParameterizedTest( name = "{index}) {0} is not a proper fraction" )
    @MethodSource( "notProperFractionsData" )
    public void test_CorrectlyDeterminIfFractionIsNotProper( BigFraction a ) {
        assertFalse( a.isProper() );
    }

    @ParameterizedTest( name = "{index}) {0} is a reduced fraction" )
    @MethodSource( "reducedFractionsData" )
    public void test_CorrectlyDeterminIfFractionIsReduced( BigFraction a ) {
        assertTrue( a.isReduced() );
    }

    @ParameterizedTest( name = "{index}) {0} is not a reduced fraction" )
    @MethodSource( "notReducedFractionsData" )
    public void test_CorrectlyDeterminIfFractionIsNotReduced( BigFraction a ) {
        assertFalse( a.isReduced() );
    }

    @ParameterizedTest( name = "{index}) {0} is equal to {1}" )
    @MethodSource( "equalFractionsData" )
    public void test_Equals( BigFraction a, BigFraction b ) {
        assertEquals( a, b );
    }

    @ParameterizedTest( name = "{index}) {0} is not equal to {1}" )
    @MethodSource( "notEqualFractionsData" )
    public void test_NotEquals( BigFraction a, BigFraction b ) {
        assertNotEquals( a, b );
    }

    @ParameterizedTest( name = "{index}) \"{0}\" is the same string as \"{1}\"" )
    @MethodSource( "toStringData" )
    public void test_ToString( String s, BigFraction a ) {
        assertEquals( s, a.toString() );
    }

    @Test
    public void test_FractionCreatedWithJustNumerator_HasOneAsDenominator() {
        assertEquals( BigInteger.ONE, bigFraction( "8723641230987234982738" ).getDenominator() );
        assertEquals( BigInteger.ONE, bigFraction( "0" ).getDenominator() );
        assertEquals( BigInteger.ONE, bigFraction( "-3400000000123972364187233" ).getDenominator() );
    }

    @Test
    public void test_ConstantsHaveCorrectValue() {
        assertTrue( BigFraction.ZERO.toBigDecimal( 0 ).equals( BigDecimal.ZERO ) );
        assertTrue( BigFraction.ONE.toBigDecimal( 0 ).equals( BigDecimal.ONE ) );
    }

    @Test
    public void test_SignsAreChangedToPositive_WhenFractionIsCreatedWithNegativeNumeratorAndDenominator() {
        BigFraction fraction = bigFraction( "-727346273418273623847234234", "-46666666345872364234877123123" );

        assertEquals( new BigInteger( "727346273418273623847234234" ), fraction.getNumerator() );
        assertEquals( new BigInteger( "46666666345872364234877123123" ), fraction.getDenominator() );
    }

    @Test
    public void test_CopyingConstructorFromFraction_WorksCorrectly() {
        Fraction fraction = new Fraction( 3, 123 );
        BigFraction newFraction = new BigFraction( fraction );

        assertEquals( fraction.getNumerator(), newFraction.getNumerator().longValueExact() );
        assertEquals( fraction.getDenominator(), newFraction.getDenominator().longValueExact() );
    }

    @Test
    public void test_CopyingConstructorFromBigFraction_WorksCorrectly() {
        BigFraction fraction = bigFraction( "1712387236416235612536123", "366239812362837412312312" );
        BigFraction newFraction = new BigFraction( fraction );

        assertEquals( fraction.getNumerator(), newFraction.getNumerator() );
        assertEquals( fraction.getDenominator(), newFraction.getDenominator() );
    }

    @Test
    public void test_ForbiddenCalculations_RaiseExceptions() {
        //Reverting zero
        assertException( ArithmeticException.class, () -> BigFraction.ZERO.reverse() );

        //Dividing by zero
        assertException( ArithmeticException.class,
                         () -> bigFraction( "87684376582734687234" ).divide( BigFraction.ZERO ) );
        assertException( ArithmeticException.class, () -> BigFraction.ZERO.divide( BigFraction.ZERO ) );

        //Raising to the power of Long.MIN_VALUE
        assertException( IllegalArgumentException.class,
                         () -> bigFraction( "98745345", "1236756234234234" ).pow( Integer.MIN_VALUE ) );

        //Zero to the power of zero
        assertException( ArithmeticException.class, () -> BigFraction.ZERO.pow( 0 ) );

        //Zero raised to negative power
        assertException( ArithmeticException.class, () -> BigFraction.ZERO.pow( -1 ) );
        assertException( ArithmeticException.class, () -> BigFraction.ZERO.pow( -14 ) );
        assertException( ArithmeticException.class, () -> BigFraction.ZERO.pow( -98 ) );
    }

    @Test
    public void test_Reduce_FractionsEqualToZero_AreNotReduced() {
        BigFraction fraction = bigFraction( "0", "3482635192838123" );
        BigFraction reducedFraction = fraction.reduce();

        assertEquals( fraction.getNumerator(), reducedFraction.getNumerator() );
        assertEquals( fraction.getDenominator(), reducedFraction.getDenominator() );
    }

    @Test
    public void test_ToBigDecimal() {
        BigFraction fraction = bigFraction( "345943582734234234", "508247892384" );
        assertEquals(
                new BigDecimal( "345943582734234234" )
                        .divide( new BigDecimal( "508247892384" ), RoundingMode.HALF_UP.HALF_UP ),
                fraction.toBigDecimal( 0 ) );
    }

    @Test
    public void test_CompareTo() {
        assertTrue( bigFraction( "2350984971679124142" ).compareTo( bigFraction( "52347767235" ) ) > 0 );
        assertTrue(
                bigFraction( "2350984971679124142" )
                        .compareTo( bigFraction( "745645634454545334534584971679124142" ) ) < 0 );
        assertTrue( bigFraction( "2350984971679124142" ).compareTo( bigFraction( "2350984971679124142" ) ) == 0 );

        assertTrue( bigFraction( "7777777777777777777", "8888888888888888888888888888" )
                            .compareTo( bigFraction( "6666666666666666666", "8888888888888888888888888888" ) ) > 0 );
        assertTrue( bigFraction( "6666666666666666666", "8888888888888888888888888888" )
                            .compareTo( bigFraction( "7777777777777777777", "8888888888888888888888888888" ) ) < 0 );
        assertTrue( bigFraction( "6666666666666666666", "8888888888888888888888888888" )
                            .compareTo( bigFraction( "3333333333333333333", "4444444444444444444444444444" ) ) == 0 );

        assertTrue( bigFraction( "-7777777777777777777", "8888888888888888888888888888" )
                            .compareTo( bigFraction( "-6666666666666666666", "8888888888888888888888888888" ) ) < 0 );
        assertTrue( bigFraction( "-6666666666666666666", "8888888888888888888888888888" )
                            .compareTo( bigFraction( "-7777777777777777777", "8888888888888888888888888888" ) ) > 0 );
        assertTrue( bigFraction( "-6666666666666666666", "8888888888888888888888888888" )
                            .compareTo( bigFraction( "-3333333333333333333", "4444444444444444444444444444" ) ) == 0 );
    }

    @Test
    public void test_Equals() {
        BigFraction fraction = bigFraction( "4000000000000000000000000000000000000",
                                            "8000000000000000000000000000000000000" );

        assertTrue( fraction.equals( fraction ) );
        assertFalse( fraction.equals( null ) );
        assertFalse( fraction.equals( new String() ) );
        assertTrue( fraction.equals(
                bigFraction( "4000000000000000000000000000000000000", "8000000000000000000000000000000000000" ) ) );
        assertTrue( fraction.equals(
                bigFraction( "1000000000000000000000000000000000000", "2000000000000000000000000000000000000" ) ) );
    }

    @Test
    public void test_HashCode() {
        BigFraction fraction1 = bigFraction( "4000000000000000000000000000000000000",
                                             "8000000000000000000000000000000000000" );

        assertEquals( fraction1.hashCode(), fraction1.hashCode() );
        assertEquals( fraction1.hashCode(),
                      bigFraction( "4000000000000000000000000000000000000", "8000000000000000000000000000000000000" )
                              .hashCode() );
        assertNotEquals( fraction1.hashCode(),
                         bigFraction( "1000000000000000000000000000000000000", "2000000000000000000000000000000000000" )
                                 .hashCode() );
    }
}