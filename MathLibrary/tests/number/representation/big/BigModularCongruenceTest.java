package number.representation.big;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static testutils.ExceptionAssert.assertException;

public class BigModularCongruenceTest {
    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                //Modulo < 1
                Arguments.of( IllegalArgumentException.class, "-6626427618723569231112", "0" ),
                Arguments.of( IllegalArgumentException.class, "8787234786178236789283423", "0" ),
                Arguments.of( IllegalArgumentException.class, "0", "0" ),
                Arguments.of( IllegalArgumentException.class, "5459082839498293492341", "-9439873029104438567782234" ),
                Arguments.of( IllegalArgumentException.class, "0", "-3249873598726587629348234982234" ),
                Arguments.of( IllegalArgumentException.class, "-29218487468132764582345423346",
                              "-1239823563845683242435253" )
        );
    }

    private static Stream<Arguments> adjustedResidueData() {
        return Stream.of(
                //Residue = 0
                Arguments.of( BigInteger.ZERO,
                              "0", "4654623323434642432345235235" ),

                //Positive residue
                Arguments.of( new BigInteger( "231233434664645756435234234" ),
                              "231233434664645756435234234", "123490237498129745987342987598234" ),
                Arguments.of( new BigInteger( "558745921306155079332721885" ),
                              "919293984237598238948394259872394234", "682938457829342384747895987" ),
                Arguments.of( BigInteger.ZERO,
                              "7012930023957192748293847234234", "7012930023957192748293847234234" ),
                Arguments.of( BigInteger.ZERO,
                              "27371674427874243524664944639638", "3910239203982034789237849234234" ),

                //Negative residue
                Arguments.of( new BigInteger( "88230489590551176704817726249087996563" ),
                              "-612938943598116298123254345345", "88230490203490120302934024372342341908" ),
                Arguments.of( new BigInteger( "44339210223144223782" ),
                              "-11109293495938459022834920934234234", "71273734857988934234" ),
                Arguments.of( BigInteger.ZERO,
                              "-818238829345712762340234234769234", "818238829345712762340234234769234" ),
                Arguments.of( BigInteger.ZERO,
                              "-32274325482149846989836380054353", "2934029589286349726348761823123" )
        );
    }

    private static Stream<Arguments> moduloData() {
        return Stream.of(
                Arguments.of( "129382345874326568234234", "2389898929837498723948923849897455345" ),
                Arguments.of( "-7777298989734767836876872634234", "9192039234082348568974379345345345345" ),
                Arguments.of( "0", "521289234948358769238975435345" )
        );
    }

    private static Stream<Arguments> modularCongruenceValuesData() {
        return Stream.of(
                Arguments.of( asList( new BigInteger( "1090123263762234234234" ),
                                      new BigInteger( "8824568362613349872709921" ),
                                      new BigInteger( "17648046601962937511185608" ),
                                      new BigInteger( "26471524841312525149661295" ),
                                      new BigInteger( "35295003080662112788136982" ),
                                      new BigInteger( "44118481320011700426612669" ) ),
                              bigModularCongruence( "1090123263762234234234", "8823478239349587638475687" ) ),
                Arguments.of( asList( new BigInteger( "2486250894259754550939" ),
                                      new BigInteger( "14884996504059772207462" ),
                                      new BigInteger( "27283742113859789863985" ),
                                      new BigInteger( "39682487723659807520508" ),
                                      new BigInteger( "52081233333459825177031" ),
                                      new BigInteger( "64479978943259842833554" ) ),
                              bigModularCongruence( "64589345934934567238674358374", "12398745609800017656523" ) ),
                Arguments.of( asList( new BigInteger( "0" ), new BigInteger( "74783599823841643827568762342" ),
                                      new BigInteger( "149567199647683287655137524684" ),
                                      new BigInteger( "224350799471524931482706287026" ),
                                      new BigInteger( "299134399295366575310275049368" ),
                                      new BigInteger( "373917999119208219137843811710" ) ),
                              bigModularCongruence( "0", "74783599823841643827568762342" ) ),
                Arguments.of( asList( new BigInteger( "329340593948575929861878698201" ),
                                      new BigInteger( "658681187897374164651142185636" ),
                                      new BigInteger( "988021781846172399440405673071" ),
                                      new BigInteger( "1317362375794970634229669160506" ),
                                      new BigInteger( "1646702969743768869018932647941" ),
                                      new BigInteger( "1976043563692567103808196135376" ) ),
                              bigModularCongruence( "-222304927384789234", "329340593948798234789263487435" ) ),
                Arguments.of( asList( new BigInteger( "123615838083277491693436940" ),
                                      new BigInteger( "558603426017876315191675734" ),
                                      new BigInteger( "993591013952475138689914528" ),
                                      new BigInteger( "1428578601887073962188153322" ),
                                      new BigInteger( "1863566189821672785686392116" ),
                                      new BigInteger( "2298553777756271609184630910" ) ),
                              bigModularCongruence( "-770239402394091238923687467234", "434987587934598823498238794" ) )
        );
    }

    private static BigModularCongruence bigModularCongruence( String residue, String modulo ) {
        return new BigModularCongruence( new BigInteger( residue ), new BigInteger( modulo ) );
    }

    private static Stream<Arguments> toStringData() {
        return Stream.of(
                Arguments.of( "a = 1230823494487234687234123 mod 77346588727386426873878734",
                              bigModularCongruence( "1230823494487234687234123", "77346588727386426873878734" ) ),
                Arguments.of( "a = 0 mod 182398426873462342342534235",
                              bigModularCongruence( "0", "182398426873462342342534235" ) ),
                Arguments.of( "a = 0 mod 42394634985792384234234123",
                              bigModularCongruence( "805498064730055300450448337", "42394634985792384234234123" ) ),
                Arguments.of( "a = 30729682688366074935 mod 34587268342341231211",
                              bigModularCongruence( "87432756783784657345132", "34587268342341231211" ) ),
                Arguments.of( "a = 55930489579095139543397331400 mod 55930489598123486782345234234",
                              bigModularCongruence( "-19028347238947902834", "55930489598123486782345234234" ) ),
                Arguments.of( "a = 119443623183354 mod 122349867345777",
                              bigModularCongruence( "-623400010230943278234234", "122349867345777" ) )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot create big modular congruence with {1} as residue and {2} as modulo, " +
                               "raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, String residue, String modulo ) {
        assertException( throwableClass,
                         () -> new BigModularCongruence( new BigInteger( residue ), new BigInteger( modulo ) ) );
    }

    @ParameterizedTest(
            name = "{index}) Correctly adjusted the residue and created \"{0} mod {2}\" from \"{1} mod \"{2}\"" )
    @MethodSource( "adjustedResidueData" )
    public void test_CorrectlyAdjustedResidueInTheConstructor(
            BigInteger adjustedResidue, String residue, String modulo ) {
        assertEquals( adjustedResidue,
                      new BigModularCongruence( new BigInteger( residue ), new BigInteger( modulo ) ).getResidue() );
    }

    @ParameterizedTest( name = "{index}) Correctly obtained modulo from \"{0} mod {1}\"" )
    @MethodSource( "moduloData" )
    public void test_GetModulo( String residue, String modulo ) {
        assertEquals( new BigInteger( modulo ),
                      new BigModularCongruence( new BigInteger( residue ), new BigInteger( modulo ) ).getModulo() );
    }

    @ParameterizedTest( name = "{index}) First values we iterated by using {1} are: {0}" )
    @MethodSource( "modularCongruenceValuesData" )
    public void test_ModularCongruenceIsIterable_ThroughItsConsecutivePositiveValues(
            List<BigInteger> expectedFirstValues, BigModularCongruence modularCongruence ) {
        assertFirstValues( expectedFirstValues, modularCongruence );
    }

    private void assertFirstValues( List<BigInteger> expectedFirstValues, BigModularCongruence
            modularCongruence ) {
        Iterator<BigInteger> iterator = modularCongruence.iterator();

        for( BigInteger expectedValue : expectedFirstValues ) {
            assertTrue( iterator.hasNext() );
            assertEquals( expectedValue, iterator.next() );
        }

        assertTrue( iterator.hasNext() );
    }

    @ParameterizedTest( name = "{index}) \"{0}\" is the same string as \"{1}\"" )
    @MethodSource( "toStringData" )
    public void test_ToString( String s, BigModularCongruence modularCongruence ) {
        assertEquals( s, modularCongruence.toString() );
    }

    @Test
    public void test_ModularCongruenceCanBeReused() {
        BigModularCongruence modularCongruence = bigModularCongruence( "18432598672937649823423422341",
                                                                       "101904372936712123123243" );

        assertFirstValues(
                asList( new BigInteger( "33791772224280768105258" ), new BigInteger( "135696145160992891228501" ),
                        new BigInteger( "237600518097705014351744" ), new BigInteger( "339504891034417137474987" ),
                        new BigInteger( "441409263971129260598230" ),
                        new BigInteger( "543313636907841383721473" ) ),
                modularCongruence );
        assertFirstValues(
                asList( new BigInteger( "33791772224280768105258" ), new BigInteger( "135696145160992891228501" ),
                        new BigInteger( "237600518097705014351744" ), new BigInteger( "339504891034417137474987" ),
                        new BigInteger( "441409263971129260598230" ),
                        new BigInteger( "543313636907841383721473" ) ),
                modularCongruence );
    }

    @Test
    public void test_HashCode() {
        BigModularCongruence modularCongruence1 = bigModularCongruence( "9929384782394823956677234",
                                                                        "77128392384539238467672341" );
        BigModularCongruence modularCongruenc2 = bigModularCongruence( "-221455792371222891446339789",
                                                                       "77128392384539238467672341" );

        assertEquals( modularCongruence1.hashCode(), modularCongruence1.hashCode() );
        assertEquals( modularCongruence1.hashCode(), modularCongruenc2.hashCode() );
        assertEquals( modularCongruence1.hashCode(),
                      bigModularCongruence( "9929384782394823956677234", "77128392384539238467672341" ).hashCode() );
    }

    @Test
    public void test_Equals() {
        BigModularCongruence modularCongruence = bigModularCongruence( "239892983483498345345",
                                                                       "99123000205029348239234" );

        assertTrue( modularCongruence.equals( modularCongruence ) );
        assertFalse( modularCongruence.equals( null ) );
        assertFalse( modularCongruence.equals( new String() ) );
        assertFalse( modularCongruence
                             .equals( bigModularCongruence( "672634782347234768234", "99123000205029348239234" ) ) );
        assertTrue( modularCongruence
                            .equals( bigModularCongruence( "239892983483498345345", "99123000205029348239234" ) ) );
        assertTrue(
                modularCongruence
                        .equals( bigModularCongruence( "-396252107836633894611591", "99123000205029348239234" ) ) );
    }
}
