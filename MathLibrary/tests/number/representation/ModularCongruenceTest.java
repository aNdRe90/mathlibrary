package number.representation;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static testutils.CollectionUtils.listOfLongs;
import static testutils.ExceptionAssert.assertException;

public class ModularCongruenceTest {
    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                //Modulo < 1
                Arguments.of( IllegalArgumentException.class, -2, 0 ),
                Arguments.of( IllegalArgumentException.class, 5, 0 ),
                Arguments.of( IllegalArgumentException.class, 0, 0 ),
                Arguments.of( IllegalArgumentException.class, -5, -9 ),
                Arguments.of( IllegalArgumentException.class, 12, -5 ),
                Arguments.of( IllegalArgumentException.class, 0, -12 )
        );
    }

    private static Stream<Arguments> adjustedResidueData() {
        return Stream.of(
                //Residue = 0
                Arguments.of( 0, 0, 9 ),

                //Positive residue
                Arguments.of( 4, 4, 9 ),
                Arguments.of( 8, 8, 9 ),
                Arguments.of( 0, 9, 9 ),
                Arguments.of( 5, 14, 9 ),
                Arguments.of( 1, 100, 9 ),
                Arguments.of( 0, 999, 9 ),

                //Negative residue
                Arguments.of( 8, -190, 9 ),
                Arguments.of( 3, -87, 9 ),
                Arguments.of( 0, -18, 9 ),
                Arguments.of( 8, -1, 9 ),
                Arguments.of( 5, -4, 9 ),
                Arguments.of( 1, -8, 9 )
        );
    }

    private static Stream<Arguments> moduloData() {
        return Stream.of(
                Arguments.of( 11, 2341 ),
                Arguments.of( -234, 6645 ),
                Arguments.of( 0, 111 ),
                Arguments.of( 5, 65456 ),
                Arguments.of( 688769, 3212 ),
                Arguments.of( -34873, 90234 ),
                Arguments.of( -100023, 543 )
        );
    }

    private static Stream<Arguments> modularCongruenceValuesData() {
        return Stream.of(
                Arguments.of( listOfLongs( 3, 10, 17, 24, 31, 38, 45, 52, 59, 66, 73, 80 ),
                              new ModularCongruence( 3, 7 ) ),
                Arguments.of( listOfLongs( 4, 21, 38, 55, 72, 89, 106, 123, 140, 157, 174, 191 ),
                              new ModularCongruence( 38, 17 ) ),
                Arguments.of( listOfLongs( 0, 74, 148, 222, 296, 370, 444, 518, 592, 666, 740, 814 ),
                              new ModularCongruence( 0, 74 ) ),
                Arguments.of( listOfLongs( 5, 14, 23, 32, 41, 50, 59, 68, 77, 86, 95, 104 ),
                              new ModularCongruence( -4, 9 ) ),
                Arguments.of( listOfLongs( 2, 16, 30, 44, 58, 72, 86, 100, 114, 128, 142, 156 ),
                              new ModularCongruence( -40, 14 ) )
        );
    }

    private static Stream<Arguments> toStringData() {
        return Stream.of(
                Arguments.of( "a = 6 mod 13",
                              new ModularCongruence( 6, 13 ) ),
                Arguments.of( "a = 0 mod 5",
                              new ModularCongruence( 0, 5 ) ),
                Arguments.of( "a = 0 mod 10",
                              new ModularCongruence( 130, 10 ) ),
                Arguments.of( "a = 3 mod 11",
                              new ModularCongruence( 14, 11 ) ),
                Arguments.of( "a = 5 mod 9",
                              new ModularCongruence( -4, 9 ) ),
                Arguments.of( "a = 34 mod 42",
                              new ModularCongruence( -50, 42 ) )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot create modular congruence with {1} as residue and {2} as modulo, " +
                               "raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long residue, long modulo ) {
        assertException( throwableClass, () -> new ModularCongruence( residue, modulo ) );
    }

    @ParameterizedTest(
            name = "{index}) Correctly adjusted the residue and created \"{0} mod {2}\" from \"{1} mod \"{2}\"" )
    @MethodSource( "adjustedResidueData" )
    public void test_CorrectlyAdjustedResidueInTheConstructor( long adjustedResidue, long residue, long modulo ) {
        assertEquals( adjustedResidue, new ModularCongruence( residue, modulo ).getResidue() );
    }

    @ParameterizedTest( name = "{index}) Correctly obtained modulo from \"{0} mod {1}\"" )
    @MethodSource( "moduloData" )
    public void test_GetModulo( long residue, long modulo ) {
        assertEquals( modulo, new ModularCongruence( residue, modulo ).getModulo() );
    }

    @ParameterizedTest( name = "{index}) First values we iterated by using {1} are: {0}" )
    @MethodSource( "modularCongruenceValuesData" )
    public void test_ModularCongruenceIsIterable_ThroughItsConsecutivePositiveValues(
            List<Long> expectedFirstValues, ModularCongruence modularCongruence ) {
        assertFirstValues( expectedFirstValues, modularCongruence );
    }

    private void assertFirstValues( List<Long> expectedFirstValues, ModularCongruence modularCongruence ) {
        Iterator<Long> iterator = modularCongruence.iterator();

        for( long expectedValue : expectedFirstValues ) {
            assertTrue( iterator.hasNext() );
            assertEquals( expectedValue, iterator.next().longValue() );
        }

        assertTrue( iterator.hasNext() );
    }

    @ParameterizedTest( name = "{index}) \"{0}\" is the same string as \"{1}\"" )
    @MethodSource( "toStringData" )
    public void test_ToString( String s, ModularCongruence modularCongruence ) {
        assertEquals( s, modularCongruence.toString() );
    }

    @Test
    public void test_OverflowDoesNotOccur_AndIterationStops_IfLongMAXValueIsReached() {
        Iterator<Long> modularCongruenceIterator = new ModularCongruence( 41, Long.MAX_VALUE - 40 ).iterator();
        assertTrue( modularCongruenceIterator.hasNext() );
        assertEquals( 41, (long) modularCongruenceIterator.next() );

        assertFalse( modularCongruenceIterator.hasNext() );
        assertException( NoSuchElementException.class, () -> modularCongruenceIterator.next() );
    }

    @Test
    public void test_ModularCongruenceCanBeReused() {
        ModularCongruence modularCongruence = new ModularCongruence( 2, 6 );
        assertFirstValues( asList( 2L, 8L, 14L, 20L, 26L, 32L, 38L, 44L, 50L, 56L, 62L, 68L ), modularCongruence );
        assertFirstValues( asList( 2L, 8L, 14L, 20L, 26L, 32L, 38L, 44L, 50L, 56L, 62L, 68L ), modularCongruence );
    }

    @Test
    public void test_HashCode() {
        ModularCongruence modularCongruence1 = new ModularCongruence( 3, 10 );
        ModularCongruence modularCongruence2 = new ModularCongruence( -7, 10 );

        assertEquals( modularCongruence1.hashCode(), modularCongruence1.hashCode() );
        assertEquals( modularCongruence1.hashCode(), modularCongruence2.hashCode() );
        assertEquals( modularCongruence1.hashCode(), new ModularCongruence( 3, 10 ).hashCode() );
    }

    @Test
    public void test_Equals() {
        ModularCongruence modularCongruence = new ModularCongruence( 7, 8 );

        assertTrue( modularCongruence.equals( modularCongruence ) );
        assertFalse( modularCongruence.equals( null ) );
        assertFalse( modularCongruence.equals( new String() ) );
        assertFalse( modularCongruence.equals( new ModularCongruence( 4, 8 ) ) );
        assertTrue( modularCongruence.equals( new ModularCongruence( 7, 8 ) ) );
        assertTrue( modularCongruence.equals( new ModularCongruence( -1, 8 ) ) );
    }
}
