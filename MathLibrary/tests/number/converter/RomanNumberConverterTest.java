package number.converter;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Stream;

import static number.converter.RomanNumberConverter.MAX_ALLOWED_VALUE;
import static org.junit.Assert.*;
import static testutils.ExceptionAssert.assertException;

public class RomanNumberConverterTest {
    private static Map<Integer, String> romanNumbersInMinimalFormPerValue = new HashMap<>();
    private static Map<Integer, String> romanNumbersNotInMinimalFormPerValue = new HashMap<>();
    private static List<String> invalidRomanNumbers = new ArrayList<>();

    static {
        romanNumbersInMinimalFormPerValue.put( 1, "I" );
        romanNumbersInMinimalFormPerValue.put( 2, "II" );
        romanNumbersInMinimalFormPerValue.put( 3, "III" );
        romanNumbersInMinimalFormPerValue.put( 4, "IV" );
        romanNumbersInMinimalFormPerValue.put( 5, "V" );
        romanNumbersInMinimalFormPerValue.put( 6, "VI" );
        romanNumbersInMinimalFormPerValue.put( 7, "VII" );
        romanNumbersInMinimalFormPerValue.put( 8, "VIII" );
        romanNumbersInMinimalFormPerValue.put( 9, "IX" );

        romanNumbersInMinimalFormPerValue.put( 10, "X" );
        romanNumbersInMinimalFormPerValue.put( 11, "XI" );
        romanNumbersInMinimalFormPerValue.put( 12, "XII" );
        romanNumbersInMinimalFormPerValue.put( 13, "XIII" );
        romanNumbersInMinimalFormPerValue.put( 14, "XIV" );
        romanNumbersInMinimalFormPerValue.put( 15, "XV" );
        romanNumbersInMinimalFormPerValue.put( 16, "XVI" );
        romanNumbersInMinimalFormPerValue.put( 17, "XVII" );
        romanNumbersInMinimalFormPerValue.put( 18, "XVIII" );
        romanNumbersInMinimalFormPerValue.put( 19, "XIX" );

        romanNumbersInMinimalFormPerValue.put( 20, "XX" );
        romanNumbersInMinimalFormPerValue.put( 21, "XXI" );
        romanNumbersInMinimalFormPerValue.put( 22, "XXII" );
        romanNumbersInMinimalFormPerValue.put( 23, "XXIII" );
        romanNumbersInMinimalFormPerValue.put( 24, "XXIV" );
        romanNumbersInMinimalFormPerValue.put( 25, "XXV" );
        romanNumbersInMinimalFormPerValue.put( 26, "XXVI" );
        romanNumbersInMinimalFormPerValue.put( 27, "XXVII" );
        romanNumbersInMinimalFormPerValue.put( 28, "XXVIII" );
        romanNumbersInMinimalFormPerValue.put( 29, "XXIX" );

        romanNumbersInMinimalFormPerValue.put( 30, "XXX" );
        romanNumbersInMinimalFormPerValue.put( 31, "XXXI" );
        romanNumbersInMinimalFormPerValue.put( 32, "XXXII" );
        romanNumbersInMinimalFormPerValue.put( 33, "XXXIII" );
        romanNumbersInMinimalFormPerValue.put( 34, "XXXIV" );
        romanNumbersInMinimalFormPerValue.put( 35, "XXXV" );
        romanNumbersInMinimalFormPerValue.put( 36, "XXXVI" );
        romanNumbersInMinimalFormPerValue.put( 37, "XXXVII" );
        romanNumbersInMinimalFormPerValue.put( 38, "XXXVIII" );
        romanNumbersInMinimalFormPerValue.put( 39, "XXXIX" );

        romanNumbersInMinimalFormPerValue.put( 40, "XL" );
        romanNumbersInMinimalFormPerValue.put( 41, "XLI" );
        romanNumbersInMinimalFormPerValue.put( 42, "XLII" );
        romanNumbersInMinimalFormPerValue.put( 43, "XLIII" );
        romanNumbersInMinimalFormPerValue.put( 44, "XLIV" );
        romanNumbersInMinimalFormPerValue.put( 45, "XLV" );
        romanNumbersInMinimalFormPerValue.put( 46, "XLVI" );
        romanNumbersInMinimalFormPerValue.put( 47, "XLVII" );
        romanNumbersInMinimalFormPerValue.put( 48, "XLVIII" );
        romanNumbersInMinimalFormPerValue.put( 49, "XLIX" );

        romanNumbersInMinimalFormPerValue.put( 50, "L" );
        romanNumbersInMinimalFormPerValue.put( 51, "LI" );
        romanNumbersInMinimalFormPerValue.put( 52, "LII" );
        romanNumbersInMinimalFormPerValue.put( 53, "LIII" );
        romanNumbersInMinimalFormPerValue.put( 54, "LIV" );
        romanNumbersInMinimalFormPerValue.put( 55, "LV" );
        romanNumbersInMinimalFormPerValue.put( 56, "LVI" );
        romanNumbersInMinimalFormPerValue.put( 57, "LVII" );
        romanNumbersInMinimalFormPerValue.put( 58, "LVIII" );
        romanNumbersInMinimalFormPerValue.put( 59, "LIX" );

        romanNumbersInMinimalFormPerValue.put( 60, "LX" );
        romanNumbersInMinimalFormPerValue.put( 61, "LXI" );
        romanNumbersInMinimalFormPerValue.put( 62, "LXII" );
        romanNumbersInMinimalFormPerValue.put( 63, "LXIII" );
        romanNumbersInMinimalFormPerValue.put( 64, "LXIV" );
        romanNumbersInMinimalFormPerValue.put( 65, "LXV" );
        romanNumbersInMinimalFormPerValue.put( 66, "LXVI" );
        romanNumbersInMinimalFormPerValue.put( 67, "LXVII" );
        romanNumbersInMinimalFormPerValue.put( 68, "LXVIII" );
        romanNumbersInMinimalFormPerValue.put( 69, "LXIX" );

        romanNumbersInMinimalFormPerValue.put( 70, "LXX" );
        romanNumbersInMinimalFormPerValue.put( 71, "LXXI" );
        romanNumbersInMinimalFormPerValue.put( 72, "LXXII" );
        romanNumbersInMinimalFormPerValue.put( 73, "LXXIII" );
        romanNumbersInMinimalFormPerValue.put( 74, "LXXIV" );
        romanNumbersInMinimalFormPerValue.put( 75, "LXXV" );
        romanNumbersInMinimalFormPerValue.put( 76, "LXXVI" );
        romanNumbersInMinimalFormPerValue.put( 77, "LXXVII" );
        romanNumbersInMinimalFormPerValue.put( 78, "LXXVIII" );
        romanNumbersInMinimalFormPerValue.put( 79, "LXXIX" );

        romanNumbersInMinimalFormPerValue.put( 80, "LXXX" );
        romanNumbersInMinimalFormPerValue.put( 81, "LXXXI" );
        romanNumbersInMinimalFormPerValue.put( 82, "LXXXII" );
        romanNumbersInMinimalFormPerValue.put( 83, "LXXXIII" );
        romanNumbersInMinimalFormPerValue.put( 84, "LXXXIV" );
        romanNumbersInMinimalFormPerValue.put( 85, "LXXXV" );
        romanNumbersInMinimalFormPerValue.put( 86, "LXXXVI" );
        romanNumbersInMinimalFormPerValue.put( 87, "LXXXVII" );
        romanNumbersInMinimalFormPerValue.put( 88, "LXXXVIII" );
        romanNumbersInMinimalFormPerValue.put( 89, "LXXXIX" );

        romanNumbersInMinimalFormPerValue.put( 90, "XC" );
        romanNumbersInMinimalFormPerValue.put( 91, "XCI" );
        romanNumbersInMinimalFormPerValue.put( 92, "XCII" );
        romanNumbersInMinimalFormPerValue.put( 93, "XCIII" );
        romanNumbersInMinimalFormPerValue.put( 94, "XCIV" );
        romanNumbersInMinimalFormPerValue.put( 95, "XCV" );
        romanNumbersInMinimalFormPerValue.put( 96, "XCVI" );
        romanNumbersInMinimalFormPerValue.put( 97, "XCVII" );
        romanNumbersInMinimalFormPerValue.put( 98, "XCVIII" );
        romanNumbersInMinimalFormPerValue.put( 99, "XCIX" );

        romanNumbersInMinimalFormPerValue.put( 100, "C" );
        romanNumbersInMinimalFormPerValue.put( 200, "CC" );
        romanNumbersInMinimalFormPerValue.put( 300, "CCC" );
        romanNumbersInMinimalFormPerValue.put( 400, "CD" );
        romanNumbersInMinimalFormPerValue.put( 500, "D" );
        romanNumbersInMinimalFormPerValue.put( 600, "DC" );
        romanNumbersInMinimalFormPerValue.put( 700, "DCC" );
        romanNumbersInMinimalFormPerValue.put( 800, "DCCC" );
        romanNumbersInMinimalFormPerValue.put( 900, "CM" );

        romanNumbersInMinimalFormPerValue.put( 101, "CI" );
        romanNumbersInMinimalFormPerValue.put( 147, "CXLVII" );
        romanNumbersInMinimalFormPerValue.put( 218, "CCXVIII" );
        romanNumbersInMinimalFormPerValue.put( 279, "CCLXXIX" );
        romanNumbersInMinimalFormPerValue.put( 320, "CCCXX" );
        romanNumbersInMinimalFormPerValue.put( 305, "CCCV" );
        romanNumbersInMinimalFormPerValue.put( 471, "CDLXXI" );
        romanNumbersInMinimalFormPerValue.put( 415, "CDXV" );
        romanNumbersInMinimalFormPerValue.put( 552, "DLII" );
        romanNumbersInMinimalFormPerValue.put( 564, "DLXIV" );
        romanNumbersInMinimalFormPerValue.put( 604, "DCIV" );
        romanNumbersInMinimalFormPerValue.put( 666, "DCLXVI" );
        romanNumbersInMinimalFormPerValue.put( 702, "DCCII" );
        romanNumbersInMinimalFormPerValue.put( 719, "DCCXIX" );
        romanNumbersInMinimalFormPerValue.put( 809, "DCCCIX" );
        romanNumbersInMinimalFormPerValue.put( 883, "DCCCLXXXIII" );
        romanNumbersInMinimalFormPerValue.put( 906, "CMVI" );
        romanNumbersInMinimalFormPerValue.put( 999, "CMXCIX" );

        romanNumbersInMinimalFormPerValue.put( 1000, "M" );
        romanNumbersInMinimalFormPerValue.put( 2000, "MM" );
        romanNumbersInMinimalFormPerValue.put( 3000, "MMM" );
        romanNumbersInMinimalFormPerValue.put( 4000, "MMMM" );
        romanNumbersInMinimalFormPerValue.put( 5000, "MMMMM" );
        romanNumbersInMinimalFormPerValue.put( 6000, "MMMMMM" );
        romanNumbersInMinimalFormPerValue.put( 7000, "MMMMMMM" );
        romanNumbersInMinimalFormPerValue.put( 8000, "MMMMMMMM" );
        romanNumbersInMinimalFormPerValue.put( 9000, "MMMMMMMMM" );
        romanNumbersInMinimalFormPerValue.put( 10000, "MMMMMMMMMM" );

        romanNumbersInMinimalFormPerValue.put( 1582, "MDLXXXII" );
        romanNumbersInMinimalFormPerValue.put( 1092, "MXCII" );
        romanNumbersInMinimalFormPerValue.put( 2001, "MMI" );
        romanNumbersInMinimalFormPerValue.put( 2726, "MMDCCXXVI" );
        romanNumbersInMinimalFormPerValue.put( 3041, "MMMXLI" );
        romanNumbersInMinimalFormPerValue.put( 3899, "MMMDCCCXCIX" );
        romanNumbersInMinimalFormPerValue.put( 4107, "MMMMCVII" );
        romanNumbersInMinimalFormPerValue.put( 4266, "MMMMCCLXVI" );
        romanNumbersInMinimalFormPerValue.put( 5248, "MMMMMCCXLVIII" );
        romanNumbersInMinimalFormPerValue.put( 5009, "MMMMMIX" );
        romanNumbersInMinimalFormPerValue.put( 6040, "MMMMMMXL" );
        romanNumbersInMinimalFormPerValue.put( 6290, "MMMMMMCCXC" );
        romanNumbersInMinimalFormPerValue.put( 7100, "MMMMMMMC" );
        romanNumbersInMinimalFormPerValue.put( 7504, "MMMMMMMDIV" );
        romanNumbersInMinimalFormPerValue.put( 8400, "MMMMMMMMCD" );
        romanNumbersInMinimalFormPerValue.put( 8900, "MMMMMMMMCM" );
        romanNumbersInMinimalFormPerValue.put( 9785, "MMMMMMMMMDCCLXXXV" );
        romanNumbersInMinimalFormPerValue.put( 9111, "MMMMMMMMMCXI" );
    }

    static {
        romanNumbersNotInMinimalFormPerValue.put( 4, "IIII" );
        romanNumbersNotInMinimalFormPerValue.put( 5, "IVI" );
        romanNumbersNotInMinimalFormPerValue.put( 7, "IVIII" );
        romanNumbersNotInMinimalFormPerValue.put( 7, "IIIIIII" );
        romanNumbersNotInMinimalFormPerValue.put( 9, "VIIII" );
        romanNumbersNotInMinimalFormPerValue.put( 19, "XVIIII" );
        romanNumbersNotInMinimalFormPerValue.put( 41, "XXXXI" );
        romanNumbersNotInMinimalFormPerValue.put( 49, "XXXXVIIII" );
        romanNumbersNotInMinimalFormPerValue.put( 50, "XLX" );
        romanNumbersNotInMinimalFormPerValue.put( 60, "XLXX" );
        romanNumbersNotInMinimalFormPerValue.put( 66, "XXXXXXIIIIII" );
        romanNumbersNotInMinimalFormPerValue.put( 67, "XXXXXXVII" );
        romanNumbersNotInMinimalFormPerValue.put( 75, "XXXXXXXV" );
        romanNumbersNotInMinimalFormPerValue.put( 90, "XXXXXXXXX" );
        romanNumbersNotInMinimalFormPerValue.put( 97, "LXXXXVII" );
        romanNumbersNotInMinimalFormPerValue.put( 144, "CXXXXIIII" );
        romanNumbersNotInMinimalFormPerValue.put( 149, "CXLVIIII" );
        romanNumbersNotInMinimalFormPerValue.put( 199, "CLXXXXVIIII" );
        romanNumbersNotInMinimalFormPerValue.put( 412, "CCCCXII" );
        romanNumbersNotInMinimalFormPerValue.put( 444, "CCCCXXXXIIII" );
        romanNumbersNotInMinimalFormPerValue.put( 500, "CDC" );
        romanNumbersNotInMinimalFormPerValue.put( 820, "CCCCCCCCXX" );
        romanNumbersNotInMinimalFormPerValue.put( 888, "CCCCCCCCXXXXXXXXIIIIIIII" );
        romanNumbersNotInMinimalFormPerValue.put( 900, "CDCCCCC" );
        romanNumbersNotInMinimalFormPerValue.put( 900, "CCCCCCCCC" );
        romanNumbersNotInMinimalFormPerValue.put( 952, "DCCCCLII" );
        romanNumbersNotInMinimalFormPerValue.put( 944, "DCCCCXLIV" );
        romanNumbersNotInMinimalFormPerValue.put( 1414, "MCCCCXIV" );
        romanNumbersNotInMinimalFormPerValue.put( 1992, "MCMLXXXXII" );
        romanNumbersNotInMinimalFormPerValue.put( 2639, "MMDCXXXVIIII" );
        romanNumbersNotInMinimalFormPerValue.put( 3941, "MMMCCCCCCCCCXXXXI" );
        romanNumbersNotInMinimalFormPerValue.put( 3944, "MMMCMXXXXIIII" );
    }

    static {
        //Null or empty
        invalidRomanNumbers.add( null );
        invalidRomanNumbers.add( "" );

        //Has whitespaces
        invalidRomanNumbers.add( "         " );
        invalidRomanNumbers.add( "III " );
        invalidRomanNumbers.add( " MMC" );
        invalidRomanNumbers.add( "I I" );

        //Forbidden signs included
        invalidRomanNumbers.add( "abvsdui" );
        invalidRomanNumbers.add( "XX!" );
        invalidRomanNumbers.add( "iv" );
        invalidRomanNumbers.add( "m" );

        //Rule 1:
        //Numerals must be arranged in descending order of size.
        //Rule 2:
        //M, C, and X cannot be equalled or exceeded by smaller denominations.
        //Rule 3:
        //D, L, and V can each only appear once.

        //Rule 1 violations:
        invalidRomanNumbers.add( "IL" );
        invalidRomanNumbers.add( "IC" );
        invalidRomanNumbers.add( "ID" );
        invalidRomanNumbers.add( "IM" );
        invalidRomanNumbers.add( "VX" );
        invalidRomanNumbers.add( "VL" );
        invalidRomanNumbers.add( "VC" );
        invalidRomanNumbers.add( "VD" );
        invalidRomanNumbers.add( "VM" );
        invalidRomanNumbers.add( "XD" );
        invalidRomanNumbers.add( "XM" );
        invalidRomanNumbers.add( "LC" );
        invalidRomanNumbers.add( "LD" );
        invalidRomanNumbers.add( "LM" );
        invalidRomanNumbers.add( "IIIIX" );
        invalidRomanNumbers.add( "XVIIX" );
        invalidRomanNumbers.add( "DCLIIV" );
        invalidRomanNumbers.add( "MLIXXX" );
        invalidRomanNumbers.add( "IIIIX" );
        invalidRomanNumbers.add( "MVX" );
        invalidRomanNumbers.add( "CIL" );
        invalidRomanNumbers.add( "MMDCIIV" );
        invalidRomanNumbers.add( "CMM" );

        //Rule 2 violations:
        invalidRomanNumbers.add( "CCCCCCCCCC" );
        invalidRomanNumbers.add( "XXXXXXXXXX" );
        invalidRomanNumbers.add( "IIIIIIIIII" );
        invalidRomanNumbers.add(
                "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" );
        invalidRomanNumbers.add( "VIIIII" );
        invalidRomanNumbers.add( "CCCCCCCCCXXXXXXXXXX" );
        invalidRomanNumbers.add( "MDCCCCCC" );
        invalidRomanNumbers.add( "XXIIIIIIIIII" );
        invalidRomanNumbers.add( "LXXXXX" );
        invalidRomanNumbers.add( "CXIIIIIIIIII" );
        invalidRomanNumbers.add( "MLIIIIIIIIIIIIIIII" );
        invalidRomanNumbers.add( "CCCCCCCCXXXXXXXXXX" );
        invalidRomanNumbers.add( "IXIX" );
        invalidRomanNumbers.add( "XCXC" );
        invalidRomanNumbers.add( "CMCM" );
        invalidRomanNumbers.add( "CLIXIX" );
        invalidRomanNumbers.add( "MXCXC" );
        invalidRomanNumbers.add( "MMCMCM" );
        invalidRomanNumbers.add( "MCMC" );
        invalidRomanNumbers.add( "CMD" );

        //Rule 3 violations:
        invalidRomanNumbers.add( "CDCD" );
        invalidRomanNumbers.add( "XLXL" );
        invalidRomanNumbers.add( "IVIV" );
        invalidRomanNumbers.add( "DD" );
        invalidRomanNumbers.add( "LLL" );
        invalidRomanNumbers.add( "VVV" );
        invalidRomanNumbers.add( "MDCD" );
        invalidRomanNumbers.add( "CLXL" );
        invalidRomanNumbers.add( "VIV" );
        invalidRomanNumbers.add( "VVVI" );
        invalidRomanNumbers.add( "MCCLL" );
        invalidRomanNumbers.add( "MDDD" );
        invalidRomanNumbers.add( "CDLLV" );
        invalidRomanNumbers.add( "MDDXXII" );
        invalidRomanNumbers.add( "DLVVIII" );
        invalidRomanNumbers.add( "MMMMDCCLLV" );
        invalidRomanNumbers.add( "MCIVIV" );
        invalidRomanNumbers.add( "MCDCD" );
        invalidRomanNumbers.add( "CXLXL" );
    }

    private RomanNumberConverter converter = new RomanNumberConverter();

    private static Stream<Arguments> invalidInputForConvertingToRomanNumberData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -297162931 ),
                Arguments.of( IllegalArgumentException.class, -532 ),
                Arguments.of( IllegalArgumentException.class, -1 ),
                Arguments.of( IllegalArgumentException.class, 0 ),
                Arguments.of( IllegalArgumentException.class, MAX_ALLOWED_VALUE + 1 ),
                Arguments.of( IllegalArgumentException.class, Integer.MAX_VALUE )
        );
    }

    private static Collection<Object[]> convertingToRomanNumbersData() {
        Collection<Object[]> data = new ArrayList<>();

        for( int value : romanNumbersInMinimalFormPerValue.keySet() ) {
            data.add( new Object[]{ romanNumbersInMinimalFormPerValue.get( value ), value } );
        }

        return data;
    }

    private static Collection<Object[]> invalidInputForParsingValueData() {
        Collection<Object[]> data = new ArrayList<>();

        for( String invalidRomanNumber : invalidRomanNumbers ) {
            data.add( new Object[]{ IllegalArgumentException.class, invalidRomanNumber } );
        }

        return data;
    }

    private static Collection<Object[]> parsingValueFromValidRomanNumbersData() {
        List<Object[]> data = new ArrayList<>();

        for( int value : romanNumbersInMinimalFormPerValue.keySet() ) {
            data.add( new Object[]{ value, romanNumbersInMinimalFormPerValue.get( value ) } );
        }

        for( int value : romanNumbersNotInMinimalFormPerValue.keySet() ) {
            data.add( new Object[]{ value, romanNumbersNotInMinimalFormPerValue.get( value ) } );
        }

        return data;
    }

    private static Collection<Object[]> invalidRomanNumbersData() {
        Collection<Object[]> data = new ArrayList<>();

        for( String invalidRomanNumber : invalidRomanNumbers ) {
            data.add( new Object[]{ invalidRomanNumber } );
        }

        return data;
    }

    private static Collection<Object[]> validRomanNumbersData() {
        Collection<Object[]> data = new ArrayList<>();

        for( String romanNumber : romanNumbersInMinimalFormPerValue.values() ) {
            data.add( new Object[]{ romanNumber } );
        }

        for( String romanNumber : romanNumbersNotInMinimalFormPerValue.values() ) {
            data.add( new Object[]{ romanNumber } );
        }

        return data;
    }

    private static Collection<Object[]> minimalFormRomanNumbersData() {
        Collection<Object[]> data = new ArrayList<>();

        for( String romanNumberInMinimalForm : romanNumbersInMinimalFormPerValue.values() ) {
            data.add( new Object[]{ romanNumberInMinimalForm } );
        }

        return data;
    }

    private static Collection<Object[]> notInMinimalFormRomanNumbersData() {
        Collection<Object[]> data = new ArrayList<>();

        for( String invalidRomanNumber : invalidRomanNumbers ) {
            data.add( new Object[]{ invalidRomanNumber } );
        }

        for( String romanNumberNotInMinimalForm : romanNumbersNotInMinimalFormPerValue.values() ) {
            data.add( new Object[]{ romanNumberNotInMinimalForm } );
        }

        return data;
    }

    @ParameterizedTest( name = "{index}) Cannot convert {1} to roman number, raises an exception: {0}" )
    @MethodSource( "invalidInputForConvertingToRomanNumberData" )
    public void test_InvalidInput_ForConvertingToRomanNumber( Class<Throwable> throwableClass, int value ) {
        assertException( throwableClass, () -> converter.convertToRomanNumber( value ) );
    }

    @ParameterizedTest( name = "{index}) {1} is the roman number {0}" )
    @MethodSource( "convertingToRomanNumbersData" )
    public void test_ConvertingValueToRomanNumberIsCorrect( String expectedRomanNumber, int value ) {
        assertEquals( expectedRomanNumber, converter.convertToRomanNumber( value ) );
    }

    @ParameterizedTest( name = "{index}) Cannot parse value from roman number {1}, raises an exception: {0}" )
    @MethodSource( "invalidInputForParsingValueData" )
    public void test_InvalidInput_ForParsingValueFromRomanNumber( Class<Throwable> throwableClass,
            String romanNumber ) {
        assertException( throwableClass, () -> converter.parseValue( romanNumber ) );
    }

    @ParameterizedTest( name = "{index}) {1} has the value {0}" )
    @MethodSource( "parsingValueFromValidRomanNumbersData" )
    public void test_ParsingValueFromValidRomanNumberIsCorrect( int expectedValue, String romanNumber ) {
        assertEquals( expectedValue, converter.parseValue( romanNumber ) );
    }

    @ParameterizedTest( name = "{index}) {0} is an invalid roman number" )
    @MethodSource( "invalidRomanNumbersData" )
    public void test_InvalidRomanNumbers_AreCorrectlyRecognized( String romanNumber ) {
        assertFalse( converter.isValid( romanNumber ) );
    }

    @ParameterizedTest( name = "{index}) {0} is a valid roman number" )
    @MethodSource( "validRomanNumbersData" )
    public void test_ValidRomanNumbers_AreCorrectlyRecognized( String romanNumber ) {
        assertTrue( converter.isValid( romanNumber ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a roman number in minimal form" )
    @MethodSource( "notInMinimalFormRomanNumbersData" )
    public void test_NotInMinimalFormRomanNumbers_AreCorrectlyRecognized( String romanNumber ) {
        assertFalse( converter.isValidMinimalForm( romanNumber ) );
    }

    @ParameterizedTest( name = "{index}) {0} is a roman number in minimal form" )
    @MethodSource( "minimalFormRomanNumbersData" )
    public void test_MinimalFormRomanNumbers_AreCorrectlyRecognized( String romanNumber ) {
        assertTrue( converter.isValidMinimalForm( romanNumber ) );
    }

    @Test
    public void test_ConvertingBigValueToRomanNumber() {
        int value = 197123;

        int thousands = value / 1000;
        StringBuffer expectedRomanNumber = new StringBuffer( thousands + 6 );

        for( int i = 0; i < thousands; i++ ) {
            expectedRomanNumber.append( 'M' );
        }
        expectedRomanNumber.append( "CXXIII" );

        assertEquals( expectedRomanNumber.toString(), converter.convertToRomanNumber( value ) );
    }

    @Test
    public void test_MaxValueParsedFromRomanNumber_IsLimited() {
        int thousands = MAX_ALLOWED_VALUE / 1000;
        StringBuffer romanNumber = new StringBuffer( thousands + 1 );

        for( int i = 0; i < thousands; i++ ) {
            romanNumber.append( 'M' );
        }

        converter.parseValue( romanNumber.toString() );
        romanNumber.append( 'I' );
        assertException( IllegalArgumentException.class, () -> converter.parseValue( romanNumber.toString() ) );
    }
}
