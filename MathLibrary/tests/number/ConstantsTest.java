package number;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class ConstantsTest {
    @Test
    public void test_ConstantsInstanceCannotBeCreated() throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException {
        Constructor<Constants> constructor = Constants.class.getDeclaredConstructor();
        assertTrue( Modifier.isPrivate( constructor.getModifiers() ) );
        constructor.setAccessible( true );
        constructor.newInstance();
    }
}
