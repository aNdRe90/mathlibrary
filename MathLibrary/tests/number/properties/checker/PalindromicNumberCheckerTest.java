package number.properties.checker;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.util.stream.Stream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static testutils.ExceptionAssert.assertException;

public class PalindromicNumberCheckerTest {
    private PalindromicNumberChecker checker = new PalindromicNumberChecker();

    private static Stream<Arguments> palindromicNumbersData() {
        return Stream.of(
                Arguments.of( 0 ),
                Arguments.of( 1 ),
                Arguments.of( 5 ),
                Arguments.of( 9 ),
                Arguments.of( 22 ),
                Arguments.of( 373 ),
                Arguments.of( 9009 ),
                Arguments.of( 87678 ),
                Arguments.of( 162261 ),
                Arguments.of( 3567653 ),
                Arguments.of( 72311327 ),
                Arguments.of( 112454211 ),
                Arguments.of( 8736281331826378L ),
                Arguments.of( 927384950059483729L )
        );
    }

    private static Stream<Arguments> nonPalindromicNumbersData() {
        return Stream.of(
                Arguments.of( -834792341 ),
                Arguments.of( -2235 ),
                Arguments.of( -98 ),
                Arguments.of( -12433421 ),
                Arguments.of( -454 ),
                Arguments.of( -1 ),

                Arguments.of( 23 ),
                Arguments.of( 753 ),
                Arguments.of( 8293 ),
                Arguments.of( 12322 ),
                Arguments.of( 134421 ),
                Arguments.of( 8290630 ),
                Arguments.of( 67811234 ),
                Arguments.of( 209818702 ),
                Arguments.of( 7672812332282767L ),
                Arguments.of( 999888777777888909L )
        );
    }

    private static Stream<Arguments> bigPalindromicInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, null )
        );
    }

    private static Stream<Arguments> bigPalindromicNumbersData() {
        return Stream.of(
                Arguments.of( new BigInteger( "0" ) ),
                Arguments.of( new BigInteger( "1" ) ),
                Arguments.of( new BigInteger( "7" ) ),
                Arguments.of( new BigInteger( "8" ) ),
                Arguments.of( new BigInteger( "7928172399105019932718297" ) ),
                Arguments.of( new BigInteger( "829910923718334445029343343920544433817329019928" ) )
        );
    }

    private static Stream<Arguments> bigNonPalindromicNumbersData() {
        return Stream.of(
                Arguments.of( new BigInteger( "-932762348125356712531231235432" ) ),
                Arguments.of( new BigInteger( "-10912348023471283234712312312401111" ) ),
                Arguments.of( new BigInteger( "-333444555666777777666555444333" ) ),
                Arguments.of( new BigInteger( "-20000000000000000000000000000000002" ) ),
                Arguments.of( new BigInteger( "6091128394013293874723444" ) ),
                Arguments.of( new BigInteger( "492039182637817230234800382476623142618176236452" ) )
        );
    }

    @ParameterizedTest( name = "{index}) {0} is a palindromic number." )
    @MethodSource( "palindromicNumbersData" )
    public void test_PalindromicNumbersCorrectlyRecognized( long n ) {
        assertTrue( checker.isPalindromic( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a palindromic number." )
    @MethodSource( "nonPalindromicNumbersData" )
    public void test_NonPalindromicNumbersCorrectlyRecognized( long n ) {
        assertFalse( checker.isPalindromic( n ) );
    }

    @ParameterizedTest( name = "{index}) Cannot check if {1} is a palindromic number, raises an exception: {0}." )
    @MethodSource( "bigPalindromicInvalidInputData" )
    public void test_InvalidInputForBigPalindromicCheck( Class<Throwable> throwableClass, BigInteger n ) {
        assertException( throwableClass, () -> checker.isPalindromic( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is a palindromic number." )
    @MethodSource( "bigPalindromicNumbersData" )
    public void test_BigPalindromicNumbersCorrectlyRecognized( BigInteger n ) {
        assertTrue( checker.isPalindromic( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a palindromic number." )
    @MethodSource( "bigNonPalindromicNumbersData" )
    public void test_BigNonPalindromicNumbersCorrectlyRecognized( BigInteger n ) {
        assertFalse( checker.isPalindromic( n ) );
    }
}
