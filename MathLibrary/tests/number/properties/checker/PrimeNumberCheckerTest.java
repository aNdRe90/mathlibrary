package number.properties.checker;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PrimeNumberCheckerTest {
    private PrimeNumberChecker checker = new PrimeNumberChecker();

    private static Stream<Arguments> primeNumbersData() {
        return Stream.of(
                Arguments.of( 2 ),
                Arguments.of( 3 ),
                Arguments.of( 5 ),
                Arguments.of( 7 ),
                Arguments.of( 11 ),
                Arguments.of( 13 ),
                Arguments.of( 17 ),
                Arguments.of( 19 ),
                Arguments.of( 71 ),
                Arguments.of( 137 ),
                Arguments.of( 557 ),
                Arguments.of( 2473 ),
                Arguments.of( 8123 ),
                Arguments.of( 145307 ),
                Arguments.of( 568903 ),
                Arguments.of( 25488997 ),
                Arguments.of( 78203947 ),
                Arguments.of( 5638291807L ),
                Arguments.of( 9211622773L ),
                Arguments.of( 46372839217333L ),
                Arguments.of( 83928300016247L ),
                Arguments.of( 7463711192834579L ),
                Arguments.of( 9277383910263383L ),
                Arguments.of( 116382291637281223L ),
                Arguments.of( 689253617239447627L ),
                Arguments.of( 4611686018427388039L ),
                Arguments.of( 617281726152333811L )
        );
    }

    private static Stream<Arguments> nonPrimeNumbersData() {
        return Stream.of(
                Arguments.of( -982381114767234L ),
                Arguments.of( -823467876 ),
                Arguments.of( -13 ),
                Arguments.of( -7 ),
                Arguments.of( -2 ),
                Arguments.of( -1 ),

                Arguments.of( 0 ),
                Arguments.of( 1 ),

                Arguments.of( 4 ),
                Arguments.of( 6 ),
                Arguments.of( 27 ),
                Arguments.of( 51 ),
                Arguments.of( 221 ),
                Arguments.of( 513 ),
                Arguments.of( 2057 ),
                Arguments.of( 7471 ),
                Arguments.of( 372844 ),
                Arguments.of( 693001 ),
                Arguments.of( 23239928 ),
                Arguments.of( 89206371 ),
                Arguments.of( 1147294039L ),
                Arguments.of( 6920872136L ),
                Arguments.of( 29384611097389L ),
                Arguments.of( 11832989935472L ),
                Arguments.of( 3372829098783251L ),
                Arguments.of( 7772291027345923L ),
                Arguments.of( 473361100927381923L ),
                Arguments.of( 382294635217432957L ),

                Arguments.of( 1 * 1 ),
                Arguments.of( 3 * 3 ),
                Arguments.of( 13 * 13 ),
                Arguments.of( 309823 * 309823 ),
                Arguments.of( 14 * 14 ),
                Arguments.of( 1098 * 1098 ),
                Arguments.of( 211831L * 211831L ),
                Arguments.of( 802937812L * 802937812L )
        );
    }

    @ParameterizedTest( name = "{index}) {0} is a prime number." )
    @MethodSource( "primeNumbersData" )
    public void test_PrimeNumbersCorrectlyRecognized( long n ) {
        assertTrue( checker.isPrime( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a prime number." )
    @MethodSource( "nonPrimeNumbersData" )
    public void test_NonPrimeNumbersCorrectlyRecognized( long n ) {
        assertFalse( checker.isPrime( n ) );
    }
}
