package number.properties.checker;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static testutils.ExceptionAssert.assertException;

public class QuadraticResidueCheckerTest {
    private QuadraticResidueChecker checker = new QuadraticResidueChecker();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, 5, -6345340895739485L ),
                Arguments.of( IllegalArgumentException.class, 5, -322453453 ),
                Arguments.of( IllegalArgumentException.class, 5, -4 ),
                Arguments.of( IllegalArgumentException.class, 5, -1 ),
                Arguments.of( IllegalArgumentException.class, 5, 0 )
        );
    }

    private static Stream<Arguments> quadraticResiduesData() {
        return Stream.of(
                //Residue is equal to zero
                Arguments.of( 0, 2 ),
                Arguments.of( 0, 5 ),
                Arguments.of( 0, 13 ),
                Arguments.of( 0, 23 ),
                Arguments.of( 0, 145 ),
                Arguments.of( 0, 20 ),
                Arguments.of( 0, 963111 ),
                Arguments.of( 0, 283423400 ),

                //Modulo is equal to one
                Arguments.of( 11, 1 ),
                Arguments.of( 18, 1 ),
                Arguments.of( 171, 1 ),
                Arguments.of( 432434, 1 ),

                //Modulo is equal to two
                Arguments.of( 3, 2 ),
                Arguments.of( 4, 2 ),
                Arguments.of( 827349, 2 ),
                Arguments.of( 197345206, 2 ),

                //Modulo factors are powers
                Arguments.of( 475, 10625 ),
                Arguments.of( 94824, 3358092 ),
                Arguments.of( 6400900, 63368910 ),
                Arguments.of( 9601984, 69914016 ),
                Arguments.of( 106021136, 140608000 ),
                Arguments.of( 34545621, 2548836422L ),
                Arguments.of( 3694084, 1159171000 ),
                Arguments.of( 771126811, 3008241955L ),
                Arguments.of( 1500625, 5199665625L ),
                Arguments.of( 6853924, 346128873032173L ),

                //Residue is comprime with composite modulo
                Arguments.of( 3, 6 ),
                Arguments.of( 16, 64 ),
                Arguments.of( 121, 440 ),
                Arguments.of( 162429, 227774 ),
                Arguments.of( 9133, 24543 ),
                Arguments.of( 123025, 10000 ),
                Arguments.of( 720033111, 210369 ),
                Arguments.of( 350889, 5831 ),
                Arguments.of( 131039037, 7787 ),
                Arguments.of( 4294825, 1752048 ),

                //Modulo has power of 2 as factor
                Arguments.of( 1, 2 ),
                Arguments.of( 23454, 2 ),
                Arguments.of( 241, 4 ),
                Arguments.of( 93609577, 4 ),
                Arguments.of( 9, 8 ),
                Arguments.of( 15858513, 8 ),
                Arguments.of( 9, 16 ),
                Arguments.of( 17, 16 ),
                Arguments.of( 58313, 32 ),
                Arguments.of( 472982969, 256 ),
                Arguments.of( 100, 512 ),
                Arguments.of( 785426027721L, 1024 ),
                Arguments.of( 4, 2 ),
                Arguments.of( 4, 4 ),
                Arguments.of( 4, 8 ),
                Arguments.of( 4, 16 ),
                Arguments.of( 3, 6 ),
                Arguments.of( 4, 6 ),
                Arguments.of( 76, 136 ),
                Arguments.of( 96016, 156928 ),
                Arguments.of( 88211521, 93455360 ),

                //Residue is not coprime with composite modulo
                Arguments.of( 225, 4095 ),
                Arguments.of( 98829, 171804 ),
                Arguments.of( 29004, 81772 ),
                Arguments.of( 25, 98125 ),
                Arguments.of( 392, 6223 ),
                Arguments.of( 1156, 867 ),
                Arguments.of( 5881422, 5876661 ),
                Arguments.of( 22399254, 14337 ),
                Arguments.of( 295868186038L, 4330611 ),
                Arguments.of( 196209, 4381 ),

                //Residue is not coprime with prime modulo
                Arguments.of( 20, 2 ),
                Arguments.of( 7, 7 ),
                Arguments.of( 69, 23 ),
                Arguments.of( 4718, 337 ),

                //Residue is coprime with odd prime modulo
                Arguments.of( 9, 1091 ),
                Arguments.of( 441, 31721 ),
                Arguments.of( 40921, 84179 ),
                Arguments.of( 70912, 102299 ),
                Arguments.of( 657678898, 502349872309L ),
                Arguments.of( 882, 727 ),
                Arguments.of( 345873645, 61909 ),
                Arguments.of( 18345888488888888L, 83663 ),
                Arguments.of( 100329423, 97381 ),
                Arguments.of( 1109812301293184L, 72384428681L ),

                //Negative residues
                Arguments.of( -26, 7 ),
                Arguments.of( -12, 19 ),
                Arguments.of( -4398, 2087 ),
                Arguments.of( -2934, 7879 ),
                Arguments.of( -66, 102 ),
                Arguments.of( -9218, 9339 ),
                Arguments.of( -1034, 558 ),
                Arguments.of( -158256, 23163 )
        );
    }

    private static Stream<Arguments> quadraticNonResiduesData() {
        return Stream.of(
                //Modulo factors are powers
                Arguments.of( 187, 351 ),
                Arguments.of( 1323, 15435 ),
                Arguments.of( 880, 219615 ),
                Arguments.of( 75, 34300 ),
                Arguments.of( 1123111, 40518317160L ),
                Arguments.of( 14823, 731794257 ),
                Arguments.of( 816600, 193978125 ),
                Arguments.of( 620289, 6390217278L ),
                Arguments.of( 5113125, 24806250 ),
                Arguments.of( 9248, 454039808 ),

                //Residue is comprime with composite modulo
                Arguments.of( 10, 120 ),
                Arguments.of( 1680, 4032 ),
                Arguments.of( 1240, 8905 ),
                Arguments.of( 29603, 76122 ),
                Arguments.of( 847, 24565 ),
                Arguments.of( 203946, 962 ),
                Arguments.of( 2098123700, 103124 ),
                Arguments.of( 164625364, 57050525 ),
                Arguments.of( 375843376931L, 376100101 ),
                Arguments.of( 177008, 130625 ),

                //Modulo has power of 2 as factor
                Arguments.of( 2, 4 ),
                Arguments.of( 3, 4 ),
                Arguments.of( 11, 8 ),
                Arguments.of( 15, 8 ),
                Arguments.of( 34, 16 ),
                Arguments.of( 43, 16 ),
                Arguments.of( 8, 16 ),
                Arguments.of( 79, 32 ),
                Arguments.of( 90, 256 ),
                Arguments.of( 112, 512 ),
                Arguments.of( 2003, 1024 ),
                Arguments.of( 14, 272 ),
                Arguments.of( 5, 6 ),
                Arguments.of( 8, 32 ),
                Arguments.of( 3102, 60032 ),
                Arguments.of( 198273, 182016 ),
                Arguments.of( 272348, 19176448 ),

                //Residue is not coprime with composite modulo
                Arguments.of( 2, 20 ),
                Arguments.of( 66, 770 ),
                Arguments.of( 52, 1300 ),
                Arguments.of( 2397, 4029 ),
                Arguments.of( 1037, 8540 ),
                Arguments.of( 2479344709L, 1143574 ),
                Arguments.of( 92384671112L, 8762340 ),
                Arguments.of( 1230925, 243565 ),
                Arguments.of( 12300000000L, 45646725 ),
                Arguments.of( 237225109056L, 4077673 ),

                //Residue is coprime with odd prime modulo
                Arguments.of( 44, 193 ),
                Arguments.of( 456, 4721 ),
                Arguments.of( 9012, 13597 ),
                Arguments.of( 69100, 73417 ),
                Arguments.of( 845934853, 9287612141L ),
                Arguments.of( 345445, 19553 ),
                Arguments.of( 9830531, 25643 ),
                Arguments.of( 1092474421, 44273 ),
                Arguments.of( 80192378234871L, 60899 ),
                Arguments.of( 834823746172323L, 781293864341L ),

                //Negative residues
                Arguments.of( -2, 5 ),
                Arguments.of( -442, 131 ),
                Arguments.of( -396, 1801 ),
                Arguments.of( -4005, 4357 ),
                Arguments.of( -8, 14 ),
                Arguments.of( -10923, 2205 ),
                Arguments.of( -461273, 9875 ),
                Arguments.of( -5345, 91113 )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot check if {1} is a quadratic residue mod {2}, raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long a, long m ) {
        assertException( throwableClass, () -> checker.isQuadraticResidue( a, m ) );
    }

    @ParameterizedTest( name = "{index}) {0} is a quadratic residue mod {1}" )
    @MethodSource( "quadraticResiduesData" )
    public void test_QuadraticResiduesCorrectlyRecognized( long a, long m ) {
        assertTrue( checker.isQuadraticResidue( a, m ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a quadratic residue mod {1}" )
    @MethodSource( "quadraticNonResiduesData" )
    public void test_QuadraticNonResiduesCorrectlyRecognized( long a, long m ) {
        assertFalse( checker.isQuadraticResidue( a, m ) );
    }
}
