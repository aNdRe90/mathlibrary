package number.properties.checker;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static java.lang.Math.sqrt;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PolygonalNumberCheckerTest {
    private PolygonalNumberChecker checker = new PolygonalNumberChecker();

    private static Stream<Arguments> triangularNumbersData() {
        return Stream.of(
                Arguments.of( 1 ),
                Arguments.of( 10 ),
                Arguments.of( 378 ),
                Arguments.of( 1431 ),
                Arguments.of( 196878 ),
                Arguments.of( 15249003 )
        );
    }

    private static Stream<Arguments> nonTriangularNumbersData() {
        return Stream.of(
                Arguments.of( -367678678 ),
                Arguments.of( -4246 ),
                Arguments.of( -1 ),
                Arguments.of( 0 ),
                Arguments.of( 5 ),
                Arguments.of( 25 ),
                Arguments.of( 1027 ),
                Arguments.of( 4022 ),
                Arguments.of( 791948293 ),
                Arguments.of( Long.MAX_VALUE ),
                Arguments.of( Long.MAX_VALUE / 7 )
        );
    }

    private static Stream<Arguments> squareNumbersData() {
        return Stream.of(
                Arguments.of( 1 ),
                Arguments.of( 4 ),
                Arguments.of( 16 ),
                Arguments.of( 169 ),
                Arguments.of( 13689 ),
                Arguments.of( 314721 ),
                Arguments.of( 168948004 ),
                Arguments.of( (long) sqrt( Long.MAX_VALUE ) * (long) sqrt( Long.MAX_VALUE ) )
        );
    }

    private static Stream<Arguments> nonSquareNumbersData() {
        return Stream.of(
                Arguments.of( -932903493 ),
                Arguments.of( -5342 ),
                Arguments.of( -2 ),
                Arguments.of( 0 ),
                Arguments.of( 3 ),
                Arguments.of( 17 ),
                Arguments.of( 302 ),
                Arguments.of( 999123 ),
                Arguments.of( 839201832 ),
                Arguments.of( Long.MAX_VALUE ),
                Arguments.of( Long.MAX_VALUE / 3 )
        );
    }

    private static Stream<Arguments> pentagonalNumbersData() {
        return Stream.of(
                Arguments.of( 1 ),
                Arguments.of( 5 ),
                Arguments.of( 330 ),
                Arguments.of( 3151 ),
                Arguments.of( 2283517 )
        );
    }

    private static Stream<Arguments> nonPentagonalNumbersData() {
        return Stream.of(
                Arguments.of( -123212222 ),
                Arguments.of( -2355 ),
                Arguments.of( -3 ),
                Arguments.of( 0 ),
                Arguments.of( 4 ),
                Arguments.of( 13 ),
                Arguments.of( 1160 ),
                Arguments.of( 2888 ),
                Arguments.of( 82634812 ),
                Arguments.of( Long.MAX_VALUE ),
                Arguments.of( Long.MAX_VALUE / 23 )
        );
    }

    private static Stream<Arguments> hexagonalNumbersData() {
        return Stream.of(
                Arguments.of( 1 ),
                Arguments.of( 6 ),
                Arguments.of( 325 ),
                Arguments.of( 4560 ),
                Arguments.of( 3044278 )
        );
    }

    private static Stream<Arguments> nonHexagonalNumbersData() {
        return Stream.of(
                Arguments.of( -656575684 ),
                Arguments.of( -2334 ),
                Arguments.of( -3 ),
                Arguments.of( 0 ),
                Arguments.of( 5 ),
                Arguments.of( 14 ),
                Arguments.of( 1538 ),
                Arguments.of( 4370 ),
                Arguments.of( 3456786 ),
                Arguments.of( Long.MAX_VALUE ),
                Arguments.of( Long.MAX_VALUE / 7 )
        );
    }

    private static Stream<Arguments> heptagonalNumbersData() {
        return Stream.of(
                Arguments.of( 1 ),
                Arguments.of( 18 ),
                Arguments.of( 148 ),
                Arguments.of( 1782 ),
                Arguments.of( 935442 ),
                Arguments.of( 19707948 )
        );
    }

    private static Stream<Arguments> nonHeptagonalNumbersData() {
        return Stream.of(
                Arguments.of( -720192831 ),
                Arguments.of( -1179 ),
                Arguments.of( -3 ),
                Arguments.of( 0 ),
                Arguments.of( 5 ),
                Arguments.of( 31 ),
                Arguments.of( 1999 ),
                Arguments.of( 928311 ),
                Arguments.of( 820091823 ),
                Arguments.of( Long.MAX_VALUE ),
                Arguments.of( Long.MAX_VALUE / 39 )
        );
    }

    private static Stream<Arguments> octagonalNumbersData() {
        return Stream.of(
                Arguments.of( 1 ),
                Arguments.of( 40 ),
                Arguments.of( 341 ),
                Arguments.of( 4256 ),
                Arguments.of( 298936 ),
                Arguments.of( 72993601 )
        );
    }

    private static Stream<Arguments> nonOctagonalNumbersData() {
        return Stream.of(
                Arguments.of( -123566778 ),
                Arguments.of( -5563 ),
                Arguments.of( -4 ),
                Arguments.of( 0 ),
                Arguments.of( 6 ),
                Arguments.of( 44 ),
                Arguments.of( 1092 ),
                Arguments.of( 650345 ),
                Arguments.of( 880912368 ),
                Arguments.of( Long.MAX_VALUE ),
                Arguments.of( Long.MAX_VALUE / 11 )
        );
    }

    @ParameterizedTest( name = "{index}) {0} is a triangular number." )
    @MethodSource( "triangularNumbersData" )
    public void test_TriangularNumbersCorrectlyRecognized( long n ) {
        assertTrue( checker.isTriangular( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a triangular number." )
    @MethodSource( "nonTriangularNumbersData" )
    public void test_NonTriangularNumbersCorrectlyRecognized( long n ) {
        assertFalse( checker.isTriangular( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is a square number." )
    @MethodSource( "squareNumbersData" )
    public void test_SquareNumbersCorrectlyRecognized( long n ) {
        assertTrue( checker.isSquare( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a square number." )
    @MethodSource( "nonSquareNumbersData" )
    public void test_NonSquareNumbersCorrectlyRecognized( long n ) {
        assertFalse( checker.isSquare( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is a pentagonal number." )
    @MethodSource( "pentagonalNumbersData" )
    public void test_PentagonalNumbersCorrectlyRecognized( long n ) {
        assertTrue( checker.isPentagonal( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a pentagonal number." )
    @MethodSource( "nonPentagonalNumbersData" )
    public void test_NonPentagonalNumbersCorrectlyRecognized( long n ) {
        assertFalse( checker.isPentagonal( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is a hexagonal number." )
    @MethodSource( "hexagonalNumbersData" )
    public void test_HexagonalNumbersCorrectlyRecognized( long n ) {
        assertTrue( checker.isHexagonal( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a hexagonal number." )
    @MethodSource( "nonHexagonalNumbersData" )
    public void test_NonHexagonalNumbersCorrectlyRecognized( long n ) {
        assertFalse( checker.isHexagonal( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is a heptagonal number." )
    @MethodSource( "heptagonalNumbersData" )
    public void test_HeptagonalNumbersCorrectlyRecognized( long n ) {
        assertTrue( checker.isHeptagonal( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a heptagonal number." )
    @MethodSource( "nonHeptagonalNumbersData" )
    public void test_NonHeptagonalNumbersCorrectlyRecognized( long n ) {
        assertFalse( checker.isHeptagonal( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is a octagonal number." )
    @MethodSource( "octagonalNumbersData" )
    public void test_OctagonalNumbersCorrectlyRecognized( long n ) {
        assertTrue( checker.isOctagonal( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a octagonal number." )
    @MethodSource( "nonOctagonalNumbersData" )
    public void test_NonOctagonalNumbersCorrectlyRecognized( long n ) {
        assertFalse( checker.isOctagonal( n ) );
    }
}
