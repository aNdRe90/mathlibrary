package number.properties.checker;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MillerRabinDeterministicPrimeNumberCheckerTest {
    private MillerRabinDeterministicPrimeNumberChecker checker = new MillerRabinDeterministicPrimeNumberChecker();

    private static Stream<Arguments> primeNumbersData() {
        return Stream.of(
                Arguments.of( 2 ),
                Arguments.of( 3 ),
                Arguments.of( 5 ),
                Arguments.of( 7 ),
                Arguments.of( 11 ),
                Arguments.of( 13 ),
                Arguments.of( 17 ),
                Arguments.of( 19 ),
                Arguments.of( 23 ),
                Arguments.of( 29 ),
                Arguments.of( 31 ),
                Arguments.of( 37 ),
                Arguments.of( 41 ),
                Arguments.of( 71 ),
                Arguments.of( 137 ),
                Arguments.of( 257 ),
                Arguments.of( 557 ),
                Arguments.of( 2473 ),
                Arguments.of( 8123 ),
                Arguments.of( 65537 ),
                Arguments.of( 145307 ),
                Arguments.of( 568903 ),
                Arguments.of( 25488997 ),
                Arguments.of( 78203947 ),
                Arguments.of( 5638291807L ),
                Arguments.of( 9211622773L ),
                Arguments.of( 1000000000039L ),
                Arguments.of( 1000000000061L ),
                Arguments.of( 1000000000063L ),
                Arguments.of( 1000000000091L ),
                Arguments.of( 1000000000121L ),
                Arguments.of( 1000000000163L ),
                Arguments.of( 1000000000169L ),
                Arguments.of( 1000000000177L ),
                Arguments.of( 1000000000189L ),
                Arguments.of( 1000000000193L ),
                Arguments.of( 1000000000211L ),
                Arguments.of( 1000000000271L ),
                Arguments.of( 1000000000303L ),
                Arguments.of( 1000000000331L ),
                Arguments.of( 1000000000333L ),
                Arguments.of( 1000000000339L ),
                Arguments.of( 1000000000459L ),
                Arguments.of( 1000000000471L ),
                Arguments.of( 1000000000537L ),
                Arguments.of( 1000000000543L ),
                Arguments.of( 2000000000003L ),
                Arguments.of( 2000000000123L ),
                Arguments.of( 2000000000137L ),
                Arguments.of( 2000000000153L ),
                Arguments.of( 2000000000179L ),
                Arguments.of( 2000000000197L ),
                Arguments.of( 2000000000203L ),
                Arguments.of( 2000000000231L ),
                Arguments.of( 2000000000237L ),
                Arguments.of( 2000000000267L ),
                Arguments.of( 2000000000303L ),
                Arguments.of( 2000000000381L ),
                Arguments.of( 2000000000419L ),
                Arguments.of( 2000000000443L ),
                Arguments.of( 2000000000447L ),
                Arguments.of( 2000000000473L ),
                Arguments.of( 2000000000477L ),
                Arguments.of( 2000000000483L ),
                Arguments.of( 2000000000489L ),
                Arguments.of( 2000000000491L ),
                Arguments.of( 3000000000013L ),
                Arguments.of( 3000000000121L ),
                Arguments.of( 3000000000157L ),
                Arguments.of( 3000000000167L ),
                Arguments.of( 3000000000187L ),
                Arguments.of( 3000000000353L ),
                Arguments.of( 3000000000373L ),
                Arguments.of( 3000000000377L ),
                Arguments.of( 3000000000409L ),
                Arguments.of( 3000000000419L ),
                Arguments.of( 3000000000457L ),
                Arguments.of( 3000000000499L ),
                Arguments.of( 3000000000509L ),
                Arguments.of( 3000000000541L ),
                Arguments.of( 3000000000617L ),
                Arguments.of( 3000000000631L ),
                Arguments.of( 3000000000649L ),
                Arguments.of( 3000000000689L ),
                Arguments.of( 3000000000713L ),
                Arguments.of( 3000000000721L ),
                Arguments.of( 4000000000039L ),
                Arguments.of( 4000000000043L ),
                Arguments.of( 4000000000069L ),
                Arguments.of( 4000000000187L ),
                Arguments.of( 4000000000193L ),
                Arguments.of( 4000000000331L ),
                Arguments.of( 4000000000361L ),
                Arguments.of( 4000000000397L ),
                Arguments.of( 4000000000433L ),
                Arguments.of( 4000000000517L ),
                Arguments.of( 4000000000531L ),
                Arguments.of( 4000000000547L ),
                Arguments.of( 4000000000603L ),
                Arguments.of( 4000000000613L ),
                Arguments.of( 4000000000681L ),
                Arguments.of( 4000000000687L ),
                Arguments.of( 4000000000693L ),
                Arguments.of( 4000000000813L ),
                Arguments.of( 4000000000817L ),
                Arguments.of( 4000000000861L ),
                Arguments.of( 5000000000053L ),
                Arguments.of( 5000000000101L ),
                Arguments.of( 5000000000129L ),
                Arguments.of( 5000000000159L ),
                Arguments.of( 5000000000167L ),
                Arguments.of( 5000000000179L ),
                Arguments.of( 5000000000243L ),
                Arguments.of( 5000000000273L ),
                Arguments.of( 5000000000287L ),
                Arguments.of( 5000000000323L ),
                Arguments.of( 5000000000327L ),
                Arguments.of( 5000000000353L ),
                Arguments.of( 5000000000371L ),
                Arguments.of( 5000000000447L ),
                Arguments.of( 5000000000449L ),
                Arguments.of( 5000000000473L ),
                Arguments.of( 5000000000483L ),
                Arguments.of( 5000000000491L ),
                Arguments.of( 5000000000519L ),
                Arguments.of( 5000000000591L ),
                Arguments.of( 13334390952317L ),
                Arguments.of( 29065965967667L ),
                Arguments.of( 45678910111213L ),
                Arguments.of( 46372839217333L ),
                Arguments.of( 67280421310721L ),
                Arguments.of( 83928300016247L ),
                Arguments.of( 89888786858483L ),
                Arguments.of( 99999999944441L ),
                Arguments.of( 7463711192834579L ),
                Arguments.of( 9277383910263383L ),
                Arguments.of( 116382291637281223L ),
                Arguments.of( 689253617239447627L ),
                Arguments.of( 9223372036854774893L ),
                Arguments.of( 9223372036854775783L ), //greatest long prime

                Arguments.of( 997 ), //n < 2,047
                Arguments.of( 999499 ), //2,047 <= n < 1,373,653
                Arguments.of( 7891123 ), //1,373,653 <= n < 9,080,191
                Arguments.of( 18097997 ), //9,080,191 <= n < 25,326,001
                Arguments.of( 1999098307 ), //25,326,001 <= n < 3,215,031,751
                Arguments.of( 3690123431L ), //3,215,031,751 <= n < 4,759,123,141
                Arguments.of( 1098234789499L ), //4,759,123,141 <= n < 1,122,004,669,633
                Arguments.of( 2130876888563L ), //1,122,004,669,633 <= n < 2,152,302,898,747
                Arguments.of( 3333432567103L ), //2,152,302,898,747 <= n < 3,474,749,660,383
                Arguments.of( 234789678778019L ), //3,474,749,660,383 <= n < 341,550,071,728,321
                Arguments.of( 2988123567776283941L ), //341,550,071,728,321 <= n < 3,825,123,056,546,413,051
                Arguments.of( 8872634612357615287L ) //3,825,123,056,546,413,051 <= n
        );
    }

    private static Stream<Arguments> nonPrimeNumbersData() {
        return Stream.of(
                Arguments.of( -98781239762387413L ),
                Arguments.of( -1345345L ),
                Arguments.of( -12 ),
                Arguments.of( -5 ),
                Arguments.of( -1 ),

                Arguments.of( 0 ),
                Arguments.of( 1 ),

                Arguments.of( 4 ),
                Arguments.of( 6 ),
                Arguments.of( 27 ),
                Arguments.of( 51 ),
                Arguments.of( 221 ),
                Arguments.of( 513 ),
                Arguments.of( 2057 ),
                Arguments.of( 7471 ),
                Arguments.of( 372844 ),
                Arguments.of( 693001 ),
                Arguments.of( 23239928 ),
                Arguments.of( 89206371 ),
                Arguments.of( 1147294039 ),
                Arguments.of( 6920872136L ),
                Arguments.of( 1000000000001L ),
                Arguments.of( 1000000000002L ),
                Arguments.of( 1000000000003L ),
                Arguments.of( 1000000000004L ),
                Arguments.of( 1000000000005L ),
                Arguments.of( 1000000000006L ),
                Arguments.of( 1000000000007L ),
                Arguments.of( 1000000000008L ),
                Arguments.of( 1000000000009L ),
                Arguments.of( 1000000000010L ),
                Arguments.of( 1000000000011L ),
                Arguments.of( 1000000000012L ),
                Arguments.of( 1000000000013L ),
                Arguments.of( 1000000000014L ),
                Arguments.of( 1000000000015L ),
                Arguments.of( 1000000000016L ),
                Arguments.of( 1000000000017L ),
                Arguments.of( 1000000000018L ),
                Arguments.of( 1000000000019L ),
                Arguments.of( 1000000000020L ),
                Arguments.of( 2000000000001L ),
                Arguments.of( 2000000000002L ),
                Arguments.of( 2000000000004L ),
                Arguments.of( 2000000000005L ),
                Arguments.of( 2000000000006L ),
                Arguments.of( 2000000000007L ),
                Arguments.of( 2000000000008L ),
                Arguments.of( 2000000000009L ),
                Arguments.of( 2000000000010L ),
                Arguments.of( 2000000000011L ),
                Arguments.of( 2000000000012L ),
                Arguments.of( 2000000000013L ),
                Arguments.of( 2000000000014L ),
                Arguments.of( 2000000000015L ),
                Arguments.of( 2000000000016L ),
                Arguments.of( 2000000000017L ),
                Arguments.of( 2000000000018L ),
                Arguments.of( 2000000000019L ),
                Arguments.of( 2000000000020L ),
                Arguments.of( 2000000000021L ),
                Arguments.of( 3000000000001L ),
                Arguments.of( 3000000000002L ),
                Arguments.of( 3000000000003L ),
                Arguments.of( 3000000000004L ),
                Arguments.of( 3000000000005L ),
                Arguments.of( 3000000000006L ),
                Arguments.of( 3000000000007L ),
                Arguments.of( 3000000000008L ),
                Arguments.of( 3000000000009L ),
                Arguments.of( 3000000000010L ),
                Arguments.of( 3000000000011L ),
                Arguments.of( 3000000000012L ),
                Arguments.of( 3000000000014L ),
                Arguments.of( 3000000000015L ),
                Arguments.of( 3000000000016L ),
                Arguments.of( 3000000000017L ),
                Arguments.of( 3000000000018L ),
                Arguments.of( 3000000000019L ),
                Arguments.of( 3000000000020L ),
                Arguments.of( 3000000000021L ),
                Arguments.of( 4000000000001L ),
                Arguments.of( 4000000000002L ),
                Arguments.of( 4000000000003L ),
                Arguments.of( 4000000000004L ),
                Arguments.of( 4000000000005L ),
                Arguments.of( 4000000000006L ),
                Arguments.of( 4000000000007L ),
                Arguments.of( 4000000000008L ),
                Arguments.of( 4000000000009L ),
                Arguments.of( 4000000000010L ),
                Arguments.of( 4000000000011L ),
                Arguments.of( 4000000000012L ),
                Arguments.of( 4000000000013L ),
                Arguments.of( 4000000000014L ),
                Arguments.of( 4000000000015L ),
                Arguments.of( 4000000000016L ),
                Arguments.of( 4000000000017L ),
                Arguments.of( 4000000000018L ),
                Arguments.of( 4000000000019L ),
                Arguments.of( 4000000000020L ),
                Arguments.of( 5000000000001L ),
                Arguments.of( 5000000000002L ),
                Arguments.of( 5000000000003L ),
                Arguments.of( 5000000000004L ),
                Arguments.of( 5000000000005L ),
                Arguments.of( 5000000000006L ),
                Arguments.of( 5000000000007L ),
                Arguments.of( 5000000000008L ),
                Arguments.of( 5000000000009L ),
                Arguments.of( 5000000000010L ),
                Arguments.of( 5000000000011L ),
                Arguments.of( 5000000000012L ),
                Arguments.of( 5000000000013L ),
                Arguments.of( 5000000000014L ),
                Arguments.of( 5000000000015L ),
                Arguments.of( 5000000000016L ),
                Arguments.of( 5000000000017L ),
                Arguments.of( 5000000000018L ),
                Arguments.of( 5000000000019L ),
                Arguments.of( 5000000000020L ),
                Arguments.of( 11832989935472L ),
                Arguments.of( 12224233685942L ),
                Arguments.of( 18158961567721L ),
                Arguments.of( 29384611097389L ),
                Arguments.of( 36930553345551L ),
                Arguments.of( 50146955964105L ),
                Arguments.of( 62298863484143L ),
                Arguments.of( 67280421584898L ),
                Arguments.of( 3372829098783251L ),
                Arguments.of( 7772291027345923L ),
                Arguments.of( 382294635217432957L ),
                Arguments.of( 473361100927381923L ),
                Arguments.of( 4611686018427387904L ),

                //Special case when tested a is not relatively prime to n
                Arguments.of( 9052031 ), //9052031 = 292001 * 31

                Arguments.of( 1029 ),//n < 2,047
                Arguments.of( 529293 ), //2,047 <= n < 1,373,653
                Arguments.of( 4672881 ), //1,373,653 <= n < 9,080,191
                Arguments.of( 18999207 ), //9,080,191 <= n < 25,326,001
                Arguments.of( 789222113 ), //25,326,001 <= n < 3,215,031,751
                Arguments.of( 4987222105L ), //3,215,031,751 <= n < 4,759,123,141
                Arguments.of( 1023784930029L ), //4,759,123,141 <= n < 1,122,004,669,633
                Arguments.of( 1498372819221L ), //1,122,004,669,633 <= n < 2,152,302,898,747
                Arguments.of( 2890333928197L ), //2,152,302,898,747 <= n < 3,474,749,660,383
                Arguments.of( 287382299182739L ), //3,474,749,660,383 <= n < 341,550,071,728,321
                Arguments.of( 83929900192837465L ), //341,550,071,728,321 <= n < 3,825,123,056,546, 413,051
                Arguments.of( 7892348729736482731L ) //3,825,123,056,546,413,051 <= n
        );
    }

    @ParameterizedTest( name = "{index}) {0} is a prime number." )
    @MethodSource( "primeNumbersData" )
    public void test_PrimeNumbersCorrectlyRecognized( long n ) {
        assertTrue( checker.isPrime( n ) );
    }

    @ParameterizedTest( name = "{index}) {0} is not a prime number." )
    @MethodSource( "nonPrimeNumbersData" )
    public void test_NonPrimeNumbersCorrectlyRecognized( long n ) {
        assertFalse( checker.isPrime( n ) );
    }
}
