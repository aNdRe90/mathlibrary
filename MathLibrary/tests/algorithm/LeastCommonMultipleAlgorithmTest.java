package algorithm;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static testutils.CollectionUtils.listOfLongs;
import static testutils.ExceptionAssert.assertException;

public class LeastCommonMultipleAlgorithmTest {
    private LeastCommonMultipleAlgorithm algorithm = new LeastCommonMultipleAlgorithm();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, null ),
                Arguments.of( IllegalArgumentException.class, new long[]{} ),
                Arguments.of( IllegalArgumentException.class, new long[]{ 5 } ),

                Arguments.of( ArithmeticException.class, new long[]{ 6, Long.MIN_VALUE } ),
                Arguments.of( ArithmeticException.class, new long[]{ Long.MIN_VALUE, Long.MIN_VALUE } ),
                Arguments.of( ArithmeticException.class, new long[]{ 1, 88, Long.MIN_VALUE, 11 } ),
                Arguments.of( ArithmeticException.class, new long[]{ 9, 20, 43, Long.MIN_VALUE } ),
                Arguments.of( ArithmeticException.class, new long[]{ -292736423, -727898234, 82191 } ) //overflow
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //Contains zero values
                Arguments.of( 0, new long[]{ 0, 0 } ),
                Arguments.of( 0, new long[]{ 0, 0, 0, 0, 0, 0, 0 } ),
                Arguments.of( 0, new long[]{ 87, 0 } ),
                Arguments.of( 0, new long[]{ 0, 0, 13, 87392 } ),
                Arguments.of( 0, new long[]{ 0, 7234987, 0, 0, 1143 } ),
                Arguments.of( 0, new long[]{ 983, 15, 0, 837, 992 } ),
                Arguments.of( 0, new long[]{ 34, 0, 420102, 52983, 0, 0, 39967, 0, 0, 91092478 } ),

                //No overflow if the result fits the Long range
                Arguments.of( 85694613350834929L, new long[]{ -292736423, 85694613350834929L } ),

                //Negative values in input
                Arguments.of( 30, new long[]{ -2, 15 } ),
                Arguments.of( 9, new long[]{ 3, -9 } ),
                Arguments.of( 28, new long[]{ -4, -7 } ),

                //Positive values
                Arguments.of( 17, new long[]{ 1, 17 } ),
                Arguments.of( 10, new long[]{ 2, 5 } ),
                Arguments.of( 10, new long[]{ 2, 2, 5, 5, 5 } ),
                Arguments.of( 121, new long[]{ 11, 121 } ),
                Arguments.of( 21252, new long[]{ 3, 92, 11, 84 } ),
                Arguments.of( 3211461, new long[]{ 3, 11, 27, 121, 983 } ),
                Arguments.of( 987012000, new long[]{ 3, 6, 12, 24, 13, 171, 999, 12000 } ),
                Arguments.of( 3050896401240660000L, new long[]{ 2, 7, 19, 339, 2150, 3488, 315000, 481238 } ),
                Arguments.of( 2520, new long[]{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } ),
                Arguments.of( 232792560,
                              new long[]{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 } )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate lcm({1}), raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long[] values ) {
        assertException( throwableClass, () -> algorithm.getLeastCommonMultiple( values ) );
    }

    @ParameterizedTest( name = "{index}) lcm({1}) = {0}" )
    @MethodSource( "solutionExistsData" )
    public void test_LcmIsCalculatedCorrectly( long lcm, long[] values ) {
        assertEquals( lcm, algorithm.getLeastCommonMultiple( values ) );
    }

    @Test
    public void test_LcmIsCalculatedCorrectly_ForListInput() {
        assertException( IllegalArgumentException.class, () -> algorithm.getLeastCommonMultiple( (List<Long>) null ) );
        assertEquals( 77, algorithm.getLeastCommonMultiple( listOfLongs( 7, 11 ) ) );
    }
}
