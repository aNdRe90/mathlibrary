package algorithm;

import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class DivisorsAlgorithmMassTest {
    private DivisorsAlgorithm algorithm = new DivisorsAlgorithm();

    @Test
    public void test_MassTest_ReturnedDivisorsDivideTheNumber_AreInAscendingOrder_AndNoDuplicatesOccur() {
        for( long n = 370000; n <= 400000; n++ ) {
            List<Long> divisors = algorithm.getDivisors( n );
            assertAllGivenValuesDivideTheNumber( n, divisors );
            assertAscendingOrderOfUniqueValues( divisors );
        }
    }

    private void assertAllGivenValuesDivideTheNumber( long n, List<Long> values ) {
        for( long value : values ) {
            assertTrue( n % value == 0 );
        }
    }

    private void assertAscendingOrderOfUniqueValues( List<Long> values ) {
        Iterator<Long> iterator = values.iterator();
        long previousValue = iterator.next();

        while( iterator.hasNext() ) {
            long currentValue = iterator.next();
            assertTrue( currentValue > previousValue );
            previousValue = currentValue;
        }
    }
}
