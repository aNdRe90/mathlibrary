package algorithm;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class ExtendedEuclideanAlgorithmMassTest {
    private ExtendedEuclideanAlgorithm algorithm = new ExtendedEuclideanAlgorithm();

    @Test
    public void test_MassTest_CorrectBezoutsCoefficientsAndGCD() {
        for( long a = 10000; a < 11000; a++ ) {
            for( long b = 5; b < 705; b++ ) {
                assertGettingBezoutsCoefficientsWithGCDIsCorrect( a, b );
            }
        }
    }

    private void assertGettingBezoutsCoefficientsWithGCDIsCorrect( long a, long b ) {
        long[] bezoutsCoefficientsWithGCD = algorithm.getBezoutsCoefficientsWithGCD( a, b );
        assertNotNull( bezoutsCoefficientsWithGCD );
        assertEquals( 3, bezoutsCoefficientsWithGCD.length );

        long x = bezoutsCoefficientsWithGCD[0];
        long y = bezoutsCoefficientsWithGCD[1];
        long gcd = bezoutsCoefficientsWithGCD[2];

        BigInteger xa = BigInteger.valueOf( x ).multiply( BigInteger.valueOf( a ) );
        BigInteger yb = BigInteger.valueOf( y ).multiply( BigInteger.valueOf( b ) );

        assertEquals( gcd, xa.add( yb ).longValueExact() );
        assertTrue( a % gcd == 0 );
        assertTrue( b % gcd == 0 );
    }
}
