package algorithm;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static testutils.ExceptionAssert.assertException;

public class NextPrimeAlgorithmTest {
    private NextPrimeAlgorithm algorithm = new NextPrimeAlgorithm();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( ArithmeticException.class, Long.MAX_VALUE ),
                Arguments.of( ArithmeticException.class, Long.MAX_VALUE - 1 ),
                Arguments.of( ArithmeticException.class, Long.MAX_VALUE - 2 )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //Input value is below 2
                Arguments.of( 2, Long.MIN_VALUE ),
                Arguments.of( 2, -23498273481L ),
                Arguments.of( 2, -12 ),
                Arguments.of( 2, 0 ),
                Arguments.of( 2, 1 ),

                //Input value is a prime number
                Arguments.of( 3, 2 ),
                Arguments.of( 5, 3 ),
                Arguments.of( 7, 5 ),
                Arguments.of( 11, 7 ),
                Arguments.of( 13, 11 ),
                Arguments.of( 17, 13 ),
                Arguments.of( 19, 17 ),
                Arguments.of( 173, 167 ),
                Arguments.of( 29, 23 ),
                Arguments.of( 7237, 7229 ),
                Arguments.of( 62311, 62303 ),
                Arguments.of( 7235663, 7235633 ),
                Arguments.of( 48392111, 48392101 ),
                Arguments.of( 392017835271299L, 392017835271287L ),
                Arguments.of( 9223372036854774893L, 9223372036854774797L ),

                //Input value is a composite number
                Arguments.of( 5, 4 ),
                Arguments.of( 7, 6 ),
                Arguments.of( 11, 8 ),
                Arguments.of( 11, 9 ),
                Arguments.of( 11, 10 ),
                Arguments.of( 13, 12 ),
                Arguments.of( 17, 14 ),
                Arguments.of( 17, 15 ),
                Arguments.of( 17, 16 ),
                Arguments.of( 19, 18 ),
                Arguments.of( 29, 24 ),
                Arguments.of( 29, 25 ),
                Arguments.of( 29, 26 ),
                Arguments.of( 29, 27 ),
                Arguments.of( 29, 28 ),
                Arguments.of( 19, 18 ),
                Arguments.of( 127, 124 ),
                Arguments.of( 3491, 3481 ),
                Arguments.of( 82421, 82399 ),
                Arguments.of( 9246551, 9246520 ),
                Arguments.of( 122387, 122363 ),
                Arguments.of( 122387, 122364 ),
                Arguments.of( 122387, 122365 ),
                Arguments.of( 122387, 122366 ),
                Arguments.of( 122387, 122367 ),
                Arguments.of( 122387, 122368 ),
                Arguments.of( 122387, 122369 ),
                Arguments.of( 122387, 122370 ),
                Arguments.of( 122387, 122371 ),
                Arguments.of( 122387, 122372 ),
                Arguments.of( 122387, 122373 ),
                Arguments.of( 721029389, 721029376 ),
                Arguments.of( 293096357, 293096343 ),
                Arguments.of( 77728301263567L, 77728301263444L ),
                Arguments.of( 617281726152333811L, 617281726152333728L )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot find a prime number coming after {1}, raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long value ) {
        assertException( throwableClass, () -> algorithm.getNextPrime( value ) );
    }

    @ParameterizedTest( name = "{index}) Next prime coming after {1} is {0}" )
    @MethodSource( "solutionExistsData" )
    public void test_NextPrimeIsCalculatedCorrectly( long nextPrime, long value ) {
        assertEquals( nextPrime, algorithm.getNextPrime( value ) );
    }
}
