package algorithm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.addExact;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class NumberOfDivisiblesAlgorithmMassTest {
    private NumberOfDivisiblesAlgorithm algorithm = new NumberOfDivisiblesAlgorithm();

    @Test
    public void test_MassTest_CorrectlyCalculatedNumberOfDivisibles() {
        List<List<Long>> divisorsData = new ArrayList<>();
        divisorsData.add( asList( 1L ) );
        divisorsData.add( asList( 92L ) );
        divisorsData.add( asList( 8L, 3L, 18L ) );
        divisorsData.add( asList( -8L, -3L, 5L ) );
        divisorsData.add( asList( 32L, -398L, -136L, 55L ) );
        divisorsData.add( asList( 2L, 19L, 11L, 87L, 100L, 5L ) );

        for( int min = -100000; min < 100000; min = addExact( min, 537 ) ) {
            long max = addExact( min, 1000 );

            for( List<Long> divisors : divisorsData ) {
                long bruteForceResult = getBruteForceResult( min, max, divisors );
                assertEquals( bruteForceResult, algorithm.getNumberOfDivisibles( min, max, divisors ) );
            }
        }
    }

    private long getBruteForceResult( int min, long max, List<Long> divisors ) {
        long result = 0;

        for( long i = min; i <= max; i++ ) {
            for( long divisor : divisors ) {
                if( i % divisor == 0 ) {
                    result++;
                    break;
                }
            }
        }

        return result;
    }
}
