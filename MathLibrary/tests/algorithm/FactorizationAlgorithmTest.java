package algorithm;

import number.representation.FactorizationFactor;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static testutils.ExceptionAssert.assertException;

public class FactorizationAlgorithmTest {
    private FactorizationAlgorithm algorithm = new FactorizationAlgorithm();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -3 ),
                Arguments.of( IllegalArgumentException.class, 0 ),
                Arguments.of( IllegalArgumentException.class, 1 )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //Prime input values
                Arguments.of( asList( factor( 5, 1 ) ), 5 ),
                Arguments.of( asList( factor( 71, 1 ) ), 71 ),
                Arguments.of( asList( factor( 769, 1 ) ), 769 ),
                Arguments.of( asList( factor( 3701, 1 ) ), 3701 ),
                Arguments.of( asList( factor( 44887, 1 ) ), 44887 ),
                Arguments.of( asList( factor( 104543, 1 ) ), 104543 ),
                Arguments.of( asList( factor( 9384721, 1 ) ), 9384721 ),
                Arguments.of( asList( factor( 14562323, 1 ) ), 14562323 ),
                Arguments.of( asList( factor( 7382911241L, 1 ) ), 7382911241L ),
                Arguments.of( asList( factor( 4492019928373L, 1 ) ), 4492019928373L ),
                Arguments.of( asList( factor( 559001221823879L, 1 ) ), 559001221823879L ),
                Arguments.of( asList( factor( 882910283744632219L, 1 ) ), 882910283744632219L ),

                //Composite values with one prime factor as an input
                Arguments.of( asList( factor( 2, 2 ) ), 4 ),
                Arguments.of( asList( factor( 2, 4 ) ), 16 ),
                Arguments.of( asList( factor( 13, 2 ) ), 169 ),
                Arguments.of( asList( factor( 19, 3 ) ), 6859 ),
                Arguments.of( asList( factor( 101, 2 ) ), 10201 ),
                Arguments.of( asList( factor( 37, 4 ) ), 1874161 ),
                Arguments.of( asList( factor( 47, 5 ) ), 229345007 ),
                Arguments.of( asList( factor( 383, 6 ) ), 3156404426880769L ),
                Arguments.of( asList( factor( 983, 6 ) ), 902237984319995569L ),

                //Two big primes to the power of one as an input
                Arguments.of( asList( factor( 2638793, 1 ), factor( 6200191, 1 ) ),
                              16361020609463L ),
                Arguments.of( asList( factor( 92817269, 1 ), factor( 699481019, 1 ) ),
                              64923917900917111L ),
                Arguments.of( asList( factor( 558112349, 1 ), factor( 882715529, 1 ) ),
                              492654437388967621L ),
                Arguments.of( asList( factor( 119283013, 1 ), factor( 2910293887L, 1 ) ),
                              347148623556841531L ),
                Arguments.of( asList( factor( 1102991831, 1 ), factor( 7928117261L, 1 ) ),
                              8744648574093094891L ),

                //Other various composite input values
                Arguments.of( asList( factor( 2, 2 ), factor( 3, 1 ) ), 12 ),
                Arguments.of( asList( factor( 3, 1 ), factor( 7, 2 ) ), 147 ),
                Arguments.of( asList( factor( 5, 1 ), factor( 13, 2 ) ), 845 ),
                Arguments.of( asList( factor( 17, 1 ), factor( 419, 1 ) ), 7123 ),
                Arguments.of( asList( factor( 7, 1 ), factor( 37, 2 ) ), 9583 ),
                Arguments.of( asList( factor( 17, 1 ), factor( 31, 3 ) ), 506447 ),
                Arguments.of( asList( factor( 41, 2 ), factor( 83, 2 ) ), 11580409 ),
                Arguments.of( asList( factor( 383, 2 ), factor( 523, 1 ) ), 76718347 ),
                Arguments.of( asList( factor( 3121, 1 ), factor( 93601, 1 ) ), 292128721 ),
                Arguments.of( asList( factor( 829, 2 ), factor( 1721, 3 ) ), 3503093040142001L ),
                Arguments.of( asList( factor( 2671, 3 ), factor( 4157, 2 ) ), 329292404303034439L ),
                Arguments.of( asList( factor( 2, 3 ), factor( 3, 1 ), factor( 5, 1 ) ), 120 ),
                Arguments.of( asList( factor( 2, 1 ), factor( 3, 1 ), factor( 137, 1 ) ), 822 ),
                Arguments.of( asList( factor( 2, 3 ), factor( 3, 1 ), factor( 5, 5 ) ), 75000 ),
                Arguments.of( asList( factor( 7, 3 ), factor( 11, 1 ), factor( 17, 2 ) ), 1090397 ),
                Arguments.of( asList( factor( 5, 1 ), factor( 107, 1 ), factor( 35999, 1 ) ), 19259465 ),
                Arguments.of( asList( factor( 67, 3 ), factor( 73, 2 ), factor( 83, 1 ) ), 133029580241L ),
                Arguments.of( asList( factor( 53, 2 ), factor( 227, 2 ), factor( 3299, 1 ) ), 477513626339L ),
                Arguments.of( asList( factor( 1223, 2 ), factor( 1867, 2 ), factor( 15053, 1 ) ), 78481015078695893L ),
                Arguments.of( asList( factor( 7, 2 ), factor( 11, 1 ), factor( 17, 3 ), factor( 97, 1 ) ), 256866379 ),
                Arguments.of( asList( factor( 59, 1 ), factor( 73, 1 ), factor( 149, 2 ), factor( 223, 2 ) ),
                              4755072409403L ),
                Arguments.of( asList( factor( 31, 3 ), factor( 61, 1 ), factor( 197, 2 ), factor( 271, 1 ),
                                      factor( 331, 1 ) ),
                              6326225282786359L ),
                Arguments.of( asList( factor( 11, 2 ), factor( 17, 2 ), factor( 83, 4 ), factor( 113, 1 ),
                                      factor( 15137, 1 ) ),
                              2838662999436800569L ),
                Arguments.of( asList( factor( 3, 3 ), factor( 7, 4 ), factor( 19, 1 ), factor( 47, 2 ), factor( 67, 2 ),
                                      factor( 71, 1 ) ),
                              867187871444223L )
        );
    }

    private static FactorizationFactor factor( long prime, int exponent ) {
        return new FactorizationFactor( prime, exponent );
    }

    @ParameterizedTest( name = "{index}) Cannot factorize {1}, raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long value ) {
        assertException( throwableClass, () -> algorithm.getFactorization( value ) );
    }

    @ParameterizedTest( name = "{index}) Factorization of {1} is: {0}" )
    @MethodSource( "solutionExistsData" )
    public void test_FactorizationIsCalculatedCorrectly( List<FactorizationFactor> factorization, long value ) {
        assertEquals( factorization, algorithm.getFactorization( value ) );
    }
}
