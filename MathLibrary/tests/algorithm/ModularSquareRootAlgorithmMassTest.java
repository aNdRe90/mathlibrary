package algorithm;

import number.representation.ModularCongruence;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static util.ModuloOperations.mod;
import static util.ModuloOperations.multiplyModulo;

public class ModularSquareRootAlgorithmMassTest {
    private ModularSquareRootAlgorithm algorithm = new ModularSquareRootAlgorithm();

    @Test
    public void test_MassTest_CorrectModularSquareRoot() {
        for( long a = 500; a < 600; a++ ) {
            for( long modulo = 350; modulo < 550; modulo++ ) {
                assertCorrectModularSquareRoots( a, modulo );
            }
        }
    }

    private void assertCorrectModularSquareRoots( long a, long modulo ) {
        List<ModularCongruence> squareRoots = algorithm.getSquareRoots( a, modulo );

        int valuesToCheckPerCongruence = 5;
        for( ModularCongruence squareRoot : squareRoots ) {
            int valuesChecked = 0;

            for( long x : squareRoot ) {
                if( valuesChecked++ == valuesToCheckPerCongruence ) {
                    break;
                }

                assertEquals( mod( a, modulo ), multiplyModulo( x, x, modulo ) );
            }
        }
    }
}
