package algorithm;

import number.representation.FactorizationFactor;
import org.junit.Test;

import java.util.List;

import static java.lang.Math.multiplyExact;
import static org.junit.Assert.assertEquals;
import static util.BasicOperations.power;

public class PollardsRhoFactorizationAlgorithmMassTest {
    private PollardsRhoFactorizationAlgorithm algorithm = new PollardsRhoFactorizationAlgorithm();

    @Test
    public void test_MassTest_CorrectFactorization() {
        for( long n = 234871230000L; n <= 234871231000L; n++ ) {
            assertCorrectFactorization( n );
        }
    }

    private void assertCorrectFactorization( long n ) {
        List<FactorizationFactor> factorization = algorithm.getFactorization( n );

        long nFromFactorization = 1;
        for( FactorizationFactor actualFactorizationFactor : factorization ) {
            long prime = actualFactorizationFactor.getPrime();
            long exponent = actualFactorizationFactor.getExponent();
            nFromFactorization = multiplyExact( nFromFactorization, power( prime, exponent ) );
        }

        assertEquals( n, nFromFactorization );
    }
}
