package algorithm;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.assertArrayEquals;
import static testutils.ExceptionAssert.assertException;

public class ExtendedEuclideanAlgorithmTest {
    private ExtendedEuclideanAlgorithm algorithm = new ExtendedEuclideanAlgorithm();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, new long[]{ 0, -21 } ),
                Arguments.of( IllegalArgumentException.class, new long[]{ 14, 0 } ),
                Arguments.of( IllegalArgumentException.class, new long[]{ 0, 0 } ),

                Arguments.of( ArithmeticException.class, new long[]{ Long.MIN_VALUE, 112 } ),
                Arguments.of( ArithmeticException.class, new long[]{ -467, Long.MIN_VALUE } ),
                Arguments.of( ArithmeticException.class, new long[]{ Long.MIN_VALUE, Long.MIN_VALUE } )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //Coprime input values
                Arguments.of( new long[]{ -2L, -1L, 1L }, new long[]{ 3, -7 } ),
                Arguments.of( new long[]{ -21L, 44L, 1L }, new long[]{ 111, 53 } ),
                Arguments.of( new long[]{ -4511L, 134L, 1L }, new long[]{ 271, 9123 } ),
                Arguments.of( new long[]{ -470747L, 41721L, 1L }, new long[]{ 98473, 1111092 } ),
                Arguments.of( new long[]{ 37330460L, -646229L, 1L }, new long[]{ 1726538, 99736251 } ),
                Arguments.of( new long[]{ -1132661794529L, 92446240458L, 1L },
                              new long[]{ 728671231111L, 8927762342340L } ),

                //Input values whose gcd > 1
                Arguments.of( new long[]{ -9L, 47L, 2L }, new long[]{ 240, 46 } ),
                Arguments.of( new long[]{ 1L, 0L, 52L }, new long[]{ 52, 52 } ),
                Arguments.of( new long[]{ 1L, -2L, 7L }, new long[]{ -35, -21 } ),
                Arguments.of( new long[]{ 3L, 1L, 2L }, new long[]{ -12, 38 } ),
                Arguments.of( new long[]{ -9L, 47L, 2L }, new long[]{ 240, 46 } ),
                Arguments.of( new long[]{ 472L, 41L, 6L }, new long[]{ -972, 11190 } ),
                Arguments.of( new long[]{ 257L, 8993L, 2L }, new long[]{ 188888, -5398 } ),
                Arguments.of( new long[]{ 110043L, -11L, 29L }, new long[]{ 928, 9283625 } ),
                Arguments.of( new long[]{ -1L, 0L, 11L }, new long[]{ -11, 70092 } ),
                Arguments.of( new long[]{ 3932797L, -74097L, 3L }, new long[]{ 987222, 52398123 } ),
                Arguments.of( new long[]{ -18043148761639L, 1054941294994L, 62L },
                              new long[]{ 738293846734234L, 12627381039723402L } ),

                //Long edge input values
                Arguments.of( new long[]{ -1, 0, 1 }, new long[]{ -1, Long.MAX_VALUE } ),
                Arguments.of( new long[]{ 1, 0, 1 }, new long[]{ 1, Long.MAX_VALUE } ),
                Arguments.of( new long[]{ 1, 0, Long.MAX_VALUE }, new long[]{ Long.MAX_VALUE, Long.MAX_VALUE } ),
                Arguments.of( new long[]{ 1, 0, Long.MAX_VALUE - 1 },
                              new long[]{ Long.MAX_VALUE - 1, Long.MAX_VALUE - 1 } ),
                Arguments.of( new long[]{ -1, 0, Long.MAX_VALUE }, new long[]{ -Long.MAX_VALUE, -Long.MAX_VALUE } ),
                Arguments.of( new long[]{ -1, 0, Long.MAX_VALUE - 1 },
                              new long[]{ -Long.MAX_VALUE + 1, -Long.MAX_VALUE + 1 } ),
                Arguments.of( new long[]{ 5, 960767920505705812L, 1 }, new long[]{ -Long.MAX_VALUE + 12, 48 } ),
                Arguments.of( new long[]{ -779152, -2077389810082541L, 45 },
                              new long[]{ Long.MAX_VALUE - 7, -3459345345L } ),
                Arguments.of( new long[]{ -1, 0, Long.MAX_VALUE },
                              new long[]{ Long.MIN_VALUE + 1, Long.MIN_VALUE + 1 } ),
                Arguments.of( new long[]{ -1, 0, Long.MAX_VALUE }, new long[]{ Long.MIN_VALUE + 1, Long.MAX_VALUE } )
        );
    }

    @ParameterizedTest
            ( name = "{index}) Cannot calculate gcd with Bezout`s coefficient of {1}, raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long[] values ) {
        assertException( throwableClass, () -> algorithm.getBezoutsCoefficientsWithGCD( values[0], values[1] ) );
    }

    @ParameterizedTest( name = "{index}) [x,y,gcd] = {0} for values {1}" )
    @MethodSource( "solutionExistsData" )
    public void test_BezoutsCoefficientsWithGCD_AreCalculatedCorrectly(
            long[] bezoutsCoefficientsWithGCD, long[] values ) {
        assertArrayEquals( bezoutsCoefficientsWithGCD,
                           algorithm.getBezoutsCoefficientsWithGCD( values[0], values[1] ) );
    }
}
