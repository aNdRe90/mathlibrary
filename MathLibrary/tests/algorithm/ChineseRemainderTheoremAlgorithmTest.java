package algorithm;

import number.representation.ModularCongruence;
import number.representation.big.BigModularCongruence;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static testutils.ExceptionAssert.assertException;

public class ChineseRemainderTheoremAlgorithmTest {
    private ChineseRemainderTheoremAlgorithm algorithm = new ChineseRemainderTheoremAlgorithm();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, null ),
                Arguments.of( IllegalArgumentException.class, emptyList() )
        );
    }

    private static Stream<Arguments> noSolutionExistsData() {
        return Stream.of(
                Arguments.of( asList( modularCongruence( 1, 3 ), modularCongruence( 0, 27 ) ) ),
                Arguments.of( asList( modularCongruence( 700, 724 ), modularCongruence( 17, 72 ) ) ),
                Arguments.of( asList( modularCongruence( 123, 7345 ), modularCongruence( 92, 100 ),
                                      modularCongruence( 4092, 4532 ) ) ),
                Arguments.of(
                        asList( modularCongruence( 122, 412 ), modularCongruence( 1, 578 ),
                                modularCongruence( 4344, 15645 ) ) ),
                Arguments.of( asList( modularCongruence( 141, 234 ), modularCongruence( 6, 13 ),
                                      modularCongruence( 111, 555 ),
                                      modularCongruence( 2342, 43448760 ) ) ),
                Arguments.of( asList( modularCongruence( 36435, 621123 ), modularCongruence( 1225634, 5786789 ),
                                      modularCongruence( 4, 234 ), modularCongruence( 645565, 43448760 ) ) ),
                Arguments.of(
                        asList( modularCongruence( 4, 98230 ), modularCongruence( 5, 978655 ),
                                modularCongruence( 6, 1234 ),
                                modularCongruence( 7, 33 ), modularCongruence( 13, 41 ), modularCongruence( 15, 52 ),
                                modularCongruence( 17, 90235 ), modularCongruence( 19, 20 ) ) ),
                Arguments.of(
                        asList( modularCongruence( 2, 4 ), modularCongruence( 1, 98728 ),
                                modularCongruence( 34945, 122123 ),
                                modularCongruence( 645788, 345579087234L ), modularCongruence( 99810, 23412320 ),
                                modularCongruence( 123, 1235 ) ) )
        );
    }

    private static ModularCongruence modularCongruence( long residue, long modulo ) {
        return new ModularCongruence( residue, modulo );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //One of the input moduli is equal to one
                Arguments.of( bigModularCongruence( "0", "1" ),
                              asList( modularCongruence( 0, 1 ) ) ),
                Arguments.of( bigModularCongruence( "0", "1" ),
                              asList( modularCongruence( 5, 1 ) ) ),
                Arguments.of( bigModularCongruence( "3", "7" ),
                              asList( modularCongruence( 0, 1 ), modularCongruence( 3, 7 ) ) ),
                Arguments.of( bigModularCongruence( "18", "91" ),
                              asList( modularCongruence( 4, 7 ), modularCongruence( 0, 1 ),
                                      modularCongruence( 5, 13 ) ) ),
                Arguments.of( bigModularCongruence( "37", "60" ),
                              asList( modularCongruence( 1, 4 ), modularCongruence( 7, 15 ), modularCongruence( 1, 12 ),
                                      modularCongruence( 0, 1 ) ) ),

                //Input residues are greater than or equal to moduli
                Arguments.of( bigModularCongruence( "5", "6" ),
                              asList( modularCongruence( 7, 2 ), modularCongruence( 5, 3 ) ) ),
                Arguments.of( bigModularCongruence( "41", "455" ),
                              asList( modularCongruence( 1, 5 ), modularCongruence( 48, 7 ),
                                      modularCongruence( 15, 13 ) ) ),
                Arguments.of( bigModularCongruence( "62678", "108900" ),
                              asList( modularCongruence( 121, 121 ), modularCongruence( 28, 25 ),
                                      modularCongruence( 1982, 36 ) ) ),

                //Input residues are negative or zero
                Arguments.of( bigModularCongruence( "42", "85" ),
                              asList( modularCongruence( -3, 5 ), modularCongruence( -9, 17 ) ) ),
                Arguments.of( bigModularCongruence( "3", "21" ),
                              asList( modularCongruence( 0, 3 ), modularCongruence( 3, 7 ) ) ),
                Arguments.of( bigModularCongruence( "52", "861" ),
                              asList( modularCongruence( 1, 3 ), modularCongruence( -4, 7 ),
                                      modularCongruence( -30, 41 ) ) ),
                Arguments.of( bigModularCongruence( "514", "900" ),
                              asList( modularCongruence( -86, 100 ), modularCongruence( -26, 45 ) ) ),
                Arguments.of( bigModularCongruence( "68", "306" ),
                              asList( modularCongruence( 0, 34 ), modularCongruence( -85, 153 ) ) ),
                Arguments.of( bigModularCongruence( "15", "396" ),
                              asList( modularCongruence( -21, 36 ), modularCongruence( 3, 4 ),
                                      modularCongruence( -18, 33 ) ) ),

                //One input congruence
                Arguments.of( bigModularCongruence( "12", "13" ),
                              asList( modularCongruence( 12, 13 ) ) ),
                Arguments.of( bigModularCongruence( "413", "769" ),
                              asList( modularCongruence( 413, 769 ) ) ),
                Arguments.of( bigModularCongruence( "60093", "104287" ),
                              asList( modularCongruence( 60093, 104287 ) ) ),
                Arguments.of( bigModularCongruence( "17", "876334" ),
                              asList( modularCongruence( 17, 876334 ) ) ),
                Arguments.of( bigModularCongruence( "901", "2345235" ),
                              asList( modularCongruence( 901, 2345235 ) ) ),
                Arguments.of( bigModularCongruence( "8019230", "67892212" ),
                              asList( modularCongruence( 8019230, 67892212 ) ) ),

                //All input moduli are prime numbers
                Arguments.of( bigModularCongruence( "0", "6" ),
                              asList( modularCongruence( 0, 2 ), modularCongruence( 0, 3 ) ) ),
                Arguments.of( bigModularCongruence( "1343567", "1516901" ),
                              asList( modularCongruence( 998, 1063 ), modularCongruence( 760, 1427 ) ) ),
                Arguments.of( bigModularCongruence( "249281018", "2972626067" ),
                              asList( modularCongruence( 17992, 41641 ), modularCongruence( 69001, 71387 ) ) ),
                Arguments.of( bigModularCongruence( "49", "506" ),
                              asList( modularCongruence( 1, 2 ), modularCongruence( 5, 11 ),
                                      modularCongruence( 3, 23 ) ) ),
                Arguments.of( bigModularCongruence( "2982", "4071" ),
                              asList( modularCongruence( 0, 3 ), modularCongruence( 15, 23 ),
                                      modularCongruence( 32, 59 ) ) ),
                Arguments.of( bigModularCongruence( "214778", "530053" ),
                              asList( modularCongruence( 99, 137 ), modularCongruence( 12, 73 ),
                                      modularCongruence( 22, 53 ) ) ),
                Arguments.of( bigModularCongruence( "8894704576", "12023215021" ),
                              asList( modularCongruence( 3, 179 ), modularCongruence( 33, 239 ),
                                      modularCongruence( 333, 607 ),
                                      modularCongruence( 1, 463 ) ) ),
                Arguments.of( bigModularCongruence( "265592962224264", "2473560055553443" ),
                              asList( modularCongruence( 791, 7283 ), modularCongruence( 72, 659 ),
                                      modularCongruence( 1414, 5407 ), modularCongruence( 39012, 95317 ) ) ),
                Arguments.of( bigModularCongruence( "570030515562157765", "7831154390326635961" ),
                              asList( modularCongruence( 4, 95819 ), modularCongruence( 62091, 81023 ),
                                      modularCongruence( 20984, 26171 ), modularCongruence( 17105, 38543 ) ) ),
                Arguments.of( bigModularCongruence( "299513", "510510" ),
                              asList( modularCongruence( 1, 2 ), modularCongruence( 2, 3 ),
                                      modularCongruence( 3, 5 ), modularCongruence( 4, 7 ),
                                      modularCongruence( 5, 11 ), modularCongruence( 6, 13 ),
                                      modularCongruence( 7, 17 ) ) ),
                Arguments.of( bigModularCongruence( "85782901756000797", "206120415195370039" ),
                              asList( modularCongruence( 96, 97 ), modularCongruence( 99, 107 ),
                                      modularCongruence( 5, 7 ),
                                      modularCongruence( 890, 1697 ), modularCongruence( 782, 1291 ),
                                      modularCongruence( 11, 61 ), modularCongruence( 12, 71 ),
                                      modularCongruence( 10, 13 ), modularCongruence( 19, 23 ) ) ),
                Arguments.of( bigModularCongruence( "1665766972559428876074542970173022025",
                                                    "5106547112349142818959748116185699313" ),
                              asList( modularCongruence( 891, 4703 ), modularCongruence( 112, 4799 ),
                                      modularCongruence( 4900, 5023 ), modularCongruence( 5102, 5573 ),
                                      modularCongruence( 1000, 1499 ), modularCongruence( 1000, 1051 ),
                                      modularCongruence( 221, 337 ), modularCongruence( 8907, 15359 ),
                                      modularCongruence( 22897, 31477 ), modularCongruence( 19077, 31489 ) ) ),

                //All input moduli are pairwise coprime
                Arguments.of( bigModularCongruence( "0", "30" ),
                              asList( modularCongruence( 0, 2 ), modularCongruence( 0, 15 ) ) ),
                Arguments.of( bigModularCongruence( "38", "210" ),
                              asList( modularCongruence( 2, 6 ), modularCongruence( 3, 35 ) ) ),
                Arguments.of( bigModularCongruence( "299477202966565925", "974457431126397590" ),
                              asList( modularCongruence( 2334520, 987168535 ),
                                      modularCongruence( 4562345, 987123674 ) ) ),
                Arguments.of( bigModularCongruence( "15", "1716" ),
                              asList( modularCongruence( 4, 11 ), modularCongruence( 3, 12 ),
                                      modularCongruence( 2, 13 ) ) ),
                Arguments.of( bigModularCongruence( "203796642445", "1714613400360" ),
                              asList( modularCongruence( 709, 9864 ), modularCongruence( 10000, 11095 ),
                                      modularCongruence( 8772, 15667 ) ) ),
                Arguments.of( bigModularCongruence( "8005218426610221113627", "7592926000316924381247408" ),
                              asList( modularCongruence( 5772809, 7890123 ), modularCongruence( 11, 8762512 ),
                                      modularCongruence( 33987828746L, 109823876233L ) ) ),
                Arguments.of( bigModularCongruence( "3495144", "4453020" ),
                              asList( modularCongruence( 3, 13 ), modularCongruence( 12, 36 ),
                                      modularCongruence( 4, 55 ),
                                      modularCongruence( 25, 173 ) ) ),
                Arguments.of( bigModularCongruence( "3828763573877711", "139822059000529710" ),
                              asList( modularCongruence( 873, 7613 ), modularCongruence( 33091, 87830 ),
                                      modularCongruence( 2100, 3997 ), modularCongruence( 40997, 52317 ) ) ),
                Arguments.of( bigModularCongruence( "29075953863505281490078634696201225673753744720",
                                                    "111151505943584441882993435602564699430315641710" ),
                              asList( modularCongruence( 98734534, 190798679234L ),
                                      modularCongruence( 7872099100L, 6728349019223L ),
                                      modularCongruence( 87662000030L, 8766345876234235L ),
                                      modularCongruence( 123, 9876723L ) ) ),
                Arguments.of( bigModularCongruence( "71938573", "6339178846" ),
                              asList( modularCongruence( 0, 7 ), modularCongruence( 3, 22 ),
                                      modularCongruence( 13, 29 ),
                                      modularCongruence( 28, 37 ), modularCongruence( 5, 169 ),
                                      modularCongruence( 3, 227 ) ) ),
                Arguments.of( bigModularCongruence( "105587977272124444004", "314997796135873395930" ),
                              asList( modularCongruence( 99, 115 ), modularCongruence( 182, 222 ),
                                      modularCongruence( 0, 73 ),
                                      modularCongruence( 77, 199 ), modularCongruence( 12, 1091 ),
                                      modularCongruence( 699, 781 ), modularCongruence( 872, 907 ),
                                      modularCongruence( 901, 1099 ) ) ),
                Arguments.of( bigModularCongruence(
                        "16409454554765161811512504714604355239503255875859396672017665915515435165458",
                        "22892123425860160024551139742955006142836546257797169620471726656680277934202" ),
                              asList( modularCongruence( 28734, 9871231 ), modularCongruence( 7888192, 9876349 ),
                                      modularCongruence( 1098726, 1109813 ),
                                      modularCongruence( 344742356, 287923034522L ),
                                      modularCongruence( 86523481, 876123347 ),
                                      modularCongruence( 342367, 987238476127L ),
                                      modularCongruence( 1283, 123983459 ), modularCongruence( 4553456, 87123563 ),
                                      modularCongruence( 11241111, 78651211 ) ) ),

                //Input moduli are not pairwise coprime
                Arguments.of( bigModularCongruence( "1056972", "2367300" ),
                              asList( modularCongruence( 792, 7284 ), modularCongruence( 72, 650 ) ) ),
                Arguments.of( bigModularCongruence( "1296986", "1987380" ),
                              asList( modularCongruence( 1346, 10980 ), modularCongruence( 121, 905 ) ) ),
                Arguments.of( bigModularCongruence( "2450", "1074635" ),
                              asList( modularCongruence( 268, 1091 ), modularCongruence( 480, 985 ) ) ),
                Arguments.of( bigModularCongruence( "18935", "34320" ),
                              asList( modularCongruence( 7, 13 ), modularCongruence( 23, 48 ),
                                      modularCongruence( 15, 110 ) ) ),
                Arguments.of( bigModularCongruence( "111089605070791", "198627960385990" ),
                              asList( modularCongruence( 723, 68966 ), modularCongruence( 111, 124555 ),
                                      modularCongruence( 1431, 231230 ) ) ),
                Arguments.of( bigModularCongruence( "1159547790433", "1385784189946" ),
                              asList( modularCongruence( 123, 8762 ), modularCongruence( 2523, 51242 ),
                                      modularCongruence( 999, 12346 ) ) ),
                Arguments.of( bigModularCongruence( "7010496", "10189452" ),
                              asList( modularCongruence( 32, 124 ), modularCongruence( 34, 86 ),
                                      modularCongruence( 12, 156 ),
                                      modularCongruence( 66, 98 ) ) ),
                Arguments.of( bigModularCongruence( "318132382732329463", "458347171217161380" ),
                              asList( modularCongruence( 123, 8764 ), modularCongruence( 664, 771 ),
                                      modularCongruence( 1123, 98780 ), modularCongruence( 64987, 10987244 ) ) ),
                Arguments.of( bigModularCongruence( "1024772669936895091861", "3422484690694086109300" ),
                              asList( modularCongruence( 18721, 98123345 ), modularCongruence( 34457, 457612 ),
                                      modularCongruence( 8761, 12350 ), modularCongruence( 41129, 123434 ) ) ),
                Arguments.of( bigModularCongruence( "1025", "1200" ),
                              asList( modularCongruence( 1, 4 ), modularCongruence( 5, 12 ),
                                      modularCongruence( 225, 400 ) ) ),
                Arguments.of( bigModularCongruence( "1633", "2366" ),
                              asList( modularCongruence( 21, 26 ), modularCongruence( 450, 1183 ) ) ),
                Arguments.of( bigModularCongruence( "1", "16" ),
                              asList( modularCongruence( 1, 2 ), modularCongruence( 1, 4 ), modularCongruence( 1, 8 ),
                                      modularCongruence( 1, 16 ) ) ),
                Arguments.of( bigModularCongruence( "375", "847" ),
                              asList( modularCongruence( 4, 7 ), modularCongruence( 12, 121 ),
                                      modularCongruence( 67, 77 ) ) ),

                //Other
                Arguments.of( bigModularCongruence( "317", "1008" ),
                              asList( modularCongruence( 23, 42 ), modularCongruence( 29, 16 ),
                                      modularCongruence( 29, 18 ) ) ),
                Arguments.of( bigModularCongruence( "150011", "452760" ),
                              asList( modularCongruence( 11, 120 ), modularCongruence( 26, 33 ),
                                      modularCongruence( 120, 343 ) ) ),
                Arguments.of( bigModularCongruence( "12194", "19312" ),
                              asList( modularCongruence( 53, 71 ), modularCongruence( 124, 1207 ),
                                      modularCongruence( 226, 272 ) ) )
        );
    }

    private static BigModularCongruence bigModularCongruence( String residue, String modulo ) {
        return new BigModularCongruence( new BigInteger( residue ), new BigInteger( modulo ) );
    }

    @ParameterizedTest( name = "{index}) {1} is an incorrect input and raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, List<ModularCongruence> input ) {
        assertException( throwableClass, () -> algorithm.getSolution( input ) );
    }

    @ParameterizedTest( name = "{index}) Modular congruence system {0} has no solutions" )
    @MethodSource( "noSolutionExistsData" )
    public void test_NullIsReturned_WhenNoSolutionExists( List<ModularCongruence> input ) {
        assertNull( algorithm.getSolution( input ) );
    }

    @ParameterizedTest( name = "{index}) {0} solves the modular congruence system {1}" )
    @MethodSource( "solutionExistsData" )
    public void test_CorrectSolutionIsReturned( BigModularCongruence result, List<ModularCongruence> input ) {
        assertEquals( result, algorithm.getSolution( input ) );
    }
}
