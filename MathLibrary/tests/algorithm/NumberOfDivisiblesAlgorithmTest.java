package algorithm;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static testutils.CollectionUtils.listOfLongs;
import static testutils.ExceptionAssert.assertException;

public class NumberOfDivisiblesAlgorithmTest {
    private NumberOfDivisiblesAlgorithm algorithm = new NumberOfDivisiblesAlgorithm();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                //Incorrect ranges
                Arguments.of( IllegalArgumentException.class, 5, 3, listOfLongs( 1 ) ),
                Arguments.of( IllegalArgumentException.class, 1987, 0, listOfLongs( 1 ) ),
                Arguments.of( IllegalArgumentException.class, 0, -4, listOfLongs( 1 ) ),
                Arguments.of( IllegalArgumentException.class, -43, -333, listOfLongs( 1 ) ),

                //Nul divisors
                Arguments.of( IllegalArgumentException.class, 4, 7, null ),

                //Empty divisors
                Arguments.of( IllegalArgumentException.class, 100, 200, emptyList() ),

                //One of divisors is zero
                Arguments.of( IllegalArgumentException.class, 5, 15, listOfLongs( 0 ) ),
                Arguments.of( IllegalArgumentException.class, 5, 15, listOfLongs( 0, 1, 2 ) ),
                Arguments.of( IllegalArgumentException.class, 5, 15, listOfLongs( -500, 0, 14 ) ),

                //One of divisors is Long.MIN_VALUE (cannot make abs(Long.MIN_VALUE))
                Arguments.of( ArithmeticException.class, 432, 19093, listOfLongs( 9, 144, Long.MIN_VALUE ) ),

                //Overflow occurs when calculating min divisible in range
                Arguments.of( ArithmeticException.class, Long.MAX_VALUE, Long.MAX_VALUE, listOfLongs( 1, 5 ) ) );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //Range [0,0]
                Arguments.of( 1, 0, 0, listOfLongs( 5 ) ),
                Arguments.of( 1, 0, 0, listOfLongs( -2, 4 ) ),
                Arguments.of( 1, 0, 0, listOfLongs( -9, -23 ) ),
                Arguments.of( 1, 0, 0, listOfLongs( 7, 71, 8109 ) ),
                Arguments.of( 1, 0, 0, listOfLongs( 28729384234234L ) ),

                //Range has only 1 element
                Arguments.of( 1, 4, 4, listOfLongs( 2 ) ),
                Arguments.of( 1, 15, 15, listOfLongs( -3 ) ),
                Arguments.of( 0, -30, -30, listOfLongs( 7 ) ),
                Arguments.of( 0, 100, 100, listOfLongs( -11 ) ),

                //One divisor
                Arguments.of( 12346, 1, 12346, listOfLongs( 1 ) ),
                Arguments.of( 12, -42, -9, listOfLongs( 3 ) ),
                Arguments.of( 10, 13, 124, listOfLongs( -11 ) ),
                Arguments.of( 23127, -666, 346231, listOfLongs( 15 ) ),
                Arguments.of( 2, 100, 300, listOfLongs( -144 ) ),

                //Duplicated divisors
                Arguments.of( 5, 7, 16, listOfLongs( 7, 3, 3, 3, 7 ) ),
                Arguments.of( 3, -10, -5, listOfLongs( 2, -2, -2, 2, 2 ) ),
                Arguments.of( 10, 1, 20, listOfLongs( 3, 7, 10, 3, -10, 7, 7 ) ),
                Arguments.of( 15, -4, 20, listOfLongs( 2, 2, 5, 5, 5 ) ),

                //1 is among the divisors
                Arguments.of( 10, 2, 11, listOfLongs( 1, 11 ) ),
                Arguments.of( 80, -88, -9, listOfLongs( 1, 11 ) ),
                Arguments.of( 52265122, 77221, 52342342, listOfLongs( 1, 5, 6 ) ),
                Arguments.of( 8936, -112, 8823, listOfLongs( 1, 145, -987633 ) ),

                //Positive ranges
                Arguments.of( 6, 1, 10, listOfLongs( 2, 5 ) ),
                Arguments.of( 11, 4, 20, listOfLongs( 2, 5 ) ),
                Arguments.of( 49, 11, 100, listOfLongs( 3, 5, -11, 31 ) ),
                Arguments.of( 50, 112, 555, listOfLongs( 11, 42 ) ),
                Arguments.of( 34, 1200, 1500, listOfLongs( 11, 67, 122, 468, 512 ) ),
                Arguments.of( 152, 11074, 11500, listOfLongs( 5, 6, 22 ) ),
                Arguments.of( 29, 1, 80, listOfLongs( 5, 6, 22 ) ),
                Arguments.of( 276178, 11074, 782399, listOfLongs( 5, 6, 22, 831 ) ),
                Arguments.of( 7, 1, 10, listOfLongs( 2, 4, 8, 3, 6 ) ),
                Arguments.of( 102, 15, 198, listOfLongs( 2, -4, -9 ) ),
                Arguments.of( 914, 79, 1982, listOfLongs( 3, 7, 11, 15 ) ),
                Arguments.of( 623, 3450, 11100, listOfLongs( 15, -131, 262, 1545, -121 ) ),
                Arguments.of( 89, 1, 150, listOfLongs( 2, 7, 19, 140 ) ),
                Arguments.of( 187483, 1, 315000, listOfLongs( 2, -7, -19, -339, -2150, 3488, 315000 ) ),
                Arguments.of( 188077, 1, 316000, listOfLongs( 2, 7, 19, 339, 2150, 3488, 315000 ) ),
                Arguments.of( 909131831, 10, 10000000000L, listOfLongs( 11, 123123123, 222555 ) ),
                Arguments.of( 4, Long.MAX_VALUE - 3, Long.MAX_VALUE, listOfLongs( 1, 2 ) ),

                //Negative ranges
                Arguments.of( 5, -10, -3, listOfLongs( 2, 7, 13 ) ),
                Arguments.of( 2006, -4321, -23, listOfLongs( 4, 5, 9 ) ),
                Arguments.of( 0, -3, -1, listOfLongs( 17, 42 ) ),
                Arguments.of( 20089342, -987177231, -711772737, listOfLongs( 14, 829, 2341 ) ),
                Arguments.of( 1076382, -7762384796232L, -7762380000000L, listOfLongs( -5, 48, -101 ) ),

                //Positive-negative ranges (includes 0 as the divisible)
                Arguments.of( 1, -2, 5, listOfLongs( 7, 13 ) ),
                Arguments.of( 27, -41, 87, listOfLongs( 7, -13 ) ),
                Arguments.of( 20046, -23876, 38821, listOfLongs( 4, 17, -39, 87 ) ),
                Arguments.of( 24387, -100001, 0, listOfLongs( 5, -19, -441, 29872 ) ),
                Arguments.of( 60705, -98734, 12341, listOfLongs( 2, 19, 33, 85, 765, 1013 ) ),

                //Not ordered divisors
                Arguments.of( 1155, 187, 2000, listOfLongs( 5, 2, 11 ) ),

                //Overflow does not occur for big divisor values (their LCM exceeds Long.MAX_VALUE)
                Arguments.of( 2, 9, 19,
                              listOfLongs( 4L, 3333099912343L, 55593453401L, 74828342341L, 74828342359L, 999342823729L,
                                           999342823777L ) ),
                Arguments.of( 1061240846046915L, 423, Long.MAX_VALUE, listOfLongs( 9872, 717172, 129389, 399932,
                                                                                   873762, 1000000 ) ),
                Arguments.of( 4695263, 225, 7888999, listOfLongs( 2, 7, 19, 339, 2150, 3488, 315000, 2812381 ) ) );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate the number of values divisible by either divisor from {3} " +
                               "in range [{1}, {2}], raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long min, long max,
            List<Long> divisors ) {
        assertException( throwableClass, () -> algorithm.getNumberOfDivisibles( min, max, divisors ) );

    }

    @ParameterizedTest( name = "{index}) There are {0} values divisible by either divisor from {3} in range [{1}, {2}]" )
    @MethodSource( "solutionExistsData" )
    public void test_CombinationsAreCalculatedCorrectly( long expectedNumberOfValues, long min, long max,
            List<Long> divisors ) {
        assertEquals( expectedNumberOfValues, algorithm.getNumberOfDivisibles( min, max, divisors ) );
    }
}
