package algorithm;

import number.representation.FactorizationFactor;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static testutils.ExceptionAssert.assertException;

public class PollardsRhoFactorizationAlgorithmTest {
    private PollardsRhoFactorizationAlgorithm algorithm = new PollardsRhoFactorizationAlgorithm();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -3 ),
                Arguments.of( IllegalArgumentException.class, 0 )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //Input value is a prime number
                Arguments.of( asList( factor( 7, 1 ) ), 7 ),
                Arguments.of( asList( factor( 13, 1 ) ), 13 ),
                Arguments.of( asList( factor( 211, 1 ) ), 211 ),
                Arguments.of( asList( factor( 1609, 1 ) ), 1609 ),
                Arguments.of( asList( factor( 82997, 1 ) ), 82997 ),
                Arguments.of( asList( factor( 720991, 1 ) ), 720991 ),
                Arguments.of( asList( factor( 3920019281L, 1 ) ), 3920019281L ),
                Arguments.of( asList( factor( 9991823327L, 1 ) ), 9991823327L ),
                Arguments.of( asList( factor( 18299382612089L, 1 ) ), 18299382612089L ),
                Arguments.of( asList( factor( 38291028361876127L, 1 ) ), 38291028361876127L ),
                Arguments.of( asList( factor( 773920116435247289L, 1 ) ), 773920116435247289L ),

                //Input value is a prime power
                Arguments.of( asList( factor( 2, 2 ) ), 4 ),
                Arguments.of( asList( factor( 3, 3 ) ), 27 ),
                Arguments.of( asList( factor( 5, 3 ) ), 125 ),
                Arguments.of( asList( factor( 3, 8 ) ), 6561 ),
                Arguments.of( asList( factor( 2, 14 ) ), 16384 ),
                Arguments.of( asList( factor( 7, 8 ) ), 5764801 ),
                Arguments.of( asList( factor( 11, 11 ) ), 285311670611L ),
                Arguments.of( asList( factor( 13, 14 ) ), 3937376385699289L ),

                //Input value is a product of two big primes to the power of 1
                Arguments.of( asList( factor( 7792601, 1 ), factor( 79220003, 1 ) ), 617329874597803L ),
                Arguments.of( asList( factor( 6093389, 1 ), factor( 1092736499, 1 ) ), 6658468562905111L ),
                Arguments.of( asList( factor( 382990997, 1 ), factor( 738293401, 1 ) ), 282759725727510797L ),
                Arguments.of( asList( factor( 88916521, 1 ), factor( 11122315363L, 1 ) ), 988957587542812123L ),

                //Input value is a composite number with more than 1 factor
                Arguments.of( asList( factor( 2, 1 ), factor( 3, 1 ) ), 6 ),
                Arguments.of( asList( factor( 5, 2 ), factor( 7, 1 ) ), 175 ),
                Arguments.of( asList( factor( 11, 1 ), factor( 13, 1 ) ), 143 ),
                Arguments.of( asList( factor( 3, 3 ), factor( 11, 2 ) ), 3267 ),
                Arguments.of( asList( factor( 5, 4 ), factor( 19, 1 ) ), 11875 ),
                Arguments.of( asList( factor( 7, 3 ), factor( 23, 3 ) ), 4173281 ),
                Arguments.of( asList( factor( 29, 4 ), factor( 31, 5 ) ), 20248854548431L ),

                Arguments.of( asList( factor( 2, 3 ), factor( 3, 1 ), factor( 5, 1 ) ), 120 ),
                Arguments.of( asList( factor( 3, 1 ), factor( 5, 2 ), factor( 7, 2 ) ), 3675 ),
                Arguments.of( asList( factor( 3, 3 ), factor( 7, 2 ), factor( 11, 1 ) ), 14553 ),
                Arguments.of( asList( factor( 11, 2 ), factor( 17, 2 ), factor( 29, 1 ) ), 1014101 ),
                Arguments.of( asList( factor( 13, 3 ), factor( 23, 2 ), factor( 41, 2 ) ), 1953680053 ),
                Arguments.of( asList( factor( 53, 2 ), factor( 59, 1 ), factor( 61, 3 ) ), 37617788111L ),
                Arguments.of( asList( factor( 19, 5 ), factor( 67, 3 ), factor( 97, 2 ) ), 7007060727919633L ),

                Arguments.of( asList( factor( 163, 1 ), factor( 191, 3 ), factor( 431, 1 ), factor( 439, 1 ) ),
                              214896576358357L ),
                Arguments
                        .of( asList( factor( 2, 1 ), factor( 37, 1 ), factor( 9902437, 1 ), factor( 12586817029L, 1 ) ),
                             9223372036854775802L ),
                Arguments.of( asList( factor( 7, 1 ), factor( 19, 3 ), factor( 73, 1 ), factor( 79, 1 ),
                                      factor( 113, 2 ), factor( 223, 1 ) ),
                              788443440339877L ),
                Arguments.of( asList( factor( 7, 1 ), factor( 37, 2 ), factor( 83, 1 ), factor( 103, 1 ),
                                      factor( 151, 2 ), factor( 503, 1 ) ),
                              939590646691501L ),
                Arguments.of( asList( factor( 3, 3 ), factor( 7, 1 ), factor( 20261789, 1 ) ),
                              3829478121L )
        );
    }

    private static FactorizationFactor factor( long prime, int exponent ) {
        return new FactorizationFactor( prime, exponent );
    }

    @ParameterizedTest( name = "{index}) Cannot factorize {1}, raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long value ) {
        assertException( throwableClass, () -> algorithm.getFactorization( value ) );
    }

    @ParameterizedTest( name = "{index}) Factorization of {1} is: {0}" )
    @MethodSource( "solutionExistsData" )
    public void test_FactorizationIsCalculatedCorrectly( List<FactorizationFactor> factorization, long value ) {
        assertEquals( factorization, algorithm.getFactorization( value ) );
    }
}
