package algorithm.big;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.util.stream.Stream;

import static org.junit.Assert.assertArrayEquals;
import static testutils.ExceptionAssert.assertException;

public class BigExtendedEuclideanAlgorithmTest {
    private BigExtendedEuclideanAlgorithm algorithm = new BigExtendedEuclideanAlgorithm();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, new String[]{ null, "12" } ),
                Arguments.of( IllegalArgumentException.class, new String[]{ "5", null } ),
                Arguments.of( IllegalArgumentException.class, new String[]{ null, null } ),
                Arguments.of( IllegalArgumentException.class, new String[]{ "0", "6" } ),
                Arguments.of( IllegalArgumentException.class, new String[]{ "16", "0" } ),
                Arguments.of( IllegalArgumentException.class, new String[]{ "0", "0" } )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //Coprime input values
                Arguments.of( bigIntegerArray( "-1", "1", "1" ),
                              new String[]{ "2", "3" } ),
                Arguments.of( bigIntegerArray( "-2280", "18311", "1" ),
                              new String[]{ "89234", "11111" } ),
                Arguments.of( bigIntegerArray( "-1179424", "10632884698435670780851891", "1" ),
                              new String[]{ "90097187162314254234234234", "9993787" } ),

                Arguments.of( bigIntegerArray( "48844", "23", "1" ),
                              new String[]{ "-187", "397123" } ),
                Arguments.of( bigIntegerArray( "3239743449617359257976583", "154", "1" ),
                              new String[]{ "465", "-9782342234234234123111111" } ),
                Arguments.of( bigIntegerArray( "4287948256484661890423", "16389469920884501526466619", "1" ),
                              new String[]{ "-89992866167645512346453345", "23544675663237544742344" } ),

                //gcd(input values) > 1
                Arguments.of( bigIntegerArray( "1", "0", "823481398234701349823123" ),
                              new String[]{ "823481398234701349823123", "823481398234701349823123" } ),
                Arguments.of( bigIntegerArray( "-1953659742059951113106068841571307", "-152662291760252413", "2" ),
                              new String[]{ "7734212799999099222", "-98976767672349984573463547465233412" } ),
                Arguments.of( bigIntegerArray( "-2441491447313686268", "6430674939764735245297", "2" ),
                              new String[]{ "234125567423423564422222", "88888892039482384234" } ),
                Arguments.of( bigIntegerArray( "1", "0", "373" ),
                              new String[]{ "373", "-6341" } ),
                Arguments.of( bigIntegerArray( "19873", "19825", "492" ),
                              new String[]{ "897155604", "-899327784" } ),
                Arguments.of( bigIntegerArray( "-195251274", "270280153", "890790112" ),
                              new String[]{ "828299513630159776", "598366300650529312" } ),
                Arguments.of( bigIntegerArray( "115088358298009802", "8060173880981", "887665443" ),
                              new String[]{ "80048129758374357688689", "-1142978796084744846286145835" } )
        );
    }

    private static BigInteger[] bigIntegerArray( String x, String y, String gcd ) {
        BigInteger[] bigIntegerArray = new BigInteger[3];
        bigIntegerArray[0] = new BigInteger( x );
        bigIntegerArray[1] = new BigInteger( y );
        bigIntegerArray[2] = new BigInteger( gcd );
        return bigIntegerArray;
    }

    @ParameterizedTest
            ( name = "{index}) Cannot calculate gcd with Bezout`s coefficient of {1}, raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, String[] values ) {
        assertException( throwableClass, () -> algorithm
                .getBezoutsCoefficientsWithGCD( bigInteger( values[0] ), bigInteger( values[1] ) ) );
    }

    private BigInteger bigInteger( String value ) {
        return value != null ? new BigInteger( value ) : null;
    }

    @ParameterizedTest( name = "{index}) [x,y,gcd] = {0} for values {1}" )
    @MethodSource( "solutionExistsData" )
    public void test_BezoutsCoefficientsWithGCD_AreCalculatedCorrectly(
            BigInteger[] bezoutsCoefficientsWithGCD, String[] values ) {
        assertArrayEquals( bezoutsCoefficientsWithGCD,
                           algorithm
                                   .getBezoutsCoefficientsWithGCD( bigInteger( values[0] ), bigInteger( values[1] ) ) );
    }
}
