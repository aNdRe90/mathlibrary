package algorithm.big;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static testutils.ExceptionAssert.assertException;

public class BigLeastCommonMultipleAlgorithmTest {
    private BigLeastCommonMultipleAlgorithm algorithm = new BigLeastCommonMultipleAlgorithm();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, null ),
                Arguments.of( IllegalArgumentException.class, new String[]{ null } ),
                Arguments.of( IllegalArgumentException.class, new String[]{ "4", null } ),
                Arguments.of( IllegalArgumentException.class, new String[]{ null, "7" } ),
                Arguments.of( IllegalArgumentException.class, new String[]{ null, null } ),
                Arguments.of( IllegalArgumentException.class, new String[]{ null, null, "-14", null } ),

                Arguments.of( IllegalArgumentException.class, new String[]{} ),
                Arguments.of( IllegalArgumentException.class, new String[]{ "6" } )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //Any of input value is equal to 0
                Arguments.of( bigInteger( "0" ),
                              new String[]{ "0", "0" } ),
                Arguments.of( bigInteger( "0" ),
                              new String[]{ "0", "0", "0", "0", "0" } ),
                Arguments.of( bigInteger( "0" ),
                              new String[]{ "0", "11" } ),
                Arguments.of( bigInteger( "0" ),
                              new String[]{ "3", "0" } ),
                Arguments.of( bigInteger( "0" ),
                              new String[]{ "0", "0", "7", "-23457", "0" } ),

                //Negative input values
                Arguments.of( bigInteger( "234" ),
                              new String[]{ "234", "-13" } ),
                Arguments.of( bigInteger( "36478754610" ),
                              new String[]{ "45665", "-87", "9182" } ),
                Arguments.of( bigInteger( "10706424300" ),
                              new String[]{ "-89276", "-12300", "-234" } ),

                //Positive input values
                Arguments.of( bigInteger( "537203551754373152" ),
                              new String[]{ "23531234", "45658765856" } ),
                Arguments.of( bigInteger( "1233978747611235409674" ),
                              new String[]{ "13546467", "91092293482222" } ),

                Arguments.of( bigInteger( "61307959510920768" ),
                              new String[]{ "87123", "23423424", "90127" } ),
                Arguments.of( bigInteger( "13243495216708093456350" ),
                              new String[]{ "99375336596382", "425553263982", "3748587989373150" } ),

                Arguments.of( bigInteger( "4664793913605840300" ),
                              new String[]{ "13", "16900", "23498711", "23492634" } ),
                Arguments.of( bigInteger( "280168999751445412571943954502714891560" ),
                              new String[]{ "4603286648", "1787607", "16452898942014752977820", "64861380267212629" } ),

                Arguments.of( bigInteger( "10227017093672337504669319157637171919419983171778765407069915850000" ),
                              new String[]{ "9823478", "34523847611", "143245636", "23445654723423412313", "43545456",
                                      "34550000", "31320139009" } ),
                Arguments.of( bigInteger(
                        "7963796922337610640838010387336872233109062285695862980056352287236942131633991900" ),
                              new String[]{ "19547", "5825827653229818", "225679", "226824009950", "154640925576280492",
                                      "425549267818", "155020149", "910956364818", "164171876881254041717267818" } )
        );
    }

    private static BigInteger bigInteger( String value ) {
        return value != null ? new BigInteger( value ) : null;
    }

    @ParameterizedTest( name = "{index}) Cannot calculate lcm({1}), raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, String[] values ) {
        assertException( throwableClass, () -> algorithm.getLeastCommonMultiple( toBigIntegerArray( values ) ) );
    }

    private BigInteger[] toBigIntegerArray( String[] values ) {
        if( values == null ) {
            return null;
        }

        BigInteger[] bigIntegerArray = new BigInteger[values.length];

        for( int i = 0; i < values.length; i++ ) {
            bigIntegerArray[i] = bigInteger( values[i] );
        }

        return bigIntegerArray;
    }

    @ParameterizedTest( name = "{index}) lcm({1}) = {0}" )
    @MethodSource( "solutionExistsData" )
    public void test_LcmIsCalculatedCorrectly( BigInteger lcm, String[] values ) {
        assertEquals( lcm, algorithm.getLeastCommonMultiple( toBigIntegerArray( values ) ) );
    }

    @Test
    public void test_LcmIsCalculatedCorrectly_ForListInput() {
        assertException( IllegalArgumentException.class,
                         () -> algorithm.getLeastCommonMultiple( (List<BigInteger>) null ) );
        assertEquals( BigInteger.valueOf( 42 ),
                      algorithm.getLeastCommonMultiple( asList( BigInteger.valueOf( 6 ), BigInteger
                              .valueOf( 7 ) ) ) );
    }
}
