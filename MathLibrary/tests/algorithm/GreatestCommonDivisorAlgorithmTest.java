package algorithm;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static testutils.ExceptionAssert.assertException;

public class GreatestCommonDivisorAlgorithmTest {
    private GreatestCommonDivisorAlgorithm algorithm = new GreatestCommonDivisorAlgorithm();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, null ),
                Arguments.of( IllegalArgumentException.class, new long[]{} ),

                Arguments.of( IllegalArgumentException.class, new long[]{ 0 } ),
                Arguments.of( IllegalArgumentException.class, new long[]{ 3 } ),

                Arguments.of( IllegalArgumentException.class, new long[]{ 0, 0 } ),
                Arguments.of( IllegalArgumentException.class, new long[]{ 0, 0, 0, 0, 0 } ),

                Arguments.of( ArithmeticException.class, new long[]{ Long.MIN_VALUE, 5 } ),
                Arguments.of( ArithmeticException.class, new long[]{ Long.MIN_VALUE, Long.MIN_VALUE } ),
                Arguments.of( ArithmeticException.class, new long[]{ 1, 4, Long.MIN_VALUE, 5 } ),
                Arguments.of( ArithmeticException.class, new long[]{ 1, 0, 9, Long.MIN_VALUE } )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //Contains zero values
                Arguments.of( 7, new long[]{ 7, 0 } ),
                Arguments.of( 234, new long[]{ 0, 234L } ),
                Arguments.of( 1, new long[]{ 0, 155, 0, 11 } ),
                Arguments.of( 5, new long[]{ 160, 20, 0, 5, 0 } ),
                Arguments.of( 17, new long[]{ 34, 0, 1700, 918, 0, 0, 0, 39967 } ),

                //Positive coprime numbers
                Arguments.of( 1, new long[]{ 829123, 66291023129L } ),
                Arguments.of( 1, new long[]{ 1, 7, 14 } ),
                Arguments.of( 1, new long[]{ 2, 3, 8, 11, 12 } ),
                Arguments.of( 1, new long[]{ 12311, 844532, 128, 664, 232342 } ),
                Arguments.of( 1, new long[]{ 932148, 5708, 9212, 777, 155 } ),

                //Positive numbers with gcd > 1
                Arguments.of( 3, new long[]{ 3, 9 } ),
                Arguments.of( 2, new long[]{ 828, 99910 } ),
                Arguments.of( 3, new long[]{ 3, 3, 3, 9, 9 } ),
                Arguments.of( 2, new long[]{ 12, 4, 30 } ),
                Arguments.of( 16, new long[]{ 16, 256, 320, 1600, 20000 } ),
                Arguments.of( 2, new long[]{ 8, 18, 990, 3600, 6426, 998 } ),
                Arguments.of( 169, new long[]{ 676, 1014, 28561, 44616, 314171 } ),
                Arguments.of( 2, new long[]{ 44, 56, 88, 12, 190, 216, 234, 286, 2228 } ),
                Arguments.of( 4, new long[]{ 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8092, 16184 } ),
                Arguments.of( 51834, new long[]{ 103668, 155502, 1762356, 68731884, 26736702876L, 320840434512L,
                        285227146281168L } ),

                //Long edge values
                Arguments.of( Long.MAX_VALUE, new long[]{ Long.MAX_VALUE, Long.MAX_VALUE } ),
                Arguments.of( Long.MAX_VALUE, new long[]{ Long.MIN_VALUE + 1, Long.MAX_VALUE } ),
                Arguments.of( Long.MAX_VALUE, new long[]{ -Long.MAX_VALUE, -Long.MAX_VALUE } ),
                Arguments.of( 49, new long[]{ 9223372036854775807L, 11172839324234L } ),

                //Contains negative numbers
                Arguments.of( 1, new long[]{ -1, -17 } ),
                Arguments.of( 2, new long[]{ -2, 30 } ),
                Arguments.of( 13, new long[]{ 12831, -26, 13000 } ),
                Arguments.of( 7, new long[]{ -14, -21, 343 } )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate gcd({1}), raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long[] values ) {
        assertException( throwableClass, () -> algorithm.getGreatestCommonDivisor( values ) );
    }

    @ParameterizedTest( name = "{index}) gcd({1}) = {0}" )
    @MethodSource( "solutionExistsData" )
    public void test_GcdIsCalculatedCorrectly( long gcd, long[] values ) {
        assertEquals( gcd, algorithm.getGreatestCommonDivisor( values ) );
    }

    @Test
    public void test_GcdIsCalculatedCorrectly_ForListInput() {
        assertException( IllegalArgumentException.class,
                         () -> algorithm.getGreatestCommonDivisor( (List<Long>) null ) );
        assertEquals( 3, algorithm.getGreatestCommonDivisor( asList( 21L, 15L ) ) );
    }
}
