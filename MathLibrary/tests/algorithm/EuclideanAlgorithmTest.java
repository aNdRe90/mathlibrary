package algorithm;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static testutils.ExceptionAssert.assertException;

public class EuclideanAlgorithmTest {
    private EuclideanAlgorithm algorithm = new EuclideanAlgorithm();

    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, new long[]{ 0, 0 } ),

                Arguments.of( ArithmeticException.class, new long[]{ Long.MIN_VALUE, 7 } ),
                Arguments.of( ArithmeticException.class, new long[]{ 0, Long.MIN_VALUE } ),
                Arguments.of( ArithmeticException.class, new long[]{ 12, Long.MIN_VALUE } ),
                Arguments.of( ArithmeticException.class, new long[]{ Long.MIN_VALUE, Long.MIN_VALUE } )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //One of the input value is 0
                Arguments.of( 16, new long[]{ 16, 0 } ),
                Arguments.of( 9, new long[]{ -9, 0 } ),
                Arguments.of( 12987, new long[]{ 0, 12987 } ),
                Arguments.of( 511, new long[]{ 0, -511 } ),

                //Both input values are the same
                Arguments.of( 1, new long[]{ 1, 1 } ),
                Arguments.of( 287, new long[]{ -287, -287 } ),
                Arguments.of( 123, new long[]{ 123, 123 } ),
                Arguments.of( 34623234, new long[]{ 34623234, 34623234 } ),

                //Negative input values do not impact the result sign
                Arguments.of( 5, new long[]{ -5, 65 } ),
                Arguments.of( 1, new long[]{ 1928, -20965 } ),
                Arguments.of( 60, new long[]{ -660, -120 } ),

                //Input values are positive different coprime numbers
                Arguments.of( 1, new long[]{ 17, 13 } ),
                Arguments.of( 1, new long[]{ 737, 1092 } ),
                Arguments.of( 1, new long[]{ 7899, 122099 } ),
                Arguments.of( 1, new long[]{ 28889642, 876239281 } ),

                //Input values are positive different numbers whose gcd > 1
                Arguments.of( 15, new long[]{ 15, 105 } ),
                Arguments.of( 21, new long[]{ 1071, 462 } ),
                Arguments.of( 8, new long[]{ 301144, 945688 } ),
                Arguments.of( 33291, new long[]{ 932148, 57080049489L } ),
                Arguments.of( 57, new long[]{ 123321456654789L, 999888777666555L } ),

                //Long value edge input
                Arguments.of( Long.MAX_VALUE, new long[]{ Long.MAX_VALUE, Long.MAX_VALUE } ),
                Arguments.of( Long.MAX_VALUE, new long[]{ Long.MIN_VALUE + 1, Long.MAX_VALUE } ),
                Arguments.of( Long.MAX_VALUE, new long[]{ -Long.MAX_VALUE, -Long.MAX_VALUE } ),
                Arguments.of( 7, new long[]{ 9223372036854775807L, 62734728234234L } )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate gcd({1}), raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, long[] values ) {
        assertException( throwableClass, () -> algorithm.getGreatestCommonDivisor( values[0], values[1] ) );
    }

    @ParameterizedTest( name = "{index}) gcd({1}) = {0}" )
    @MethodSource( "solutionExistsData" )
    public void test_GcdIsCalculatedCorrectly( long gcd, long[] values ) {
        assertEquals( gcd, algorithm.getGreatestCommonDivisor( values[0], values[1] ) );
    }
}
