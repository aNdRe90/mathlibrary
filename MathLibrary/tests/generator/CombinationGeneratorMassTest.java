package generator;

import function.BinomialCoefficientFunction;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.util.Collections.min;
import static org.junit.Assert.*;

public class CombinationGeneratorMassTest {
    @Test
    public void test_MassTest_GeneratedCombinationsAreCorrect_InSizeAndLexicographicOrder() {
        for( int elementsSize = 1; elementsSize <= 10; elementsSize++ ) {
            List<Integer> elements = createIntegerElements( elementsSize );

            for( int combinationLength = 1; combinationLength <= elements.size(); combinationLength++ ) {
                testIfAllCombinationsAreCorrect( elements, combinationLength );
            }
        }
    }

    private List<Integer> createIntegerElements( int size ) {
        List<Integer> elements = new ArrayList<>( size );

        for( int i = 0; i < size; i++ ) {
            elements.add( i );
        }

        return elements;
    }

    private void testIfAllCombinationsAreCorrect( List<Integer> elements, int combinationLength ) {
        List<Integer> expectedFirstCombination = elements.subList( 0, combinationLength );
        List<Integer> expectedLastCombination = elements
                .subList( elements.size() - combinationLength, elements.size() );
        int expectedNumberOfCombinations = binomialCoefficient( elements.size(), combinationLength );

        Iterator<List<Integer>> combinationGeneratorIterator =
                new CombinationGenerator<>( elements, combinationLength ).iterator();

        assertTrue( combinationGeneratorIterator.hasNext() );
        assertEquals( expectedFirstCombination, combinationGeneratorIterator.next() );

        List<Integer> previousCombination = expectedFirstCombination;
        List<Integer> nextCombination;

        for( int i = 1; i < expectedNumberOfCombinations - 1; i++ ) {
            assertTrue( combinationGeneratorIterator.hasNext() );
            nextCombination = combinationGeneratorIterator.next();

            assertEquals( combinationLength, nextCombination.size() );
            assertNextCombinationIsNextInLexicographicOrder( previousCombination, nextCombination );
            previousCombination = nextCombination;
        }

        if( expectedNumberOfCombinations > 1 ) {
            assertTrue( combinationGeneratorIterator.hasNext() );
            assertEquals( expectedLastCombination, combinationGeneratorIterator.next() );
        }

        assertFalse( combinationGeneratorIterator.hasNext() );
    }

    private int binomialCoefficient( int n, int k ) {
        return new BinomialCoefficientFunction().getBinomialCoefficient( n, k ).intValueExact();
    }

    private void assertNextCombinationIsNextInLexicographicOrder( List<Integer> A, List<Integer> B ) {
        //A - previous combination
        //B - next combination

        //Combination A precedes B if the smallest value from A that does not appear in B is less than the smallest
        //value from B that does not appear in A.

        List<Integer> valuesOfANotAppearingInB = new ArrayList<>( A );
        valuesOfANotAppearingInB.removeAll( B );

        List<Integer> valuesOfBNotAppearingInA = new ArrayList<>( B );
        valuesOfBNotAppearingInA.removeAll( A );

        assertTrue( min( valuesOfANotAppearingInB ) < min( valuesOfBNotAppearingInA ) );
    }
}
