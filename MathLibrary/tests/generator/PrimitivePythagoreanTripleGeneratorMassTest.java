package generator;

import algorithm.GreatestCommonDivisorAlgorithm;
import org.junit.Test;

import java.util.List;

import static generator.PrimitivePythagoreanTripleGenerator.*;
import static java.lang.Math.addExact;
import static java.lang.Math.multiplyExact;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PrimitivePythagoreanTripleGeneratorMassTest {
    private GreatestCommonDivisorAlgorithm gcdAlgorithm = new GreatestCommonDivisorAlgorithm();

    @Test
    public void test_MassTest_CorrectlyGeneratedPrimitivePythagoreanTriples_ForLimitedMaxShortestSide() {
        for( int maxShortestSide = 500; maxShortestSide < 1000; maxShortestSide++ ) {
            for( List<Long> triple : createGeneratorForMaxShortestSide( maxShortestSide ) ) {
                assertEquals( 3, triple.size() );
                long a = triple.get( 0 );
                long b = triple.get( 1 );
                long c = triple.get( 2 );

                assertTrue( a <= maxShortestSide );
                assertCorrectPrimitivePythagoreanTriple( a, b, c );
            }
        }
    }

    private void assertCorrectPrimitivePythagoreanTriple( long a, long b, long c ) {
        assertTrue( a < b && b < c );
        assertEquals( multiplyExact( c, c ),
                      addExact( multiplyExact( a, a ), multiplyExact( b, b ) ) ); //c^2 = a^2 + b^2
        assertTrue( gcdAlgorithm.getGreatestCommonDivisor( a, b, c ) == 1 );
    }

    @Test
    public void test_MassTest_CorrectlyGeneratedPrimitivePythagoreanTriples_ForLimitedMaxHypotenuse() {
        for( int maxHypotenuse = 500; maxHypotenuse < 1000; maxHypotenuse++ ) {
            for( List<Long> triple : createGeneratorForMaxHypotenuse( maxHypotenuse ) ) {
                assertEquals( 3, triple.size() );
                long a = triple.get( 0 );
                long b = triple.get( 1 );
                long c = triple.get( 2 );

                assertTrue( c <= maxHypotenuse );
                assertCorrectPrimitivePythagoreanTriple( a, b, c );
            }
        }
    }

    @Test
    public void test_MassTest_CorrectlyGeneratedPrimitivePythagoreanTriples_ForLimitedMaxPerimeter() {
        for( int maxPerimeter = 500; maxPerimeter < 1000; maxPerimeter++ ) {
            for( List<Long> triple : createGeneratorForMaxPerimeter( maxPerimeter ) ) {
                assertEquals( 3, triple.size() );
                long a = triple.get( 0 );
                long b = triple.get( 1 );
                long c = triple.get( 2 );

                assertTrue( addExact( a, addExact( b, c ) ) <= maxPerimeter );
                assertCorrectPrimitivePythagoreanTriple( a, b, c );
            }
        }
    }

}
