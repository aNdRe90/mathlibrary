package generator;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Stream;

import static generator.PrimitivePythagoreanTripleGenerator.*;
import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static org.junit.Assert.*;
import static testutils.CollectionUtils.listOfLongs;
import static testutils.ExceptionAssert.assertException;

public class PrimitivePythagoreanTripleGeneratorTest {
    private static Stream<Arguments> maxShortestSideInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -6 ),
                Arguments.of( IllegalArgumentException.class, 0 )
        );
    }

    private static Stream<Arguments> maxHypotenuseInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -2 ),
                Arguments.of( IllegalArgumentException.class, 0 )
        );
    }

    private static Stream<Arguments> maxPerimeterInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -4 ),
                Arguments.of( IllegalArgumentException.class, 0 )
        );
    }

    private static Stream<Arguments> maxShortestSideSolutionExistsData() {
        return Stream.of(
                Arguments.of( emptySet(),
                              1 ),
                Arguments.of( emptySet(),
                              2 ),
                Arguments.of( new HashSet<>( asList( listOfLongs( 3, 4, 5 ) ) ),
                              3 ),
                Arguments.of( new HashSet<>( asList( listOfLongs( 3, 4, 5 ) ) ),
                              4 ),
                Arguments.of( new HashSet<>(
                                      asList( listOfLongs( 3, 4, 5 ), listOfLongs( 5, 12, 13 ), listOfLongs( 7, 24, 25 ),
                                              listOfLongs( 8, 15, 17 ), listOfLongs( 9, 40, 41 ),
                                              listOfLongs( 11, 60, 61 ), listOfLongs( 12, 35, 37 ),
                                              listOfLongs( 13, 84, 85 ) ) ),
                              13 ),
                Arguments.of( new HashSet<>(
                                      asList( listOfLongs( 3, 4, 5 ), listOfLongs( 5, 12, 13 ), listOfLongs( 7, 24, 25 ),
                                              listOfLongs( 8, 15, 17 ), listOfLongs( 9, 40, 41 ),
                                              listOfLongs( 11, 60, 61 ), listOfLongs( 12, 35, 37 ),
                                              listOfLongs( 13, 84, 85 ) ) ),
                              14 ),
                Arguments.of( new HashSet<>(
                                      asList( listOfLongs( 3, 4, 5 ), listOfLongs( 5, 12, 13 ), listOfLongs( 7, 24, 25 ),
                                              listOfLongs( 8, 15, 17 ), listOfLongs( 9, 40, 41 ),
                                              listOfLongs( 11, 60, 61 ), listOfLongs( 12, 35, 37 ),
                                              listOfLongs( 13, 84, 85 ), listOfLongs( 15, 112, 113 ),
                                              listOfLongs( 16, 63, 65 ), listOfLongs( 17, 144, 145 ),
                                              listOfLongs( 19, 180, 181 ), listOfLongs( 20, 21, 29 ),
                                              listOfLongs( 20, 99, 101 ), listOfLongs( 21, 220, 221 ),
                                              listOfLongs( 23, 264, 265 ), listOfLongs( 24, 143, 145 ),
                                              listOfLongs( 25, 312, 313 ), listOfLongs( 27, 364, 365 ),
                                              listOfLongs( 28, 45, 53 ), listOfLongs( 28, 195, 197 ),
                                              listOfLongs( 29, 420, 421 ), listOfLongs( 31, 480, 481 ),
                                              listOfLongs( 32, 255, 257 ), listOfLongs( 33, 544, 545 ),
                                              listOfLongs( 33, 56, 65 ), listOfLongs( 35, 612, 613 ),
                                              listOfLongs( 36, 77, 85 ), listOfLongs( 36, 323, 325 ),
                                              listOfLongs( 37, 684, 685 ), listOfLongs( 39, 80, 89 ),
                                              listOfLongs( 39, 760, 761 ), listOfLongs( 40, 399, 401 ),
                                              listOfLongs( 41, 840, 841 ), listOfLongs( 43, 924, 925 ),
                                              listOfLongs( 44, 117, 125 ), listOfLongs( 44, 483, 485 ),
                                              listOfLongs( 45, 1012, 1013 ), listOfLongs( 47, 1104, 1105 ),
                                              listOfLongs( 48, 55, 73 ), listOfLongs( 48, 575, 577 ),
                                              listOfLongs( 49, 1200, 1201 ), listOfLongs( 51, 140, 149 ),
                                              listOfLongs( 51, 1300, 1301 ), listOfLongs( 52, 165, 173 ),
                                              listOfLongs( 52, 675, 677 ), listOfLongs( 53, 1404, 1405 ),
                                              listOfLongs( 55, 1512, 1513 ), listOfLongs( 56, 783, 785 ),
                                              listOfLongs( 57, 176, 185 ), listOfLongs( 57, 1624, 1625 ),
                                              listOfLongs( 59, 1740, 1741 ), listOfLongs( 60, 91, 109 ),
                                              listOfLongs( 60, 221, 229 ), listOfLongs( 60, 899, 901 ),
                                              listOfLongs( 61, 1860, 1861 ), listOfLongs( 63, 1984, 1985 ),
                                              listOfLongs( 64, 1023, 1025 ), listOfLongs( 65, 72, 97 ),
                                              listOfLongs( 65, 2112, 2113 ), listOfLongs( 67, 2244, 2245 ),
                                              listOfLongs( 68, 285, 293 ), listOfLongs( 68, 1155, 1157 ),
                                              listOfLongs( 69, 260, 269 ), listOfLongs( 69, 2380, 2381 ),
                                              listOfLongs( 71, 2520, 2521 ), listOfLongs( 72, 1295, 1297 ),
                                              listOfLongs( 73, 2664, 2665 ), listOfLongs( 75, 308, 317 ),
                                              listOfLongs( 75, 2812, 2813 ), listOfLongs( 76, 357, 365 ),
                                              listOfLongs( 76, 1443, 1445 ), listOfLongs( 77, 2964, 2965 ),
                                              listOfLongs( 79, 3120, 3121 ), listOfLongs( 80, 1599, 1601 ),
                                              listOfLongs( 81, 3280, 3281 ), listOfLongs( 83, 3444, 3445 ),
                                              listOfLongs( 84, 187, 205 ), listOfLongs( 84, 437, 445 ),
                                              listOfLongs( 84, 1763, 1765 ), listOfLongs( 85, 132, 157 ),
                                              listOfLongs( 85, 3612, 3613 ), listOfLongs( 87, 416, 425 ),
                                              listOfLongs( 87, 3784, 3785 ), listOfLongs( 88, 105, 137 ),
                                              listOfLongs( 88, 1935, 1937 ), listOfLongs( 89, 3960, 3961 ),
                                              listOfLongs( 91, 4140, 4141 ), listOfLongs( 92, 525, 533 ),
                                              listOfLongs( 92, 2115, 2117 ), listOfLongs( 93, 476, 485 ),
                                              listOfLongs( 93, 4324, 4325 ), listOfLongs( 95, 168, 193 ),
                                              listOfLongs( 95, 4512, 4513 ), listOfLongs( 96, 247, 265 ),
                                              listOfLongs( 96, 2303, 2305 ), listOfLongs( 97, 4704, 4705 ),
                                              listOfLongs( 99, 4900, 4901 ), listOfLongs( 100, 621, 629 ),
                                              listOfLongs( 100, 2499, 2501 ) ) ),
                              100 )
        );
    }

    private static Stream<Arguments> maxHypotenuseSolutionExistsData() {
        return Stream.of(
                Arguments.of( emptySet(),
                              1 ),
                Arguments.of( emptySet(),
                              4 ),
                Arguments.of( new HashSet<>( asList( listOfLongs( 3, 4, 5 ) ) ),
                              5 ),
                Arguments.of( new HashSet<>( asList( listOfLongs( 3, 4, 5 ) ) ),
                              11 ),
                Arguments.of( new HashSet<>(
                                      asList( listOfLongs( 3, 4, 5 ), listOfLongs( 5, 12, 13 ), listOfLongs( 7, 24, 25 ),
                                              listOfLongs( 8, 15, 17 ), listOfLongs( 20, 21, 29 ) ) ),
                              30 ),
                Arguments.of( new HashSet<>(
                                      asList( listOfLongs( 3, 4, 5 ), listOfLongs( 5, 12, 13 ), listOfLongs( 7, 24, 25 ),
                                              listOfLongs( 8, 15, 17 ), listOfLongs( 20, 21, 29 ) ) ),
                              35 ),
                Arguments.of( new HashSet<>(
                                      asList( listOfLongs( 3, 4, 5 ), listOfLongs( 5, 12, 13 ), listOfLongs( 7, 24, 25 ),
                                              listOfLongs( 8, 15, 17 ), listOfLongs( 9, 40, 41 ),
                                              listOfLongs( 11, 60, 61 ), listOfLongs( 12, 35, 37 ),
                                              listOfLongs( 13, 84, 85 ), listOfLongs( 15, 112, 113 ),
                                              listOfLongs( 16, 63, 65 ), listOfLongs( 17, 144, 145 ),
                                              listOfLongs( 19, 180, 181 ), listOfLongs( 20, 21, 29 ),
                                              listOfLongs( 20, 99, 101 ), listOfLongs( 21, 220, 221 ),
                                              listOfLongs( 23, 264, 265 ), listOfLongs( 24, 143, 145 ),
                                              listOfLongs( 25, 312, 313 ), listOfLongs( 27, 364, 365 ),
                                              listOfLongs( 28, 45, 53 ), listOfLongs( 28, 195, 197 ),
                                              listOfLongs( 29, 420, 421 ), listOfLongs( 31, 480, 481 ),
                                              listOfLongs( 32, 255, 257 ), listOfLongs( 33, 56, 65 ),
                                              listOfLongs( 36, 77, 85 ), listOfLongs( 36, 323, 325 ),
                                              listOfLongs( 39, 80, 89 ), listOfLongs( 40, 399, 401 ),
                                              listOfLongs( 44, 117, 125 ), listOfLongs( 44, 483, 485 ),
                                              listOfLongs( 48, 55, 73 ), listOfLongs( 51, 140, 149 ),
                                              listOfLongs( 52, 165, 173 ), listOfLongs( 57, 176, 185 ),
                                              listOfLongs( 60, 91, 109 ), listOfLongs( 60, 221, 229 ),
                                              listOfLongs( 65, 72, 97 ), listOfLongs( 68, 285, 293 ),
                                              listOfLongs( 69, 260, 269 ), listOfLongs( 75, 308, 317 ),
                                              listOfLongs( 76, 357, 365 ), listOfLongs( 84, 187, 205 ),
                                              listOfLongs( 84, 437, 445 ), listOfLongs( 85, 132, 157 ),
                                              listOfLongs( 87, 416, 425 ), listOfLongs( 88, 105, 137 ),
                                              listOfLongs( 93, 476, 485 ), listOfLongs( 95, 168, 193 ),
                                              listOfLongs( 96, 247, 265 ), listOfLongs( 104, 153, 185 ),
                                              listOfLongs( 105, 208, 233 ), listOfLongs( 115, 252, 277 ),
                                              listOfLongs( 119, 120, 169 ), listOfLongs( 120, 209, 241 ),
                                              listOfLongs( 120, 391, 409 ), listOfLongs( 132, 475, 493 ),
                                              listOfLongs( 133, 156, 205 ), listOfLongs( 135, 352, 377 ),
                                              listOfLongs( 136, 273, 305 ), listOfLongs( 140, 171, 221 ),
                                              listOfLongs( 145, 408, 433 ), listOfLongs( 152, 345, 377 ),
                                              listOfLongs( 155, 468, 493 ), listOfLongs( 160, 231, 281 ),
                                              listOfLongs( 161, 240, 289 ), listOfLongs( 168, 425, 457 ),
                                              listOfLongs( 175, 288, 337 ), listOfLongs( 180, 299, 349 ),
                                              listOfLongs( 189, 340, 389 ), listOfLongs( 203, 396, 445 ),
                                              listOfLongs( 204, 253, 325 ), listOfLongs( 207, 224, 305 ),
                                              listOfLongs( 225, 272, 353 ), listOfLongs( 228, 325, 397 ),
                                              listOfLongs( 252, 275, 373 ), listOfLongs( 261, 380, 461 ),
                                              listOfLongs( 280, 351, 449 ), listOfLongs( 297, 304, 425 ),
                                              listOfLongs( 319, 360, 481 ) ) ),
                              500 )
        );
    }

    private static Stream<Arguments> maxPerimeterSolutionExistsData() {
        return Stream.of(
                Arguments.of( emptySet(),
                              1 ),
                Arguments.of( emptySet(),
                              11 ),
                Arguments.of( new HashSet<>( asList( listOfLongs( 3, 4, 5 ) ) ),
                              12 ),
                Arguments.of( new HashSet<>( asList( listOfLongs( 3, 4, 5 ) ) ),
                              27 ),
                Arguments.of( new HashSet<>(
                                      asList( listOfLongs( 3, 4, 5 ), listOfLongs( 5, 12, 13 ), listOfLongs( 7, 24, 25 ),
                                              listOfLongs( 8, 15, 17 ), listOfLongs( 20, 21, 29 ) ) ),
                              71 ),
                Arguments.of( new HashSet<>(
                                      asList( listOfLongs( 3, 4, 5 ), listOfLongs( 5, 12, 13 ), listOfLongs( 7, 24, 25 ),
                                              listOfLongs( 8, 15, 17 ), listOfLongs( 20, 21, 29 ) ) ),
                              83 ),
                Arguments.of( new HashSet<>(
                                      asList( listOfLongs( 3, 4, 5 ), listOfLongs( 5, 12, 13 ), listOfLongs( 7, 24, 25 ),
                                              listOfLongs( 8, 15, 17 ), listOfLongs( 9, 40, 41 ),
                                              listOfLongs( 11, 60, 61 ), listOfLongs( 12, 35, 37 ),
                                              listOfLongs( 13, 84, 85 ), listOfLongs( 15, 112, 113 ),
                                              listOfLongs( 16, 63, 65 ), listOfLongs( 17, 144, 145 ),
                                              listOfLongs( 19, 180, 181 ), listOfLongs( 20, 21, 29 ),
                                              listOfLongs( 20, 99, 101 ), listOfLongs( 21, 220, 221 ),
                                              listOfLongs( 23, 264, 265 ), listOfLongs( 24, 143, 145 ),
                                              listOfLongs( 25, 312, 313 ), listOfLongs( 27, 364, 365 ),
                                              listOfLongs( 28, 45, 53 ), listOfLongs( 28, 195, 197 ),
                                              listOfLongs( 29, 420, 421 ), listOfLongs( 31, 480, 481 ),
                                              listOfLongs( 32, 255, 257 ), listOfLongs( 33, 56, 65 ),
                                              listOfLongs( 33, 544, 545 ), listOfLongs( 36, 77, 85 ),
                                              listOfLongs( 36, 323, 325 ), listOfLongs( 39, 80, 89 ),
                                              listOfLongs( 40, 399, 401 ), listOfLongs( 44, 117, 125 ),
                                              listOfLongs( 44, 483, 485 ), listOfLongs( 48, 55, 73 ),
                                              listOfLongs( 48, 575, 577 ), listOfLongs( 51, 140, 149 ),
                                              listOfLongs( 52, 165, 173 ), listOfLongs( 57, 176, 185 ),
                                              listOfLongs( 60, 91, 109 ), listOfLongs( 60, 221, 229 ),
                                              listOfLongs( 65, 72, 97 ), listOfLongs( 68, 285, 293 ),
                                              listOfLongs( 69, 260, 269 ), listOfLongs( 75, 308, 317 ),
                                              listOfLongs( 76, 357, 365 ), listOfLongs( 84, 187, 205 ),
                                              listOfLongs( 84, 437, 445 ), listOfLongs( 85, 132, 157 ),
                                              listOfLongs( 87, 416, 425 ), listOfLongs( 88, 105, 137 ),
                                              listOfLongs( 92, 525, 533 ), listOfLongs( 93, 476, 485 ),
                                              listOfLongs( 95, 168, 193 ), listOfLongs( 96, 247, 265 ),
                                              listOfLongs( 104, 153, 185 ), listOfLongs( 105, 208, 233 ),
                                              listOfLongs( 115, 252, 277 ), listOfLongs( 119, 120, 169 ),
                                              listOfLongs( 120, 209, 241 ), listOfLongs( 120, 391, 409 ),
                                              listOfLongs( 132, 475, 493 ), listOfLongs( 133, 156, 205 ),
                                              listOfLongs( 135, 352, 377 ), listOfLongs( 136, 273, 305 ),
                                              listOfLongs( 140, 171, 221 ), listOfLongs( 145, 408, 433 ),
                                              listOfLongs( 152, 345, 377 ), listOfLongs( 155, 468, 493 ),
                                              listOfLongs( 160, 231, 281 ), listOfLongs( 161, 240, 289 ),
                                              listOfLongs( 168, 425, 457 ), listOfLongs( 175, 288, 337 ),
                                              listOfLongs( 180, 299, 349 ), listOfLongs( 189, 340, 389 ),
                                              listOfLongs( 203, 396, 445 ), listOfLongs( 204, 253, 325 ),
                                              listOfLongs( 207, 224, 305 ), listOfLongs( 217, 456, 505 ),
                                              listOfLongs( 220, 459, 509 ), listOfLongs( 225, 272, 353 ),
                                              listOfLongs( 228, 325, 397 ), listOfLongs( 252, 275, 373 ),
                                              listOfLongs( 261, 380, 461 ), listOfLongs( 280, 351, 449 ),
                                              listOfLongs( 297, 304, 425 ), listOfLongs( 319, 360, 481 ) ) ),
                              1200 )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot generate primitive triples for max shortest side = {1}, " +
                               "raises an exception: {0}" )
    @MethodSource( "maxShortestSideInvalidInputData" )
    public void test_InvalidInputForMaxShortestSide_RaisesAnException(
            Class<Throwable> throwableClass, long maxShortestSide ) {
        assertException( throwableClass, () -> createGeneratorForMaxShortestSide( maxShortestSide ) );
    }

    @ParameterizedTest( name = "{index}) Cannot generate primitive triples for max hypotenuse = {1}, " +
                               "raises an exception: {0}" )
    @MethodSource( "maxHypotenuseInvalidInputData" )
    public void test_InvalidInputForMaxHypotenuse_RaisesAnException(
            Class<Throwable> throwableClass, long maxHypotenuse ) {
        assertException( throwableClass, () -> createGeneratorForMaxHypotenuse( maxHypotenuse ) );
    }

    @ParameterizedTest( name = "{index}) Cannot generate primitive triples for max perimeter = {1}, " +
                               "raises an exception: {0}" )
    @MethodSource( "maxPerimeterInvalidInputData" )
    public void test_InvalidInputForMaxPerimeter_RaisesAnException(
            Class<Throwable> throwableClass, long maxPerimeter ) {
        assertException( throwableClass, () -> createGeneratorForMaxPerimeter( maxPerimeter ) );
    }

    @ParameterizedTest( name = "{index}) Primitive triples generated for max shortest side = {1} are: {0}" )
    @MethodSource( "maxShortestSideSolutionExistsData" )
    public void test_PrimitiveTriplesAreGeneratedCorrectly_ForMaxShortestSide(
            Set<List<Long>> expectedTriples, long maxShortestSide ) {
        assertEquals( expectedTriples, getGeneratedTriples( createGeneratorForMaxShortestSide( maxShortestSide ) ) );
    }

    private Set<List<Long>> getGeneratedTriples( PrimitivePythagoreanTripleGenerator generator ) {
        Set<List<Long>> triples = new HashSet<>();

        Iterator<List<Long>> generatorIterator = generator.iterator();
        while( generatorIterator.hasNext() ) {
            triples.add( generatorIterator.next() );
        }

        assertFalse( generatorIterator.hasNext() );
        assertException( NoSuchElementException.class, () -> generatorIterator.next() );

        return triples;
    }

    @ParameterizedTest( name = "{index}) Primitive triples generated for max hypotenuse = {1} are: {0}" )
    @MethodSource( "maxHypotenuseSolutionExistsData" )
    public void test_PrimitiveTriplesAreGeneratedCorrectly_ForMaxHypotenuse(
            Set<List<Long>> expectedTriples, long maxHypotenuse ) {
        assertEquals( expectedTriples, getGeneratedTriples( createGeneratorForMaxHypotenuse( maxHypotenuse ) ) );
    }

    @ParameterizedTest( name = "{index}) Primitive triples generated for max perimeter = {1} are: {0}" )
    @MethodSource( "maxPerimeterSolutionExistsData" )
    public void test_PrimitiveTriplesAreGeneratedCorrectly_ForMaxPerimeter(
            Set<List<Long>> expectedTriples, long maxPerimeter ) {
        assertEquals( expectedTriples, getGeneratedTriples( createGeneratorForMaxPerimeter( maxPerimeter ) ) );
    }

    @Test
    public void test_RuntimeException_WhenGettingNextTriple_WithUnknownMode() {
        assertException( RuntimeException.class,
                         () -> new PrimitivePythagoreanTripleGenerator( null, 3 ).iterator().next() );
    }

    @Test
    public void test_GeneratorDoesNotChangeState_WhenHasNextIsInvoked() {
        assertGeneratorDoesNotChangeStateWhenHasNextIsInvoked( createGeneratorForMaxShortestSide( 10 ) );
        assertGeneratorDoesNotChangeStateWhenHasNextIsInvoked( createGeneratorForMaxHypotenuse( 20 ) );
        assertGeneratorDoesNotChangeStateWhenHasNextIsInvoked( createGeneratorForMaxPerimeter( 40 ) );
    }

    private void assertGeneratorDoesNotChangeStateWhenHasNextIsInvoked(
            PrimitivePythagoreanTripleGenerator generator ) {
        Iterator<List<Long>> iterator = generator.iterator();

        for( int i = 0; i < 10; i++ ) {
            assertTrue( iterator.hasNext() );
        }

        while( iterator.hasNext() ) {
            iterator.next();
        }

        for( int i = 0; i < 10; i++ ) {
            assertFalse( iterator.hasNext() );
        }
    }

    @Test
    public void test_GeneratorCanBeReused() {
        Set<List<Long>> expectedTriples = new HashSet<>( asList( asList( 3L, 4L, 5L ), asList( 5L, 12L, 13L ) ) );

        PrimitivePythagoreanTripleGenerator generator1 = createGeneratorForMaxShortestSide( 5 );
        assertEquals( expectedTriples, getGeneratedTriples( generator1 ) );
        assertEquals( expectedTriples, getGeneratedTriples( generator1 ) );

        PrimitivePythagoreanTripleGenerator generator2 = createGeneratorForMaxHypotenuse( 13 );
        assertEquals( expectedTriples, getGeneratedTriples( generator2 ) );
        assertEquals( expectedTriples, getGeneratedTriples( generator2 ) );

        PrimitivePythagoreanTripleGenerator generator3 = createGeneratorForMaxPerimeter( 30 );
        assertEquals( expectedTriples, getGeneratedTriples( generator3 ) );
        assertEquals( expectedTriples, getGeneratedTriples( generator3 ) );
    }
}
