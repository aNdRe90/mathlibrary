package generator;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.sqrt;
import static org.junit.Assert.assertTrue;

public class PrimeGeneratorMassTest {
    @Test
    public void test_MassTest_InfiniteGeneratorProducesIncreasingPrimes() {
        Set<Long> generatedPrimes = new HashSet<>();
        int primesToGenerate = 50000;

        long previousPrime = -1;
        int primesGenerated = 0;

        for( long prime : new PrimeGenerator() ) {
            assertTrue( isPrime( prime, generatedPrimes ) );
            assertTrue( prime > previousPrime );

            if( primesGenerated++ == primesToGenerate ) {
                break;
            }

            generatedPrimes.add( prime );
            previousPrime = prime;
        }
    }

    private boolean isPrime( long prime, Set<Long> generatedPrimes ) {
        for( long generatedPrime : generatedPrimes ) {
            long primeCheckLimit = (long) sqrt( prime );

            if( generatedPrime > primeCheckLimit ) {
                break;
            }

            if( prime % generatedPrime == 0 ) {
                return false;
            }
        }

        return true;
    }

    @Test
    public void test_MassTest_GeneratorProducesIncreasingPrimes_WithUpperLimit() {
        Set<Long> generatedPrimes = new HashSet<>();
        int maxPrime = 500000;

        long primeCheckLimit = (long) sqrt( maxPrime );
        long previousPrime = -1;

        for( long prime : new PrimeGenerator( maxPrime ) ) {
            assertTrue( isPrime( prime, generatedPrimes ) );
            assertTrue( prime > previousPrime );

            if( prime <= primeCheckLimit ) {
                generatedPrimes.add( prime );
            }

            previousPrime = prime;

        }
    }

    @Test
    public void test_MassTest_GeneratorProducesIncreasingPrimes_WithLowerAndUpperLimit() {
        Set<Long> generatedPrimes = new HashSet<>();
        int minPrime = 400000;
        int maxPrime = 500000;
        long primeCheckLimit = (long) sqrt( maxPrime );

        for( long prime : new PrimeGenerator( primeCheckLimit ) ) {
            generatedPrimes.add( prime );
        }

        long previousPrime = -1;
        for( long prime : new PrimeGenerator( minPrime, maxPrime ) ) {
            assertTrue( isPrime( prime, generatedPrimes ) );
            assertTrue( prime > previousPrime );

            if( prime <= primeCheckLimit ) {
                generatedPrimes.add( prime );
            }

            previousPrime = prime;
        }
    }
}
