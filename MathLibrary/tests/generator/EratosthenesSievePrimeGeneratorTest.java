package generator;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Stream;

import static generator.EratosthenesSievePrimeGenerator.MAX_SIEVE_SIZE;
import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static testutils.ExceptionAssert.assertException;

public class EratosthenesSievePrimeGeneratorTest {
    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -1891293231 ),
                Arguments.of( IllegalArgumentException.class, -345345 ),
                Arguments.of( IllegalArgumentException.class, -1 ),
                Arguments.of( IllegalArgumentException.class, 0 ),
                Arguments.of( IllegalArgumentException.class, 1 ),
                Arguments.of( IllegalArgumentException.class, MAX_SIEVE_SIZE + 1 )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                Arguments.of( asList( 2 ), 2 ),
                Arguments.of( asList( 2, 3 ), 3 ),
                Arguments.of( asList( 2, 3 ), 4 ),
                Arguments.of( asList( 2, 3, 5 ), 5 ),
                Arguments.of( asList( 2, 3, 5 ), 6 ),
                Arguments.of( asList( 2, 3, 5, 7 ), 7 ),
                Arguments.of( asList( 2, 3, 5, 7 ), 8 ),
                Arguments.of( asList( 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37 ), 40 ),
                Arguments
                        .of( asList( 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79,
                                     83, 89, 97, 101 ), 101 )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot create the Eratosthenes sieve of size {1}, raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, int sieveSize ) {
        assertException( throwableClass, () -> new EratosthenesSievePrimeGenerator( sieveSize ) );
    }

    @ParameterizedTest( name = "{index}) Eratosthenes sieve of size {1} generates primes: {0}" )
    @MethodSource( "solutionExistsData" )
    public void test_EratosthenesSieveGeneratesPrimesCorrectly( List<Integer> expectedPrimes, int sieveSize ) {
        EratosthenesSievePrimeGenerator generator = new EratosthenesSievePrimeGenerator( sieveSize );
        Iterator<Integer> sieveIterator = generator.iterator();

        for( int expectedPrime : expectedPrimes ) {
            assertTrue( sieveIterator.hasNext() );
            assertEquals( expectedPrime, (int) sieveIterator.next() );
        }

        assertException( NoSuchElementException.class, () -> sieveIterator.next() );
    }

    @Test
    public void test_GeneratorIteratorDoesNotChangeState_WhenHasNextIsInvoked() {
        Iterator<Integer> iterator = new EratosthenesSievePrimeGenerator( 5 ).iterator();

        for( int i = 0; i < 10; i++ ) {
            assertTrue( iterator.hasNext() );
        }

        while( iterator.hasNext() ) {
            iterator.next();
        }

        for( int i = 0; i < 10; i++ ) {
            assertFalse( iterator.hasNext() );
        }
    }

    @Test
    public void test_SieveCanBeReused() {
        EratosthenesSievePrimeGenerator generator = new EratosthenesSievePrimeGenerator( 6 );
        assertEquals( asList( 2, 3, 5 ), getGeneratedPrimes( generator ) );
        assertEquals( asList( 2, 3, 5 ), getGeneratedPrimes( generator ) );
    }

    private Collection<Integer> getGeneratedPrimes( EratosthenesSievePrimeGenerator primeGenerator ) {
        Collection<Integer> primes = new LinkedList<>();
        for( int prime : primeGenerator ) {
            primes.add( prime );
        }
        return primes;
    }
}
