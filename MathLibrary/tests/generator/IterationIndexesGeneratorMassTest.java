package generator;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.util.Collections.nCopies;
import static org.junit.Assert.*;

public class IterationIndexesGeneratorMassTest {
    @Test
    public void test_MassTest_NumberOfGeneratedIterationIndexesIsCorrect_AndFirstAndLastIndexesAreCorrect() {
        int[][] allRanges = new int[][]{ { 4 }, { 5, 6 }, { 1, 9, 2 }, { 4, 9, 2, 8 }, { 5, 10, 2, 3, 4 },
                { 8, 6, 10, 9, 2, 2 }, { 6, 13, 2, 10, 8, 2, 1 }, { 4, 9, 2, 5, 8, 11, 2, 9 },
                { 8, 10, 6, 5, 3, 5, 7, 8, 5 } };

        for( int[] ranges : allRanges ) {
            testIfAllIterationIndexesAreCorrect( ranges );
        }
    }

    private void testIfAllIterationIndexesAreCorrect( int[] ranges ) {
        long expectedNumberOfGeneratorIterations = product( ranges );
        List<Integer> expectedFirstIndexes = getFirstIndexes( ranges );
        List<Integer> expectedLastIndexes = getLastIndexes( ranges );

        Iterator<List<Integer>> iterator = new IterationIndexesGenerator( ranges ).iterator();
        assertTrue( iterator.hasNext() );
        assertEquals( expectedFirstIndexes, iterator.next() );

        for( int i = 1; i < expectedNumberOfGeneratorIterations - 1; i++ ) {
            assertTrue( iterator.hasNext() );
            List<Integer> iterationIndexes = iterator.next();

            assertIndexesOfValuesInsideRanges( iterationIndexes, ranges );
        }

        if( expectedNumberOfGeneratorIterations > 1 ) {
            assertTrue( iterator.hasNext() );
            assertEquals( expectedLastIndexes, iterator.next() );
        }

        assertFalse( iterator.hasNext() );
    }

    private long product( int[] values ) {
        long product = 1;
        for( long value : values ) {
            product *= value;
        }
        return product;
    }

    private List<Integer> getFirstIndexes( int[] ranges ) {
        return nCopies( ranges.length, 0 );
    }

    private List<Integer> getLastIndexes( int[] ranges ) {
        List<Integer> lastIteration = new ArrayList<>( ranges.length );
        for( int i = 0; i < ranges.length; i++ ) {
            lastIteration.add( ranges[i] - 1 );
        }
        return lastIteration;
    }

    private void assertIndexesOfValuesInsideRanges( List<Integer> iterationIndexes, int[] ranges ) {
        assertEquals( ranges.length, iterationIndexes.size() );

        for( int i = 0; i < ranges.length; i++ ) {
            int ithIndex = iterationIndexes.get( i );
            assertTrue( ithIndex >= 0 && ithIndex < ranges[i] );
        }
    }
}
