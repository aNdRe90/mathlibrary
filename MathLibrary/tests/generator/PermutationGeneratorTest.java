package generator;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static org.junit.Assert.*;
import static testutils.ExceptionAssert.assertException;

public class PermutationGeneratorTest {
    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, null ),
                Arguments.of( IllegalArgumentException.class, emptyList() ),
                Arguments.of( IllegalArgumentException.class, singletonList( 0 ) ),

                //Maximum of 20 elements can be given as an input
                Arguments.of( IllegalArgumentException.class, nCopies( 21, 0 ) ),
                Arguments.of( IllegalArgumentException.class, nCopies( 55, 0 ) )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //Permutations of 2 elements
                Arguments.of( asList( asList( 0, 1 ), asList( 1, 0 ) ),
                              asList( 0, 1 ) ),

                //Permutations of 3 elements
                Arguments.of( asList( asList( 0, 1, 2 ),
                                      asList( 0, 2, 1 ),
                                      asList( 1, 0, 2 ),
                                      asList( 1, 2, 0 ),
                                      asList( 2, 0, 1 ),
                                      asList( 2, 1, 0 ) ),
                              asList( 0, 1, 2 ) ),

                //Permutations of 4 elements
                Arguments.of( asList( asList( 0, 1, 2, 3 ), asList( 0, 1, 3, 2 ), asList( 0, 2, 1, 3 ),
                                      asList( 0, 2, 3, 1 ),
                                      asList( 0, 3, 1, 2 ), asList( 0, 3, 2, 1 ), asList( 1, 0, 2, 3 ),
                                      asList( 1, 0, 3, 2 ),
                                      asList( 1, 2, 0, 3 ), asList( 1, 2, 3, 0 ), asList( 1, 3, 0, 2 ),
                                      asList( 1, 3, 2, 0 ),
                                      asList( 2, 0, 1, 3 ), asList( 2, 0, 3, 1 ), asList( 2, 1, 0, 3 ),
                                      asList( 2, 1, 3, 0 ),
                                      asList( 2, 3, 0, 1 ), asList( 2, 3, 1, 0 ), asList( 3, 0, 1, 2 ),
                                      asList( 3, 0, 2, 1 ),
                                      asList( 3, 1, 0, 2 ), asList( 3, 1, 2, 0 ), asList( 3, 2, 0, 1 ),
                                      asList( 3, 2, 1, 0 ) ),
                              asList( 0, 1, 2, 3 ) ),

                //Elements being integers other than integers 0, 1, 2, ..., n - 1
                Arguments.of( asList( asList( 8, 43, 72 ),
                                      asList( 8, 72, 43 ),
                                      asList( 43, 8, 72 ),
                                      asList( 43, 72, 8 ),
                                      asList( 72, 8, 43 ),
                                      asList( 72, 43, 8 ) ),
                              asList( 8, 43, 72 ) ),

                //String elements
                Arguments.of( asList( asList( "a", "b", "c", "d" ), asList( "a", "b", "d", "c" ),
                                      asList( "a", "c", "b", "d" ),
                                      asList( "a", "c", "d", "b" ), asList( "a", "d", "b", "c" ),
                                      asList( "a", "d", "c", "b" ),
                                      asList( "b", "a", "c", "d" ), asList( "b", "a", "d", "c" ),
                                      asList( "b", "c", "a", "d" ),
                                      asList( "b", "c", "d", "a" ), asList( "b", "d", "a", "c" ),
                                      asList( "b", "d", "c", "a" ),
                                      asList( "c", "a", "b", "d" ), asList( "c", "a", "d", "b" ),
                                      asList( "c", "b", "a", "d" ),
                                      asList( "c", "b", "d", "a" ), asList( "c", "d", "a", "b" ),
                                      asList( "c", "d", "b", "a" ),
                                      asList( "d", "a", "b", "c" ), asList( "d", "a", "c", "b" ),
                                      asList( "d", "b", "a", "c" ),
                                      asList( "d", "b", "c", "a" ), asList( "d", "c", "a", "b" ),
                                      asList( "d", "c", "b", "a" ) ),
                              asList( "a", "b", "c", "d" ) )
        );
    }

    @ParameterizedTest(
            name = "{index}) Cannot calculate permutations of {1}, raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, List<Object> elements ) {
        assertException( throwableClass, () -> new PermutationGenerator<>( elements ) );
    }

    @ParameterizedTest( name = "{index}) Permutations of {1} are: {0}" )
    @MethodSource( "solutionExistsData" )
    public void test_PermutationsAreCalculatedCorrectly(
            List<List<Object>> expectedPermutations, List<Object> elements ) {
        Iterator<List<Object>> permutationGeneratorIterator = new PermutationGenerator<>( elements ).iterator();

        for( List<Object> expectedPermutation : expectedPermutations ) {
            assertTrue( permutationGeneratorIterator.hasNext() );
            assertEquals( expectedPermutation, permutationGeneratorIterator.next() );
        }

        assertFalse( permutationGeneratorIterator.hasNext() );
        assertException( NoSuchElementException.class, () -> permutationGeneratorIterator.next() );
    }

    @Test
    public void test_GeneratorDoesNotChangeState_WhenHasNextIsInvoked() {
        Iterator<List<Integer>> iterator = new PermutationGenerator<>( asList( 0, 1, 2 ) ).iterator();

        for( int i = 0; i < 10; i++ ) {
            assertTrue( iterator.hasNext() );
        }

        while( iterator.hasNext() ) {
            iterator.next();
        }

        for( int i = 0; i < 10; i++ ) {
            assertFalse( iterator.hasNext() );
        }
    }

    @Test
    public void test_GeneratorCanBeReused() {
        PermutationGenerator<Integer> generator = new PermutationGenerator<>( asList( 0, 1 ) );

        Iterator<List<Integer>> iterator = generator.iterator();
        assertEquals( asList( 0, 1 ), iterator.next() );
        assertEquals( asList( 1, 0 ), iterator.next() );
        assertFalse( iterator.hasNext() );

        iterator = generator.iterator();
        assertEquals( asList( 0, 1 ), iterator.next() );
        assertEquals( asList( 1, 0 ), iterator.next() );
        assertFalse( iterator.hasNext() );
    }
}
