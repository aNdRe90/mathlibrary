package generator;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.util.Collections.reverse;
import static org.junit.Assert.*;
import static util.BasicOperations.factorial;

public class PermutationGeneratorMassTest {
    @Test
    public void test_MassTest_GeneratedPermutationsAreCorrect_InSizeAndLexicographicOrder() {
        for( int elementsSize = 2; elementsSize < 10; elementsSize++ ) {
            List<Integer> elements = createIntegerElements( elementsSize );
            testIfAllPermutationsAreCorrect( elements );
        }
    }

    private List<Integer> createIntegerElements( int size ) {
        List<Integer> elements = new ArrayList<>( size );

        for( int i = 0; i < size; i++ ) {
            elements.add( i );
        }

        return elements;
    }

    private void testIfAllPermutationsAreCorrect( List<Integer> elements ) {
        List<Integer> expectedFirstPermutation = elements;
        List<Integer> expectedLastPermutation = getReversed( elements );

        int permutationLength = elements.size();
        int expectedNumberOfPermutations = factorial( permutationLength ).intValueExact();

        Iterator<List<Integer>> permutationGeneratorIterator = new PermutationGenerator<>( elements ).iterator();

        assertTrue( permutationGeneratorIterator.hasNext() );
        assertEquals( expectedFirstPermutation, permutationGeneratorIterator.next() );

        List<Integer> previousPermutation = expectedFirstPermutation;
        List<Integer> nextPermutation;

        for( int i = 1; i < expectedNumberOfPermutations - 1; i++ ) {
            assertTrue( permutationGeneratorIterator.hasNext() );
            nextPermutation = permutationGeneratorIterator.next();

            assertEquals( permutationLength, nextPermutation.size() );
            assertNextPermutationIsNextInLexicographicOrder( previousPermutation, nextPermutation );
            previousPermutation = nextPermutation;
        }

        if( expectedNumberOfPermutations > 1 ) {
            assertTrue( permutationGeneratorIterator.hasNext() );
            assertEquals( expectedLastPermutation, permutationGeneratorIterator.next() );
        }

        assertFalse( permutationGeneratorIterator.hasNext() );
    }

    private List<Integer> getReversed( List<Integer> elements ) {
        List<Integer> expectedLastPermutation = new ArrayList<>( elements );
        reverse( expectedLastPermutation );
        return expectedLastPermutation;
    }

    private void assertNextPermutationIsNextInLexicographicOrder( List<Integer> A, List<Integer> B ) {
        //A - previous permutation
        //B - next permutation

        //Permutation A = (a_0, a_1, a_2, ..., a_{n − 1}) precedes the permutation B = (b_0, b_1, b_2, ..., b_{n − 1})
        //if for some 0 <= k < n, we have a_0 = b_0, a_1 = b_1, ..., a_{k − 1} = b_{k − 1} and a_k < b_k.

        assertEquals( A.size(), B.size() );

        for( int k = 0; k < A.size(); k++ ) {
            if( A.get( k ) != B.get( k ) ) {
                assertTrue( A.get( k ) < B.get( k ) );
                break;
            }
        }
    }
}
