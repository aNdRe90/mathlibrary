package generator;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static testutils.ExceptionAssert.assertException;

public class IterationIndexesGeneratorTest {
    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                //Null or empty ranges
                Arguments.of( IllegalArgumentException.class, null ),
                Arguments.of( IllegalArgumentException.class, new int[]{} ),

                //At least one ranges is negative
                Arguments.of( IllegalArgumentException.class, new int[]{ -1 } ),
                Arguments.of( IllegalArgumentException.class, new int[]{ 5, -3 } ),
                Arguments.of( IllegalArgumentException.class, new int[]{ -5, 3 } ),
                Arguments.of( IllegalArgumentException.class, new int[]{ -5, -4, -12 } ),
                Arguments.of( IllegalArgumentException.class, new int[]{ 7, -1, 5, 9, 10, -29 } ),

                //At least one ranges is zero
                Arguments.of( IllegalArgumentException.class, new int[]{ 0 } ),
                Arguments.of( IllegalArgumentException.class, new int[]{ 0, 0, 0 } ),
                Arguments.of( IllegalArgumentException.class, new int[]{ 2, 0, -1, -9 } ),
                Arguments.of( IllegalArgumentException.class, new int[]{ 4, 9, -2, 9, 0 } ),
                Arguments.of( IllegalArgumentException.class, new int[]{ 9, 0, -1, 0, 71, -8 } )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //One range
                Arguments.of( asList( asList( 0 ) ),
                              new int[]{ 1 } ),
                Arguments.of( asList( asList( 0 ), asList( 1 ), asList( 2 ) ),
                              new int[]{ 3 } ),
                Arguments.of( asList( asList( 0 ), asList( 1 ), asList( 2 ), asList( 3 ), asList( 4 ), asList( 5 ),
                                      asList( 6 ) ),
                              new int[]{ 7 } ),

                //More than one range
                Arguments.of( asList( asList( 0, 0 ),
                                      asList( 0, 1 ) ),
                              new int[]{ 1, 2 } ),
                Arguments.of( asList( asList( 0, 0 ), asList( 0, 1 ), asList( 0, 2 ),
                                      asList( 1, 0 ), asList( 1, 1 ), asList( 1, 2 ),
                                      asList( 2, 0 ), asList( 2, 1 ), asList( 2, 2 ) ),
                              new int[]{ 3, 3 } ),
                Arguments.of( asList( asList( 0, 0, 0 ), asList( 0, 0, 1 ), asList( 0, 0, 2 ), asList( 0, 0, 3 ),
                                      asList( 0, 1, 0 ), asList( 0, 1, 1 ), asList( 0, 1, 2 ), asList( 0, 1, 3 ),
                                      asList( 1, 0, 0 ), asList( 1, 0, 1 ), asList( 1, 0, 2 ), asList( 1, 0, 3 ),
                                      asList( 1, 1, 0 ), asList( 1, 1, 1 ), asList( 1, 1, 2 ), asList( 1, 1, 3 ),
                                      asList( 2, 0, 0 ), asList( 2, 0, 1 ), asList( 2, 0, 2 ), asList( 2, 0, 3 ),
                                      asList( 2, 1, 0 ), asList( 2, 1, 1 ), asList( 2, 1, 2 ), asList( 2, 1, 3 ) ),
                              new int[]{ 3, 2, 4 } )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot generate iteration indexes for ranges {1}, raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, int[] ranges ) {
        assertException( throwableClass, () -> new IterationIndexesGenerator( ranges ) );
    }

    @ParameterizedTest( name = "{index}) Iteration indexes for ranges {1} are: {0}" )
    @MethodSource( "solutionExistsData" )
    public void test_IterationIndexesAreCalculatedCorrectly(
            List<List<Integer>> expectedIterationIndexes, int[] ranges ) {
        IterationIndexesGenerator generator = new IterationIndexesGenerator( ranges );
        Iterator<List<Integer>> iterationIndexesIterator = generator.iterator();

        for( List<Integer> iterationIndexes : expectedIterationIndexes ) {
            assertTrue( iterationIndexesIterator.hasNext() );
            assertEquals( iterationIndexes, iterationIndexesIterator.next() );
        }

        assertException( NoSuchElementException.class, () -> iterationIndexesIterator.next() );
    }

    @Test
    public void test_GeneratorDoesNotChangeState_WhenHasNextIsInvoked() {
        Iterator<List<Integer>> iterator = new IterationIndexesGenerator( 5 ).iterator();

        for( int i = 0; i < 10; i++ ) {
            assertTrue( iterator.hasNext() );
        }

        while( iterator.hasNext() ) {
            iterator.next();
        }

        for( int i = 0; i < 10; i++ ) {
            assertFalse( iterator.hasNext() );
        }
    }

    @Test
    public void test_GeneratorCanBeReused() {
        IterationIndexesGenerator generator = new IterationIndexesGenerator( 2, 1 );

        Iterator<List<Integer>> iterator = generator.iterator();
        assertEquals( asList( 0, 0 ), iterator.next() );
        assertEquals( asList( 1, 0 ), iterator.next() );
        assertFalse( iterator.hasNext() );

        iterator = generator.iterator();
        assertEquals( asList( 0, 0 ), iterator.next() );
        assertEquals( asList( 1, 0 ), iterator.next() );
        assertFalse( iterator.hasNext() );
    }
}
