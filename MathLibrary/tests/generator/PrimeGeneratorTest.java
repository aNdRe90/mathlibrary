package generator;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;
import static testutils.CollectionUtils.listOfLongs;
import static testutils.ExceptionAssert.assertException;

public class PrimeGeneratorTest {
    private static Stream<Arguments> invalidInputDataForGivenMaximumPrime() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -4562384723L ),
                Arguments.of( IllegalArgumentException.class, -3 ),
                Arguments.of( IllegalArgumentException.class, 0 ),
                Arguments.of( IllegalArgumentException.class, 1 )
        );
    }

    private static Stream<Arguments> invalidInputDataForGivenPrimeRange() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -3, -5 ),
                Arguments.of( IllegalArgumentException.class, 3, 1 ),
                Arguments.of( IllegalArgumentException.class, Long.MAX_VALUE, 9872634872364L )
        );
    }

    private static Stream<Arguments> solutionExistsDataForGivenMaximumPrime() {
        return Stream.of(
                //Maximum prime is a prime number
                Arguments.of( listOfLongs( 2 ),
                              2 ),
                Arguments.of( listOfLongs( 2, 3 ),
                              3 ),
                Arguments.of( listOfLongs( 2, 3, 5 ),
                              5 ),
                Arguments.of( listOfLongs( 2, 3, 5, 7, 11 ),
                              11 ),
                Arguments.of( listOfLongs( 2, 3, 5, 7, 11, 13, 17, 19 ),
                              19 ),
                Arguments.of( listOfLongs( 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47 ),
                              47 ),
                Arguments.of( listOfLongs( 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
                                           73, 79, 83, 89, 97, 101, 103 ),
                              103 ),

                //Maximum prime is a composite number
                Arguments.of( listOfLongs( 2, 3 ),
                              4 ),
                Arguments.of( listOfLongs( 2, 3, 5 ),
                              6 ),
                Arguments.of( listOfLongs( 2, 3, 5, 7 ),
                              10 ),
                Arguments.of( listOfLongs( 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31 ),
                              32 ),
                Arguments.of( listOfLongs( 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41 ),
                              42 ),
                Arguments.of( listOfLongs( 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
                                           73, 79, 83, 89, 97 ),
                              100 ),
                Arguments.of( listOfLongs( 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
                                           73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151,
                                           157,
                                           163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239,
                                           241,
                                           251, 257, 263, 269, 271, 277, 281, 283, 293, 307 ),
                              310 )
        );
    }

    private static Stream<Arguments> solutionExistsDataForGivenRange() {
        return Stream.of(
                //No primes generated in the given range
                Arguments.of( emptyList(),
                              -16, -11 ),
                Arguments.of( emptyList(),
                              -13, 0 ),
                Arguments.of( emptyList(),
                              -4, 1 ),
                Arguments.of( emptyList(),
                              0, 0 ),
                Arguments.of( emptyList(),
                              0, 1 ),
                Arguments.of( emptyList(),
                              1, 1 ),

                //No primes generated for the range where min and max are the same and are composite
                Arguments.of( emptyList(),
                              4, 4 ),
                Arguments.of( emptyList(),
                              15, 15 ),
                Arguments.of( emptyList(),
                              2001, 2001 ),
                Arguments.of( emptyList(),
                              40000, 40000 ),
                Arguments.of( emptyList(),
                              74562332, 74562332 ),

                //Only one prime generated for the range where min and max are the same and are prime
                Arguments.of( listOfLongs( 7 ),
                              7, 7 ),
                Arguments.of( listOfLongs( 173 ),
                              173, 173 ),
                Arguments.of( listOfLongs( 997 ),
                              997, 997 ),
                Arguments.of( listOfLongs( 30211 ),
                              30211, 30211 ),

                //Given range where min and max are not the same
                Arguments.of( listOfLongs( 2, 3, 5 ),
                              Long.MIN_VALUE, 5 ),
                Arguments.of( listOfLongs( 3, 5, 7 ),
                              3, 7 ),
                Arguments.of( listOfLongs( 2, 3, 5, 7, 11, 13 ),
                              1, 14 ),
                Arguments.of( listOfLongs( 2, 3 ),
                              -3, 4 ),
                Arguments.of( listOfLongs( 5, 7, 11, 13 ),
                              4, 15 ),
                Arguments.of( listOfLongs( 7, 11, 13 ),
                              7, 13 ),
                Arguments.of( listOfLongs( 287346509, 287346541, 287346547, 287346571, 287346601 ),
                              287346508, 287346621 ),
                Arguments.of( listOfLongs( 100003, 100019, 100043, 100049, 100057, 100069, 100103, 100109, 100129,
                                           100151,
                                           100153, 100169, 100183, 100189, 100193, 100207, 100213, 100237, 100267,
                                           100271,
                                           100279,
                                           100291, 100297, 100313, 100333, 100343, 100357, 100361, 100363, 100379,
                                           100391,
                                           100393,
                                           100403, 100411, 100417, 100447, 100459, 100469, 100483, 100493, 100501,
                                           100511,
                                           100517,
                                           100519, 100523, 100537, 100547, 100549, 100559, 100591, 100609, 100613,
                                           100621,
                                           100649,
                                           100669, 100673, 100693, 100699, 100703, 100733, 100741, 100747, 100769,
                                           100787,
                                           100799,
                                           100801, 100811, 100823, 100829, 100847, 100853, 100907, 100913, 100927,
                                           100931,
                                           100937,
                                           100943, 100957, 100981, 100987, 100999 ),
                              100000, 101000 ),


                //Only last Long prime (equal to 9223372036854775783) is generated
                Arguments.of( singletonList( 9223372036854775783L ),
                              9223372036854775783L, 9223372036854775783L ),
                Arguments.of( singletonList( 9223372036854775783L ),
                              9223372036854775783L, Long.MAX_VALUE ),
                Arguments.of( singletonList( 9223372036854775783L ),
                              9223372036854775782L, Long.MAX_VALUE )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot generate prime generator for range [2; {1}], raises an exception: {0}" )
    @MethodSource( "invalidInputDataForGivenMaximumPrime" )
    public void test_InvalidMaximumPrimeForPrimeGeneration_RaisesAnException(
            Class<Throwable> throwableClass, long maximumPrime ) {
        assertException( throwableClass, () -> new PrimeGenerator( maximumPrime ) );
    }

    @ParameterizedTest( name = "{index}) Cannot generate prime generator for range [{1}; {2}], raises an exception: {0}" )
    @MethodSource( "invalidInputDataForGivenPrimeRange" )
    public void test_InvalidRangeForPrimeGeneration_RaisesAnException(
            Class<Throwable> throwableClass, long minimumPrime, long maximumPrime ) {
        assertException( throwableClass, () -> new PrimeGenerator( minimumPrime, maximumPrime ) );
    }

    @ParameterizedTest( name = "{index}) Primes generated in range [2; {1}] are: {0}" )
    @MethodSource( "solutionExistsDataForGivenMaximumPrime" )
    public void test_CorrectPrimesGenerated_ForGivenMaximumPrimeValue( List<Long> expectedPrimes, long maximumPrime ) {
        Iterator<Long> primeGeneratorIterator = new PrimeGenerator( maximumPrime ).iterator();

        for( long expectedPrime : expectedPrimes ) {
            assertTrue( primeGeneratorIterator.hasNext() );
            assertEquals( expectedPrime, (long) primeGeneratorIterator.next() );
        }

        assertFalse( primeGeneratorIterator.hasNext() );
        assertException( NoSuchElementException.class, () -> primeGeneratorIterator.next() );
    }

    @ParameterizedTest( name = "{index}) Primes generated in range [{1}; {2}] are: {0}" )
    @MethodSource( "solutionExistsDataForGivenRange" )
    public void test_CorrectPrimesGenerated_ForGivenRange(
            List<Long> expectedPrimes, long minimumPrime, long maximumPrime ) {
        Iterator<Long> primeGeneratorIterator = new PrimeGenerator( minimumPrime, maximumPrime ).iterator();

        for( long expectedPrime : expectedPrimes ) {
            assertTrue( primeGeneratorIterator.hasNext() );
            assertEquals( expectedPrime, (long) primeGeneratorIterator.next() );
        }

        assertFalse( primeGeneratorIterator.hasNext() );
        assertException( NoSuchElementException.class, () -> primeGeneratorIterator.next() );
    }

    @Test
    public void test_GeneratorCanBeReused() {
        PrimeGenerator generator1 = new PrimeGenerator();
        assertEquals( 2, (long) generator1.iterator().next() );
        assertEquals( 2, (long) generator1.iterator().next() );

        PrimeGenerator generator2 = new PrimeGenerator( 88 );
        assertEquals( 2, (long) generator2.iterator().next() );
        assertEquals( 2, (long) generator2.iterator().next() );

        PrimeGenerator generator3 = new PrimeGenerator( 10, 20 );
        assertEquals( 11, (long) generator3.iterator().next() );
        assertEquals( 11, (long) generator3.iterator().next() );
    }

    @Test
    public void test_InfiniteGenerator_GeneratesCorrectResults() {
        Iterator<Long> iterator = new PrimeGenerator().iterator();

        //1st prime, 1001st prime, 2001st prime, 3001st prime, ..., 10001st prime
        long[] expectedPrimes = new long[]{ 2, 7927, 17393, 27457, 37831, 48619, 59369, 70663, 81817, 93187, 104743 };

        for( int i = 1; i <= 10001; i++ ) {
            assertTrue( iterator.hasNext() );
            long prime = iterator.next();

            if( i % 1000 == 1 ) {
                assertEquals( expectedPrimes[i / 1000], prime );
            }
        }

        assertTrue( iterator.hasNext() );
    }

    @Test
    public void test_GeneratorDoesNotChangeState_WhenHasNextIsInvoked() {
        assertGeneratorDoesNotChangeStateWhenHasNextIsInvoked( new PrimeGenerator( 5 ) );
        assertGeneratorDoesNotChangeStateWhenHasNextIsInvoked( new PrimeGenerator( 2, 9 ) );
    }

    private void assertGeneratorDoesNotChangeStateWhenHasNextIsInvoked( PrimeGenerator generator ) {
        Iterator<Long> iterator = generator.iterator();

        for( int i = 0; i < 10; i++ ) {
            assertTrue( iterator.hasNext() );
        }

        while( iterator.hasNext() ) {
            iterator.next();
        }

        for( int i = 0; i < 10; i++ ) {
            assertFalse( iterator.hasNext() );
        }
    }
}
