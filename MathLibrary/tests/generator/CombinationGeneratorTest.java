package generator;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static org.junit.Assert.*;
import static testutils.ExceptionAssert.assertException;

public class CombinationGeneratorTest {
    private static Stream<Arguments> invalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, null, 3 ),
                Arguments.of( IllegalArgumentException.class, emptyList(), 2 ),
                Arguments.of( IllegalArgumentException.class, asList( 0, 1, 2 ), -3 ),
                Arguments.of( IllegalArgumentException.class, singletonList( 0 ), 0 ),
                Arguments.of( IllegalArgumentException.class, singletonList( 0 ), 2 ),
                Arguments.of( IllegalArgumentException.class, asList( 0, 1, 2 ), 6 ),
                Arguments.of( IllegalArgumentException.class, asList( 0, 1, 2, 3, 4, 5 ), 534 ),

                //Maximum number of combinations generated is Long.MAX_VALUE = 9,223,372,036,854,775,807
                //binomialCoefficient(70, 35) = 112,186,277,816,662,845,432 > Long.MAX_VALUE
                Arguments.of( ArithmeticException.class, nCopies( 70, "a" ), 35 )
        );
    }

    private static Stream<Arguments> solutionExistsData() {
        return Stream.of(
                //n-element combinations of n elements
                Arguments.of( asList( asList( 0 ) ),
                              asList( 0 ), 1 ),
                Arguments.of( asList( asList( 0 ) ),
                              asList( 0 ), 1 ),
                Arguments.of( asList( asList( 0, 1 ) ),
                              asList( 0, 1 ), 2 ),
                Arguments.of( asList( asList( 0, 1, 2 ) ),
                              asList( 0, 1, 2 ), 3 ),
                Arguments.of( asList( asList( 0, 1, 2, 3, 4, 5, 6 ) ),
                              asList( 0, 1, 2, 3, 4, 5, 6 ), 7 ),

                //k-element combinations of n elements, where n - 1 = k
                Arguments.of( asList( asList( 0, 1 ),
                                      asList( 0, 2 ),
                                      asList( 1, 2 ) ),
                              asList( 0, 1, 2 ), 2 ),
                Arguments.of( asList( asList( 0, 1, 2, 3 ),
                                      asList( 0, 1, 2, 4 ),
                                      asList( 0, 1, 3, 4 ),
                                      asList( 0, 2, 3, 4 ),
                                      asList( 1, 2, 3, 4 ) ),
                              asList( 0, 1, 2, 3, 4 ), 4 ),
                Arguments.of( asList( asList( 0, 1, 2, 3, 4, 5, 6, 7 ),
                                      asList( 0, 1, 2, 3, 4, 5, 6, 8 ),
                                      asList( 0, 1, 2, 3, 4, 5, 7, 8 ),
                                      asList( 0, 1, 2, 3, 4, 6, 7, 8 ),
                                      asList( 0, 1, 2, 3, 5, 6, 7, 8 ),
                                      asList( 0, 1, 2, 4, 5, 6, 7, 8 ),
                                      asList( 0, 1, 3, 4, 5, 6, 7, 8 ),
                                      asList( 0, 2, 3, 4, 5, 6, 7, 8 ),
                                      asList( 1, 2, 3, 4, 5, 6, 7, 8 ) ),
                              asList( 0, 1, 2, 3, 4, 5, 6, 7, 8 ), 8 ),

                //1-element combinations of n elements
                Arguments.of( asList( asList( 0 ),
                                      asList( 1 ) ),
                              asList( 0, 1 ), 1 ),
                Arguments.of( asList( asList( 0 ),
                                      asList( 1 ),
                                      asList( 2 ),
                                      asList( 3 ),
                                      asList( 4 ) ),
                              asList( 0, 1, 2, 3, 4 ), 1 ),
                Arguments.of( asList( asList( 0 ),
                                      asList( 1 ),
                                      asList( 2 ),
                                      asList( 3 ),
                                      asList( 4 ),
                                      asList( 5 ),
                                      asList( 6 ),
                                      asList( 7 ) ),
                              asList( 0, 1, 2, 3, 4, 5, 6, 7 ), 1 ),

                //k-element combinations of n elements, where 1 < k < n - 1
                Arguments.of( asList( asList( 0, 1 ),
                                      asList( 0, 2 ),
                                      asList( 0, 3 ),
                                      asList( 1, 2 ),
                                      asList( 1, 3 ),
                                      asList( 2, 3 ) ),
                              asList( 0, 1, 2, 3 ), 2 ),
                Arguments.of( asList( asList( 0, 1, 2, 3 ),
                                      asList( 0, 1, 2, 4 ),
                                      asList( 0, 1, 2, 5 ),
                                      asList( 0, 1, 3, 4 ),
                                      asList( 0, 1, 3, 5 ),
                                      asList( 0, 1, 4, 5 ),
                                      asList( 0, 2, 3, 4 ),
                                      asList( 0, 2, 3, 5 ),
                                      asList( 0, 2, 4, 5 ),
                                      asList( 0, 3, 4, 5 ),
                                      asList( 1, 2, 3, 4 ),
                                      asList( 1, 2, 3, 5 ),
                                      asList( 1, 2, 4, 5 ),
                                      asList( 1, 3, 4, 5 ),
                                      asList( 2, 3, 4, 5 ) ),
                              asList( 0, 1, 2, 3, 4, 5 ), 4 ),

                //Elements being integers other than integers 0, 1, 2, ..., n - 1
                Arguments.of( asList( asList( 4, 18, 33 ),
                                      asList( 4, 18, 67 ),
                                      asList( 4, 18, 100 ),
                                      asList( 4, 33, 67 ),
                                      asList( 4, 33, 100 ),
                                      asList( 4, 67, 100 ),
                                      asList( 18, 33, 67 ),
                                      asList( 18, 33, 100 ),
                                      asList( 18, 67, 100 ),
                                      asList( 33, 67, 100 ) ),
                              asList( 4, 18, 33, 67, 100 ), 3 ),

                //String elements
                Arguments.of( asList( asList( "a", "b", "g", "k", "u" ),
                                      asList( "a", "b", "g", "k", "w" ),
                                      asList( "a", "b", "g", "k", "z" ),
                                      asList( "a", "b", "g", "u", "w" ),
                                      asList( "a", "b", "g", "u", "z" ),
                                      asList( "a", "b", "g", "w", "z" ),
                                      asList( "a", "b", "k", "u", "w" ),
                                      asList( "a", "b", "k", "u", "z" ),
                                      asList( "a", "b", "k", "w", "z" ),
                                      asList( "a", "b", "u", "w", "z" ),
                                      asList( "a", "g", "k", "u", "w" ),
                                      asList( "a", "g", "k", "u", "z" ),
                                      asList( "a", "g", "k", "w", "z" ),
                                      asList( "a", "g", "u", "w", "z" ),
                                      asList( "a", "k", "u", "w", "z" ),
                                      asList( "b", "g", "k", "u", "w" ),
                                      asList( "b", "g", "k", "u", "z" ),
                                      asList( "b", "g", "k", "w", "z" ),
                                      asList( "b", "g", "u", "w", "z" ),
                                      asList( "b", "k", "u", "w", "z" ),
                                      asList( "g", "k", "u", "w", "z" ) ),
                              asList( "a", "b", "g", "k", "u", "w", "z" ), 5 )
        );
    }

    @ParameterizedTest(
            name = "{index}) Cannot calculate {2}-element combinations of {1}, raises an exception: {0}" )
    @MethodSource( "invalidInputData" )
    public void test_InvalidInputRaisesAnException( Class<Throwable> throwableClass, List<Object> elements, int r ) {
        assertException( throwableClass, () -> new CombinationGenerator<>( elements, r ) );
    }

    @ParameterizedTest( name = "{index}) {2}-element combinations of {1} are: {0}" )
    @MethodSource( "solutionExistsData" )
    public void test_CombinationsAreCalculatedCorrectly(
            List<List<Object>> expectedCombinations, List<Object> elements, int r ) {
        Iterator<List<Object>> combinationGeneratorIterator = new CombinationGenerator<>( elements, r ).iterator();

        for( List<Object> expectedCombination : expectedCombinations ) {
            assertTrue( combinationGeneratorIterator.hasNext() );
            assertEquals( expectedCombination, combinationGeneratorIterator.next() );
        }

        assertFalse( combinationGeneratorIterator.hasNext() );
        assertException( NoSuchElementException.class, () -> combinationGeneratorIterator.next() );
    }

    @Test
    public void test_GeneratorDoesNotChangeState_WhenHasNextIsInvoked() {
        Iterator<List<Integer>> iterator = new CombinationGenerator<>( asList( 0, 1, 2 ), 2 ).iterator();

        for( int i = 0; i < 10; i++ ) {
            assertTrue( iterator.hasNext() );
        }

        while( iterator.hasNext() ) {
            iterator.next();
        }

        for( int i = 0; i < 10; i++ ) {
            assertFalse( iterator.hasNext() );
        }
    }

    @Test
    public void test_GeneratorCanBeReused() {
        CombinationGenerator<Integer> generator = new CombinationGenerator<>( asList( 0, 1, 2 ), 2 );

        Iterator<List<Integer>> iterator = generator.iterator();
        assertEquals( asList( 0, 1 ), iterator.next() );
        assertEquals( asList( 0, 2 ), iterator.next() );
        assertEquals( asList( 1, 2 ), iterator.next() );
        assertFalse( iterator.hasNext() );

        iterator = generator.iterator();
        assertEquals( asList( 0, 1 ), iterator.next() );
        assertEquals( asList( 0, 2 ), iterator.next() );
        assertEquals( asList( 1, 2 ), iterator.next() );
        assertFalse( iterator.hasNext() );
    }
}
