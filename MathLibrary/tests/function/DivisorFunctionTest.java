package function;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static testutils.ExceptionAssert.assertException;

public class DivisorFunctionTest {
    private DivisorFunction function = new DivisorFunction();

    private static Stream<Arguments> numberOfDivisorsInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, 0 )
        );
    }

    private static Stream<Arguments> sumOfDivisorsInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, 0 ),
                Arguments.of( ArithmeticException.class, Long.MAX_VALUE - 6 ) //overflow
        );
    }

    private static Stream<Arguments> divisorFunctionValueInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, 0, 0 ),
                Arguments.of( IllegalArgumentException.class, 0, 2 ),
                Arguments.of( IllegalArgumentException.class, 0, 5 ),
                Arguments.of( IllegalArgumentException.class, 0, -3 ),
                Arguments.of( IllegalArgumentException.class, 2, -1 ),
                Arguments.of( IllegalArgumentException.class, 876, -3 ),
                Arguments.of( IllegalArgumentException.class, 342312, -9873 )
        );
    }

    private static Stream<Arguments> numberOfDivisorsSolutionExistsData() {
        return Stream.of(
                //Value is equal to 1
                Arguments.of( 1, 1 ),

                //Value is a prime number
                Arguments.of( 2, 7 ),
                Arguments.of( 2, 71 ),
                Arguments.of( 2, 3821 ),
                Arguments.of( 2, 7321 ),
                Arguments.of( 2, 444893 ),
                Arguments.of( 2, 34574567 ),
                Arguments.of( 2, 92816728123111L ),
                Arguments.of( 2, 8263771236854774897L ),

                //Value is a composite number
                Arguments.of( 4, 8 ),
                Arguments.of( 16, 120 ),
                Arguments.of( 4, 7123 ),
                Arguments.of( 4, 52618 ),
                Arguments.of( 12, 9991825 ),
                Arguments.of( 6, 733421133 ),
                Arguments.of( 12, 1236457653 ),
                Arguments.of( 16, 182930547263522L ),
                Arguments.of( 96, 9223372036854775807L ),
                Arguments.of( 18432, 9223372036854775800L ),

                //Value is a negative number
                Arguments.of( 1, -1 ),
                Arguments.of( 2, -7 ),
                Arguments.of( 4, -15 ),
                Arguments.of( 16, -8274 ),
                Arguments.of( 18, -172244 ),
                Arguments.of( 24, -9001311 ),
                Arguments.of( 180, -36006768 ),
                Arguments.of( 1440, -698615280000L ),
                Arguments.of( 5760, -5224228724000000000L )
        );
    }

    private static Stream<Arguments> sumOfDivisorsSolutionExistsData() {
        return Stream.of(
                //Value is equal to 1
                Arguments.of( 1, 1 ),

                //Value is a prime number
                Arguments.of( 12, 11 ),
                Arguments.of( 90, 89 ),
                Arguments.of( 2342, 2341 ),
                Arguments.of( 8123838, 8123837 ),
                Arguments.of( 23487230, 23487229 ),
                Arguments.of( 87263438, 87263437 ),
                Arguments.of( 623010001242L, 623010001241L ),
                Arguments.of( 301023019283918330L, 301023019283918329L ),

                //Value is a composite number
                Arguments.of( 18, 10 ),
                Arguments.of( 252, 130 ),
                Arguments.of( 504, 220 ),
                Arguments.of( 8448, 6923 ),
                Arguments.of( 89208, 72365 ),
                Arguments.of( 924000, 921653 ),
                Arguments.of( 44851968, 22725344 ),
                Arguments.of( 6483772800L, 3829478121L ),
                Arguments.of( 14427150300L, 7238191232L ),
                Arguments.of( 1615755854524812L, 639200118273516L ),

                //Value is a negative number
                Arguments.of( 1, -1 ),
                Arguments.of( 14, -13 ),
                Arguments.of( 624, -423 ),
                Arguments.of( 998, -997 ),
                Arguments.of( 5040, -1824 ),
                Arguments.of( 53144, -16128 ),
                Arguments.of( 6714160, -2239488 ),
                Arguments.of( 123121200, -91092831 ),
                Arguments.of( 775239480, -762318763 ),
                Arguments.of( 838842606144L, -827192014321L ),
                Arguments.of( 9717576624000L, -2647658762496L )
        );
    }

    private static Stream<Arguments> divisorFunctionValueSolutionExistsData() {

        return Stream.of(
                //Divisors per value

                //Value is equal to 1
                Arguments.of( new long[]{ 1 }, 1 ),

                //Value is a prime number
                Arguments.of( new long[]{ 1, 17 }, 17, 3 ),
                Arguments.of( new long[]{ 1, 331 }, 331, 4 ),
                Arguments.of( new long[]{ 1, 1567 }, 1567, 2 ),
                Arguments.of( new long[]{ 1, 20963 }, 20963, 6 ),
                Arguments.of( new long[]{ 1, 87913327 }, 87913327, 11 ),
                Arguments.of( new long[]{ 1, 70234891823111227L }, 70234891823111227L, 8 ),

                //Value is a composite number
                Arguments.of( new long[]{ 1, 2, 7, 14 }, 14 ),
                Arguments.of( new long[]{ 1, 11, 13, 19, 143, 209, 247, 361, 2717, 3971, 4693, 51623 }, 51623 ),
                Arguments.of( new long[]{ 1, 3, 29957437, 89872311 }, 89872311 ),
                Arguments.of( new long[]{ 1, 2, 4, 5, 8, 10, 16, 20, 25, 32, 40, 50, 64, 80, 100, 125, 128, 160, 200,
                        250, 320, 400, 500, 640, 800, 997, 1000, 1600, 1994, 2000, 3200, 3988, 4000,
                        4985, 7976, 8000, 9970, 15952, 16000, 19940, 24925, 31904, 39880, 49850, 63808,
                        79760, 99700, 124625, 127616, 159520, 199400, 249250, 319040, 398800, 498500, 638080,
                        797600, 997000, 1595200, 1994000, 3190400, 3988000, 7976000, 15952000 }, 15952000 ),
                Arguments
                        .of( new long[]{ 1, 2, 4, 8, 16, 32, 163, 326, 652, 1304, 2608, 5216, 2223149, 4446298, 8892596,
                                17785192, 34701343, 35570384, 69402686, 71140768, 138805372, 277610744, 362373287,
                                555221488, 724746574, 1110442976, 1449493148, 2898986296L, 5656318909L, 5797972592L,
                                11312637818L, 11595945184L, 22625275636L, 45250551272L, 90501102544L, 181002205088L,
                                77146255989107L, 154292511978214L, 308585023956428L, 617170047912856L,
                                1234340095825712L,
                                2468680191651424L, 12574839726224441L, 25149679452448882L, 50299358904897764L,
                                100598717809795528L, 201197435619591056L, 402394871239182112L }, 402394871239182112L ),

                //Value is a negative number
                Arguments.of( new long[]{ 1 }, -1 ),
                Arguments.of( new long[]{ 1, 2, 4, 5, 10, 20 }, -20 ),
                Arguments
                        .of( new long[]{ 1, 3, 7, 21, 392129161, 1176387483, 2744904127L, 8234712381L }, -8234712381L ),
                Arguments.of(
                        new long[]{ 1, 2, 3, 4, 6, 12, 151, 281, 302, 453, 562, 604, 843, 906, 1124, 1686, 1812,
                                3372, 42431, 84862, 127293, 169724, 254586, 509172 },
                        -509172 )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate number of divisors of {1}, raises an exception: {0}" )
    @MethodSource( "numberOfDivisorsInvalidInputData" )
    public void test_InvalidInputForNumberOfDivisors_RaisesAnException( Class<Throwable> throwableClass, long n ) {
        assertException( throwableClass, () -> function.getNumberOfDivisors( n ) );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate sum of divisors of {1}, raises an exception: {0}" )
    @MethodSource( "sumOfDivisorsInvalidInputData" )
    public void test_InvalidInputForSumOfDivisors_RaisesAnException( Class<Throwable> throwableClass, long n ) {
        assertException( throwableClass, () -> function.getSumOfDivisors( n ) );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate divisorFunction({1}, {2}), raises an exception: {0}" )
    @MethodSource( "divisorFunctionValueInvalidInputData" )
    public void test_InvalidInputForDivisorFunctionValue_RaisesAnException( Class<Throwable> throwableClass, long n,
            int x ) {
        assertException( throwableClass, () -> function.getDivisorFunctionValue( n, x ) );
    }

    @ParameterizedTest( name = "{index}) Number of divisors of {1} is {0}" )
    @MethodSource( "numberOfDivisorsSolutionExistsData" )
    public void test_NumberOfDivisors_IsCalculatedCorrectly( long numberOfDivisors, long n ) {
        assertEquals( numberOfDivisors, function.getNumberOfDivisors( n ) );
    }

    @ParameterizedTest( name = "{index}) Sum of divisors of {1} is {0}" )
    @MethodSource( "sumOfDivisorsSolutionExistsData" )
    public void test_SumOfDivisors_IsCalculatedCorrectly( long sumOfDivisors, long n ) {
        assertEquals( sumOfDivisors, function.getSumOfDivisors( n ) );
    }

    @ParameterizedTest( name = "{index}) divisorFunction({1}, [x = 1, 2, 3, ..., 10]) is correct" )
    @MethodSource( "divisorFunctionValueSolutionExistsData" )
    public void test_DivisorFunctionValue_IsCalculatedCorrectly( long[] divisorsOfN, long n ) {
        for( int x = 0; x < 10; x++ ) {
            BigInteger expectedDivisorFunctionValue = getSumOfPowers( divisorsOfN, x );
            assertEquals( expectedDivisorFunctionValue, function.getDivisorFunctionValue( n, x ) );
        }
    }

    private BigInteger getSumOfPowers( long[] values, int exponent ) {
        BigInteger sumOfPowers = BigInteger.ZERO;

        for( long value : values ) {
            sumOfPowers = sumOfPowers.add( BigInteger.valueOf( value ).pow( exponent ) );
        }

        return sumOfPowers;
    }
}
