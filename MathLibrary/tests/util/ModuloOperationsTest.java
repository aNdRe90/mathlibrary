package util;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.stream.Stream;

import static java.lang.Math.sqrt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static testutils.ExceptionAssert.assertException;
import static util.ModuloOperations.*;

public class ModuloOperationsTest {
    private static Stream<Arguments> modInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -5, 0 ),
                Arguments.of( IllegalArgumentException.class, 52, 0 ),
                Arguments.of( IllegalArgumentException.class, 0, 0 ),
                Arguments.of( IllegalArgumentException.class, -223, -6 ),
                Arguments.of( IllegalArgumentException.class, 8765, -7 ),
                Arguments.of( IllegalArgumentException.class, 0, -8 )
        );
    }

    private static Stream<Arguments> modSolutionExistsData() {
        return Stream.of(
                Arguments.of( 0, 0, 6734 ),

                Arguments.of( 543, 543, 6734 ),
                Arguments.of( 1287, 1287, 6734 ),
                Arguments.of( 0, 6734, 6734 ),
                Arguments.of( 3066, 97342, 6734 ),
                Arguments.of( 0, 1158248, 6734 ),

                Arguments.of( 5461, -1273, 6734 ),
                Arguments.of( 6686, -48, 6734 ),
                Arguments.of( 5743, -991, 6734 ),
                Arguments.of( 4297, -918261, 6734 ),
                Arguments.of( 4293, -298737, 6734 ),
                Arguments.of( 2465, -112334123, 6734 ),

                Arguments.of( 87623847234L, 87623847234L, Long.MAX_VALUE ),
                Arguments.of( 9223371912887652684L, -123967123123L, Long.MAX_VALUE )
        );
    }

    private static Stream<Arguments> addModuloInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -6, 3, -6543 ),
                Arguments.of( IllegalArgumentException.class, 12, -34, -4342 ),
                Arguments.of( IllegalArgumentException.class, -56, -11, -42 ),
                Arguments.of( IllegalArgumentException.class, 23, 14, -1 ),
                Arguments.of( IllegalArgumentException.class, 2, 4, 0 )
        );
    }

    private static Stream<Arguments> addModuloSolutionExistsData() {
        return Stream.of(
                Arguments.of( 7, 6, 1, 9 ),
                Arguments.of( 341, 532, 731, 922 ),
                Arguments.of( 5, 234, 93, 14 ),
                Arguments.of( 149127644, 3463423, 145664221, 545234235253253L ),
                Arguments.of( 87634823742L, 790361289109L, 281273817623L, 984000282990L ),
                Arguments.of( 463684599, 475784352313L, 5667344543L, 700128111L ),
                Arguments.of( 126, -632, 54, 176 ),
                Arguments.of( 27, -143, 90, 40 ),
                Arguments.of( 58, -53, 35, 76 ),
                Arguments.of( 35, -123, 158, 6345 ),
                Arguments.of( 246, -309, 3098, 2543 ),
                Arguments.of( 7180, -7841, 74541, 9920 ),
                Arguments.of( 586981, -238761238, 848399, 1019230 ),
                Arguments.of( 3600811, -823940192381L, 823939903069L, 3890123 ),
                Arguments.of( 3483498129021L, -7123435182371L, 10606933311392L, 93485712034230L ),
                Arguments.of( 120740775203212L, -20348971823782L, 628912309345981238L, 473829102938172L ),
                Arguments.of( 758, 78, -9340, 835 ),
                Arguments.of( 785, 6901, -7938, 911 ),
                Arguments.of( 121, 8001, -8903, 1023 ),
                Arguments.of( 4399, 11098, -6699, 19809 ),
                Arguments.of( 145, 5261, -3098, 2018 ),
                Arguments.of( 468, 7176, -1092, 1872 ),
                Arguments.of( 21810688974L, 623788347, -1098234298723L, 22388423987L ),
                Arguments.of( 752866466941L, 7823491283719821L, -7823567520191052L, 829102938172L ),
                Arguments.of( 8700622433071958L, 8982348798234234L, -281726365162276L, 8701087889528303L ),
                Arguments.of( 538329852521L, 723487236478231L, -923487123172L, 723472364231L ),
                Arguments.of( 23, -24, -78, 25 ),
                Arguments.of( 320, -171, -209, 350 ),
                Arguments.of( 1402, -987, -512, 2901 ),
                Arguments.of( 17442993266762L, -87234912832876L, -1091283987234L, 17628198347812L ),
                Arguments.of( 50632712167790L, -7123273467123L, -45349823476123L, 51552904555518L ),
                Arguments.of( 372819283711L, -9834752387621L, -600927812312L, 10808499483644L ),

                //Long data edges
                Arguments.of( 41016714250715L, Long.MAX_VALUE, 0, 73287123129871L ),
                Arguments.of( 3539537286368L, Long.MIN_VALUE, 0, 9823471823123L ),
                Arguments.of( 2599472243081L, 0, Long.MAX_VALUE, 5543093489234L ),
                Arguments.of( 323259893662L, 0, Long.MIN_VALUE, 409234871283L ),
                Arguments.of( 430827981, Long.MAX_VALUE - 4, 4, 4574563234L ),
                Arguments.of( 2213775834L, Long.MIN_VALUE + 7, -7, 7234198271L ),
                Arguments.of( 43228, Long.MAX_VALUE - 4, -Long.MAX_VALUE, 43232 ),
                Arguments.of( 76523421230L, Long.MAX_VALUE, Long.MIN_VALUE, 76523421231L ),
                Arguments.of( 234238401, -Long.MAX_VALUE, Long.MAX_VALUE - 10, 234238411 ),
                Arguments.of( 234981723000L, Long.MIN_VALUE, Long.MAX_VALUE - 22, 234981723023L ),

                //Overflow occurs along the way
                Arguments.of( 1, Long.MAX_VALUE - 1, 2, Long.MAX_VALUE )
        );
    }

    private static Stream<Arguments> subtractModuloInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -6, 3, -6543 ),
                Arguments.of( IllegalArgumentException.class, 54, -9, -223 ),
                Arguments.of( IllegalArgumentException.class, -56, -11, -42 ),
                Arguments.of( IllegalArgumentException.class, 23, 14, -1 ),
                Arguments.of( IllegalArgumentException.class, 2, 4, 0 )
        );
    }

    private static Stream<Arguments> subtractModuloSolutionExistsData() {
        return Stream.of(
                Arguments.of( 6, 15, 124, 23 ),
                Arguments.of( 45, 543, 600, 51 ),
                Arguments.of( 63, 212, 250, 101 ),
                Arguments.of( 11, 923, 72, 56 ),
                Arguments.of( 14, 801, 690, 97 ),
                Arguments.of( 1967, 3290, 1323, 4012 ),
                Arguments.of( 3700411261L, 6723476123987234L, 765592093, 7382913491L ),
                Arguments.of( 31271974496L, 83459209323L, 52187234827L, 31289258407L ),
                Arguments.of( 645546564352345L, 3672348127361111L, 7982348971231921L, 4955547408223155L ),
                Arguments.of( 5740332305728L, 8329871239831L, 12155467654990831L, 8392871627111L ),
                Arguments.of( 40, -39, 389, 78 ),
                Arguments.of( 112, -41, 109, 131 ),
                Arguments.of( 2340, -209, 543, 3092 ),
                Arguments.of( 250715373465903L, -9302394187267823L, 34539457823L, 734857238476273L ),
                Arguments.of( 336429332988577L, -427346586234823L, 82394827384234L, 423085373303817L ),
                Arguments.of( 33547019239940L, -76575354363411L, 22092349283472L, 132214722886823L ),
                Arguments.of( 89, 67, -298, 92 ),
                Arguments.of( 36, 146, -202, 312 ),
                Arguments.of( 1007, 345, -662, 1890 ),
                Arguments.of( 2546805055204L, 834592341203819L, -348562348273461L, 5844593487238L ),
                Arguments.of( 386135928094261L, 1000234982638123L, -72364817623712L, 686463872167574L ),
                Arguments.of( 32687732124869L, 23453493648746L, -9234238476123L, 58772542360514L ),
                Arguments.of( 50, -472, -109, 59 ),
                Arguments.of( 74, -782, -700, 78 ),
                Arguments.of( 520, -508, -488, 540 ),
                Arguments.of( 58, -12, -598, 132 ),
                Arguments.of( 38, -84, -280, 158 ),
                Arguments.of( 1766, -124, -1890, 4932 ),
                Arguments.of( 833409209, -983457394823L, -345234617236123L, 874359823L ),
                Arguments.of( 547985236388L, -345234762734L, -893219999122L, 789098740398L ),
                Arguments.of( 4489284884272161L, -871232348782341231L, -4593845798742222L, 6700982983599009L ),
                Arguments.of( 99999431000000L, -987234081292131L, -789637290111222L, 297596222180909L ),

                //Long data edges
                Arguments.of( 6610177476376L, Long.MAX_VALUE, 0, 11239823478123L ),
                Arguments.of( 2732760272112L, Long.MIN_VALUE, 0, 23445999292340L ),
                Arguments.of( 299284493574534776L, 0, Long.MAX_VALUE, 453459834782348123L ),
                Arguments.of( 547973147389204L, -1, Long.MAX_VALUE, 834592834781231L ),
                Arguments.of( 67740588407830L, 0, Long.MIN_VALUE, 78239182738123L ),
                Arguments.of( 480275631774952L, 2, Long.MIN_VALUE + 3, 664234192381923L ),
                Arguments.of( 1767981761202L, -5, Long.MIN_VALUE + 3, 3456456234234L ),
                Arguments.of( 76523421230L, Long.MIN_VALUE, -Long.MAX_VALUE, 76523421231L ),
                Arguments.of( 23423421231239870L, Long.MAX_VALUE - 8, Long.MAX_VALUE, 23423421231239878L ),
                Arguments.of( 21297627978051L, Long.MIN_VALUE, -38, 50000234917263L ),

                //Overflow occurs along the way
                Arguments.of( 9223372036854775805L, Long.MIN_VALUE + 2, 3, Long.MAX_VALUE )
        );
    }

    private static Stream<Arguments> multiplyModuloInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -1, 5, -120 ),
                Arguments.of( IllegalArgumentException.class, 3, -7, -434 ),
                Arguments.of( IllegalArgumentException.class, -41, -9, -64 ),
                Arguments.of( IllegalArgumentException.class, 53, 3, -1 ),
                Arguments.of( IllegalArgumentException.class, 1, 1, 0 )
        );
    }

    private static Stream<Arguments> multiplyModuloSolutionExistsData() {
        long maxSquareRoot = (long) sqrt( Long.MAX_VALUE );

        return Stream.of(
                Arguments.of( 1, 2, 5, 3 ),
                Arguments.of( 12, 3, 4, 19 ),
                Arguments.of( 68428, 440, 873, 78923 ),
                Arguments.of( 43940046, 1102, 39873, 2912398234987L ),
                Arguments.of( 9, -2, 5, 19 ),
                Arguments.of( 2, -14, 67, 10 ),
                Arguments.of( 6064408, -347893, 19438577625L, 8234677 ),
                Arguments.of( 4778785733L, -1230, 8478, 4789213673L ),
                Arguments.of( 13, 7, -3, 34 ),
                Arguments.of( 48, 56, -78, 192 ),
                Arguments.of( 193213, 8477, -1038, 473281 ),
                Arguments.of( 7438262632882043L, 1982, -948274, 7438264512361111L ),
                Arguments.of( 42, -7, -6, 50 ),
                Arguments.of( 2269, -101, -89, 6720 ),
                Arguments.of( 148375328437L, -123123L, -8939823499L, 203428745534L ),
                Arguments.of( 18540808, -5434, -3412, 829872938723L ),
                Arguments.of( 46, 346, 982, 123 ),
                Arguments.of( 7106, -9342, 187239, 7754 ),
                Arguments.of( 1298, 2098376, -4553423, 3902 ),
                Arguments.of( 343578, -1092378, -7382181, 821091 ),

                //a = 0 mod m   or   b = 0 mod m
                Arguments.of( 0, 0, 0, 15 ),
                Arguments.of( 0, 2, 0, 41 ),
                Arguments.of( 0, -4, 0, 65 ),
                Arguments.of( 0, 0, 643, 910 ),
                Arguments.of( 0, 0, -39, 1231 ),
                Arguments.of( 0, 18, 17, 18 ),
                Arguments.of( 0, -233, 138, 69 ),
                Arguments.of( 0, -8280, 3, 920 ),
                Arguments.of( 0, 0, -2020, 1010 ),

                //Long data edges
                Arguments.of( 49194881913529L, maxSquareRoot, maxSquareRoot, 234654323412312L ),
                Arguments.of( 548014519221L, -maxSquareRoot, maxSquareRoot, 677645349823L ),
                Arguments.of( 6982515932319997245L, maxSquareRoot, -maxSquareRoot, 8102943981623123123L ),
                Arguments.of( 994593328701454L, -maxSquareRoot, -maxSquareRoot, 4440239498121111L ),

                //Overflow occurs along the way
                Arguments.of( 119843859996578382L, 8348762349123123L, 5573450010293478L, 238111743672348123L ),
                Arguments.of( 152972033771702217L, -102983248762635623L, 662718237726312312L, 238111743672348123L ),
                Arguments.of( 622090629189332696L, 734285123983252348L, -393457033234876123L, 823981230234012300L ),
                Arguments.of( 37605841800641883L, -559340293419283654L, -2012893876324651263L, 119823100120230123L )
        );
    }

    private static Stream<Arguments> powerModuloInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, 0, 0, 12 ),
                Arguments.of( IllegalArgumentException.class, 3, 5, 0 ),
                Arguments.of( IllegalArgumentException.class, 2, -5, 2 ),
                Arguments.of( IllegalArgumentException.class, 4, 7, -2 ),
                Arguments.of( IllegalArgumentException.class, 4, -27, -52 )
        );
    }

    private static Stream<Arguments> powerModuloSolutionExistsData() {
        return Stream.of(
                //m = 1
                Arguments.of( 0, 1, 2, 1 ),
                Arguments.of( 0, 33, 27, 1 ),
                Arguments.of( 0, 312, 2534, 1 ),
                Arguments.of( 0, 22342, 5667675, 1 ),
                Arguments.of( 0, 2, 0, 1 ),
                Arguments.of( 0, 1, 3, 1 ),
                Arguments.of( 0, -2, 5, 1 ),
                Arguments.of( 0, -9384, 512377441, 1 ),
                Arguments.of( 0, -62341, 5903450, 1 ),

                //a = 0 mod m
                Arguments.of( 0, 0, 2, 4 ),
                Arguments.of( 0, 0, 3467, 5 ),
                Arguments.of( 0, 0, 12334645, 7 ),
                Arguments.of( 0, 0, Long.MAX_VALUE, 234234 ),
                Arguments.of( 0, 3, 2, 3 ),
                Arguments.of( 0, 4, 17282922, 2 ),
                Arguments.of( 0, 2535507, 34, 281723 ),
                Arguments.of( 0, 25068915599L, 34938432, 1928378123 ),
                Arguments.of( 0, 8902934276234234L, 283928374234L, 8902934276234234L ),

                //a = 1
                Arguments.of( 1, 1, 7, 33 ),
                Arguments.of( 1, 1, 2345, 31 ),
                Arguments.of( 1, 1, 7255967, 71 ),

                //a > 0
                Arguments.of( 3, 2, 232, 13 ),
                Arguments.of( 16, 4, 512, 31 ),
                Arguments.of( 1, 13, 6, 9 ),
                Arguments.of( 6286, 17, 12352, 12345 ),
                Arguments.of( 0, 20, 20, 10000000000L ),
                Arguments.of( 3663262, 632, 144, 9293823 ),
                Arguments.of( 173, 1235, 74234, 829 ),
                Arguments.of( 94318, 9274629340123L, 6346856, 123123 ),
                Arguments.of( 34812521239552L, 72632, 6346856, 42937237421211L ),
                Arguments.of( 36931319003L, 823862342123L, 73556121, 715299391290L ),

                //a < 0
                Arguments.of( 13, -2, 5, 15 ),
                Arguments.of( 4, -2, 6, 15 ),
                Arguments.of( 580, -7, 31, 827 ),
                Arguments.of( 75, -7, 32, 827 ),
                Arguments.of( 309728099, -102, 423, 987123667 ),
                Arguments.of( 982814913, -102, 424, 987123667 ),
                Arguments.of( 122544016, -12090, 190871, 176212312 ),
                Arguments.of( 35965856, -12090, 190872, 176212312 ),
                Arguments.of( 159, -8271928371L, 71238793, 1230 ),
                Arguments.of( 351, -8271928371L, 71238794, 1230 ),
                Arguments.of( 9932188757466L, -67123768712341L, 52099812309809L, 9991929123899L ),
                Arguments.of( 1199972598609L, -67123768712341L, 52099812309810L, 9991929123899L ),

                //b = 0
                Arguments.of( 1, 23, 0, 312 ),
                Arguments.of( 1, 12357, 0, 234 ),
                Arguments.of( 1, 89766345323423L, 0, 111 ),
                Arguments.of( 1, 1, 0, 10000000000L ),
                Arguments.of( 1, Long.MAX_VALUE, 0, 23423423111L ),
                Arguments.of( 1, 738290234111L, 0, Long.MAX_VALUE ),

                //b = 1
                Arguments.of( 14, 14, 1, 23 ),
                Arguments.of( 2964, 9874, 1, 3455 ),
                Arguments.of( 3455, 3455, 1, 9874 ),
                Arguments.of( Long.MAX_VALUE - 1, Long.MAX_VALUE - 1, 1, Long.MAX_VALUE ),
                Arguments.of( 123, Long.MAX_VALUE, 1, Long.MAX_VALUE - 123 ),

                //Long data edges
                Arguments.of( 0, Long.MAX_VALUE, Long.MAX_VALUE / 2, Long.MAX_VALUE ),
                Arguments.of( 1, Long.MAX_VALUE - 1, 324234, Long.MAX_VALUE ),
                Arguments.of( 19683, Long.MAX_VALUE - 2, 9, Long.MAX_VALUE - 5 ),
                Arguments.of( 4611686018427387904L, 9223372036854775804L, 99938273716231L, 9223372036854775800L ),

                //Overflow occurs along the way
                Arguments.of( 1, 5, 4611686018427388026L, 133 ),
                Arguments.of( 10623508079761843L, 5467567312312131L, 4611686018427388056L, 213129387324411223L )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate {1} mod {2}, raises an exception: {0}" )
    @MethodSource( "modInvalidInputData" )
    public void test_Mod_InvalidInput( Class<Throwable> throwableClass, long a, long m ) {
        assertException( throwableClass, () -> mod( a, m ) );
    }

    @ParameterizedTest( name = "{index}) {1} mod {2} = {0}" )
    @MethodSource( "modSolutionExistsData" )
    public void test_Abs( long expectedMod, long a, long m ) {
        assertEquals( expectedMod, mod( a, m ) );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate ({1} + {2}) mod {3}, raises an exception: {0}" )
    @MethodSource( "addModuloInvalidInputData" )
    public void test_AddModulo_InvalidInput( Class<Throwable> throwableClass, long a, long b, long m ) {
        assertException( throwableClass, () -> addModulo( a, b, m ) );
    }

    @ParameterizedTest( name = "{index}) ({1} + {2}) mod {3} = {0}" )
    @MethodSource( "addModuloSolutionExistsData" )
    public void test_AddModulo( long expectedSum, long a, long b, long m ) {
        assertEquals( expectedSum, addModulo( a, b, m ) );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate ({1} - {2}) mod {3}, raises an exception: {0}" )
    @MethodSource( "subtractModuloInvalidInputData" )
    public void test_SubtractModulo_InvalidInput( Class<Throwable> throwableClass, long a, long b, long m ) {
        assertException( throwableClass, () -> subtractModulo( a, b, m ) );
    }

    @ParameterizedTest( name = "{index}) ({1} - {2}) mod {3} = {0}" )
    @MethodSource( "subtractModuloSolutionExistsData" )
    public void test_SubtractModulo( long expectedDifference, long a, long b, long m ) {
        assertEquals( expectedDifference, subtractModulo( a, b, m ) );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate ({1} * {2}) mod {3}, raises an exception: {0}" )
    @MethodSource( "multiplyModuloInvalidInputData" )
    public void test_MultiplyModulo_InvalidInput( Class<Throwable> throwableClass, long a, long b, long m ) {
        assertException( throwableClass, () -> multiplyModulo( a, b, m ) );
    }

    @ParameterizedTest( name = "{index}) ({1} * {2}) mod {3} = {0}" )
    @MethodSource( "multiplyModuloSolutionExistsData" )
    public void test_MultiplyModulo( long expectedProduct, long a, long b, long m ) {
        assertEquals( expectedProduct, multiplyModulo( a, b, m ) );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate ({1}^{2}) mod {3}, raises an exception: {0}" )
    @MethodSource( "powerModuloInvalidInputData" )
    public void test_PowerModulo_InvalidInput( Class<Throwable> throwableClass, long a, long b, long m ) {
        assertException( throwableClass, () -> powerModulo( a, b, m ) );
    }

    @ParameterizedTest( name = "{index}) ({1}^{2}) mod {3} = {0}" )
    @MethodSource( "powerModuloSolutionExistsData" )
    public void test_PowerModulo( long expectedPower, long a, long b, long m ) {
        assertEquals( expectedPower, powerModulo( a, b, m ) );
    }

    @Test
    public void test_ModuloOperationsInstanceCannotBeCreated() throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException {
        Constructor<ModuloOperations> constructor = ModuloOperations.class.getDeclaredConstructor();
        assertTrue( Modifier.isPrivate( constructor.getModifiers() ) );
        constructor.setAccessible( true );
        constructor.newInstance();
    }
}
