package util;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.math.BigInteger;
import java.util.Collection;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static testutils.CollectionUtils.listOfLongs;
import static testutils.ExceptionAssert.assertException;
import static util.BasicOperations.*;

public class BasicOperationsTest {
    private static Stream<Arguments> absInvalidInputData() {
        return Stream.of(
                Arguments.of( ArithmeticException.class, Long.MIN_VALUE )
        );
    }

    private static Stream<Arguments> absSolutionExistsData() {
        return Stream.of(
                Arguments.of( 0, 0 ),
                Arguments.of( 1, 1 ),
                Arguments.of( 390845, 390845 ),
                Arguments.of( 1923823749812379112L, 1923823749812379112L ),
                Arguments.of( 1, -1 ),
                Arguments.of( 65432, -65432 ),
                Arguments.of( 8348273461827361723L, -8348273461827361723L ),
                Arguments.of( Long.MAX_VALUE, -Long.MAX_VALUE ),
                Arguments.of( Long.MAX_VALUE, Long.MIN_VALUE + 1 )
        );
    }

    private static Stream<Arguments> powerInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, 0, 0 ),
                Arguments.of( IllegalArgumentException.class, 3, -1 ),
                Arguments.of( IllegalArgumentException.class, 11, -4 ),
                Arguments.of( IllegalArgumentException.class, 456345, -923987 ),
                Arguments.of( IllegalArgumentException.class, 654, Long.MIN_VALUE ),

                //Overflow cases
                Arguments.of( ArithmeticException.class, 3, Long.MAX_VALUE - 1 ),
                Arguments.of( ArithmeticException.class, Long.MAX_VALUE - 1, 2 ),
                Arguments.of( ArithmeticException.class, Long.MAX_VALUE, Long.MAX_VALUE ),
                Arguments.of( ArithmeticException.class, 2, 64 ),
                Arguments.of( ArithmeticException.class, -3, 40 )
        );
    }

    private static Stream<Arguments> powerSolutionExistsData() {
        return Stream.of(
                //a = 0
                Arguments.of( 0, 0, 4 ),
                Arguments.of( 0, 0, 254 ),
                Arguments.of( 0, 0, 134633 ),

                //b = 0
                Arguments.of( 1, 3, 0 ),
                Arguments.of( 1, 532, 0 ),
                Arguments.of( 1, 238743, 0 ),
                Arguments.of( 1, Long.MAX_VALUE, 0 ),
                Arguments.of( 1, -34, 0 ),
                Arguments.of( 1, -676455646, 0 ),
                Arguments.of( 1, Long.MIN_VALUE, 0 ),

                //a < 0
                Arguments.of( -1, -1, 234345 ),
                Arguments.of( 1, -1, 6 ),
                Arguments.of( -27, -3, 3 ),
                Arguments.of( 256, -4, 4 ),
                Arguments.of( 531441, -3, 12 ),
                Arguments.of( -13060694016L, -6, 13 ),

                //a > 0
                Arguments.of( 27, 3, 3 ),
                Arguments.of( 125, 5, 3 ),
                Arguments.of( 31381059609L, 9, 11 ),
                Arguments.of( 8388608, 2, 23 )
        );
    }

    private static Stream<Arguments> factorialInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, -1 ),
                Arguments.of( IllegalArgumentException.class, -2 ),
                Arguments.of( IllegalArgumentException.class, -324987 ),
                Arguments.of( IllegalArgumentException.class, Integer.MIN_VALUE )
        );
    }

    private static Stream<Arguments> factorialSolutionExistsData() {
        return Stream.of(
                Arguments.of( BigInteger.ONE, 0 ),
                Arguments.of( BigInteger.ONE, 1 ),
                Arguments.of( BigInteger.valueOf( 2 ), 2 ),
                Arguments.of( BigInteger.valueOf( 6 ), 3 ),
                Arguments.of( BigInteger.valueOf( 24 ), 4 ),
                Arguments.of( BigInteger.valueOf( 120 ), 5 ),
                Arguments.of( BigInteger.valueOf( 720 ), 6 ),
                Arguments.of( BigInteger.valueOf( 5040 ), 7 ),
                Arguments.of( BigInteger.valueOf( 40320 ), 8 ),
                Arguments.of( BigInteger.valueOf( 362880 ), 9 ),
                Arguments.of( BigInteger.valueOf( 3628800 ), 10 ),
                Arguments.of( BigInteger.valueOf( 39916800 ), 11 ),
                Arguments.of( BigInteger.valueOf( 479001600 ), 12 ),
                Arguments.of( BigInteger.valueOf( 6227020800L ), 13 ),
                Arguments.of( BigInteger.valueOf( 87178291200L ), 14 ),
                Arguments.of( BigInteger.valueOf( 1307674368000L ), 15 ),
                Arguments.of( BigInteger.valueOf( 20922789888000L ), 16 ),
                Arguments.of( BigInteger.valueOf( 355687428096000L ), 17 ),
                Arguments.of( BigInteger.valueOf( 6402373705728000L ), 18 ),
                Arguments.of( BigInteger.valueOf( 121645100408832000L ), 19 ),
                Arguments.of( BigInteger.valueOf( 2432902008176640000L ), 20 ),
                Arguments.of( new BigInteger( "60415263063373835637355132068513997507264512000000000" ), 43 ),
                Arguments.of( new BigInteger(
                                      "207986607530614516434889573226252709222712518908365286496652422317405760295930638776430109826354519132675660433931363055910963871453772379754931444766652739192303201763588723618347593740385428725846122572271049818916876323493243976023302916666394540247449307010665731331903556896427962603583291932028351318888786128689538489086713005449989591695585446014805881310771610743578696897019623882572957311572603371048376255338230572538584588079078669943174850854858995580594914244562856410918607028520410684463210865874698240000000000000000000000000000000000000000000000000000000000000000000000" ),
                              289 )
        );
    }

    private static Stream<Arguments> sumInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, null ),
                Arguments.of( IllegalArgumentException.class, asList() ),

                //Overflow
                Arguments.of( ArithmeticException.class, listOfLongs( Long.MAX_VALUE, 1 ) ),
                Arguments.of( ArithmeticException.class, listOfLongs( 8234182736123123123L, 7712637517265123123L ) ),
                Arguments.of( ArithmeticException.class,
                              listOfLongs( Long.MAX_VALUE - 8234, Long.MAX_VALUE - 126351243 ) )
        );
    }

    private static Stream<Arguments> sumSolutionExistsData() {
        return Stream.of(
                //1 element
                Arguments.of( 0, listOfLongs( 0 ) ),
                Arguments.of( 4, listOfLongs( 4 ) ),
                Arguments.of( 3451, listOfLongs( 3451 ) ),

                //Multiple elements
                Arguments.of( 22, listOfLongs( 8, 14 ) ),
                Arguments.of( 96313, listOfLongs( 87123, 9190 ) ),
                Arguments.of( 784853, listOfLongs( 3, 812, 771238, 12800 ) ),
                Arguments.of( 11397, listOfLongs( 5, 410, 66, 791, 10000, 87, 11, 12, 15 ) ),

                //Negative values included
                Arguments.of( -10, listOfLongs( 9, -19 ) ),
                Arguments.of( -9912923, listOfLongs( 0, -9912923 ) ),
                Arguments.of( -268, listOfLongs( -17, -201, -9, -41 ) ),
                Arguments.of( 200, listOfLongs( 1827, 200, -1827 ) ),

                //Big values
                Arguments.of( Long.MAX_VALUE - 69, listOfLongs( Long.MAX_VALUE - 100, 31 ) ),
                Arguments.of( Long.MAX_VALUE - 999, listOfLongs( Long.MAX_VALUE - 1000, 200, 301, -500 ) )
        );
    }

    private static Stream<Arguments> productInvalidInputData() {
        return Stream.of(
                Arguments.of( IllegalArgumentException.class, null ),
                Arguments.of( IllegalArgumentException.class, listOfLongs() ),

                //Overflow
                Arguments.of( ArithmeticException.class, listOfLongs( Long.MAX_VALUE, 2 ) ),
                Arguments.of( ArithmeticException.class, listOfLongs( 48376271623124L, 789128371726365L ) ),
                Arguments.of( ArithmeticException.class, listOfLongs( 321, 8723, -9874, 987234, 9991 ) ),
                Arguments.of( ArithmeticException.class, listOfLongs( -1276, -11, 8763, 9182, 6302, 665, 51203 ) ),
                Arguments.of( ArithmeticException.class,
                              listOfLongs( Long.MAX_VALUE - 8234, Long.MAX_VALUE - 126351243 ) )
        );
    }

    private static Stream<Arguments> productSolutionExistsData() {
        return Stream.of(
                //1 element
                Arguments.of( 0, listOfLongs( 0 ) ),
                Arguments.of( 17, listOfLongs( 17 ) ),
                Arguments.of( 827364, listOfLongs( 827364 ) ),

                //Contains 0
                Arguments.of( 0, listOfLongs( 0, 183214 ) ),
                Arguments.of( 0, listOfLongs( 81, 0, 1920, 827 ) ),
                Arguments.of( 0, listOfLongs( 44, 912, 123, 0, 111 ) ),
                Arguments.of( 0, listOfLongs( 0, 0, 0, 0 ) ),

                //Multiple elements
                Arguments.of( 721, listOfLongs( 1, 721 ) ),
                Arguments.of( 1792, listOfLongs( 16, 14, 8 ) ),
                Arguments.of( 1827348, listOfLongs( 1, 1, 1, 1, 1827348 ) ),
                Arguments.of( 105125440, listOfLongs( 4, 7, 8, 10, 71, 661 ) ),
                Arguments.of( 1011507209181L, listOfLongs( 8123, 1012389, 123 ) ),

                //Negative values included
                Arguments.of( -174, listOfLongs( 87, -2 ) ),
                Arguments.of( 0, listOfLongs( 0, -520, 13 ) ),
                Arguments.of( -9745512, listOfLongs( -8287, 12, 98 ) ),
                Arguments.of( 1728, listOfLongs( -72, -6, 4 ) ),
                Arguments.of( 5972773036032L, listOfLongs( 16, -10029, 2203, -32, -66, -8 ) ),

                //Big values
                Arguments.of( 161339629504012536L, listOfLongs( 182736152, 882910293 ) ),
                Arguments.of( -13600471589119872L, listOfLongs( 2341, 888, -65482, 99912 ) ),
                Arguments.of( 23253478189292736L, listOfLongs( 72, -726152, 11111, -40029 ) )
        );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate |{1}|, raises an exception: {0}" )
    @MethodSource( "absInvalidInputData" )
    public void test_Abs_InvalidInput( Class<Throwable> exceptionClass, long value ) {
        assertException( exceptionClass, () -> abs( value ) );
    }

    @ParameterizedTest( name = "{index}) |{1}| = {0}" )
    @MethodSource( "absSolutionExistsData" )
    public void test_Abs( long expectedAbs, long value ) {
        assertEquals( expectedAbs, abs( value ) );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate {1}^{2}, raises an exception: {0}" )
    @MethodSource( "powerInvalidInputData" )
    public void test_Power_InvalidInput( Class<Throwable> exceptionClass, long a, long b ) {
        assertException( exceptionClass, () -> power( a, b ) );
    }

    @ParameterizedTest( name = "{index}) {1}^{2} = {0}" )
    @MethodSource( "powerSolutionExistsData" )
    public void test_Power( long expectedPower, long a, long b ) {
        assertEquals( expectedPower, power( a, b ) );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate {1}!, raises an exception: {0}" )
    @MethodSource( "factorialInvalidInputData" )
    public void test_Factorial_InvalidInput( Class<Throwable> exceptionClass, int value ) {
        assertException( exceptionClass, () -> factorial( value ) );
    }

    @ParameterizedTest( name = "{index}) {1}! = {0}" )
    @MethodSource( "factorialSolutionExistsData" )
    public void test_Factorial( BigInteger expectedFactorial, int value ) {
        assertEquals( expectedFactorial, factorial( value ) );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate the sum of values from {1}, raises an exception: {0}" )
    @MethodSource( "sumInvalidInputData" )
    public void test_Sum_InvalidInput( Class<Throwable> exceptionClass, Collection<Long> values ) {
        assertException( exceptionClass, () -> sum( values ) );
    }

    @ParameterizedTest( name = "{index}) Sum of values from {1} is equal to {0}" )
    @MethodSource( "sumSolutionExistsData" )
    public void test_Sum( long expectedSum, Collection<Long> values ) {
        assertEquals( expectedSum, sum( values ) );
    }

    @ParameterizedTest( name = "{index}) Cannot calculate the product of values from {1}, raises an exception: {0}" )
    @MethodSource( "productInvalidInputData" )
    public void test_Product_InvalidInput( Class<Throwable> exceptionClass, Collection<Long> values ) {
        assertException( exceptionClass, () -> product( values ) );
    }

    @ParameterizedTest( name = "{index}) Product of values from {1} is equal to {0}" )
    @MethodSource( "productSolutionExistsData" )
    public void test_Product( long expectedProduct, Collection<Long> values ) {
        assertEquals( expectedProduct, product( values ) );
    }

    @Test
    public void test_BasicOperationsInstanceCannotBeCreated() throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException {
        Constructor<BasicOperations> constructor = BasicOperations.class.getDeclaredConstructor();
        assertTrue( Modifier.isPrivate( constructor.getModifiers() ) );
        constructor.setAccessible( true );
        constructor.newInstance();
    }
}
