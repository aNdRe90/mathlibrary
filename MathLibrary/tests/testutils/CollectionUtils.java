package testutils;

import java.util.ArrayList;
import java.util.List;

public class CollectionUtils {
    public static List<Long> listOfLongs( long... values ) {
        if( values == null ) {
            return null;
        }

        List<Long> longList = new ArrayList<>( values.length );

        for( int i = 0; i < values.length; i++ ) {
            longList.add( values[i] );
        }

        return longList;
    }
}
