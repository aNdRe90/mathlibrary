package testutils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ExceptionAssert {
    public static void assertException( Class<? extends Throwable> throwableClass, TestAction action ) {
        try {
            action.performTest();
            fail( throwableClass.getSimpleName() + " was expected to be thrown." );
        }
        catch( Throwable throwable ) {
            assertEquals( throwableClass, throwable.getClass() );
        }
    }
}
