package testutils;

public interface TestAction {
    void performTest();
}
